import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0


Item {
    id:move_current_position_component
    width: 332
    height: 43

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    property int buttonWidth: 47
    property int fontSize: 14
    property int spaceing: 5

    property string targetIO:"hello" //to avoid error give fake value
    property variant __targetIO:  eval(targetIO)

    function moveToPosition(pos){
            __targetIO.value = pos/1000
            MoveMachine_bMove.value = true
        //console.log("moveToPosition: ", targetIO," ", pos)

     }


    Button {
        id: button_decrease_5
        x: 0
        width: buttonWidth
        height: move_current_position_component.height
        text: "-5"
        __horizontalTextAlignement: Text.AlignHCenter
        fontSize: move_current_position_component.fontSize
        __verticalTextAlignement: Text.AlignVCenter
        __styleName: styleName
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

        onPressed: {
            moveToPosition(-5)
        }

    }

    Button {
        id: button_decrease_1
        x: button_decrease_5.x + buttonWidth + spaceing
        width: buttonWidth
        height: move_current_position_component.height
        text: "-1"
        __horizontalTextAlignement: Text.AlignHCenter
        fontSize: move_current_position_component.fontSize
        __verticalTextAlignement: Text.AlignVCenter
        __styleName: styleName
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

        onPressed: moveToPosition(-1)
    }

    Button {
        id: button_decrease_01
        x: button_decrease_1.x + buttonWidth + spaceing
        width: buttonWidth
        height: move_current_position_component.height
        text: "-0.1"
        __horizontalTextAlignement: Text.AlignHCenter
        fontSize: move_current_position_component.fontSize
        __verticalTextAlignement: Text.AlignVCenter
        __styleName: styleName
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

        onPressed: moveToPosition(-0.1)
    }

    Button {
        id: button_increase_01
        x: button_decrease_01.x + buttonWidth + spaceing
        width: buttonWidth
        height: move_current_position_component.height
        text: "0.1"
        __horizontalTextAlignement: Text.AlignHCenter
        fontSize: move_current_position_component.fontSize
        __verticalTextAlignement: Text.AlignVCenter
        __styleName: styleName
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

        onPressed: moveToPosition(0.1)
    }

    Button {
        id: button_increase_1
        x: button_increase_01.x + buttonWidth + spaceing
        width: buttonWidth
        height: move_current_position_component.height
        text: "1"
        __horizontalTextAlignement: Text.AlignHCenter
        fontSize: move_current_position_component.fontSize
        __verticalTextAlignement: Text.AlignVCenter
        __styleName: styleName
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

        onPressed: moveToPosition(1)
    }

    Button {
        id: button_increase_5
        x: button_increase_1.x +buttonWidth + spaceing
        width: buttonWidth
        height: move_current_position_component.height
        text: "5"
        __horizontalTextAlignement: Text.AlignHCenter
        fontSize: move_current_position_component.fontSize
        __verticalTextAlignement: Text.AlignVCenter
        __styleName: styleName
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

        onPressed: moveToPosition(5)
    }

}
