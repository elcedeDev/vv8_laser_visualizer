import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "localVariables.js" as LV
import "MainFunctions.js" as MF


FocusScope {
    id:current_position_component
    width: 615
    height: 224

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    GroupBox {
        id: groupbox_currentpos
        x: 0
        y: 0
        width: 301
        height: 173
        headerText: db.dbTr("current position") + db.tr
        headerWidth: 190
        colorChoice: 3
        fontSize: 14
        Column{
            y:33
            spacing:6
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_currentX
                    width: 43
                    height: parent.height
                    text: bUseRotation.value ? "A" : "X"
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_currentX
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    readOnly: true
                    textIO: bUseRotation.value ? "Axis_A_dPosition" : "Axis_X_dPosition"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                }

                Button {
                    id: button_currentX_decrease
                    width: 43
                    height: parent.height
                    __styleName: current_position_component.styleName
                    __imageNormal: imagePath + S.Style[styleName].elcedeButton["CMinusUp"]
                    __imagePressed: imagePath + S.Style[styleName].elcedeButton["CMinusDown"]
                    foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                    foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty

                    onPressed: {
                        if(Machine_bBusy.value === false){
                            if (bUseRotation.value) {
                                Axis_A_Rotation_dAccelerationMax.value = 4.0;
                                Axis_A_Rotation_dAccelerationMin.value = -4.0;
                                Axis_A_Rotation_dMoveSpeedNominal.value = -0.075;
                             } else {
                                Axis_X_Flatbed_Master_dAccelerationMax.value =  4.0;
                                Axis_X_Flatbed_Master_dAccelerationMin.value =  -4.0;
                                Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  -0.075;
                             }
                            //Axis_X_Master_dMoveSpeedNominal.value = -0.075;
                            MoveMachine_bReduceAcceleration.value = true;
                        }
                    }
                    onReleased: {
                        if(Machine_bBusy.value === false){
                            if (bUseRotation.value) {
                                Axis_A_Rotation_dAccelerationMax.value = 10.0;
                                Axis_A_Rotation_dAccelerationMin.value = -10.0;
                                Axis_A_Rotation_dMoveSpeedNominal.value = 0;
                             } else {
                                Axis_X_Flatbed_Master_dAccelerationMax.value =  10.0;
                                Axis_X_Flatbed_Master_dAccelerationMin.value =  -10.0;
                                Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0;
                             }
                            //Axis_X_Master_dMoveSpeedNominal.value = 0;
                            MoveMachine_bReduceAcceleration.value = false;
                        }
                    }
                }

                Button {
                    id: button_currentX_increase
                    width: 43
                    height: parent.height
                    __styleName: current_position_component.styleName
                    __imageNormal: imagePath + S.Style[styleName].elcedeButton["CPlusUp"]
                    __imagePressed: imagePath + S.Style[styleName].elcedeButton["CPlusDown"]
                    foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                    foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
                    onPressed: {
                        if(Machine_bBusy.value === false){
                            if (bUseRotation.value) {
                                Axis_A_Rotation_dAccelerationMax.value = 4.0;
                                Axis_A_Rotation_dAccelerationMin.value = -4.0;
                                Axis_A_Rotation_dMoveSpeedNominal.value = 0.075;
                             } else {
                                Axis_X_Flatbed_Master_dAccelerationMax.value =  4.0;
                                Axis_X_Flatbed_Master_dAccelerationMin.value =  -4.0;
                                Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0.075;
                             }
                            //Axis_X_Master_dMoveSpeedNominal.value = 0.075;
                            MoveMachine_bReduceAcceleration.value = true;
                        }
                    }
                    onReleased: {
                        if(Machine_bBusy.value === false){
                            if (bUseRotation.value) {
                                Axis_A_Rotation_dAccelerationMax.value = 10.0;
                                Axis_A_Rotation_dAccelerationMin.value = -10.0;
                                Axis_A_Rotation_dMoveSpeedNominal.value = 0.0;
                             } else {
                                Axis_X_Flatbed_Master_dAccelerationMax.value =  10.0;
                                Axis_X_Flatbed_Master_dAccelerationMin.value =  -10.0;
                                Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0.0;
                             }
                            //Axis_X_Master_dMoveSpeedNominal.value = 0;
                            MoveMachine_bReduceAcceleration.value = false;
                        }
                    }
                }
            }
            Row{
                height: 43
                spacing:6

                TextField {
                    id: label_currentY
                    width: 43
                    height: parent.height
                    text: "Y"
                    fontSize: 16
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_currentY
                    width: 150
                    height: parent.height
                    readOnly: true
                    unitEnable: true
                    textIO: "Axis_Y_dPosition"
                    horizontalTextAlignement: 2
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                }

                Button {
                    id: button_currentY_decrease
                    width: 43
                    height: parent.height
                    __styleName: current_position_component.styleName
                    __imageNormal: imagePath + S.Style[styleName].elcedeButton["CMinusUp"]
                    __imagePressed: imagePath + S.Style[styleName].elcedeButton["CMinusDown"]
                    foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                    foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
                    onPressed: {
                        if(Machine_bBusy.value === false){
                            if(bUseRotation.value)
                                Axis_Y_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                            else
                                Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = -0.075;

                            MoveMachine_bReduceAcceleration.value = true;
                        }
                    }
                    onReleased: {
                        if(Machine_bBusy.value === false){
                            if(bUseRotation.value)
                                Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                            else
                                Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0;

                            MoveMachine_bReduceAcceleration.value = false;
                        }
                    }

                }



                Button {
                    id: button_currentY_increase
                    width: 43
                    height: parent.height
                    __styleName: current_position_component.styleName
                    __imageNormal: imagePath + S.Style[styleName].elcedeButton["CPlusUp"]
                    __imagePressed: imagePath + S.Style[styleName].elcedeButton["CPlusDown"]
                    foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                    foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
                    onPressed: {
                        if(Machine_bBusy.value === false){
                            if(bUseRotation.value)
                                Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                            else
                                Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0.075;

                            MoveMachine_bReduceAcceleration.value = true;
                        }
                    }
                    onReleased: {
                        if(Machine_bBusy.value === false){
                            if(bUseRotation.value)
                                Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                            else
                                Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0;

                            MoveMachine_bReduceAcceleration.value = false;
                        }
                    }
                }
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_currentZ
                    width: 43
                    height: parent.height
                    text: "Z"
                    fontSize: 16
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_currentZ
                    width: 150
                    height: parent.height
                    textIO: "Axis_Z_dPosition"
                    horizontalTextAlignement: 2
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    readOnly: true
                    isEnabled: true
                    __styleName: styleName
                }

                Button {
                    id: button_currentZ_decrease
                    width: 43
                    height: parent.height
                    __styleName: current_position_component.styleName
                    __imageNormal: imagePath + S.Style[styleName].elcedeButton["CMinusUp"]
                    __imagePressed: imagePath + S.Style[styleName].elcedeButton["CMinusDown"]
                    foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                    foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
                    onPressed: {
                        if(Machine_bBusy.value === false){
                            Axis_Z_dSpeedNominal.value = -0.025;
                            MoveMachine_bReduceAcceleration.value = true;
                        }
                    }
                    onReleased: {
                        if(Machine_bBusy.value === false){
                            Axis_Z_dSpeedNominal.value = 0;
                            MoveMachine_bReduceAcceleration.value = false;
                        }
                    }
                }

                Button {
                    id: button_currentZ_increase
                    width: 43
                    height: parent.height
                    __styleName: current_position_component.styleName
                    __imageNormal: imagePath + S.Style[styleName].elcedeButton["CPlusUp"]
                    __imagePressed: imagePath + S.Style[styleName].elcedeButton["CPlusDown"]
                    foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                    foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
                    onPressed: {
                        if(Machine_bBusy.value === false){
                            Axis_Z_dSpeedNominal.value = 0.025;
                            MoveMachine_bReduceAcceleration.value = true;
                        }
                    }
                    onReleased: {
                        if(Machine_bBusy.value === false){
                            Axis_Z_dSpeedNominal.value = 0;
                            MoveMachine_bReduceAcceleration.value = false;
                        }
                    }
                }
            }
        }
    }

    GroupBox {
        id: group_distance
        x: 315
        y: 0
        width: 300
        height: 225
        headerText: db.dbTr("target position")  + db.tr
        headerWidth: 190
        colorChoice: 3
        fontSize: 14
        Column{
            y:33
            width: 299
            spacing:6
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_targetX
                    width: 43
                    height: parent.height
                    text:  bUseRotation.value ? "A" : "X"
                    horizontalTextAlignement: 0
                    fontSize: 16
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_targetX
                    width: 250
                    height: parent.height
                    horizontalTextAlignement: 2
                    readOnly: false
                    isEnabled: true
                    textIO: "Axis_X_dTargetPosition"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    validator:DoubleValidator{}
                    __styleName: styleName
                }
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_targetY
                    width: 43
                    height: parent.height
                    text: "Y"
                    fontSize: 16
                    readOnly: true
                }

                TextField {
                    id: text_field_targetY
                    width: 250
                    height: parent.height
                    readOnly: false
                    unitEnable: true
                    horizontalTextAlignement: 2
                    textIO: "Axis_Y_dTargetPosition"
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    validator:DoubleValidator{}
                    __styleName: styleName

                }


            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_targetZ
                    width: 43
                    height: parent.height
                    text: "Z"
                    fontSize: 16
                    readOnly: true
                }
                TextField {
                    id: text_field_targetZ
                    width: 250
                    height: parent.height
                    horizontalTextAlignement: 2
                    textIO: "Axis_Z_dTargetPosition"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    readOnly: false
                    isEnabled: true
                    validator:DoubleValidator{}
                    __styleName: styleName
                }
            }

            Button{
                id: button_absolute_relative
                width: 300
                height: 43
                fontSize: 12
                text: bAbsoluteRelative.value ? db.dbTr("Absolute position") + db.tr :  db.dbTr("Relative position") + db.tr
                onClicked: bAbsoluteRelative.value = !(bAbsoluteRelative.value)
            }

        }
    }
}
