//build date:2019-04-04
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "localVariables.js" as LV
import "MainFunctions.js" as MF


FocusScope {
    id: position_component
    width: 360
    height: parent.height

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    Component.onCompleted: {
        Axis_Z_dTargetPosition.value = 0.1
        bAbsoluteRelative.value = true
    }

    Rectangle{
        anchors.fill: parent
        color: S.Style[S.styleName].backgroundColor
        Column{
            x: 20
            y: 400
            height: parent.height
            width: parent.width
            spacing: 6
            Label{
                id: label_adjust
                width: parent.width
                text: db.dbTr("Adjust") + db.tr
                colorChoice: 1
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_height
                    width: 150
                    height: parent.height
                    text: db.dbTr("Height") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_height
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: true
                    textIO: bUseRotation.value ? "Axis_Z_Rotation_In_dOffset" : "Axis_Z_Flatbed_In_dOffset"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                }
            }

            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_speed
                    width: 150
                    height: parent.height
                    text: db.dbTr("las_Override") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_speed
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    //isEnabled:  false
                    textIO: "Override_User_dSet"
                    unitEnable: true
                    unit: "%"
                    ioFactor: 100
                    __styleName: styleName
                }
            }
            Row{
                height: 43
                spacing:6
                Label{
                    id: zero_position
                    //width: parent.width
                    width: 150
                    text: db.dbTr("Zero Position") + db.tr
                    colorChoice: 1
                }
                Button{
                    id: save_zero_position
                    width: 72
                    height: parent.height
                    foregroundImageNormal:  imagePath + "save-w.png"
                    foregroundImagePressed: imagePath + "save.png"
                    onClicked: {
                        MF.openSaveZeroPosFileDlg()
                    }
                }
                Button{
                    id: load_zero_position
                    height: parent.height
                    width: 72
                    text:"..."
                    onClicked: {
                        MF.openLoadZeroPosFileDlg()
                    }
                }
            }

            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_zero_X
                    width: 150
                    height: parent.height
                    text: "X"
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_zero_X
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: false
                    textIO: "Job_Offset_dX"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                    digitsAfterComma: bUseInches.value ? 3 : 2
                }
            }

            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_zero_Y
                    width: 150
                    height: parent.height
                    text: "Y"
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_zero_Y
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: false
                    textIO: "Job_Offset_dY"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                    digitsAfterComma: bUseInches.value ? 3 : 2
                }
            }
            Label{
                id: current_position
                width: parent.width
                text: db.dbTr("Current Position") + db.tr
                colorChoice: 1
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_current_X
                    width: 150
                    height: parent.height
                    text: "X"
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_current_X
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: false
                    textIO: "Axis_X_dPosition"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                    digitsAfterComma: bUseInches.value ? 3 : 2
                }
            }

            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_current_Y
                    width: 150
                    height: parent.height
                    text: "Y"
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName

                }

                TextField {
                    id: text_field_current_Y
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: false
                    textIO: "Axis_Y_dPosition"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                    digitsAfterComma: bUseInches.value ? 3 : 2
                }
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_current_Z
                    width: 150
                    height: parent.height
                    text: "Z"
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_current_Z
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: false
                    textIO: "Axis_Z_dPosition"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                    digitsAfterComma: bUseInches.value ? 3 : 2
                }
            }
        }

    }
}
