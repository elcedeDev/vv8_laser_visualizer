//Build date: 2018-10-11 15:57
import QtQuick 2.0
import vectovision 1.0
import elcede.database 1.0
//import Analyser.Analyser 1.0
import "LanguageFunctions.js" as LF

Database{
    id:db
    //dbname:  "VVLaser" //"VV8_SB"
    dbname:  "123_013" //"VV8_SB"
    username: "STL"
    password: "STL"
    hostname: "localhost"
    port:     5432

    language: strVVSwitchLanguage.value
    property variant toolList

    property string strLanguage: strVVSwitchLanguage.value
    onStrLanguageChanged:{
        console.log(" ------------- onStrLanguageChanged ----------------- ", strVVSwitchLanguage.value)
        db.language = LF.selectedLangShort(strVVSwitchLanguage.value)
    }

    property bool bMaterialLoad: Material_bLoad.value

    property bool bDBMaterialLoaded: false

}
