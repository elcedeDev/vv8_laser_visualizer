//Build date: 2018-10-30
import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import vectovision 1.1
import elcede.database 1.0



/*!
    \qmltype dbElementChooser
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A file chooser list

 */
FocusScope {
    id: dbElementChooser
    objectName: "dbElementChooserElcede"

    property bool __smooth: S.isFast

    property alias userInputField: inputField.text

    property variant elements: db.materials

    property real   itemHeight: 40

    property alias isInputFieldReadOnly: inputField.readOnly

    property alias interactive: fileChooseListView.interactive
    property alias inputFieldVisible: inputField.visible
    property alias selectable: fileChooseListView.selectable

    function forceActiveFocus(){
        fileChooseListView.forceActiveFocus();
    }

    /*!
        \qmlsignal Label::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFactor)


    onElementsChanged: {
        //var javaNameList = elements;
        //fileNameList = javaNameList;
    }



    function selectFile(strFile){
        for(var i = 0; i < fileNameList.length; ++i)
        {
            if(fileNameList[i] === strFile) {
                fileChooseListView.currentIndex = i;
                currentFile = strFile;
                inputField.text = strFile;
                return;
            }
        }
    }

    property string __styleName: S.styleName

    property int __borderWidth: S.Style[__styleName].fileChooserSubcomponentBorderWidth

    property string __imageFile: (S.Style[__styleName].fileChooserFileImage !== "") ?
                                        S.Style[__styleName].imageFolder + S.Style[__styleName].fileChooserFileImage : ""
    property string __errorImage: (S.Style[__styleName].fileChooserErrorImage !== "") ?
                                      S.Style[__styleName].imageFolder + S.Style[__styleName].fileChooserErrorImage : ""

    property string __mainContainerColor: S.Style[__styleName].backgroundColor
    property string __fontColor: S.Style[__styleName].mediumFontColor
    property string __fontSelectedColor: S.Style[__styleName].lightFontColor
    property string __highlightColor: S.Style[__styleName].lightColor
    property string title: "Select material"
    property alias __currentIndex: fileChooseListView.currentIndex
    property string currentFile: ""

    property string errorText: "path does not exist"


    implicitWidth:  S.Style.FilechooserWidthInitial
    implicitHeight:  S.Style.FilechooserHeightInitial
    clip: true
    property variant fileNameList: elements //[]

    function elementSelectedAction(){
        inputField.text = fileNameList[__currentIndex]
        currentFile = fileNameList[__currentIndex]
    }

    GroupBox {
        id: mainbox
        colorChoice: 1
        headerVisible: false
        anchors.fill: parent
        Label{
            id: titleLabel
            anchors{
                top: parent.top
                topMargin: __borderWidth
                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
            }
            text: title
            height: (text != "") ? S.Style.textfieldHeightInitial : 0
            horizontalTextAlignement: Qt.AlignHCenter
            colorChoice: parent.colorChoice
        }

        GroupBox {
            id: aradexGroupbox2
            colorChoice: 2
            headerVisible: false
            anchors{
                top: titleLabel.bottom
                //   topMargin: __borderWidth
                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
                bottom: inputField.top
                bottomMargin: __borderWidth
            }
            clip:true

            ListView{
                id: fileChooseListView
                objectName: "dbChooserListViewElcede"

                anchors.fill: parent
                anchors.margins: __borderWidth
                clip: true

                signal styleChanged(string strStyle)
                signal newStyleAdded(string strNewStyleName, variant newStyleObject)

                property bool selectable: true

                model: fileNameList.length
                opacity:  1
                delegate:
                    Component{
                    MouseArea{
                        height: itemHeight
                        width: aradexGroupbox2.width - 2 * __borderWidth
                        onClicked: {
                            if(fileChooseListView.selectable){
                                fileChooseListView.currentIndex = index;
                                elementSelectedAction()
                            }
                        }
                        onDoubleClicked: if(fileChooseListView.selectable) elementSelectedAction()
                        Item {
                            anchors.fill: parent
                            Item{
                                id: image_inside
                                width: itemHeight
                                height: width
                                Image{
                                    anchors.fill: parent
                                    anchors.margins: 4
                                    smooth: true
                                    source: __imageFile

                                }
                            }
                            Label{
                                anchors{
                                    right: parent.right
                                    left: image_inside.right
                                }
                                text: fileNameList[index]
                                __textColorNormal: (index === fileChooseListView.currentIndex) ? dbElementChooser.__fontSelectedColor : dbElementChooser.__fontColor
                                __styleName : dbElementChooser.__styleName
                            }
                        }
                    }
                }

                highlight:Rectangle { color: __highlightColor; radius: 3 }
                highlightMoveDuration: 0
                focus: true
            }



        }
        TextField {
            id: inputField
            readOnly: true
            //enabled: false
            anchors{
                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
                bottom: parent.bottom
                bottomMargin: __borderWidth * 4
            }
            text: dbElementChooser.currentFile
        }

    }
}


