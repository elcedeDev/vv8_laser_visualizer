// 20190627: Error_iNumber and Warning_iNumber is filtered out
// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1



FocusScope {
    id: errorspinner
    objectName: "AradexErrorSpinnerLanguage"
    width: 800
    height: 80

    opacity: ioListLenght === 0 ? 0 : 1

    property real errorCounterWidth: 0.12
    property real errorNumberWidth: 0.23
    property real errorTextWidth: 0.65
    property variant ioList: []

    property int ioListLenght: ioList.length

    onIoListLenghtChanged: {
        console.log("--------- onIoListLenghtChanged:", ioListLenght)
        if(ioListLenght === 0)
            opacity = 0
        else
            opacity = 1
    }

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFact)
    signal timerActivated()
    signal languageChanged()
    signal requestIoList(string pattern)
    signal ioAdded(variant io, string pattern)
    onIoAdded: {
        if(pattern.match(showErrors && __errorString)){
            if(io.name !== "Error_iNumber")
                ioList = ioList.concat([[io, 1]])
        }else if(pattern.match(showWarnings && __warningString)){
            if(io.name !== "Warning_iNumber")
                ioList = ioList.concat([[io, 2]])
        }else if(pattern.match(showMessages && __messageString)){
            ioList = ioList.concat([[io, 3]])
        }
        ioList = ioList.sort(function(a, b){ return a[1] - b[1]})
        //console.log("onIoAdded: ", io.name, io.num)
    }

    signal ioRemoved(variant io, string pattern)
    onIoRemoved: {
        ioList = ioList.filter(function(x, index){
            if(x[0].name === io.name){
                if(errorListView.currentIndex == errorListView.count){
                    errorListView.currentIndex --
                }
                return false
            }else{
                return true
            }

        })
        ioList.sort(function(a, b){ return a[1] - b[1]})
        console.log("onIoRemoved -- start")
        for(var i = 0; i <= ioList.length; ++i ){
            console.log("ioList[" + i + "]:", ioList[i])
        }
        console.log("onIoRemoved -- end")

    }

    //property variant ioList: []

    // internal counter for triggering garbage collection
    property int __iGcCounter: 0


/*
    onLanguageChanged: {
        var tmp = ioList
        ioList = []
        ioList = tmp
    }
*/
    onTimerActivated: {
        if(showErrors) requestIoList(__errorString)
        if(showWarnings) requestIoList(__warningString)
        if(showMessages) requestIoList(__messageString)

        // trigger garbage collection because signal with string parameters
        // let slowly increase memory consumption
        __iGcCounter = __iGcCounter + 1
        if (__iGcCounter >= 1000) {
            gc();
            __iGcCounter = 0;
            //console.log("ErrorSpinner: triggering garbage collection")
        }
    }
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)
    onStyleChanged:{
        __styleName = strStyle
        var tmp = ioList
        ioList = []
        ioList = tmp

    }
    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    implicitWidth:  S.Style.ErrorWidgetWidthInitial
    implicitHeight:  S.Style.ErrorWidgetHeightInitial
    property bool __isFast: S.isFast
    property bool showErrors: false
    property bool showWarnings: false
    property bool showMessages: false
    property string __errorString: "Error"
    property string __warningString: "Warning"
    property string __messageString: "Message"
    property variant errorModel: ermo
    //property variant errorModel: [{name:"name0", text: "text0", num: 0}, {name:"name1", text: "text1", num: 1}]
    //property variant errorModel: []
    //property alias errorModelPath: errorModelLoader.source
    property QtObject ermo

    property string __backgroundImage: S.Style[__styleName].imageFolder +
                                       S.Style[__styleName].errorwidgetBackgroundImage;


    property string __nextImage: (S.Style[__styleName].messageNextImage !== "") ?
                                     S.Style[__styleName].imageFolder +
                                     S.Style[__styleName].messageNextImage : ""

    property string __previousImage: (S.Style[__styleName].messagePreviousImage !== "") ?
                                         S.Style[__styleName].imageFolder +
                                         S.Style[__styleName].messagePreviousImage : ""

    property string __errorImage: (S.Style[__styleName].errorwidgetErrorImage !== "") ?
                                      S.Style[__styleName].imageFolder +
                                      S.Style[__styleName].errorwidgetErrorImage : ""
    property string __warningImage: (S.Style[__styleName].errorwidgetWarningImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                        S.Style[__styleName].errorwidgetWarningImage : ""
    property string __messageImage: (S.Style[__styleName].errorwidgetMessageImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                        S.Style[__styleName].errorwidgetMessageImage : ""


    // only IO containing ErrorString

    // List of Io Names coming from C++. Values are set once on startup
    property int __count: 0


    // integer Value of Io variable. Values update every <updateTime> ms
    property variant errorIoStates: []
    // List of all ListElements from ErrorListModel.qml that are present on control.
    //    property variant errorIoValues: []//: errorIoList.map(function(x){return x.value})
    property variant errorIoValueMap
    // at first names and ErrorStates (0 = active, 1 = inactive ...) are combined
    // then only errors with state active are kept
    property int __errorCount: ioList.filter(function(x){return x[1] === 1 }).length
    property int __warningCount: ioList.filter(function(x){return x[1] === 2 }).length
    property int __messageCount: ioList.filter(function(x){return x[1] === 3 }).length

    Component.onCompleted: {
        var arr = []
        for (var i = 0; i < 2; i++) {
            arr.push({
                name: "name" + i,
                text: "text" + i,
                num: 1
            });
        }

        errorModel = arr;

        var tempMap = new Object();

        for (var i = 0; i < errorModel.length; i++){
            tempMap[errorModel[i].name] = errorModel[i]
            //console.log("tempMap: ", errorModel[i].name,tempMap[errorModel[i].name].text)
        }
        errorIoValueMap = tempMap
/*
        for (var key in errorIoValueMap) {
            if (errorIoValueMap.hasOwnProperty(key)) {
                console.log(key + " -> " + errorIoValueMap[key].text);
            }
        }
*/
    }


    onErrorModelChanged: {

        var tempMap = new Object();

        if( typeof errorModel === 'undefined'){
            var arr = []
            for (var i = 0; i < 2; i++) {
                arr.push({
                    name: "name" + i,
                    text: "text" + i,
                    num: 1
                });
            }

            errorModel = arr;
        }

        for (var i = 0; i < errorModel.length; i++){
            tempMap[errorModel[i].name] = errorModel[i]
            //console.log("tempMap: ", errorModel[i].name,tempMap[errorModel[i].name].text)
        }

        errorIoValueMap = tempMap

        errorListView.forceActiveFocus()
    }


    Component.onDestruction: {
        //ioList.destroy()
        //errorIoValueMap.destroy()
    }


    function dropDown()
    {
        //errorListView.currentIndex = __errorCount + __warningCount
        var iMessages = __errorCount + __warningCount +__messageCount
        if(errorListView.height < errorListView.parent.height * iMessages )
            errorListView.height = errorListView.parent.height * iMessages
        else
            errorListView.height = errorListView.parent.height

        errorListView.width = errorListView.parent.width
    }


    // overlay for Buttons and counters
    BorderImage {
        id: background
        anchors{
            top: parent.top
            bottom: parent.bottom
            left: notificationCounter.left
            right: parent.right
        }
        z: 1
        border{
            left: S.Style[__styleName].textfieldBorderLeftSize
            top: S.Style[__styleName].textfieldBorderTopSize
            right: S.Style[__styleName].textfieldBorderRightSize
            bottom: S.Style[__styleName].textfieldBorderBottomSize
        }
        source: __backgroundImage
    }
    Button{
        id: previousButton
        width: height
        height: (errorspinner.height)*0.8
        smooth: __isFast
        z:2
        anchors{
            top: errorspinner.top
            right: nextButton.left
            topMargin: height / 8
            leftMargin: width / 8
            rightMargin: width / 8
        }
        __imageNormal: __previousImage
        __imagePressed: __previousImage
        __buttonBorderLeftSize: 0
        __buttonBorderRightSize: 0
        __buttonBorderTopSize: 0
        __buttonBorderBottomSize: 0
        onClicked: {
            if(errorListView.currentIndex > 0){
                errorListView.currentIndex--
            }
            //            console.log("clicked NEXT")
        }
    }
    Button{
        id: nextButton
        width: height
        height: (errorspinner.height)*0.8
        smooth: __isFast
        z:2
        anchors{
            top: errorspinner.top
            right: errorspinner.right
            topMargin: height / 8
            leftMargin: width / 8
            rightMargin: width / 8
        }
        __imageNormal: __nextImage
        __imagePressed: __nextImage
        __buttonBorderLeftSize: 0
        __buttonBorderRightSize: 0
        __buttonBorderTopSize: 0
        __buttonBorderBottomSize: 0
        onClicked: {
            if(errorListView.currentIndex < errorListView.count){
                errorListView.currentIndex++
            }
            //            console.log("clicked NEXT")
        }
    }


    Item{
        id: notificationCounter
        anchors{
            top: defaultBanner.top
            topMargin: defaultBanner.height / 16
            bottom: defaultBanner.bottom
            bottomMargin: defaultBanner.height / 16
            right: previousButton.left
            //rightMargin: 6
        }
        height: (errorspinner.height)*0.8
        width: height
        z: 1
        MouseArea{
            id: errorArea
            anchors{
                top: parent.top
                left: parent.left
                right: parent.right
            }

            height: __errorCount > 0 ? parent.height/3 : 0
            onClicked: {
                dropDown()
                errorListView.currentIndex = 0
            }
            Image{
                id: levelImage
                source: __errorImage
                smooth: __isFast
                width: height
                anchors{
                    top: parent.top
                    right: parent.right
                    bottom: parent.bottom
                }
            }
            Text{
                id: levelImageLabel
                text: __errorCount
                horizontalAlignment: Qt.AlignRight
                font.pointSize: height/1.5 > 0 ? height/1.5 : 1
                color: S.Style[__styleName].errorWidgetFontColor
                clip: true
                width: height * 2
                anchors{
                    top: parent.top
                    right: levelImage.left
                    rightMargin: parent.height / 4
                    bottom: parent.bottom
                }
            }
        }
        MouseArea{
            id: warningArea

            anchors{
                top: errorArea.bottom
                left: parent.left
                right: parent.right
            }
            height: __warningCount > 0 ? parent.height/3 : 0
            onClicked: {
                dropDown()
                errorListView.currentIndex = __errorCount
            }
            Image{
                id: levelImage2
                source: __warningImage
                smooth: __isFast
                width: height
                anchors{
                    top: parent.top
                    right: parent.right
                    bottom: parent.bottom
                }
            }
            Text{
                id: levelImageLabel2
                text: __warningCount
                horizontalAlignment: Qt.AlignRight
                font.pointSize: height/1.5 > 0 ? height/1.5 : 1
                color: S.Style[__styleName].errorWidgetFontColor
                clip: true
                width: height * 2
                anchors{
                    top: parent.top
                    right: levelImage2.left
                    rightMargin: parent.height / 4
                    bottom: parent.bottom
                }
            }

        }
        MouseArea{
            id: messageArea
            anchors{
                top: warningArea.bottom
                left: parent.left
                right: parent.right
            }
            height: __messageCount > 0 ? parent.height/3 : 0
            onClicked: {
                dropDown()
                errorListView.currentIndex = __errorCount + __warningCount
            }
            Image{
                id: levelImage3
                source: __messageImage
                smooth: __isFast
                width: height
                anchors{
                    top: parent.top
                    right: parent.right
                    bottom: parent.bottom
                }
            }
            Text{
                id: levelImageLabel3
                text: __messageCount
                horizontalAlignment: Qt.AlignRight
                font.pointSize: height/1.5 > 0 ? height/1.5 : 1
                color: S.Style[__styleName].errorWidgetFontColor
                z:2
                clip: true
                width: height * 2
                anchors{
                    top: parent.top
                    right: levelImage3.left
                    rightMargin: parent.height / 4
                    bottom: parent.bottom
                }
            }

        }
    }
    // Default Image if no Errors/Warnings/Messages are present
    ELErrorWidget{
        id: defaultBanner
        height: parent.height
        width:  parent.width - parent.height*2.54 //3 button on the right
        errtext: typeof errorModel === "undefined" ? " " : errorModel[1].text
        errperc: "0 / 0"
        errno:   typeof errorModel === "undefined" ? 1 : errorModel[1].num
        messageType: 3
        opacity: (ioList.length > 0) ? 0 : 1
        errorCounterWidth: errorspinner.errorCounterWidth
        errorNumberWidth: errorspinner.errorNumberWidth
        errorTextWidth: errorspinner.errorTextWidth
    }

    ListView {
        id: errorListView
        //anchors.fill: parent
        height: parent.height
        width: parent.width
        clip: true
        snapMode: ListView.SnapToItem
        //        highlightMoveSpeed: 1
        model: ioList.length

        delegate:
            Component{
            ELErrorWidget{
                id: errorBanner
                //height: errorListView.height
                height: errorListView.parent.height
                width:  errorListView.width - height*2.54 //3 button on the right

                //errtext: ioList[index] === undefined ? " " : typeof errorIoValueMap[ioList[index][0].name] === 'undefined' ? " " : errorIoValueMap[ioList[index][0].name].text
                errtext: ioList[index] === undefined ? " " : typeof errorIoValueMap[ioList[index][0].name] === 'undefined' ? ioList[index][0].name : errorIoValueMap[ioList[index][0].name].text
                //errtext: errorIoValueMap[ioList[index][0].name].text
                errperc: (index + 1) + "/" + ioList.length

                errno:  ioList[index] === undefined ? 1 : typeof errorIoValueMap[ioList[index][0].name] === 'undefined' ? 0 : errorIoValueMap[ioList[index][0].name].num
                //errno:  errorIoValueMap[ioList[index][0].name].num
                messageType: typeof ioList[index] === 'undefined' ? 1 : ioList[index][1]
                errorCounterWidth: errorspinner.errorCounterWidth
                errorNumberWidth: errorspinner.errorNumberWidth
                errorTextWidth: errorspinner.errorTextWidth
            }
        }

        onMovementEnded: {
            errorListView.currentIndex = errorListView.visibleArea.yPosition * errorListView.count
            //            console.log("MOVED errorListView.currentIndex: " + errorListView.currentIndex + " / " + errorListView.visibleArea.yPosition)

        }

        // Workaround: Compute visibleArea
        Component.onCompleted: errorListView.visibleArea
    }

}
