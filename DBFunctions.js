//build date:2019-03-21
function saveMat() {

    db.material.strFile                             = Material_MaterialFile.value
    db.material.bStripperboard                      = Material_bStripperboard.value
    db.material.bContourLookAhead                   = Material_bContourLookAhead.value
    db.material.DeleteTools_bActive                 = Material_DeleteTools_bActive.value
    db.material.OptimizeEmptyMoves_bActive          = Material_OptimizeEmptyMoves_bActive.value
    db.material.bSteel                              = Material_bSteel.value
    db.material.bDynamicControl                     = Laser_bDynamicControl.value
    db.material.bDynamicFrequency                   = Laser_bDynamicFrequency.value
    db.material.bDynamicPulseWidth                  = Laser_bDynamicPulseWidth.value
    db.material.bDynamicAnalogPower                 = Laser_bDynamicAnalogPower.value
    db.material.dThickness                          = Material_dThickness.value
    db.material.dSlopeTime                          = Material_dSlopeTime.value
    db.material.dCentripetalForceLimitFactor        = Material_dCentripetalForceLimitFactor.value
    db.material.dMachineSpeedMax                    = Material_dMachineSpeedMax.value
    db.material.dMachineAccelerationMax             = Material_dMachineAccelerationMax.value
    db.material.dBackgroundPower                    = Material_dBackgroundPower.value
    db.material.dRotaryDiameter                     = Material_dRotaryDiameter.value
    db.material.dCylinderRotaryDiameter             = Material_dCylinderRotaryDiameter.value
    db.material.dZ1Height                           = Material_dZ1Height.value
    db.material.strEngraverIniFile                  = Material_strEngraverIniFile.value
    db.material.OptimizeEmptyMoves_strToolSequence  = Material_OptimizeEmptyMoves_strToolSequence.value
    db.material.strSlaveMaterialFileName_0_20       = Material_strSlaveMaterialFileName_0_20.value
    db.material.strSlaveMaterialFileName_21_40      = Material_strSlaveMaterialFileName_21_40.value
    db.material.iMaterial                           = Material_iMaterial.value
    db.material.iType                               = Material_iType.value
    db.material.dMarkSpeed                          = Material_dMarkSpeed.value
    db.toolList =  db.material.tools
    //console.log("----- saveDB MaterialFunctions.js ----- toolList.length:", db.toolList.length)
    for(var i = 0; i < db.toolList.length; ++i)
    {
        updateToolIntoolList(i)
    }
    //TODO: add error DB handling here
    db.material.update()
    Material_bSave.value = true
    console.log("----- saveMat() MaterialFunctions.js ----- material saved:", db.material.strFile)
}


function loadMat() {

    console.log("------------------- loadDB DBFunctions.js ----------------------- ", db.material.name)
    //db.material.strFile =  Material_MaterialFile.value
    //TODO: add error DB handling here

    db.bDBMaterialLoaded = false

    if( !db.material.fetch() ){
        console.log("---- loadDB DBFunctions.js ---- db.material.fetch() failed :",  db.material.name)
        return false
    }else
    {
        //TODO: add error DB handling here
        db.toolList =  db.material.tools


        //console.log("----- loadDB MaterialFunctions.js -----: ", Material_MaterialFile.value, db.material.strFile)
        //Material_MaterialFile IO is created but not used in Plc_Material
        //Material_strFile IO is used for the file system
        Material_strFile.value                              = "/opt/v8/application/machine/Materials/work.material"
        Material_MaterialFile.value                         = db.material.strFile
        Material_bStripperboard.value                       = db.material.bStripperboard
        Material_bContourLookAhead.value                    = db.material.bContourLookAhead
        Material_DeleteTools_bActive.value                  = db.material.DeleteTools_bActive
        Material_OptimizeEmptyMoves_bActive.value           = db.material.OptimizeEmptyMoves_bActive
        Material_bSteel.value                               = db.material.bSteel
        Laser_bDynamicControl.value                         = db.material.bDynamicControl
        Laser_bDynamicFrequency.value                       = db.material.bDynamicFrequency
        Laser_bDynamicPulseWidth.value                      = db.material.bDynamicPulseWidth
        Laser_bDynamicAnalogPower.value                     = db.material.bDynamicAnalogPower
        Material_dThickness.value                           = db.material.dThickness
        Material_dSlopeTime.value                           = db.material.dSlopeTime
        Material_dCentripetalForceLimitFactor.value         = db.material.dCentripetalForceLimitFactor
        Material_dMachineSpeedMax.value                     = db.material.dMachineSpeedMax
        Material_dMachineAccelerationMax.value              = db.material.dMachineAccelerationMax
        Material_dBackgroundPower.value                     = db.material.dBackgroundPower
        Material_dRotaryDiameter.value                      = db.material.dRotaryDiameter
        Material_dCylinderRotaryDiameter.value              = db.material.dCylinderRotaryDiameter
        Material_dZ1Height.value                            = db.material.dZ1Height
        Material_strEngraverIniFile.value                   = db.material.strEngraverIniFile
        Material_OptimizeEmptyMoves_strToolSequence.value   = db.material.OptimizeEmptyMoves_strToolSequence
        Material_strSlaveMaterialFileName_0_20.value        = db.material.strSlaveMaterialFileName_0_20
        Material_strSlaveMaterialFileName_21_40.value       = db.material.strSlaveMaterialFileName_21_40
        Material_iMaterial.value                            = db.material.iMaterial
        Material_iType.value                                = db.material.iType
        Material_dMarkSpeed.value                           = db.material.dMarkSpeed

        for(var i = 0; i < db.toolList.length; ++i)
        {
            loadToolFromtoolList(i)
        }
        //set tool 0 some default value
        Material_Tool_0_dLsrAnalogPower.value = 0

        //save the values to file system (if interpolator needs ???)
        Material_bSave.value = true
        db.bDBMaterialLoaded = true
        return true;
    }
}



function updateToolIntoolList(i){
    db.toolList[i].num  = i
    db.toolList[i].bBoarderOnly =              eval("Material_Tool_" + i + "_bBoarderOnly.value")
    db.toolList[i].bDelete =                   eval("Material_Tool_" + i + "_bDelete.value")
    db.toolList[i].bEngravingTool =            eval("Material_Tool_" + i + "_bEngravingTool.value")
    db.toolList[i].bSlotShortEdges =           eval("Material_Tool_" + i + "_bSlotShortEdges.value")
    db.toolList[i].bSlotSmoothenEdges =        eval("Material_Tool_" + i + "_bSlotSmoothenEdges.value")

    db.toolList[i].bSlotTurnClockwise =        eval("Material_Tool_" + i + "_bSlotTurnClockwise.value")
    db.toolList[i].bZMillCompensation =        eval("Material_Tool_" + i + "_bZMillCompensation.value")
    db.toolList[i].dAccelerationMax =          eval("Material_Tool_" + i + "_dAccelerationMax.value")
    db.toolList[i].dAccLsrFrequency =          eval("Material_Tool_" + i + "_dAccLsrFrequency.value")

    db.toolList[i].dAccLsrPulseWidth =         eval("Material_Tool_" + i + "_dAccLsrPulseWidth.value")
    db.toolList[i].dAnalogOutput =             eval("Material_Tool_" + i + "_dAnalogOutput.value")
    db.toolList[i].dAngle =                    eval("Material_Tool_" + i + "_dAngle.value")
    db.toolList[i].dBeamRotation =             eval("Material_Tool_" + i + "_dBeamRotation.value")

    db.toolList[i].dChannelWidth =             eval("Material_Tool_" + i + "_dChannelWidth.value")
    db.toolList[i].dConstLsrFrequency =        eval("Material_Tool_" + i + "_dConstLsrFrequency.value")
    db.toolList[i].dConstLsrPulseWidth =       eval("Material_Tool_" + i + "_dConstLsrPulseWidth.value")
    db.toolList[i].dDeccLsrFrequency =         eval("Material_Tool_" + i + "_dDeccLsrFrequency.value")

    db.toolList[i].dDeccLsrPulseWidth =        eval("Material_Tool_" + i + "_dDeccLsrPulseWidth.value")
    db.toolList[i].dDiameter =                 eval("Material_Tool_" + i + "_dDiameter.value")
    db.toolList[i].dDistanceDepth =            eval("Material_Tool_" + i + "_dDistanceDepth.value")
    db.toolList[i].dEnvelopeCut =              eval("Material_Tool_" + i + "_dEnvelopeCut.value")

    db.toolList[i].dHeightXm =                 eval("Material_Tool_" + i + "_dHeightXm.value")
    db.toolList[i].dHeightXmYm =               eval("Material_Tool_" + i + "_dHeightXmYm.value")
    db.toolList[i].dHeightXmYp =               eval("Material_Tool_" + i + "_dHeightXmYp.value")
    db.toolList[i].dHeightXp =                 eval("Material_Tool_" + i + "_dHeightXp.value")

    db.toolList[i].dHeightXpYm =               eval("Material_Tool_" + i + "_dHeightXpYm.value")
    db.toolList[i].dHeightXpYp =               eval("Material_Tool_" + i + "_dHeightXpYp.value")
    db.toolList[i].dHeightYm =                 eval("Material_Tool_" + i + "_dHeightYm.value")
    db.toolList[i].dHeightYp =                 eval("Material_Tool_" + i + "_dHeightYp.value")

    db.toolList[i].dLookAheadLimitAngleMin =   eval("Material_Tool_" + i + "_dLookAheadLimitAngleMin.value")
    db.toolList[i].dLookAheadLinitAngleMax =   eval("Material_Tool_" + i + "_dLookAheadLinitAngleMax.value")
    db.toolList[i].dLsrAnalogPower =           eval("Material_Tool_" + i + "_dLsrAnalogPower.value")
    db.toolList[i].dLsrExponentAnalogPower =   eval("Material_Tool_" + i + "_dLsrExponentAnalogPower.value")

    db.toolList[i].dLsrExponentFrequency =     eval("Material_Tool_" + i + "_dLsrExponentFrequency.value")
    db.toolList[i].dLsrFactorAnalogPower =     eval("Material_Tool_" + i + "_dLsrFactorAnalogPower.value")
    db.toolList[i].dLsrFactorFrequency =       eval("Material_Tool_" + i + "_dLsrFactorFrequency.value")
    db.toolList[i].dMaxDeviation =             eval("Material_Tool_" + i + "_dMaxDeviation.value")

    db.toolList[i].dMaxPulseWidthSpeed =       eval("Material_Tool_" + i + "_dMaxPulseWidthSpeed.value")
    db.toolList[i].dPiercingSpeed =            eval("Material_Tool_" + i + "_dPiercingSpeed.value")
    db.toolList[i].dSpeedMax =                 eval("Material_Tool_" + i + "_dSpeedMax.value")
    db.toolList[i].dSpindleSpeed =             eval("Material_Tool_" + i + "_dSpindleSpeed.value")

    db.toolList[i].dStartMacroAnalogPower =    eval("Material_Tool_" + i + "_dStartMacroAnalogPower.value")
    db.toolList[i].dStartMacroLsrFrequency =   eval("Material_Tool_" + i + "_dStartMacroLsrFrequency.value")
    db.toolList[i].dStartMacroLsrPulseWidth =  eval("Material_Tool_" + i + "_dStartMacroLsrPulseWidth.value")
    db.toolList[i].dStartMacroTime =           eval("Material_Tool_" + i + "_dStartMacroTime.value")

    db.toolList[i].dStopMacroAnalogPower =     eval("Material_Tool_" + i + "_dStopMacroAnalogPower.value")
    db.toolList[i].dStopMacroLsrFrequency =    eval("Material_Tool_" + i + "_dStopMacroLsrFrequency.value")
    db.toolList[i].dStopMacroLsrPulseWidth =   eval("Material_Tool_" + i + "_dStopMacroLsrPulseWidth.value")
    db.toolList[i].dStopMacroTime =            eval("Material_Tool_" + i + "_dStopMacroTime.value")
    db.toolList[i].dToolTest =                 eval("Material_Tool_" + i + "_dToolTest.value")

    db.toolList[i].dWorkHeight =               eval("Material_Tool_" + i + "_dWorkHeight.value")
    db.toolList[i].dWorkSpeedXm =              eval("Material_Tool_" + i + "_dWorkSpeedXm.value")
    db.toolList[i].dWorkSpeedXmYm =            eval("Material_Tool_" + i + "_dWorkSpeedXmYm.value")
    db.toolList[i].dWorkSpeedXmYp =            eval("Material_Tool_" + i + "_dWorkSpeedXmYp.value")

    db.toolList[i].dWorkSpeedXp =              eval("Material_Tool_" + i + "_dWorkSpeedXp.value")
    db.toolList[i].dWorkSpeedXpYm =            eval("Material_Tool_" + i + "_dWorkSpeedXpYm.value")
    db.toolList[i].dWorkSpeedXpYp =            eval("Material_Tool_" + i + "_dWorkSpeedXpYp.value")
    db.toolList[i].dWorkSpeedYm =              eval("Material_Tool_" + i + "_dWorkSpeedYm.value")

    db.toolList[i].dWorkSpeedYp =              eval("Material_Tool_" + i + "_dWorkSpeedYp.value")
    db.toolList[i].iControlType =              eval("Material_Tool_" + i + "_iControlType.value")
    db.toolList[i].iPocketTool =               eval("Material_Tool_" + i + "_iPocketTool.value")
    db.toolList[i].iPocketType =               eval("Material_Tool_" + i + "_iPocketType.value")

    db.toolList[i].iType =                     eval("Material_Tool_" + i + "_iType.value")
    db.toolList[i].strNote =                   eval("Material_Tool_" + i + "_strNote.value")
    db.toolList[i].ToolTest_bActive =          eval("Material_Tool_" + i + "_ToolTest_bActive.value")
    db.toolList[i].iToolType =                 eval("Material_Tool_" + i + "_iToolType.value")
    db.toolList[i].bPulseOn =                  eval("Material_Tool_" + i + "_bPulseOn.value")
    db.toolList[i].dLsrSpotDistance =          eval("Material_Tool_" + i + "_dLsrSpotDistance.value")
}

function setIOToolValue(strTool,  valTool,  i)
{
    eval("Material_Tool_" + i + "_" + strTool).value =  valTool
    //if(i === 1)
    //    console.log("----- loadDB MaterialFunctions.js ----- Material_Tool_" + i + "_" + strTool, ":  ", eval("Material_Tool_" + i + "_" + strTool).value, " -- ",  valTool, " mat_id --:", db.toolList[i].mat_id)
}

//loads the values from toolList to the Material_Tool_ IOs
function loadToolFromtoolList(i)
{
    setIOToolValue("bBoarderOnly",            db.toolList[i].bBoarderOnly, i)
    setIOToolValue("bDelete",                 db.toolList[i].bDelete, i)
    setIOToolValue("bEngravingTool",          db.toolList[i].bEngravingTool, i)
    setIOToolValue("bSlotShortEdges",         db.toolList[i].bSlotShortEdges, i)
    setIOToolValue("bSlotSmoothenEdges",      db.toolList[i].bSlotSmoothenEdges, i)
    setIOToolValue("bSlotTurnClockwise",      db.toolList[i].bSlotTurnClockwise, i)
    setIOToolValue("bZMillCompensation",      db.toolList[i].bZMillCompensation, i)
    setIOToolValue("dAccelerationMax",        db.toolList[i].dAccelerationMax, i)
    setIOToolValue("dAccLsrFrequency",        db.toolList[i].dAccLsrFrequency, i)
    setIOToolValue("dAccLsrPulseWidth",       db.toolList[i].dAccLsrPulseWidth, i)
    setIOToolValue("dAnalogOutput",           db.toolList[i].dAnalogOutput, i)
    setIOToolValue("dAngle",                  db.toolList[i].dAngle, i)
    setIOToolValue("dBeamRotation",           db.toolList[i].dBeamRotation, i)
    setIOToolValue("dChannelWidth",           db.toolList[i].dChannelWidth, i)
    setIOToolValue("dConstLsrFrequency",      db.toolList[i].dConstLsrFrequency, i)
    setIOToolValue("dConstLsrPulseWidth",     db.toolList[i].dConstLsrPulseWidth, i)
    setIOToolValue("dDeccLsrFrequency",       db.toolList[i].dDeccLsrFrequency, i)
    setIOToolValue("dDeccLsrPulseWidth",      db.toolList[i].dDeccLsrPulseWidth, i)
    setIOToolValue("dDiameter",               db.toolList[i].dDiameter, i)
    setIOToolValue("dDistanceDepth",          db.toolList[i].dDistanceDepth, i)
    setIOToolValue("dEnvelopeCut",            db.toolList[i].dEnvelopeCut, i)
    setIOToolValue("dHeightXm",               db.toolList[i].dHeightXm, i)
    setIOToolValue("dHeightXmYm",             db.toolList[i].dHeightXmYm, i)
    setIOToolValue("dHeightXmYp",             db.toolList[i].dHeightXmYp, i)
    setIOToolValue("dHeightXp",               db.toolList[i].dHeightXp, i)
    setIOToolValue("dHeightXpYm",             db.toolList[i].dHeightXpYm, i)
    setIOToolValue("dHeightXpYp",             db.toolList[i].dHeightXpYp, i)
    setIOToolValue("dHeightYm",               db.toolList[i].dHeightYm, i)
    setIOToolValue("dHeightYp",               db.toolList[i].dHeightYp, i)
    setIOToolValue("dLookAheadLimitAngleMin", db.toolList[i].dLookAheadLimitAngleMin, i)
    setIOToolValue("dLookAheadLinitAngleMax", db.toolList[i].dLookAheadLinitAngleMax, i)
    setIOToolValue("dLsrAnalogPower",         db.toolList[i].dLsrAnalogPower, i)
    setIOToolValue("dLsrExponentAnalogPower", db.toolList[i].dLsrExponentAnalogPower, i)
    setIOToolValue("dLsrExponentFrequency",   db.toolList[i].dLsrExponentFrequency, i)
    setIOToolValue("dLsrFactorAnalogPower",   db.toolList[i].dLsrFactorAnalogPower, i)
    setIOToolValue("dLsrFactorFrequency",     db.toolList[i].dLsrFactorFrequency, i)
    setIOToolValue("dMaxDeviation",           db.toolList[i].dMaxDeviation, i)
    setIOToolValue("dMaxPulseWidthSpeed",     db.toolList[i].dMaxPulseWidthSpeed, i)
    setIOToolValue("dPiercingSpeed",          db.toolList[i].dPiercingSpeed, i)
    setIOToolValue("dSpeedMax",               db.toolList[i].dSpeedMax, i)
    setIOToolValue("dSpindleSpeed",           db.toolList[i].dSpindleSpeed, i)
    setIOToolValue("dStartMacroAnalogPower",  db.toolList[i].dStartMacroAnalogPower, i)
    setIOToolValue("dStartMacroLsrFrequency", db.toolList[i].dStartMacroLsrFrequency, i)
    setIOToolValue("dStartMacroLsrPulseWidth",db.toolList[i].dStartMacroLsrPulseWidth, i)
    setIOToolValue("dStartMacroTime",         db.toolList[i].dStartMacroTime, i)
    setIOToolValue("dStopMacroAnalogPower",   db.toolList[i].dStopMacroAnalogPower, i)
    setIOToolValue("dStopMacroLsrFrequency",  db.toolList[i].dStopMacroLsrFrequency, i)
    setIOToolValue("dStopMacroLsrPulseWidth", db.toolList[i].dStopMacroLsrPulseWidth, i)
    setIOToolValue("dStopMacroTime",          db.toolList[i].dStopMacroTime, i)
    setIOToolValue("dToolTest",               db.toolList[i].dToolTest, i)
    setIOToolValue("dWorkHeight",             db.toolList[i].dWorkHeight, i)
    setIOToolValue("dWorkSpeedXm",            db.toolList[i].dWorkSpeedXm, i)
    setIOToolValue("dWorkSpeedXmYm",          db.toolList[i].dWorkSpeedXmYm, i)
    setIOToolValue("dWorkSpeedXmYp",          db.toolList[i].dWorkSpeedXmYp, i)
    setIOToolValue("dWorkSpeedXp",            db.toolList[i].dWorkSpeedXp, i)
    setIOToolValue("dWorkSpeedXpYm",          db.toolList[i].dWorkSpeedXpYm, i)
    setIOToolValue("dWorkSpeedXpYp",          db.toolList[i].dWorkSpeedXpYp, i)
    setIOToolValue("dWorkSpeedYm",            db.toolList[i].dWorkSpeedYm, i)
    setIOToolValue("dWorkSpeedYp",            db.toolList[i].dWorkSpeedYp, i)
    setIOToolValue("iControlType",            db.toolList[i].iControlType, i)
    setIOToolValue("iPocketType",             db.toolList[i].iPocketType, i)
    setIOToolValue("iType",                   db.toolList[i].iType, i)
    setIOToolValue("strNote",                 db.toolList[i].strNote, i)
    setIOToolValue("ToolTest_bActive",        db.toolList[i].ToolTest_bActive, i)
    setIOToolValue("iToolType",               db.toolList[i].iToolType, i)
    setIOToolValue("bPulseOn",                db.toolList[i].bPulseOn, i)
    setIOToolValue("dLsrSpotDistance",        db.toolList[i].dLsrSpotDistance, i)
}


function copy()
{
    var i = db.material.copy(Material_MaterialFile.value)
    if( i > 0 && db.fetchMaterial(i)) {
        Material_MaterialFile.value = db.material.name
        //console.log("db.material.name: ", db.material.name, " db.material.strFile: ", db.material.strFile )
        loadMat()
        //console.log("Material is copied: name", Material_MaterialFile.value, "id: ", i)
        return true
    }
    else
    {
        console.log("----------- Material is NOT copied: name ", Material_MaterialFile.value, " -------------- ")
        return false
    }
}

function copyIOToolValue(strTool, fromTool, toTool){
    eval("Material_Tool_" + toTool + "_" + strTool).value =  eval("Material_Tool_" + fromTool + "_" + strTool).value
}

function copyTool(fromTool, toTool){
    copyIOToolValue("bBoarderOnly",            fromTool, toTool)
    copyIOToolValue("bDelete",                 fromTool, toTool)
    copyIOToolValue("bEngravingTool",          fromTool, toTool)
    copyIOToolValue("bSlotShortEdges",         fromTool, toTool)
    copyIOToolValue("bSlotSmoothenEdges",      fromTool, toTool)
    copyIOToolValue("bSlotTurnClockwise",      fromTool, toTool)
    copyIOToolValue("bZMillCompensation",      fromTool, toTool)
    copyIOToolValue("dAccelerationMax",        fromTool, toTool)
    copyIOToolValue("dAccLsrFrequency",        fromTool, toTool)
    copyIOToolValue("dAccLsrPulseWidth",       fromTool, toTool)
    copyIOToolValue("dAnalogOutput",           fromTool, toTool)
    copyIOToolValue("dAngle",                  fromTool, toTool)
    copyIOToolValue("dBeamRotation",           fromTool, toTool)
    copyIOToolValue("dChannelWidth",           fromTool, toTool)
    copyIOToolValue("dConstLsrFrequency",      fromTool, toTool)
    copyIOToolValue("dConstLsrPulseWidth",     fromTool, toTool)
    copyIOToolValue("dDeccLsrFrequency",       fromTool, toTool)
    copyIOToolValue("dDeccLsrPulseWidth",      fromTool, toTool)
    copyIOToolValue("dDiameter",               fromTool, toTool)
    copyIOToolValue("dDistanceDepth",          fromTool, toTool)
    copyIOToolValue("dEnvelopeCut",            fromTool, toTool)
    copyIOToolValue("dHeightXm",               fromTool, toTool)
    copyIOToolValue("dHeightXmYm",             fromTool, toTool)
    copyIOToolValue("dHeightXmYp",             fromTool, toTool)
    copyIOToolValue("dHeightXp",               fromTool, toTool)
    copyIOToolValue("dHeightXpYm",             fromTool, toTool)
    copyIOToolValue("dHeightXpYp",             fromTool, toTool)
    copyIOToolValue("dHeightYm",               fromTool, toTool)
    copyIOToolValue("dHeightYp",               fromTool, toTool)
    copyIOToolValue("dLookAheadLimitAngleMin", fromTool, toTool)
    copyIOToolValue("dLookAheadLinitAngleMax", fromTool, toTool)
    copyIOToolValue("dLsrAnalogPower",         fromTool, toTool)
    copyIOToolValue("dLsrExponentAnalogPower", fromTool, toTool)
    copyIOToolValue("dLsrExponentFrequency",   fromTool, toTool)
    copyIOToolValue("dLsrFactorAnalogPower",   fromTool, toTool)
    copyIOToolValue("dLsrFactorFrequency",     fromTool, toTool)
    copyIOToolValue("dMaxDeviation",           fromTool, toTool)
    copyIOToolValue("dMaxPulseWidthSpeed",     fromTool, toTool)
    copyIOToolValue("dPiercingSpeed",          fromTool, toTool)
    copyIOToolValue("dSpeedMax",               fromTool, toTool)
    copyIOToolValue("dSpindleSpeed",           fromTool, toTool)
    copyIOToolValue("dStartMacroAnalogPower",  fromTool, toTool)
    copyIOToolValue("dStartMacroLsrFrequency", fromTool, toTool)
    copyIOToolValue("dStartMacroLsrPulseWidth",fromTool, toTool)
    copyIOToolValue("dStartMacroTime",         fromTool, toTool)
    copyIOToolValue("dStopMacroAnalogPower",   fromTool, toTool)
    copyIOToolValue("dStopMacroLsrFrequency",  fromTool, toTool)
    copyIOToolValue("dStopMacroLsrPulseWidth", fromTool, toTool)
    copyIOToolValue("dStopMacroTime",          fromTool, toTool)
    copyIOToolValue("dToolTest",               fromTool, toTool)
    copyIOToolValue("dWorkHeight",             fromTool, toTool)
    copyIOToolValue("dWorkSpeedXm",            fromTool, toTool)
    copyIOToolValue("dWorkSpeedXmYm",          fromTool, toTool)
    copyIOToolValue("dWorkSpeedXmYp",          fromTool, toTool)
    copyIOToolValue("dWorkSpeedXp",            fromTool, toTool)
    copyIOToolValue("dWorkSpeedXpYm",          fromTool, toTool)
    copyIOToolValue("dWorkSpeedXpYp",          fromTool, toTool)
    copyIOToolValue("dWorkSpeedYm",            fromTool, toTool)
    copyIOToolValue("dWorkSpeedYp",            fromTool, toTool)
    copyIOToolValue("iControlType",            fromTool, toTool)
    copyIOToolValue("iPocketType",             fromTool, toTool)
    copyIOToolValue("iType",                   fromTool, toTool)
    //copyIOToolValue("strNote",                 fromTool, toTool)
    copyIOToolValue("ToolTest_bActive",        fromTool, toTool)
    copyIOToolValue("iToolType",               fromTool, toTool)
    copyIOToolValue("bPulseOn",                fromTool, toTool)
    copyIOToolValue("dLsrSpotDistance",        fromTool, toTool)

    eval("Material_Tool_" + toTool + "_strNote").value =  eval("Material_Tool_" + fromTool + "_strNote").value + " copy of tool" + fromTool
}


function saveTool(i){
    //update in the toolList (Database.dll)
    updateToolIntoolList(i)
    //save into database
    //db.toolList[i].update()
}

//cancel user chenges in given tool
function cancelToolChanges(i)
{
    loadToolFromtoolList(i)
}
