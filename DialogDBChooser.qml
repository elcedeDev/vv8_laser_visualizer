import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Dialog{
    id: messageDialog
    objectName: "AradexDialog"
    property alias status: messageDialog.status
    property string __styleName: S.styleName
//    signal scaledSizeChanged(real scaleFactor)
/*
    onStyleChanged:{
        __styleName = strStyle
    }
*/
    property int colorChoice: 1
    property string titleText: ""
    property bool __smooth: S.isFast
//    signal styleChanged(string strStyle)

    property bool buttonCancelVisible: true
    property string buttonCancelText: "Cancel"
    property bool __buttonNoVisible: false
    property string __buttonNoText: "No"
    property bool buttonOkVisible: true
    //property string buttonOkText: "Yes"
    property alias buttonOkText: acceptButton.text
    property alias dialogHeight: maindialogBox.height
    property alias dialogWidth: maindialogBox.width
    property string fileChooserText
    //property string currentFileName: ""
    property alias currentFileName: fileChooser.currentFile
    property alias errorText: fileChooser.errorText
    property alias __fileChooser: fileChooser
/*
   // ez akkor kell ha input text edit van ??!!
    onCurrentFileNameChanged: {
        console.log("--------------------onCurrentFileNameChanged----------------",currentFileName )
        fileChooser.currentFile = currentFileName
    }
*/

/*
    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }
*/
    function initializeDir() {
        fileChooser.elements = db.materials
        fileChooser.selectFile(Material_MaterialFile.value)
    }



    GroupBox{
        id: maindialogBox
        colorChoice: messageDialog.colorChoice
        height: S.Style.dialogFileChooserHeightInitial
        width: S.Style.dialogFileChooserWidthInitial
        anchors.centerIn: parent
        headerText: messageDialog.titleText
        headerWidth: width//Math.min(__paintedHeaderWidth * 1.2, width)
        fontSize: 14

        DBChooser{
            id: fileChooser
            anchors{
                horizontalCenter: parent.horizontalCenter
                left: parent.left
                //right: parent.right
                bottom: buttonRow.top
                top: maindialogBox.top
                topMargin: maindialogBox.__headerHeight * 2
            }
            title: messageDialog.fileChooserText
            currentFile: messageDialog.currentFileName
        }

        Row{
            id: buttonRow
            anchors{
                bottom: maindialogBox.bottom
                bottomMargin: S.Style.dialogButtonHeightInitial / 2
                horizontalCenter: maindialogBox.horizontalCenter
            }
            spacing: S.Style.dialogButtonHeightInitial / 2
            Button{
                id: cancelButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonCancelText
                onClicked: {
                    messageDialog.close()
                }
                opacity: (buttonCancelVisible === true) ? 1 : 0
            }
            Button{
                id: rejectButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: __buttonNoText
                onClicked: {
                    messageDialog.setNo()
                    messageDialog.close()
                }
                opacity: (__buttonNoVisible === true) ? 1 : 0
            }
            Button{
                id: acceptButton
                width: S.Style.dialogButtonWidthInitial
                height: S.Style.dialogButtonHeightInitial
                text: buttonOkText
                onClicked: {
                    currentFileName = fileChooser.currentFile
                    messageDialog.setOk()
                    messageDialog.close()
                }
                opacity: (buttonOkVisible === true) ? 1 : 0
            }
        }
    }
}

