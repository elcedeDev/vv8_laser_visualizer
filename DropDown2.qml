import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

//dropDown list with images
Item {
    id:comboBox
    width: 140
    height: 43;
    z: 1
    property variant imageitems: [imagePath + "ButtonNormal.png",
                                  imagePath + "ButtonNormal.png"
                                 ]
    property variant textitems: ["Red", "Green"]
    property variant valuitems: [1, 2]

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    property string displaytext: chosenItem.text
    property alias currentIndexOfItems: listView.currentIndex
    property alias menuButton: chosenItem

    property string textIO
    property variant valueItem: textIO === "" ? 1 : eval(textIO).value

    property bool isDirty: false

    function rootObject() {
        var next = parent
        while (next && next.parent)
            next = next.parent
        return next
    }

    onTextIOChanged: {
        if (textIO !== ""){
            valueItem = eval(textIO).value
        }
    }

    onValueItemChanged: {
        setTextItem()
        if(imageitems !== undefined)
            if( imageitems[comboBox.valueItem] !== undefined)
                chosenItem.__imageNormal = imageitems[comboBox.valueItem]
        else
                chosenItem.__imageNormal = imagePath + "ButtonNormal.png"
        //console.log("----------------------------- dropdown2 --------------------- changed")
    }

    onDisplaytextChanged: {
        chosenItem.text = displaytext
    }

    signal comboClicked;




    function setTextItem(){
        if(comboBox.valueItem === undefined) return "";
        if(comboBox.valuitems === undefined) return "";
        if(comboBox.textitems === undefined) return "";
        for(var i = 0; i < comboBox.valuitems.length; ++i){
            if(comboBox.valueItem === comboBox.valuitems[i]){
                listView.currentIndex = i
                listView.forceActiveFocus()
                chosenItem.text = comboBox.textitems[i]
                if(textIO.length ){
                    var tmpIO = eval(textIO)
                    tmpIO.value = comboBox.valuitems[i]
                }
                return comboBox.textitems[i]
            }
        }
        if( comboBox.valueItem > comboBox.valuitems.length || comboBox.valueItem < 0 )
            chosenItem.text = "undefined"
        return comboBox.valueItem;
    }

    smooth:true;

    Component.onCompleted: {
        if( comboBox.valueItem === undefined )
            comboBox.valueItem = 1

        setTextItem()
    }


    Button {
        id:chosenItem
        width:comboBox.width
        height:comboBox.height
        //text: db.dbTr("Menu")
        __imageNormal: comboBox.valuitems.length < comboBox.valueItem ? imagePath + "ButtonNormal.png" : comboBox.valueItem < 0 ? imagePath + "ButtonNormal.png": imageitems[comboBox.valueItem]
        __imagePressed: comboBox.valuitems.length < comboBox.valueItem ? imagePath + "ButtonNormal.png" : comboBox.valueItem < 0 ? imagePath + "ButtonNormal.png": imageitems[comboBox.valueItem]

        smooth:true

        onClicked: {
            var globalCoords = comboBox.mapToItem(mainrect, 0, 0)
            dlg.x =  globalCoords.x
            dlg.y =  globalCoords.y
            dlg.open()

            comboBox.state = comboBox.state==="dropDown"?"":"dropDown"
        }
        onTextChanged: {
            //console.log("---- chosenItem -- onTextChanged: text", text )
        }
    }

    Dialog2{
        id:dlg
        Rectangle {
            id:dropDown
            x: 0
            y: 0
            width:comboBox.width;
            height:0;
            clip:true;
            radius:4;

            opacity: 1
            color: S.Style[comboBox.styleName].lightColor

            ListView {
                id:listView
                height:500;
                model: comboBox.textitems
                currentIndex: 0
                delegate: Item{
                    width:comboBox.width;
                    height: comboBox.height;

                    BorderImage{
                        source: imageitems[index]
                        width:parent.width
                        height: parent.height
                    }

                    Label {
                        text: modelData
                        width: chosenItem.width
                        height: chosenItem.height
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            isDirty = false
                            dlg.close()
                            listView.forceActiveFocus()
                            comboBox.state = ""
                            if(listView.currentIndex !== index) isDirty = true
                            listView.currentIndex = index;

                            //comboBox.displaytext = comboBox.textitems[index]
                            comboBox.valueItem = comboBox.valuitems[index]
                        }
                    }
                }
            }
        }
    }


    states: State {
        name: "dropDown";
        PropertyChanges { target: dropDown; height:comboBox.height*comboBox.textitems.length }
    }

    transitions: Transition {
        NumberAnimation { target: dropDown; properties: "height"; easing.type: Easing.OutExpo; duration: 300 }
    }
}


