// $Revision: 17742 $
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1


FocusScope {
    id: root
    objectName: "AradexErrorWidget"
    property string __styleName: S.styleName
    signal styleChanged(string strStyle)

    width: 400
    height: 80

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFact)

    onStyleChanged:{
//        console.log("ERRORWIDGET:     " + strStyle)
        __styleName = strStyle
//        console.log("After" + S.Style[__styleName].errorNumberImage + " / " + S.Style[__styleName].errorwidgetBackgroundImage)
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    Component.onCompleted: __styleName = S.styleName


    property string fontFamily: S.Style[__styleName].errorWidgetFontFamily
    property string fontColor: S.Style[__styleName].errorWidgetFontColor
    property int fontSize: S.Style[__styleName].errorWidgetFontSize
    property int fontTextSize: S.Style[__styleName].errorWidgetTextFontSize


    // percentage of widget size reserved for
    // [errorcounter , errorNumber, errortext, previousButton,nextButton]
    property variant widthShare: [0.1, 0.15, 0.70, 0.05]
//    property variant widthShare: [0.1, 0.15, 0.6556, 0.0944]
//    implicitWidth:  S.Style.ErrorWidgetWidthInitial
//    implicitHeight:  S.Style.ErrorWidgetHeightInitial
    property real errorCounterWidth: 0.12
    property real errorNumberWidth: 0.23
    property real errorTextWidth: 0.65

    property string ioTemplate: "error_"

    property int errno
    property string errtext
    property string errperc
    property int messageType
    signal nextClicked()
    signal previousClicked()


    ////////////////////////////
    ////        Images      ////
    ////////////////////////////
    property string backgroundImage: S.Style[__styleName].imageFolder +
                                     S.Style[__styleName].errorwidgetBackgroundImage;

    //  ERROR   //
    // Background Image of the errorCounter when showing an error
    property string errorCounterImage: (S.Style[__styleName].errorCounterImage !== "") ?
                                           S.Style[__styleName].imageFolder +
                                           S.Style[__styleName].errorCounterImage : ""
    // Background Image of the error Number when showing an error
    property string errorNumberImage: (S.Style[__styleName].errorNumberImage !== "") ?
                                          S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].errorNumberImage : ""
    // Background Image of the error Text when showing an error
    property string errorTextImage: (S.Style[__styleName].errorTextImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                        S.Style[__styleName].errorTextImage : ""
    //  Warning   //
    // Background Image of the error Counter when showing a Warning
    property string warningCounterImage: (S.Style[__styleName].warningCounterImage !== "") ?
                                            S.Style[__styleName].imageFolder +
                                             S.Style[__styleName].warningCounterImage : ""
    // Background Image of the error Number when showing a Warning
    property string warningNumberImage: (S.Style[__styleName].warningNumberImage !== "") ?
                                            S.Style[__styleName].imageFolder +
                                             S.Style[__styleName].warningNumberImage : ""
    // Background Image of the error Text when showing a Warning
    property string warningTextImage: (S.Style[__styleName].warningTextImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].warningTextImage : ""

    //  Message   //
    // Background Image of the error Counter when showing a Message
    property string messageCounterImage: (S.Style[__styleName].messageCounterImage !== "") ?
                                            S.Style[__styleName].imageFolder +
                                             S.Style[__styleName].messageCounterImage : ""
    // Background Image of the error Number when showing a Message
    property string messageNumberImage: (S.Style[__styleName].messageNumberImage !== "") ?
                                            S.Style[__styleName].imageFolder +
                                             S.Style[__styleName].messageNumberImage : ""
    // Background Image of the error Text when showing a Message
    property string messageTextImage: (S.Style[__styleName].messageTextImage !== "") ?
                                        S.Style[__styleName].imageFolder +
                                          S.Style[__styleName].messageTextImage : ""

    MouseArea{
        anchors.fill: root
        BorderImage {
            id: background
            anchors.fill: parent
            border{
                left: S.Style[__styleName].textfieldBorderLeftSize
                top: S.Style[__styleName].textfieldBorderTopSize
                right: S.Style[__styleName].textfieldBorderRightSize
                bottom: S.Style[__styleName].textfieldBorderBottomSize
            }
            source: backgroundImage
        }

        BorderImage {
            id: errorcounterErrorImage
            width: errorCounterWidth * parent.width
            height: parent.height

            anchors{
                top: parent.top
                left: parent.left

            }
            border{
                left: S.Style[__styleName].textfieldBorderLeftSize
                top: S.Style[__styleName].textfieldBorderTopSize
                right: S.Style[__styleName].textfieldBorderRightSize
                bottom: S.Style[__styleName].textfieldBorderBottomSize
            }
            source: messageType === 1 ? errorCounterImage :(messageType === 2 ? warningCounterImage : messageCounterImage)

        }

        BorderImage {
            id: errornumberErrorImage
            width: errorNumberWidth * parent.width
            height: parent.height

            anchors{
                top: parent.top
                left: errorcounterErrorImage.right
            }
            border{
                left: S.Style[__styleName].textfieldBorderLeftSize
                top: S.Style[__styleName].textfieldBorderTopSize
                right: S.Style[__styleName].textfieldBorderRightSize
                bottom: S.Style[__styleName].textfieldBorderBottomSize
            }
            source: messageType === 1 ? errorNumberImage :(messageType === 2 ? warningNumberImage : messageNumberImage)


        }

        BorderImage {
            id: errortextErrorImage
            width: errorTextWidth * parent.width
            height: parent.height
            anchors{
                top: parent.top
                left: errornumberErrorImage.right
            }
            border{
                left: S.Style[__styleName].textfieldBorderLeftSize
                top: S.Style[__styleName].textfieldBorderTopSize
                right: S.Style[__styleName].textfieldBorderRightSize
                bottom: S.Style[__styleName].textfieldBorderBottomSize
            }
            source: messageType === 1 ? errorTextImage :(messageType === 2 ? warningTextImage : messageTextImage)

        }
        Text{
            id: errorcounter
            font.family: root.fontFamily
            font.pointSize: {root.fontSize/1.3 > 0 ? root.fontSize/1.3 : 1}
            font.bold: true
            color: root.fontColor
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.fill: errorcounterErrorImage
            text: errperc
        }
        Text{
            id: errornumber
            font.family: root.fontFamily
            font.pixelSize: root.fontSize
            font.bold: true
            color: root.fontColor
            anchors.fill: errornumberErrorImage
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: errno
        }
        Text{
            id: errortext
            font.family: root.fontFamily
            font.pixelSize: root.fontTextSize
            color: root.fontColor
            anchors{
                fill:errortextErrorImage
                rightMargin: 10
            }
            clip: true
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            //wrapMode: Text.WordWrap
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            text: errtext
        }
    }
}
