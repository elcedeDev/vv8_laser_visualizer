// $Revision: 17742 $
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1



/*!
    \qmltype NumericInput
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief Inputfield with increase and decrease buttons.
    \based on AradexNumericInput
 */
FocusScope {
    id: label
    objectName: "ElcedeNumericInput"

    /*! \internal
        String holding relative path to the image folder.
    */
    property string __imageFolder: S.Style[__styleName].imageFolder

    /*!
        \qmlsignal Label::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)

    property alias textFieldEnabled: textfield1.enabled

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFactor)


    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    implicitWidth:  S.Style.NumericInputWidthInitial
    implicitHeight:  S.Style.NumericInputHeightInitial

    clip: true
    focus: textfield1.focus

    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }

    property bool __doNotAccept: false

    property string valueIO: ""

//    property string __fontColor: S.Style[__styleName].numericinputFontColorMiddle
    property int __fontColorChoice: 0//S.Style[__styleName].numericinputFontColorMiddle
    property string __fontColorLimits: S.Style[__styleName].numericinputFontColorLimits
    property real minValue: 0
    property real maxValue: 100
    property real initialValue: 20
    property alias digitsAfterComma: textfield1.digitsAfterComma
    property real __value: parseFloat(textfield1.text)
    property real __range: Math.abs(maxValue - minValue)
    property real __logRange: Math.log(__range) / Math.log(10)
    property real __stepSize: Math.pow(10,-digitsAfterComma)
    property bool rangeVisible: true
    property alias ioOffset: textfield1.ioOffset
    property alias ioFactor: textfield1.ioFactor
    property alias selectByMouse: textfield1.selectByMouse
    property alias unit: textfield1.unit
    property alias unitEnable: textfield1.unitEnable
	property alias text: textfield1.text
    property bool buttonsVisible: true
    property variant numValidator: DoubleValidator{}
	

    function increase(incUnits){
        var incValue
        if (textfield1.bUseTextIo()) {
			incValue = textfield1.__textIO.value * ioFactor
		} else {
			incValue = textfield1.text * ioFactor
		}
		if((incValue + ( __stepSize * incUnits)) >= maxValue){
			incValue = maxValue
		} else if((incValue + ( __stepSize * incUnits)) <= minValue){
			incValue = minValue
		} else {
			incValue = incValue + __stepSize * incUnits
		}

		incValue = incValue / ioFactor
		if (textfield1.bUseTextIo()) {
			textfield1.__textIO.value = incValue
		} else {
            textfield1.text = incValue
		}
    }
	
	function decrease(decUnits){
        var incValue
        if (textfield1.bUseTextIo()) {
			incValue = textfield1.__textIO.value * ioFactor
		} else {
			incValue = textfield1.text * ioFactor
		}
		if((incValue - ( __stepSize * decUnits)) >= maxValue){
			incValue = maxValue
		} else if((incValue - ( __stepSize * decUnits)) <= minValue){
			incValue = minValue
		} else {
			incValue = incValue - __stepSize * decUnits
		}
		incValue = incValue / ioFactor
		if (textfield1.bUseTextIo()) {
			textfield1.__textIO.value = incValue
		} else {
            textfield1.text = incValue
		}
    }
	
    Timer {
        id: decreaseTimer
        interval: 200; running: leftButton.__pressed ; repeat: true
        property int addedTime: 0
        property int changeFactor: 1
        onTriggered: {
            addedTime = addedTime + interval
            if (addedTime > 3000){
                addedTime = 0
                changeFactor = changeFactor * 10
            }
            decrease(changeFactor)
        }
        onRunningChanged: {
            if(!running){
                addedTime = 0
                changeFactor = 1
            }
        }
    }
    Timer {
        id: increaseTimer
        interval: 200; running: rightButton.__pressed ; repeat: true
        property int addedTime: 0
        property int changeFactor: 1
        onTriggered: {
            addedTime = addedTime + interval
            if (addedTime > 3000){
                addedTime = 0
                changeFactor = changeFactor * 10
            }

            increase(changeFactor)
        }
        onRunningChanged: {
            if(!running){
                addedTime = 0
                changeFactor = 1
            }
        }
    }




    Item{
        id: backgroundImage
        anchors.fill: parent
        Button{
            id: leftButton
            anchors{
                top: parent.top
                bottom: parent.bottom
                left: parent.left
            }
            width: height
            __imageNormal: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonLeftImageNormal
            __imagePressed: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonLeftImagePressed
            __imageDisabled: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonLeftImageDisabled
            __focusImage: ""
            __activeFocusOnPress: false
            foregroundImageNormal: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonMinus
            foregroundImagePressed: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonMinus
            __foregroundImageHeight: S.Style[__styleName].numericinputButtonPlusMinusHeight
            __foregroundImageWidth: S.Style[__styleName].numericinputButtonPlusMinusWidth
            __foregroundHorizontalAlignement: S.Style[__styleName].numericinputButtonPlusMinusHorizontalAlignment
            __foregroundVerticalAlignement: S.Style[__styleName].numericinputButtonPlusMinusVerticalAlignment
            onPressed: {
                focus = true
                decrease(1)
            }
            visible:buttonsVisible
        }
        Button{
            id: rightButton
            anchors{
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }
            width: height
            __imageNormal: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonRightImageNormal
            __imagePressed: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonRightImagePressed
            __imageDisabled: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonRightImageDisabled
            __focusImage: ""
            __activeFocusOnPress: false
            foregroundImageNormal: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonPlus
            foregroundImagePressed: S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputButtonPlus
            __foregroundImageHeight: S.Style[__styleName].numericinputButtonPlusMinusHeight
            __foregroundImageWidth: S.Style[__styleName].numericinputButtonPlusMinusWidth
            __foregroundHorizontalAlignement: S.Style[__styleName].numericinputButtonPlusMinusHorizontalAlignment
            __foregroundVerticalAlignement: S.Style[__styleName].numericinputButtonPlusMinusVerticalAlignment
            onPressed: {
                //console.log("textfield1.textIO: " + textfield1.text)
                focus = true
                increase(1)
            }
            visible:buttonsVisible
        }

        // no dynamic loading since that causes problems in editor
        TextField {
            id: textfield1
            anchors{
                top: parent.top
                bottom: parent.bottom
                right: buttonsVisible ? rightButton.left : parent.right
                left:  buttonsVisible ? leftButton.right : parent.left
            }
            __textBorderLeftSize: S.Style[__styleName].textfieldTextBorderLeftSize + 1 + minLabel.width
            __textBorderRightSize: S.Style[__styleName].textfieldTextBorderRightSize + 1 + maxLabel.width
            textIO: (label.valueIO != "") ? label.valueIO : ""
            fontColorChoice: label.__fontColorChoice
            fontSize: S.Style[__styleName].numericinputFontSizeMiddle
            __verticalTextAlignement: 4 // Align in the middle
            horizontalTextAlignement: Qt.AlignHCenter

            __imageEnabled: S.Style[__styleName].numericinputTextfieldImageEnabled != "" ?
                                S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputTextfieldImageEnabled : ""
            __imageDisabled: S.Style[__styleName].numericinputTextfieldImageDisabled != 0 ?
                S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputTextfieldImageDisabled : ""
            __imageReadOnly: S.Style[__styleName].numericinputTextfieldImageReadOnly != 0 ?
                S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputTextfieldImageReadOnly : ""
            __imageFocus: S.Style[__styleName].numericinputTextfieldImageFocus != 0 ?
                S.Style[__styleName].imageFolder + S.Style[__styleName].numericinputTextfieldImageFocus : ""

            text: initialValue
            ioFactor: label.ioFactor
            ioOffset: label.ioOffset
            substituteCommaWithPoint: true

            onAccepted: {

                if(!isNaN(textfield1.text)){
                    var textTmp = Number(textfield1.text)
                    if(textTmp > maxValue){
                        textfield1.commitIO(maxValue)
                    }else if(textTmp < minValue){
                        textfield1.commitIO(minValue)
                    }
                }
            }
            validator: numValidator


            Text{
                id: minLabel
                anchors{
                    left: parent.left
                    leftMargin: S.Style[__styleName].textfieldTextBorderRightSize
                    verticalCenter: parent.verticalCenter
                }

                color: label.__fontColorLimits
                font.pointSize: S.Style[__styleName].numericinputFontSizeLimits > 0 ? S.Style[__styleName].numericinputFontSizeLimits : 1
                text: minValue
                visible: rangeVisible
            }
            Text{
                id: maxLabel
                anchors{
                    right: parent.right
                    rightMargin: S.Style[__styleName].textfieldTextBorderRightSize
                    verticalCenter: parent.verticalCenter

                }
                color: label.__fontColorLimits
                font.pointSize: S.Style[__styleName].numericinputFontSizeLimits > 0 ? S.Style[__styleName].numericinputFontSizeLimits : 1
                text: maxValue
                visible: rangeVisible
            }
//            Component.onCompleted: {
//                console.log("TEEXXXXXXXT: " + textfield1.text +  " / " + initialValue)
//                textfield1.text = initialValue
//                console.log("TEXT  AFTER: " + textfield1.text +  " / " + initialValue)
//            }
        }
    }
}
