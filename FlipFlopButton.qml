import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Item {
    id: flipflop_button

    property string left_text: "left_text"
    property string right_text: "right_text"

    property string stateIO: "state_IO"
    property bool   initialState: false
    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath
    property string iconImage: S.Style[styleName].elcedeButton["05ClearTable"] //"05ClearTable.png"
    property int    fontSize: 10

    signal leftClicked()
    signal rightClicked()

    height: 43
    width: 349

    Button {
        id: icon_button
        width: parent.height
        height: parent.height
        isEnabled: false
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
        __imageDisabled: imagePath + iconImage
        __styleName: styleName
    }

    RadioGroup {
        id: radio_group
        width: parent.width - parent.height - 6
        height: parent.height
        anchors.left: parent.left
        anchors.leftMargin: parent.height + 6
        stateIO: parent.stateIO
        initialState: parent.initialState

        RadioButton {
            id: button_left
            width: (parent.width / 2) - 3
            height: parent.height
            anchors.left: parent.left
            anchors.leftMargin: 0
            text:left_text
            fontSize: parent.parent.fontSize
            onClicked_changeStateTo: "true"
            __styleName: styleName
            foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
            foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
            onClicked: leftClicked()

        }

        RadioButton {
            id: button_right
            width: (parent.width / 2) - 3
            height: parent.height
            anchors.left: button_left.right
            anchors.leftMargin: 6
            text: right_text
            fontSize: parent.parent.fontSize
            onClicked_changeStateTo: "false"
            __styleName: styleName
            foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
            foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
            onClicked: rightClicked()
        }
    }
}
