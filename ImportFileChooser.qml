//Build date: 2018-10-30
import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import vectovision 1.1


/*!
    \qmltype FileChooser
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A file chooser list

 */
FocusScope {
    id: importFileChooser
    objectName: "ImportFileChooser"

    property bool __smooth: S.isFast

    property alias userInputField: inputField.text
    property alias fileChooseListView: fileChooseListView

    /*! \internal
        String holding relative path to the image folder.
    */
    property string __imageFolder: S.Style[__styleName].imageFolder

    property string currentPath: ""

    /*!
        The file extension of displayed files.
        Use "*" or "*.*" to display all files
        For several extensions use the '|', e.g. "zip|pdf".
        The selector is case insensitive.
    */
    property string fileExtension: "*"

    /*!
        Enables or disables the hiding of directories.
    */
    property bool hideDirectories: false

    signal styleChanged(string strStyle)
    onStyleChanged: {
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }



    /*!
        \qmlsignal Label::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*!
        \qmlsignal Label::scaledSizeChanged(real scaleFactor)


    */
    signal scaledSizeChanged(real scaleFactor)

    // send by ResizeDeclarativeView
    signal setPath(string objId, string path)
    onSetPath: {
        if(objId == fileChooser){
            //            console.log(path)
            currentPathText.text = path;
        }
    }
    // send by ResizeDeclarativeView
    signal setDirContent(string objId, variant elementsNameList, variant elementsInfoList)
    onSetDirContent: {
        if(objId == fileChooser){
            //            console.log(elementsNameList)
            //            console.log(elementsInfoList)

            // use pure java arrays for list manipulation
            var javaNameList = elementsNameList;
            var javaInfoList = elementsInfoList;
            var i = 0;

            if (hideDirectories === true) {
                for (i = javaInfoList.length - 1; i >= 0; --i) {
                    if (javaInfoList[i] === 1) {
                        // remove wrong files from the list
                        javaInfoList.splice(i, 1);
                        javaNameList.splice(i, 1);
                    }
                }
            }

            fileExtension.toLowerCase();

            if (fileExtension != "*" && fileExtension != "*.*")
            {
                for (i = javaNameList.length - 1; i >= 0; --i) {
                    if (javaInfoList[i] === 0) {
                        // It's a file!
                        // check for files without extension
                        if (fileExtension === "") {
                            if (javaNameList[i].indexOf(".") !== -1) {
                                // remove files with an extension from the list
                                javaNameList.splice(i, 1);
                                javaInfoList.splice(i, 1);
                            }
                        }
                        else {
                            var currentFileName = javaNameList[i];
                            currentFileName.toLowerCase();

                            // e.g. regex is /\.(zip|pdf)$/i
                            var pattern = "\\.(" + fileExtension +")$";
                            var re = new RegExp(pattern,"i");

                            if (re.test(currentFileName) === false) {
                                //                               console.log("Remove file " + javaNameList[i]);
                                // remove wrong files from the list
                                javaNameList.splice(i, 1);
                                javaInfoList.splice(i, 1);
                            }
                        }
                    }
                }
            }

            fileNameList = javaNameList;
            fileInfoList = javaInfoList;
        }
    }

    signal goToDir(string objId, bool localFileSystem, string basePath, string newElement);
    function initializeDir(){
        goToDir(fileChooser, localFileSystem, currentPath, "")
        inputField.text = currentFile
    }
    //    function initializeDir(local, path, file){
    //        goToDir(fileChooser, local, path, "")
    //        inputField.text = file
    //    }

    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */

    function selectFile(){
        for(var i = 0; i < fileNameList.length; ++i)
        {
            if(fileNameList[i] === currentFile) {
                fileChooseListView.currentIndex = i;
                return;
            }
        }
    }

    property string __styleName: S.styleName

    property int __borderWidth: S.Style[__styleName].fileChooserSubcomponentBorderWidth

    property string __imageMoveUp: (S.Style[__styleName].fileChooserBackImage != "") ?
                                       S.Style[__styleName].imageFolder + S.Style[__styleName].fileChooserBackImage : ""
    property string __imageFile: (S.Style[__styleName].fileChooserFolderImage != "") ?
                                     S.Style[__styleName].imageFolder + S.Style[__styleName].fileChooserFolderImage : ""
    property string __imageFolders: (S.Style[__styleName].fileChooserFileImage != "") ?
                                        S.Style[__styleName].imageFolder + S.Style[__styleName].fileChooserFileImage : ""
    property string __errorImage: (S.Style[__styleName].fileChooserErrorImage != "") ?
                                      S.Style[__styleName].imageFolder + S.Style[__styleName].fileChooserErrorImage : ""

    property string __mainContainerColor: S.Style[__styleName].backgroundColor
    property string __fontColor: S.Style[__styleName].mediumFontColor
    property string __fontSelectedColor: S.Style[__styleName].lightFontColor
    property string __highlightColor: S.Style[__styleName].lightColor
    property string title: ""
    property alias __currentIndex: fileChooseListView.currentIndex
    property string currentFile: ""

    property bool localFileSystem: true;
    property string errorText: "path does not exist"


    implicitWidth:  S.Style.FilechooserWidthInitial
    implicitHeight:  S.Style.FilechooserHeightInitial
    clip: true
    property variant fileNameList: []
    property variant fileInfoList: []
    //    : [["..", 1], ["Folder1", 1], ["File1", 0], ["File2", 0], ["File3", 0], ["File4", 0], ["File5", 0], ["File6", 0], ["File7", 0], ["File8", 0], ["File9", 0], ["File10", 0]]

    function elementSelectedAction(){
        if(fileInfoList[__currentIndex] === 1){
            //                console.log("1:     " + localFileSystem+ " " + currentPathText.text);
            goToDir(fileChooser, localFileSystem, currentPathText.text, fileNameList[__currentIndex]);
            var lastChar = currentPathText.text.charAt(currentPathText.text.length - 1)
            var conjunction = (lastChar == "\\" || lastChar == "/") ? "" : "/"
            currentPath = currentPathText.text + conjunction
            inputField.text = ""
        }else if(fileInfoList[__currentIndex] === 0){
            //                console.log("0:     " + localFileSystem+ " " + fileNameList[__currentIndex]);
            //inputField.text = fileNameList[__currentIndex]
            inputField.text = ""
            currentFile = fileNameList[__currentIndex]

            var lastChar = currentPath.charAt(currentPath.length - 1)
            var conjunction = (lastChar == "\\" || lastChar == "/") ? "" : "/"
            currentPath = currentPath + conjunction
        }else{
            //                console.log("else:     " + localFileSystem+ " " + currentPathText.text);
            goToDir(fileChooser, localFileSystem, currentPathText.text, "");
            inputField.text = ""
        }
    }

    GroupBox {
        id: mainbox
        colorChoice: 1
        headerVisible: false
        anchors.fill: parent
        Label{
            id: titleLabel
            anchors{
                top: parent.top
                topMargin: __borderWidth
                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
            }
            text: title
            height: (text != "") ? S.Style.textfieldHeightInitial : 0
            horizontalTextAlignement: Qt.AlignHCenter
            colorChoice: parent.colorChoice
        }

        TextField {
            id: currentPathText
            height: (fileChooser.hideDirectories === true) ? 0 : S.Style.textfieldHeightInitial
            anchors{
                top: titleLabel.bottom
                topMargin: __borderWidth
                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
            }
            onAccepted: {
                goToDir(fileChooser, localFileSystem, currentPathText.text, "");
                currentPath = currentPathText.text
            }
        }

        GroupBox {
            id: aradexGroupbox2
            colorChoice: 2
            headerVisible: false
            //property alias fileChooseListView: fileChooseListView
            anchors{
                top: currentPathText.bottom
                //                topMargin: __borderWidth
                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
                bottom: inputField.top
                bottomMargin: __borderWidth
            }
            clip:true

            Item {
                anchors.fill: parent
                opacity: (fileInfoList[0] ===  (1 << 8)) ? 1 : 0
                Item{
                    id: image_inside
                    width: label_inside.height
                    height: width

                    Image{
                        anchors.fill: parent
                        anchors.margins: 4
                        smooth: true
                        source: (fileInfoList[0] === (1 << 8)) ? __errorImage : ""


                    }
                }
                Label{
                    id: label_inside
                    height: 40
                    anchors{
                        right: parent.right
                        left: image_inside.right
                    }
                    text: errorText
                    __textColorNormal: fileChooser.__fontColor
                }
            }
            ListView{
                id: fileChooseListView
                objectName: "fileChooseListViewAradex"

                anchors.fill: parent
                anchors.margins: __borderWidth
                clip: true

                signal styleChanged(string strStyle)
                signal newStyleAdded(string strNewStyleName, variant newStyleObject)


                model: fileNameList.length
                opacity: (fileInfoList[0] ===  (1 << 8)) ? 0 : 1
                delegate:
                    Component{
                    MouseArea{
                        height: label_inside.height
                        width: aradexGroupbox2.width - 2 * __borderWidth
                        onClicked: { fileChooseListView.currentIndex = index;
                            elementSelectedAction()}
                        onDoubleClicked: elementSelectedAction()
                        Item {
                            anchors.fill: parent
                            Item{
                                id: image_inside
                                width: label_inside.height
                                height: width

                                Image{
                                    anchors.fill: parent
                                    anchors.margins: 4
                                    //                                            width: label_inside.height
                                    //                                            height: width
                                    smooth: true
                                    source: {(fileNameList[index] === "..") ? __imageMoveUp :
                                                                              (fileInfoList[index] === 0) ? __imageFolders :
                                                                                                            (fileInfoList[index] === 1) ? __imageFile : ""
                                    }


                                }
                            }
                            Label{
                                anchors{
                                    right: parent.right
                                    left: image_inside.right
                                }
                                text: fileNameList[index]
                                __textColorNormal: (index === fileChooseListView.currentIndex) ? fileChooser.__fontSelectedColor : fileChooser.__fontColor
                                __styleName : label_inside.__styleName
                            }
                        }
                    }
                }
                highlight:Rectangle { color: __highlightColor; radius: 3 }
                highlightMoveDuration: 0
                focus: true
            }
        }

        TextField {
            id: inputField
            anchors{

                left: parent.left
                leftMargin: __borderWidth
                right: parent.right
                rightMargin: __borderWidth
                bottom: parent.bottom
                bottomMargin: __borderWidth * 4
            }
            text: fileChooser.currentFile

        }

    }

}
