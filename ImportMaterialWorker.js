//Build date: 2018-10-30
function pause(millis)
{
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    } while((curDate - date) < millis);
}



WorkerScript.onMessage = function(message) {

    for(var i = 0; i < message.fileNameList.length; ++i)
    {
        //console.log(i, ":     " + message.currentPath + "/" + message.fileNameList[i], "info: ",  message.fileInfoList[i], WorkerScript.bEndImport );

        if( WorkerScript.bEndImport ) break;
        if(message.fileInfoList[i] === 1) continue;
        if(message.fileNameList[i] === "work.material") continue;


        WorkerScript.sendMessage({indexVal: i, fileName: message.fileNameList[i], endImport: false })

        pause(1000)
    }

    WorkerScript.sendMessage({indexVal: 0, fileName: "", endImport: true })
}
