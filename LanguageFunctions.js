function selectedLangShort(strlanguage){
    switch(strlanguage){
    case "English":     return "en";
    case "Magyar":      return "hu";
    case "Deutsch":     return "de";
    case "Espanol":     return "es";
    case "Japanese":    return "jp";
    case "Francais":    return "fr";
    case "Danish":      return "dk";
    }
}

function selectedLangLong(strlanguage){
    switch(strlanguage){
    case "English": return strlanguage;
    case "Magyar": return strlanguage;
    case "Deutsch": return strlanguage;
    case "Espanol": return strlanguage;
    case "Japanese": return "日本語";
    case "Francais": return strlanguage;
    case "Danish": return strlanguage;
    }
    return "English";
}
