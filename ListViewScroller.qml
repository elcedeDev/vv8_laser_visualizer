import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0

/*!
    use in ListView
    set the orientation, currentIndex, count  properties to the parent ListView same properties
    set the owner property to the parent ListView
*/ 

Item{
    id:listViewScroller
    anchors.fill: parent
    property ListView owner: listView
    property alias orientation: listView.orientation
    property alias currentIndex: listView.currentIndex
    property int count: listView.count

    Rectangle{
        id: topMove
        width:68
        height:38
        color: "#19311919"
        opacity: parent.currentIndex < parent.count -1 ? 1:0
        x: parent.width/2 - width/2
        y: 0
        visible: parent.orientation === ListView.Vertical ? true:false
        Text {
            x:20
            y:-10
            text: "<"
            font.pointSize:36
            color: Qt.rgba(1,1,1,0.5)
            rotation: 90
        }
        MouseArea{
            width: parent.width
            height: parent.height
            onClicked: {
                if (mouse.button === Qt.LeftButton){
                    owner.currentIndex += 1
                }
            }
        }
    }

    Rectangle{
        id: bottomMove
        width:68
        height:38
        color: "#19311919"
        opacity: parent.currentIndex > 0 ? 1:0
        x: parent.width/2 - width/2
        y: parent.height - height
        visible: parent.orientation === ListView.Vertical ? true:false
        Text {
            x:20
            y:-10
            text: ">"
            font.pointSize:36
            color: Qt.rgba(1,1,1,0.5)
            rotation: 90
        }
        MouseArea{
            width: parent.width
            height: parent.height
            onClicked: {
                if (mouse.button === Qt.LeftButton){
                    owner.currentIndex -= 1
                }
            }
        }
    }



    Rectangle{
        id: leftMove
        width:38
        height:68
        color: "#19311919"
        opacity: parent.currentIndex < parent.count -1 ? 1:0
        x: 0
        y: parent.height/2 - height/2
        visible: parent.orientation === ListView.Vertical ? false:true
        Text {
            x: 5
            y: 8
            text: "<"
            font.pointSize:36
            color: Qt.rgba(1,1,1,0.5)
        }
        MouseArea{
            width: parent.width
            height: parent.height
            onClicked: {
                if (mouse.button === Qt.LeftButton){
                    owner.currentIndex += 1
                }
            }
        }
    }


    Rectangle{
        id: rightMove
        width:38
        height:68
        color:Qt.rgba(0,0,0,0.1)
        opacity: parent.currentIndex > 0 ? 1:0
        x: parent.width - width
        y: parent.height /2 - height/2
        visible: parent.orientation === ListView.Vertical ? false:true
        Text {
            x: 5
            y: 8
            text: ">"
            font.pointSize:36
            color: Qt.rgba(1,1,1,0.5)
        }
        MouseArea{
            width: parent.width
            height: parent.height
            onClicked: {
                if (mouse.button === Qt.LeftButton){
                    owner.currentIndex -= 1
                }
            }
        }
    }

    //this is needed for the alias properties but not used elsewhere
    ListView{
        id:listView
    }
}
