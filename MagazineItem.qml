//build date:2019-03-13
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0


Item {
    id: item1

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    property string hopper_num

    property string hopper_tool:        "Hopper_Place" + hopper_num + "_iTool"
    property string hopper_boutside:    "Hopper_Place" + hopper_num + "_bOutside"
    property string hopper_lifetime:    "Hopper_Place" + hopper_num + "_dLifeTime"

    property int iHopperiTool: eval(hopper_tool).value

    property string hopper_name: "Material_Tool_" + iHopperiTool + "_strNote"

    property double dlifeTime: eval(hopper_lifetime).value

    x: 5
    height: 40
    width: 620

    TextField {
        id: mag_pos_1_label
        x: 0
        y: 5
        width: 70
        height: parent.height
        text: hopper_num
        readOnly: true
        horizontalTextAlignement: 4
        fontColorChoice: 1
        fontPlaceholderColorChoice: 0
        ioOffset: 2
        transformOrigin: Item.Center
        unitEnable: false
        selectByMouse: false
    }

    Button{
        id: mag_pos_1_tool
        x: 90
        y: 5
        width: 70
        height: parent.height
        text: eval(hopper_tool).value
        property bool bClicked

        __imageNormal: dlifeTime > 0 ? imagePath + "rect-green-32.png" : imagePath + "rect-red-32.png"
        __fontColorNormal: "black"
        onClicked:{
            bClicked = true
            dlgToolChooser.open()
        }

        Connections {
            target: dlgToolChooser
            onToolChosen:{
                if(mag_pos_1_tool.bClicked){
                    mag_pos_1_tool.bClicked = false
                    var varHopperTool = eval(hopper_tool)
                    varHopperTool.value = dlgToolChooser.dlg_tool_number
                    iHopperiTool = varHopperTool.value
                }
            }
        }
    }

    TextField{
        id: mag_pos_name
        x: 180
        y: 5
        width: 360 //180
        height: parent.height
        textIO: hopper_name
        enabled: false
    }

    TextField {
        id: mag_pos_1_lifetime
        x: 560 //380
        y: 5
        width: 120
        height: parent.height
        text: "0"
        textIO: hopper_lifetime
        ioFactor: dMToM.value
        unitEnable: true
        unit: strUnitM.value
        digitsAfterComma:0
        substituteCommaWithPoint:true
        horizontalTextAlignement: Qt.AlignRight
        validator: DoubleValidator{bottom:0;top:999999}

    }

}
