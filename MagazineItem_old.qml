import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0


Item {
    id: item1

    property string hopper_num

    property string hopper_tool:        "Hopper_Place" + hopper_num + "_iTool"
    property string hopper_boutside:    "Hopper_Place" + hopper_num + "_bOutside"
    property string hopper_lifetime:    "Hopper_Place" + hopper_num + "_dLifeTime"

    x: 5
    height: 40
    width: 400

    TextField {
        id: mag_pos_1_label
        x: 0
        y: 5
        width: 42
        height: 36
        text: hopper_num
        readOnly: true
        horizontalTextAlignement: 4
        fontColorChoice: 1
        fontPlaceholderColorChoice: 0
        ioOffset: 2
        transformOrigin: Item.Center
        unitEnable: false
        selectByMouse: false
    }



    ELNumericInput {
        id: mag_pos_1_tool
        x: 50
        y: 5
        width: 101
        height: 36
        rangeVisible: true
        minValue: 0
        maxValue: 20
        valueIO: hopper_tool
        digitsAfterComma:0
        buttonsVisible: false
        numValidator: IntValidator{bottom: 0; top: 20}

    }

    ELNumericInput {
        id: mag_pos_1_length
        x: 157
        y: 5
        width: 160
        height: 36
        text: "0"
        valueIO: hopper_lifetime
        onValueIOChanged:  {
            if( hopper_lifetime.value > 0 )
                mag_pos_1_label.fontColorChoice = 2 //--green
            else
                mag_pos_1_label.fontColorChoice = 3 //--red
        }
        selectByMouse: false
        rangeVisible: true
        maxValue: 999999
        ioFactor: dMToM.value
        initialValue: 0
        unitEnable: true
        unit: strUnitM.value
        buttonsVisible: false
        numValidator: DoubleValidator{bottom:0;top:999999}

    }




    Indicator {
        id: mag_pos_1_outside
        x: 311
        y: 9
        width: 30
        height: 30
        doubleIndicator: false
        valueIO: hopper_boutside

    }
}
