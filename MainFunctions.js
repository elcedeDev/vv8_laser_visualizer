//build date:2019-06-10, added move machine command to "Go to Service-Position" command, SB.
//2019-06-10, added move machine command to "Go to Service-Position" command, SB.
//2019-06-21, removed "MoveMachine_bReduceAcceleration", SB
Qt.include("localVariables.js")

function pause(millis)
{
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    } while((curDate - date) < millis);
}


function toggleCombo()
{

    //if(bMachineIsRotation.value) {
    if(Machine_Configuration_Machine_Type.value === "LCS"){
        machinelogo.source = "images/LCSevo.svg"
        return
    }
    if(bUseRotation.value) {
        //toRotation()
        machinelogo.source = "images/ROTAMATEevo.svg"
    }else{
        //toFlatbed()
        machinelogo.source = "images/LASERMATEevo.svg"
    }


}

function toFlatbed()
{


    bUseRotation.value =                    false;
    bUseFlatbed.value =                     true;
    MoveMachine_Rotation_In_bActive.value = false;
    MoveMachine_Flatbed_In_bActive.value =  true;
    //bEnableMilling.value = 	false;
    //bEnableEngraving.value =  true;
    Machine_bUseJobSplitting.value =        false;
    Toolchange_Milling_bActive.value =      false;
    //ToolChange_Milling_Debug_bNoReference.value =  true;
    //Axis_Z_Reference_bOk.value =  false;
}


function toRotation()
{
    bUseRotation.value =                    true;
    bUseFlatbed.value =                     false;
    MoveMachine_Rotation_In_bActive.value = true;
    MoveMachine_Flatbed_In_bActive.value =  false;

    //while( bMachineIsRotation.value === false )
    while( bUseRotation.value === false )
    {
        //console.log("wait while bMachineIsRotation turns true ...")
    }

    ChangeShell_In_bStartChangeOutShell.value = true

 }


function isEnabled(){
    //if( (_EmergencyChain_Error_iOpen.value === 1) ||
    if( (!_EmergencyChain_Out_bIsClosed.value ) ||
            Machine_bBusy.value ||
            (Axis_X_dSpeedNominal.value > 0.00001 ) ||
            (Axis_Y_dSpeedNominal.value > 0.00001 ) ||
            (Axis_Z_dSpeedNominal.value > 0.00001 ) ||
            ChangeShell_In_bStartChangeInShell.value ||
            ChangeShell_In_bStartChangeOutShell.value )
        return false;
    else
        return true;
}


function gotoZeroPosition()
{
    if( Machine_bBusy.value ) {
        return
    }

    //MoveMachine_bReduceAcceleration.value = true;
    Axis_Z_dDistanceNominal.value = -(Axis_Z_dPosition.value - 0.100);
    Axis_X_dDistanceNominal.value = -(Axis_X_dPosition.value - Job_Offset_dX.value);
    Axis_Y_dDistanceNominal.value = -(Axis_Y_dPosition.value - Job_Offset_dY.value);

    MoveMachine_bMove.value = true;
}


function gotoServicePosition()
{
    if(bUseRotation.value)
    {
        Axis_Z_dDistanceNominal.value = -(Axis_Z_dPosition.value - Machine_Configuration_MaxTable_Rotation_dPosition_Z.value);
        Axis_X_dDistanceNominal.value = -(Axis_X_dPosition.value - Machine_Configuration_MaxTable_Rotation_dPosition_X.value);
        Axis_Y_dDistanceNominal.value = -(Axis_Y_dPosition.value - Machine_Configuration_MaxTable_Rotation_dPosition_Y.value);
    }
    else
    {
        Axis_Z_dDistanceNominal.value = -(Axis_Z_dPosition.value - Machine_Configuration_MaxTable_Flatbed_dPosition_Z.value);
        Axis_X_dDistanceNominal.value = -(Axis_X_dPosition.value - Machine_Configuration_MaxTable_Flatbed_dPosition_X.value);
        Axis_Y_dDistanceNominal.value = -(Axis_Y_dPosition.value - Machine_Configuration_MaxTable_Flatbed_dPosition_Y.value);
    }
	
	MoveMachine_bMove.value = true;
}



function clearTable()    {
    //MoveMachine_bReduceAcceleration.value =  true;
    Axis_Z_dDistanceNominal.value = -(Axis_Z_dPosition.value - ClearTable_Position_dZ.value);
    Axis_Y_dDistanceNominal.value = -(Axis_Y_dPosition.value - ClearTable_Position_dY.value);

    if(bUseRotation.value)
        Axis_X_dDistanceNominal.value = -(Axis_X_dPosition.value - ChangeShell_dCylinderRotaryDiameter.value * 3.14 / 4.0);
    else
        Axis_X_dDistanceNominal.value = -(Axis_X_dPosition.value - ClearTable_Position_dX.value);

    MoveMachine_bMove.value = true;
}


function selectJob(){

    selectJob.__isPressed = false;
    var path = dialogFileChooser.currentPath
    path = path.replace(/\//g, "\\");
    //path = "Z:" + path;
    Job_strFile.value = path + dialogFileChooser.currentFileName
    Job_strLoadPath.value = path
    Job_iLine.value = 0;
    Job_bRotate90.value = false;
    Job_Scale_dY.value = 1;
    Job_Scale_dX.value = 1;
    Job_In_bLoaded.value = true;
    //Job_strSimulatorFile.value = Job_strFile.value;


}

function saveZeroPosition()
{
    var path = dialogFileChooser.currentPath
    //path = path.replace(/\//g, "\\");
    //path = "Z:" + path;
    //Job_Offset_strFile.value = path + dialogFileChooser.currentFileName
    Job_Offset_strFile.value = path + dialogFileChooser.__fileChooser.userInputField
    Job_Offset_strNullpointLoadPath.value = path
    ZeroPosition_bSave.value = true
}

function loadZeroPosition()
{
    var path = dialogFileChooser.currentPath
    //path = path.replace(/\//g, "\\");
    //path = "Z:" + path;
    Job_Offset_strFile.value = path + dialogFileChooser.currentFileName
    Job_Offset_strNullpointLoadPath.value = path
    //ZeroPosition_bSave.value = true
    //Job_Offset_bLoad.value = true
    ZeroPosition_bLoad.value = true
}

function convertUnits(){
    if( bUseInches.value ) {
        dMPSToMPM.value 	= 2362.20472
        dMToMM.value 		= 39.3700787
        dMMToM.value 		= 0.0393700787
        dAccUnit.value  	= 0.0393700787
        dMToM.value	 		= 39.3700787
        strUnitMM.value		= '"'
        strUnitM.value 	 	= '"'
        strUnitSpeed.value 	= '"/min'
        strUnitAcc.value   	= '"/s2'

    } else {
        dMPSToMPM.value 	= 60.0
        dMToMM.value 		= 1000.0
        dMMToM.value 		= 0.001
        dAccUnit.value  	= 1
        dMToM.value	 		= 1
        strUnitMM.value		= "mm";
        strUnitM.value 	 	= "m";
        strUnitSpeed.value 	= "m/min";
        strUnitAcc.value   	= "m/s2";

    }
}
function getTimeString(secs){
    //var hours = Math.floor(secs/(60*60));
    //var minutes = Math.floor((secs-hours*60*60)/60);
    var minutes = Math.floor((secs)/60);
    var seconds = secs-minutes*60;
    var timeString = minutes + ":" + ("0" + seconds).slice(-2)
//    var timeString = ("0" + hours).slice(-2) + ":" + ("0" + minutes).slice(-2) + ":" + ("0" + seconds).slice(-2)
//    var timeString = hours + ":" + minutes + ":" + seconds

    return timeString;
}

function getSumofWarmupTime(iWarmUpStepStatus){
    if(iWarmUpStepStatus === 0) return 0
    if(iWarmUpStepStatus === 1)// short method
    {
        return  (Spindle_WarmUp_dStepShortWarmupTime_0.value
              +  Spindle_WarmUp_dStepShortWarmupTime_1.value
              +  Spindle_WarmUp_dStepShortWarmupTime_2.value
              +  Spindle_WarmUp_dStepShortWarmupTime_3.value)
    }
    else if(iWarmUpStepStatus === 2)//long method
    {
         return  (Spindle_WarmUp_dStepWarmupTime_0.value
               +  Spindle_WarmUp_dStepWarmupTime_1.value
               +  Spindle_WarmUp_dStepWarmupTime_2.value
               +  Spindle_WarmUp_dStepWarmupTime_3.value
               +  Spindle_WarmUp_dStepWarmupTime_4.value)
    }
}


function getRemainingWarmUpTime(sumofWarmUpTime, iStep){
    var strWarmUpTimeName
    if(Spindle_bTimeStatus_TimeStatus.value === 1)// short method)
    {
        strWarmUpTimeName = "Spindle_WarmUp_dStepShortWarmupTime_"
    }
    else if(Spindle_bTimeStatus_TimeStatus.value === 2)
    {
        strWarmUpTimeName = "Spindle_WarmUp_dStepWarmupTime_"
    }

    if(Spindle_WarmUp_iWarmupStepCounter.value < iStep)
        return eval(strWarmUpTimeName + iStep).value
    if(Spindle_WarmUp_iWarmupStepCounter.value > iStep)
        return 0
    if(Spindle_WarmUp_iWarmupStepCounter.value === iStep)
    {
        var iSumProcessedTime = 0
        for(var i = 0; i < iStep; ++i)
        {
            iSumProcessedTime += eval(strWarmUpTimeName + i).value
        }
        var iVal = eval(strWarmUpTimeName + iStep).value
        return iVal - (Spindle_WarmUp_iWarmupElapsedTime.value - iSumProcessedTime)
    }
}

function openLoadZeroPosFileDlg()
{
    console.log("function openLoadZeroPosFileDlg()")
    var path = Job_Offset_strNullpointLoadPath.value
    path = path.replace(/\\/g, "/");
    var dlg = dialogFileChooser

    dlg.titleText =         db.dbTr("Select zero position file") + db.tr
    dlg.buttonOkText =      db.dbTr("Load") + db.tr
    dlg.fileChooserText =   db.dbTr("Load File") + db.tr
    dlg.fileExtension = "zeropostion"
    dlg.currentPath = path
    dlg.type = "LOADZEROPOSITION"
    var fullName = Job_Offset_strFile.value

    dlg.currentFileName = fullName.substring(fullName.lastIndexOf('/') + 1)
    //backward compatibility
    //dlg.currentFileName = dlg.currentFileName.substring(dlg.currentFileName.lastIndexOf('\\') + 1)

    dlg.initializeDir()

    dlg.open()
}


function openSaveZeroPosFileDlg()
{

    console.log("function openSaveZeroPosFileDlg()")
    var path = Job_Offset_strNullpointLoadPath.value
    path = path.replace(/\\/g, "/");
    var dlg = dialogFileChooser

    dlg.titleText =         db.dbTr("Save zeroposition file") + db.tr
    dlg.buttonOkText =      db.dbTr("Save") + db.tr
    dlg.fileChooserText =   db.dbTr("Save File") + db.tr
    dlg.fileExtension = "zeropostion"
    dlg.type = "SAVEZEROPOSITION"
    dlg.currentPath = path
    var fullName = Job_Offset_strFile.value

    dlg.currentFileName = fullName.substring(fullName.lastIndexOf('/') + 1)
    //backward compatibility
    //dlg.currentFileName = dlg.currentFileName.substring(dlg.currentFileName.lastIndexOf('\\') + 1)

    dlg.initializeDir()

    dlg.open()
}
