import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1


Item{
    id: modaldlg
    parent: mainrect
    width: parent.width
    height: parent.height

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    property alias dialogboxwidth: dialogbox.width
    property alias dialogboxheight: dialogbox.height
    property alias title: title.text
    property alias dialogboxx: dialogbox.x
    property alias dialogboxy: dialogbox.y
    property bool  result: false
    property alias radius: dialogbox.radius

    signal oK()

    property alias container: container

    property int   animationDuration: 300

    property real  faderdimm: 0.4
    property real  dialogboxdimm: 0.9

    property int   margin: 10

    state:"Hidden"

    visible:false
    function open() {
        modaldlg.visible = true;
        modaldlg.state="Visible"
    }
    function close() {
        modaldlg.state="Hidden";
        fader.opacity = 0.0;  dialogbox.opacity = 0.0;
        //modaldlg.visible = false;
    }
    Rectangle {
        id: fader
        width: parent.width
        height: parent.height
        opacity: 0.0
        visible: parent.visible
        color: S.Style[S.styleName].backgroundColor
        Behavior on opacity {
            SequentialAnimation {
                NumberAnimation {duration: animationDuration}
                ScriptAction {script: {
                        visible = false;
                        modaldlg.visible = false;
                    }
                }
            }
        }
    }


    Rectangle{
        id:dialogbox
        x: parent.width/2 - width /2
        y: parent.height/2 - height/2
        z:1
        width: parent.width
        height: parent.height
        radius:8
        opacity: 0.0
        visible: true
        color: "#333333"
        Behavior on opacity {
            SequentialAnimation {
                NumberAnimation {duration: animationDuration}
                ScriptAction {script: { if(opacity === 0.0 ){
                            visible = false;
                        }
                    }
                }
            }
        }

        Label{
            id:title
            text:"title text"
            fontSize: 12
            colorChoice: 0
            width:parent.width - 2*margin
            anchors.left: parent.left
            anchors.leftMargin: margin / 2
        }

        Rectangle{
            id:container
            anchors.top: title.bottom
            anchors.topMargin: margin
            anchors.bottom: buttons.top
            anchors.bottomMargin: margin
            anchors.left: parent.left
            anchors.leftMargin: margin
            anchors.right: parent.right
            anchors.rightMargin: margin
            color: "#333333"
        }

        Row{
            id: buttons
            x: 10
            y: parent.height - height - 10
            width: parent.width
            height: 43
            spacing: 10

            Button{
                text: db.dbTr("Cancel") + db.tr
                width: (parent.width / 2) - parent.spacing*2
                height: parent.height
                __horizontalTextAlignement: Text.AlignHCenter
                __verticalTextAlignement: Text.AlignVCenter
                onClicked: {close(); result = false}
                visible: true
                foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
            }

            Button{
                text: db.dbTr("OK") + db.tr
                width: (parent.width / 2) - parent.spacing
                height: parent.height
                __horizontalTextAlignement: Text.AlignHCenter
                __verticalTextAlignement: Text.AlignVCenter
                onClicked: {close(); result = true; oK()}
                visible: true
                foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
                foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
            }
        }
    }
    //mouse event blocker
    MouseArea{
        anchors.fill: parent
        //width: parent.width
       // height: parent.height
    }

    states: [
        State { name: "Visible" },
        State { name: "Hidden" }
    ]
    transitions:
        Transition {
        from: "Hidden"
        to: "Visible"
        PropertyAnimation {
            target: fader; property: "opacity"; to: faderdimm; duration: animationDuration; easing.type: Easing.Linear
        }
        PropertyAnimation {
            target: dialogbox; property: "opacity"; to: dialogboxdimm; duration: animationDuration; easing.type: Easing.Linear

        }
    }

}
