import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

FocusScope {
    id:override
    property alias labelWidth:label_override.width
    height:43

    MultilineTextField {
        id: label_override
        width: 120
        height: parent.height
        text: db.dbTr("override") + db.tr
        fontSize:10
        readOnly: true
    }

    NumericInput {
        id: input_override
        anchors.left: label_override.right
        anchors.leftMargin: 5
        height: parent.height
        selectByMouse: false
        valueIO: "Override_User_dSet"
        unitEnable: true
        unit: "%"
        rangeVisible: false
        minValue: 0
        maxValue: 100
        __stepSize:1
        ioFactor: 100
        initialValue: Override_User_dSet.value
        digitsAfterComma: 0
    }
}
