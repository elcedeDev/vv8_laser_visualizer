//build date:2019-06-21
//2019-06-10, removed "Y-Axis on Clear table position" condition for Wood-Lifter button, only X and Z are checked, SB
//2019-06-20, removed fixed acceleration values for arrow buttons
//2019-06-21, removed "MoveMachine_bReduceAcceleration", SB
import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
import "MainFunctions.js" as MF

Item {
    id: positionControl
    objectName: "positionControl"

    property string __style: S.styleName //mainrect.rootStyle
    property string __imagePath: S.Style[__style].imagePath

    width: 900
    height: 198

	//function to disable button when machine is not on clear table position
    function woodLifterEnabled(){
        if( (Math.abs(Math.abs(Axis_X_dPosition.value) - Math.abs(ClearTable_Position_dX.value)) < 0.003) &&
				//Only X and Z-Positions are important for Wood-Lifter, SB
                //(Math.abs(Math.abs(Axis_Y_dPosition.value) - Math.abs(ClearTable_Position_dY.value)) < 0.003) 
                !MoveMachine_bMove.value){
            return true
        }else
            return false
    }

    property bool testAbs: true

    //visible: false

    Rectangle{
        height: parent.height
        width: parent.width
        // color: "white"
        color: S.Style[__style].backgroundColor
    }


    property bool isJobStart: Job_bStart.value
    property bool isFullScreen: screenJob.state === "1"
    property bool isJob_bFinished: Job_bFinished.value


    function setZeroPosition()
    {
        //Job_Offset_dX.value = Axis_X_Master_dPosition.value;
        Job_Offset_dX.value = Axis_X_dPosition.value + 0.000001;
        Job_Offset_dY.value = Axis_Y_dPosition.value + 0.000001;
        Job_bZeroPositionSet.value = true;
    }




    property int colWidth: 140
    property int colSpacing: 20
    property int buttonHeight: 50

    property int buttonWidth: 50


    Button{
        id:button_back
        x: 2
        width: 30
        height:parent.height
        foregroundImageNormal:  __imagePath + "arrow-06-w.png"
        foregroundImagePressed: __imagePath + "arrow-06.png"
        __imageNormal: __imagePath +  "EmptyDown.png"
        onClicked: {
            positionControl.state       = "HIDDEN"
            screenJob.diecadviewerContainer.state = "FULL"
        }
    }
    Column{
        id: col2
        y: buttonHeight
        x: button_back.x + button_back.width + 30
        //x: col3.x + col3.width + 30
        width: buttonWidth
        height:parent.height - buttonHeight

        Button{
            //button_currentX_decrease
            id:button_left
            width: parent.width
            height: parent.height  - 8
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-left.png"
            foregroundImagePressed: __imagePath + "arrow-left-w.png"
            __imageNormal: __imagePath +  "EmptyUp.png"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        /*
                        Axis_X_Rotation_Master_dAccelerationMax.value = 4.0;
                        Axis_X_Rotation_Master_dAccelerationMin.value = -4.0;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                        */
						//No fixed values should be used in the HMI, only variables, to make sure machine limits are not exceeded
                        //Axis_Y_Rotation_Master_dAccelerationMax.value = 4.0;
                        //Axis_Y_Rotation_Master_dAccelerationMin.value = -4.0;
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                    } else {
                        //Axis_X_Flatbed_Master_dAccelerationMax.value =  4.0;
                        //Axis_X_Flatbed_Master_dAccelerationMin.value =  -4.0;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  -0.075;
                    }
                    //Axis_X_Master_dMoveSpeedNominal.value = -0.075;
                    //MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
               // if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        /*
                        Axis_X_Rotation_Master_dAccelerationMax.value = Axis_X_Rotation_Master_Limit_dAccelerationMax.value;
                        Axis_X_Rotation_Master_dAccelerationMin.value = Axis_X_Rotation_Master_Limit_dAccelerationMin.value;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0;
                        */
                        Axis_Y_Rotation_Master_dAccelerationMax.value = Axis_Y_Rotation_Master_Limit_dAccelerationMax.value;
                        Axis_Y_Rotation_Master_dAccelerationMin.value = Axis_Y_Rotation_Master_Limit_dAccelerationMin.value;
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;

                    } else {
                        Axis_X_Flatbed_Master_dAccelerationMax.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMax.value;
                        Axis_X_Flatbed_Master_dAccelerationMin.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMin.value;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0;
                    }
                    //Axis_X_Master_dMoveSpeedNominal.value = 0;
                    //MoveMachine_bReduceAcceleration.value = false;
                // }
            }
        }
    }
    Column{
        id: col3
        y: buttonHeight
        x: col2.x + col2.width + 1
        width: buttonWidth
        height:parent.height - buttonHeight
        spacing: colSpacing*2
        Button{
            //button_currentY_increase
            id:button_up
            width: parent.width
            height: buttonHeight
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-up.png"
            foregroundImagePressed: __imagePath + "arrow-up-w.png"
            __imageNormal: __imagePath +  "EmptyUp.png"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        //Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                        //Axis_X_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0.075; // asked by Jon Foster
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0.075;
						//MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
               // if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        //Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0;
						//MoveMachine_bReduceAcceleration.value = false;
               // }
            }
        }
        Button{
            //button_currentY_decrease
            id:button_down
            width: parent.width
            height: buttonHeight
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-down.png"
            foregroundImagePressed: __imagePath + "arrow-down-w.png"
            __imageNormal: __imagePath + "ButtonPressedNoFrame.svg"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        //Axis_Y_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                        //Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = -0.075;// asked by Jon Foster
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = -0.075;
						//MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
               // if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        //Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0;
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0;
						//MoveMachine_bReduceAcceleration.value = false;
              //  }
            }
        }
    }
    Column{
        id:col4
        y: buttonHeight
        x: col3.x + col3.width + 1
        width: buttonWidth
        height:parent.height - buttonHeight
        spacing: colSpacing
        Button{
            //button_currentX_increase
            id:button_right
            width: parent.width
            height: parent.height -8
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-right.png"
            foregroundImagePressed: __imagePath + "arrow-right-w.png"
            __imageNormal: __imagePath +  "EmptyUp.png"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        /*
                        Axis_X_Rotation_Master_dAccelerationMax.value = 4.0;
                        Axis_X_Rotation_Master_dAccelerationMin.value = -4.0;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                        */
						//No fixed values should be used in the HMI, only variables, to make sure machine limits are not exceeded
                        //Axis_Y_Rotation_Master_dAccelerationMax.value = 4.0;
                        //Axis_Y_Rotation_Master_dAccelerationMin.value = -4.0;
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                    } else {
						//No fixed values should be used in the HMI, only variables, to make sure machine limits are not exceeded
                        //Axis_X_Flatbed_Master_dAccelerationMax.value =  4.0;
                        //Axis_X_Flatbed_Master_dAccelerationMin.value =  -4.0;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0.075;
                    }
						//MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
               // if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        /*
                        Axis_X_Rotation_Master_dAccelerationMax.value = Axis_X_Rotation_Master_Limit_dAccelerationMax.value;
                        Axis_X_Rotation_Master_dAccelerationMin.value = Axis_X_Rotation_Master_Limit_dAccelerationMin.value;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0.0;
                        */
                        Axis_Y_Rotation_Master_dAccelerationMax.value = Axis_Y_Rotation_Master_Limit_dAccelerationMax.value;
                        Axis_Y_Rotation_Master_dAccelerationMin.value = Axis_Y_Rotation_Master_Limit_dAccelerationMin.value;
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0.0;
                    } else {
                        Axis_X_Flatbed_Master_dAccelerationMax.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMax.value;
                        Axis_X_Flatbed_Master_dAccelerationMin.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMin.value;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0.0;
                    }
						//MoveMachine_bReduceAcceleration.value = false;
               // }
            }
        }
    }


    Column{
        id:col5
        y: buttonHeight/2
        x: col4.x + col4.width + 30
        //x: button_back.x + button_back.width + 2
        width: colWidth*2
        height:parent.height - buttonHeight
        spacing: 10 //colSpacing
		
		//Only for WoodLifter_bOption = true
            ToggleButtonFlat{
                id: toggle_woodlifter_on_off
                width: colWidth
				height: buttonHeight*1.4
                text: db.dbTr("Wood lifter UP") + db.tr
                stateIO: Machine_Configuration_UseWoodLifter.value ? "WoodLifter_bUp" : ""
                enabled: woodLifterEnabled()
				//SB only display if wood lifter option is true and if bUseFlatbed is true
                visible: Machine_Configuration_UseWoodLifter.value && bUseFlatbed.value
            }
		

			//Cover only used for Rotary machines
			ToggleButtonFlat{
            id: toggle_cover_on_off
            text: db.dbTr("Cover close") + db.tr
            width: colWidth
            height: buttonHeight*1.4
            stateIO: "Rotation_PneumaticCylinder_Cover_Out_bClose"
			//SB only display if bUseRotation is true
			visible: bUseRotation.value
        }


        Row{
            id: row_checkbox_set_as_zero_pos
            spacing: 10
            RadioButton{
                id: checkbox_set_as_zero_pos
                height: 32
                width: 32
                __imagePressed: imagePath + "checked-checkbox-32.png"
                __imageNormal:  imagePath + "unchecked-checkbox-32.png"
               // Wrong IO was used: property string txtIO: "Job_bZeroPositionSet"
				property string txtIO: "bSetActualPositionAsZeroPosition"
                property bool bBox: eval(txtIO).value
                onClicked: {
                    if(__pressed)
                        __pressed = false
                    else
                        __pressed = true
                    eval(txtIO).value = __pressed
                }

                onBBoxChanged: {
                    __pressed = bBox
                }
            }
            Label{
                width:colWidth*2
                text: db.dbTr("Set as Zero Position") + db.tr
                colorChoice: 1
            }
        }

        Row{
            id: row_checkbox_cleartable_afterjob
            spacing: 10
            RadioButton{
                id: checkbox_cleartable_afterjob
                height: 32
                width: 32
                __imagePressed: imagePath + "checked-checkbox-32.png"
                __imageNormal:  imagePath + "unchecked-checkbox-32.png"
                property string txtIO: "ClearTable_bOn"
                property bool bBox: eval(txtIO).value
                onClicked: {
                    if(__pressed)
                        __pressed = false
                    else
                        __pressed = true
                    eval(txtIO).value = __pressed
                }

                onBBoxChanged: {
                    __pressed = bBox
                }
            }
            Label{
                width:colWidth*2
                text: db.dbTr("Clear table after Job") + db.tr
                colorChoice: 1
            }
        }

    }
    Column{
        id: col6
        y: buttonHeight/2
        x: col5.x + colWidth + 30
        width: colWidth
        height:parent.height - buttonHeight
        spacing: colSpacing
    }
    Column{
        id:col7
        y: buttonHeight
        x: col6.x + col6.width + 30
        width: colWidth
        height:parent.height - buttonHeight
        spacing: colSpacing
        Button{
            id:button_goto_position
            width: parent.width
            height: buttonHeight*1.4
            text: db.dbTr("Go to\n position") + db.tr
            __horizontalTextAlignement: Text.AlignHCenter
            __verticalTextAlignement: Text.AlignVCenter
            enabled: MF.isEnabled()
            onClicked: {
                if( !bAbsoluteRelative.value ) { // relative
                    //MoveMachine_bReduceAcceleration.value = true;
                    Axis_X_dDistanceNominal.value = Axis_X_dTargetPosition.value;
                    Axis_Y_dDistanceNominal.value = Axis_Y_dTargetPosition.value;
                    Axis_Z_dDistanceNominal.value = Axis_Z_dTargetPosition.value;
                    MoveMachine_bMove.value = true;
                } else {
                    Axis_X_dDistanceNominal.value = - ( - Axis_X_dTargetPosition.value + Axis_X_dPosition.value);
                    Axis_Y_dDistanceNominal.value = - ( - Axis_Y_dTargetPosition.value + Axis_Y_dPosition.value);
                    Axis_Z_dDistanceNominal.value = - ( - Axis_Z_dTargetPosition.value + Axis_Z_dPosition.value);
                    MoveMachine_bMove.value = true;
                }
            }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_set_zero_pos
            width: parent.width
            height: buttonHeight
            text: db.dbTr("Set zero position") + db.tr
            __horizontalTextAlignement: Text.AlignHCenter
            __verticalTextAlignement: Text.AlignVCenter
            enabled: MF.isEnabled()

            onClicked: {
                setZeroPosition()
            }
            onReleased: { __isPressed = false }
        }
    }
    Column{
        id:col8
        y: 4
        x: col7.x + col7.width + 30
        width: colWidth
        height:parent.height
        spacing: 6

        property bool bAbsRel: bAbsoluteRelative.value
        onBAbsRelChanged: {
            if(bAbsoluteRelative.value)
            {
                Axis_X_dTargetPosition.value = Axis_X_dPosition.value
                Axis_Y_dTargetPosition.value = Axis_Y_dPosition.value
                Axis_Z_dTargetPosition.value = ClearTable_Position_dZ.value
            }
            else
            {
                Axis_X_dTargetPosition.value = 0
                Axis_Y_dTargetPosition.value = 0
                Axis_Z_dTargetPosition.value = 0
            }
        }

        Button{
            id: button_absolute_relative
            width: parent.width
            height: 43
            fontSize: 12
            text: bAbsoluteRelative.value ? db.dbTr("Absolute pos") + db.tr :  db.dbTr("Relative pos") + db.tr
            onClicked: bAbsoluteRelative.value = !(bAbsoluteRelative.value) //testAbs = !testAbs
        }

        TextField{
            id:textfield_target_X
            width: parent.width
            height: 43
            readOnly: false
            isEnabled: true
            horizontalTextAlignement: 2
            textIO: "Axis_X_dTargetPosition"
            unitEnable: true
            unit: strUnitMM.value
            ioFactor: dMToMM.value
            digitsAfterComma: bUseInches.value ? 3 : 2

        }
        TextField{
            id:textfield_target_Y
            width: parent.width
            height: 43
            readOnly: false
            unitEnable: true
            horizontalTextAlignement: 2
            textIO: "Axis_Y_dTargetPosition"
            unit: strUnitMM.value
            ioFactor: dMToMM.value
            digitsAfterComma: bUseInches.value ? 3 : 2
        }
        TextField{
            id:textfield_target_Z
            width: parent.width
            height: 43
            horizontalTextAlignement: 2
            textIO: "Axis_Z_dTargetPosition"
            unitEnable: true
            unit: strUnitMM.value
            ioFactor: dMToMM.value
            readOnly: false
            isEnabled: true
            digitsAfterComma: bUseInches.value ? 3 : 2
        }
    }
}

