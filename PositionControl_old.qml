//build date:2018-10-24
import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
import "MainFunctions.js" as MF

Item {
    id: positionControl
    objectName: "positionControl"

    property string __style: S.styleName //mainrect.rootStyle
    property string __imagePath: S.Style[__style].imagePath

    width: 900
    height: 198

    property bool testAbs: true

    //visible: false

    Rectangle{
        height: parent.height
        width: parent.width
        // color: "white"
        color: S.Style[__style].backgroundColor
    }


    property bool isJobStart: Job_bStart.value
    property bool isFullScreen: screenJob.state === "1"
    property bool isJob_bFinished: Job_bFinished.value




    function setZeroPosition()
    {
        Job_Offset_dX.value = Axis_X_dPosition.value + 0.000001;
        Job_Offset_dY.value = Axis_Y_dPosition.value + 0.000001;
        Job_bZeroPositionSet.value = true;
    }



    function openSaveZeroPosFileDlg()
    {
        var path = Job_Offset_strNullpointLoadPath.value
        path = path.replace(/\\/g, "/");
        var dlg = dialogFileChooser

        dlg.titleText =         db.dbTr("Select zeroposition file") + db.tr
        dlg.buttonOkText =      db.dbTr("Save") + db.tr
        dlg.fileChooserText =   db.dbTr("Save File") + db.tr
        dlg.fileExtension = "zeropostion"
        dlg.type = "SAVEZEROPOSITION"
        dlg.currentPath = path
        var fullName = Job_Offset_strFile.value

        dlg.currentFileName = fullName.substring(fullName.lastIndexOf('/') + 1)
        //backward compatibility
        //dlg.currentFileName = dlg.currentFileName.substring(dlg.currentFileName.lastIndexOf('\\') + 1)

        dlg.initializeDir()

        dlg.open()
    }



    function openLoadZeroPosFileDlg()
    {
        var path = Job_Offset_strNullpointLoadPath.value
        path = path.replace(/\\/g, "/");
        var dlg = dialogFileChooser

        dlg.titleText =         db.dbTr("Select zero position file") + db.tr
        dlg.buttonOkText =      db.dbTr("Load") + db.tr
        dlg.fileChooserText =   db.dbTr("Load File") + db.tr
        dlg.fileExtension = "zeropostion"
        dlg.currentPath = path
        dlg.type = "LOADZEROPOSITION"
        var fullName = Job_Offset_strFile.value

        dlg.currentFileName = fullName.substring(fullName.lastIndexOf('/') + 1)
        //backward compatibility
        //dlg.currentFileName = dlg.currentFileName.substring(dlg.currentFileName.lastIndexOf('\\') + 1)

        dlg.initializeDir()

        dlg.open()
    }



    property int colWidth: 140
    property int colSpacing: 20
    property int buttonHeight: 50

    property int buttonWidth: 50


    Button{
        id:button_back
        x: 2
        width: 30
        height:parent.height
        foregroundImageNormal:  __imagePath + "arrow-06-w.png"
        foregroundImagePressed: __imagePath + "arrow-06.png"
        __imageNormal: __imagePath +  "EmptyDown.png"
        onClicked: {
            positionControl.state       = "HIDDEN"
            screenJob.diecadviewerContainer.state = "FULL"
        }
    }
    Column{
        id: col2
        y: buttonHeight
        x: button_back.x + button_back.width + 30
        //x: col3.x + col3.width + 30
        width: buttonWidth
        height:parent.height - buttonHeight

        Button{
            //button_currentX_decrease
            id:button_left
            width: parent.width
            height: parent.height  - 8
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-left.png"
            foregroundImagePressed: __imagePath + "arrow-left-w.png"
            __imageNormal: __imagePath +  "EmptyUp.png"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        Axis_X_Rotation_Master_dAccelerationMax.value = 4.0;
                        Axis_X_Rotation_Master_dAccelerationMin.value = -4.0;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                    } else {
                        Axis_X_Flatbed_Master_dAccelerationMax.value =  4.0;
                        Axis_X_Flatbed_Master_dAccelerationMin.value =  -4.0;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  -0.075;
                    }
                    //Axis_X_Master_dMoveSpeedNominal.value = -0.075;
                    MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
                if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        Axis_X_Rotation_Master_dAccelerationMax.value = Axis_X_Rotation_Master_Limit_dAccelerationMax.value;
                        Axis_X_Rotation_Master_dAccelerationMin.value = Axis_X_Rotation_Master_Limit_dAccelerationMin.value;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0;
                    } else {
                        Axis_X_Flatbed_Master_dAccelerationMax.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMax.value;
                        Axis_X_Flatbed_Master_dAccelerationMin.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMin.value;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0;
                    }
                    //Axis_X_Master_dMoveSpeedNominal.value = 0;
                    MoveMachine_bReduceAcceleration.value = false;
                }
            }
        }
    }
    Column{
        id: col3
        y: buttonHeight
        x: col2.x + col2.width + 1
        width: buttonWidth
        height:parent.height - buttonHeight
        spacing: colSpacing*2
        Button{
            //button_currentY_increase
            id:button_up
            width: parent.width
            height: buttonHeight
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-up.png"
            foregroundImagePressed: __imagePath + "arrow-up-w.png"
            __imageNormal: __imagePath +  "EmptyUp.png"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0.075;

                    MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
                if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0;

                    MoveMachine_bReduceAcceleration.value = false;
                }
            }
        }
        Button{
            //button_currentY_decrease
            id:button_down
            width: parent.width
            height: buttonHeight
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-down.png"
            foregroundImagePressed: __imagePath + "arrow-down-w.png"
            __imageNormal: __imagePath + "ButtonPressedNoFrame.svg"
            onPressed: {
                if(Machine_bBusy.value === false){
                   if(bUseRotation.value)
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = -0.075;
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = -0.075;

                    MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
                if(Machine_bBusy.value === false){
                    if(bUseRotation.value)
                        Axis_Y_Rotation_Master_dMoveSpeedNominal.value = 0;
                    else
                        Axis_Y_Flatbed_Master_dMoveSpeedNominal.value = 0;

                    MoveMachine_bReduceAcceleration.value = false;
                }
            }
        }
    }
    Column{
        id:col4
        y: buttonHeight
        x: col3.x + col3.width + 1
        width: buttonWidth
        height:parent.height - buttonHeight
        spacing: colSpacing
        Button{
            //button_currentX_increase
            id:button_right
            width: parent.width
            height: parent.height -8
            //enabled: MF.isEnabled()
            foregroundImageNormal:  __imagePath + "arrow-right.png"
            foregroundImagePressed: __imagePath + "arrow-right-w.png"
            __imageNormal: __imagePath +  "EmptyUp.png"
            onPressed: {
                if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        Axis_X_Rotation_Master_dAccelerationMax.value = 4.0;
                        Axis_X_Rotation_Master_dAccelerationMin.value = -4.0;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0.075;
                    } else {
                        Axis_X_Flatbed_Master_dAccelerationMax.value =  4.0;
                        Axis_X_Flatbed_Master_dAccelerationMin.value =  -4.0;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0.075;
                    }
                    MoveMachine_bReduceAcceleration.value = true;
                }
            }
            onReleased: {
                if(Machine_bBusy.value === false){
                    if (bUseRotation.value) {
                        Axis_X_Rotation_Master_dAccelerationMax.value = Axis_X_Rotation_Master_Limit_dAccelerationMax.value;
                        Axis_X_Rotation_Master_dAccelerationMin.value = Axis_X_Rotation_Master_Limit_dAccelerationMin.value;
                        Axis_X_Rotation_Master_dMoveSpeedNominal.value = 0.0;
                    } else {
                        Axis_X_Flatbed_Master_dAccelerationMax.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMax.value;
                        Axis_X_Flatbed_Master_dAccelerationMin.value =  Axis_X_Flatbed_Master_Limit_dAccelerationMin.value;
                        Axis_X_Flatbed_Master_dMoveSpeedNominal.value =  0.0;
                    }
                    MoveMachine_bReduceAcceleration.value = false;
                }
            }
        }
    }


    Column{
        id:col5
        y: buttonHeight
        x: col4.x + col4.width + 30
        //x: button_back.x + button_back.width + 2
        width: colWidth*3 + 60
        height:parent.height - buttonHeight
        spacing: 10 //colSpacing

        Button{
            id:button_goto_position
            width: parent.width
            height: buttonHeight
            text: db.dbTr("Go to position") + db.tr
            __horizontalTextAlignement: Text.AlignHCenter
            __verticalTextAlignement: Text.AlignVCenter
            enabled: MF.isEnabled()
            onClicked: MF.gotoPosition(!bAbsoluteRelative.value)
            onReleased: { __isPressed = false }
        }


        Row{
            id: row_checkbox_set_as_zero_pos
            spacing: 10
            RadioButton{
                id: checkbox_set_as_zero_pos
                height: 32
                width: 32
                __imagePressed: imagePath + "checked-checkbox-32.png"
                __imageNormal:  imagePath + "unchecked-checkbox-32.png"
                property string txtIO: "Job_bZeroPositionSet"
                property bool bBox: eval(txtIO).value
                onClicked: {
                    if(__pressed)
                        __pressed = false
                    else
                        __pressed = true
                    eval(txtIO).value = __pressed
                }

                onBBoxChanged: {
                    __pressed = bBox
                }
            }
            Label{
                width:colWidth*2
                text: db.dbTr("Set as Zero Position") + db.tr
                colorChoice: 1
            }
        }

        Row{
            id: row_checkbox_cleartable_afterjob
            spacing: 10
            RadioButton{
                id: checkbox_cleartable_afterjob
                height: 32
                width: 32
                __imagePressed: imagePath + "checked-checkbox-32.png"
                __imageNormal:  imagePath + "unchecked-checkbox-32.png"
                property string txtIO: "ClearTable_bOn"
                property bool bBox: eval(txtIO).value
                onClicked: {
                    if(__pressed)
                        __pressed = false
                    else
                        __pressed = true
                    eval(txtIO).value = __pressed
                }

                onBBoxChanged: {
                    __pressed = bBox
                }
            }
            Label{
                width:colWidth*2
                text: db.dbTr("Clear table after Job") + db.tr
                colorChoice: 1
            }
        }

    }


    Column{
        id:col8
        y: 4
        x: col5.x + col5.width + 30
        width: colWidth
        height:parent.height
        spacing: 6

        property bool bAbsRel: bAbsoluteRelative.value
        onBAbsRelChanged: {
            if(bAbsoluteRelative.value)
            {
                Axis_X_dTargetPosition.value = Axis_X_dPosition.value
                Axis_Y_dTargetPosition.value = Axis_Y_dPosition.value
                Axis_Z_dTargetPosition.value = ClearTable_Position_dZ.value
            }
            else
            {
                Axis_X_dTargetPosition.value = 0
                Axis_Y_dTargetPosition.value = 0
                Axis_Z_dTargetPosition.value = 0
            }
        }

        Button{
            id: button_absolute_relative
            width: parent.width
            height: 43
            fontSize: 12
            text: bAbsoluteRelative.value ? db.dbTr("Absolute pos") + db.tr :  db.dbTr("Relative pos") + db.tr
            onClicked: {
                bAbsoluteRelative.value = !(bAbsoluteRelative.value) //testAbs = !testAbs

            }
        }

        TextField{
            id:textfield_target_X
            width: parent.width
            height: 43
            readOnly: false
            isEnabled: true
            horizontalTextAlignement: 2
            textIO: "Axis_X_dTargetPosition"
            unitEnable: true
            unit: strUnitMM.value
            ioFactor: dMToMM.value

        }
        TextField{
            id:textfield_target_Y
            width: parent.width
            height: 43
            readOnly: false
            unitEnable: true
            horizontalTextAlignement: 2
            textIO: "Axis_Y_dTargetPosition"
            unit: strUnitMM.value
            ioFactor: dMToMM.value
        }
        TextField{
            id:textfield_target_Z
            width: parent.width
            height: 43
            horizontalTextAlignement: 2
            textIO: "Axis_Z_dTargetPosition"
            unitEnable: true
            unit: strUnitMM.value
            ioFactor: dMToMM.value
            readOnly: false
            isEnabled: true
        }
    }
}

