import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "MainFunctions.js" as MF
Item {
    id:screenCombo
    width: 987
    height: 651

    property string styleName: S.styleName

    property bool isFlatBed: bUseFlatbed.value
    property bool isRotationUsed: bUseRotation.value


   FlipFlopButton{
       id: rotation_flatbed
       x: 300
       y: 300
       left_text: db.dbTr("Rotation") + db.tr
       right_text: db.dbTr("Flatbed") + db.tr
       stateIO: "bUseRotation"
   }

   onIsRotationUsedChanged: {
       MF.toggleCombo()
       if(isRotationUsed){
           MF.toRotation()
       }else{
           MF.toFlatbed()
       }
   }
}

