//Build date: 2019-06-21
//2019-06-21 Changed IO for work-height display, so that the current value is displayed instead of the theoretical value, SB
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0
//import "customerSettings.js" as CS
import "customerColors.js" as CC

Item {
    id: screenJob1
    width: 1216
    height: 1000
    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    property alias diecadviewerContainer: diecadviewerContainer

    property string colorJobBackground: "#FFFFFF"
    property string colorJobProcessed: "#000000"
    property string colorJobText: "#000000"

    property string viewerName: "diecadviewer"

    property bool isJobPart1: Job_Part_1_bExists.value
    property bool isJobPart2: Job_Part_2_bExists.value
    property bool isJobPart3: Job_Part_3_bExists.value
    property bool isJobPart4: Job_Part_4_bExists.value

    property int iNumOfJobParts: 0

    onIsJobPart1Changed: numOfJobParts()
    onIsJobPart2Changed: numOfJobParts()
    onIsJobPart3Changed: numOfJobParts()
    onIsJobPart4Changed: numOfJobParts()

    Component.onCompleted: CC.customerColors()

    function numOfJobParts()
    {
        iNumOfJobParts = 0;
        if(Job_Part_1_bExists.value) ++iNumOfJobParts;
        if(Job_Part_2_bExists.value) ++iNumOfJobParts;
        if(Job_Part_3_bExists.value) ++iNumOfJobParts;
        if(Job_Part_4_bExists.value) ++iNumOfJobParts;
    }


    function engravingToolNumber()
    {
        for(var i = 1; i < 21; ++i)
        {
            var bEngravingTool = eval("Material_Tool_" + i + "_bEngravingTool").value
            if(bEngravingTool) return i;
        }
        return -1;
    }

    Rectangle{
        x: 925
        width: 340
        height: 400
        color: S.Style[S.styleName].backgroundColor
        Column{
            x: 20
            y: 20
            height: parent.height
            spacing:6
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_Thickness
                    width: 150
                    height: parent.height
                    text: db.dbTr("Thickness") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName

                }

                TextField {
                    id: text_field_Thickness
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled:  false
                    textIO: "Material_dThickness"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                }
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_Program
                    width: 150
                    height: parent.height
                    text: db.dbTr("Program") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2

                    readOnly: true
                    __styleName: styleName
                }
                Row{
                    height:parent.height
                    TextField {
                        id: text_field_currentPart
                        width: 50
                        height: parent.height
                        fontPlaceholderColorChoice: 2
                        fontColorChoice: 2
                        horizontalTextAlignement: 2
                        digitsAfterComma: 0
                        isEnabled: true
                        textIO: "Job_InOut_iCurrentPart"

                        __styleName: styleName
                    }
                    Label{
                        height:parent.height
                        width:50
                        text:db.dbTr("of") + db.tr
                        colorChoice: 1
                        horizontalTextAlignement: Qt.AlignHCenter
                        __verticalTextAlignement: Qt.AlignVCenter
                    }

                    TextField {
                        id: text_field_numOfParts
                        width: 50
                        height: parent.height
                        fontPlaceholderColorChoice: 2
                        fontColorChoice: 2
                        horizontalTextAlignement: 2
                        digitsAfterComma: 0
                        readOnly: true
                        text: iNumOfJobParts.toString()
                        __styleName: styleName
                    }
                }
            }

            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_jobline
                    width: 150
                    height: parent.height
                    text: db.dbTr("Line") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_line_in_job
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    digitsAfterComma: 0
                    textIO: "Job_iLine"
                    unitEnable: false
                    __styleName: styleName
                    validator: IntValidator{}
                }
            }

            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_tool
                    width: 150
                    height: parent.height
                    text: db.dbTr("Tool") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_tool
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    digitsAfterComma: 0
                    isEnabled: false
                    textIO: "Job_iTool"
                    unitEnable: false
                    __styleName: styleName
                }
            }
            Row{
                height: 43
                spacing:6
                TextField {
                    id: label_current_height
                    width: 150
                    height: parent.height
                    text: db.dbTr("Height") + db.tr
                    fontSize: 16
                    opacity: 1
                    fontPlaceholderColorChoice: 2
                    readOnly: true
                    __styleName: styleName
                }

                TextField {
                    id: text_field_current_height
                    width: 150
                    height: parent.height
                    fontPlaceholderColorChoice: 2
                    fontColorChoice: 2
                    horizontalTextAlignement: 2
                    isEnabled: false
					//Changed this to the actual Inc_HeightSensor value, so that the real value instead of the theoretical value is displayed (including Z override)
                    //textIO: bUseRotation.value ? "Z_Adjust_Rotation_Spg_dHeightAboveMaterial" : "Z_Adjust_Flatbed_Spg_dHeightAboveMaterial"
					textIO: "Inc_HeightSensor"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    __styleName: styleName
                }
            }
        }
    }

/*
    property string jobFileName:  Job_strFile.value

    onJobFileNameChanged:{
        diecadviewer.filename = "files:" + jobFileName
    }
*/

    property bool load_jobIO: Job_In_bLoaded.value
    //this is not needed if Plc_main will create the /opt/v8/application/machine/simulator.acc file
    //the 20170317 version doesn't create the file
    onLoad_jobIOChanged:{
        if( Job_In_bLoaded.value === false ) {
            diecadviewer.refresh()
        }
    }


    //signal zeroPositionChanged()

    Rectangle{
        id: diecadviewerContainer
        x: 20
        y: 140
        width:905
        height: 620
        color: colorJobBackground
        clip: true
        //state: "FULL"
        state: positionControl.state === "HIDDEN" ? "FULL" : "REDUCED"
        onHeightChanged: {
            diecadviewer.height = height
            diecadviewer.reDraw()
        }

        Connections{
            target:mainrect
            onPositionControlStateChanged:{
                //console.log("onPositionControlStateChanged ---- ", pState)
                diecadviewerContainer.state = pState === "SHOWED" ? "REDUCED" : "FULL"
            }
        }

        Connections{
            target: diecadviewer
            onZeroPositionChanged:{
                //console.log("--------Connections -- onZeroPositionChanged: -----------------")
                Job_Offset_dX.value = diecadviewer.zeroPosition_Out.x
                Job_Offset_dY.value = diecadviewer.zeroPosition_Out.y
            }
        }


        DieCadViewer  {
            id: diecadviewer
            filename: "files:Z:/opt/v8/application/machine/LoadedJob.acc"
            //filename: "files:Z:/opt/v8/application/machine/simulator.acc"
            //filename: "files:Z:/opt/v8/application/machine/Bender.acc"
            //filename:"files:" + Job_strFile.value
            anchors.fill: parent
            toolIndex: 0
            toolColor: "#ffffff"
            processedColor: colorJobProcessed
            textColor: colorJobText
            backgroundColor: colorJobBackground
            height:parent.height

            engravingTool: engravingToolNumber()
            isJobMoveAllowed: !Job_bStart.value

            property double zpX: Job_Offset_dX.value
            property double zpY: Job_Offset_dY.value
            property double mpX: Axis_X_dPosition.value
            property double mpY: Axis_Y_dPosition.value
            property bool bShow: Machine_bShowMachineSize.value



            //property double macW: bUseRotation.value  ? (Machine_Configuration_Rotation_Limit_Position_X_Max.value - Machine_Configuration_Rotation_Limit_Position_X_Min.value) : (Machine_Configuration_Flatbed_Limit_Position_X_Max.value - Machine_Configuration_Flatbed_Limit_Position_X_Min.value)
            //property double macH: bUseRotation.value  ? (Machine_Configuration_Rotation_Limit_Position_Y_Max.value - Machine_Configuration_Rotation_Limit_Position_Y_Min.value) : (Machine_Configuration_Flatbed_Limit_Position_Y_Max.value - Machine_Configuration_Flatbed_Limit_Position_Y_Min.value)

			//These values are used to show the red rectangle in the Diecad viewer.
			property double macW: Job_Range_dMaxX.value
			property double macH: Job_Range_dMaxY.value
            //property double macW: bUseRotation.value  ? Machine_Configuration_Axis_X_Rotation_Range_Setup_dMax.value : Machine_Configuration_Axis_X_Flatbed_Range_Setup_dMax.value
            //property double macH: bUseRotation.value  ? Machine_Configuration_Axis_Y_Rotation_Range_Setup_dMax.value : Machine_Configuration_Axis_Y_Flatbed_Range_Setup_dMax.value

            onMpXChanged: machinePosition = Qt.point(Axis_X_dPosition.value, Axis_Y_dPosition.value)
            onMpYChanged: machinePosition = Qt.point(Axis_X_dPosition.value, Axis_Y_dPosition.value)

            onBShowChanged:  showMachine = Machine_bShowMachineSize.value

            onMacWChanged: machineSize = Qt.size(macW, macH)
            onMacHChanged: machineSize = Qt.size(macW, macH)

            onZpXChanged: zeroPosition_In = Qt.point(Job_Offset_dX.value, Job_Offset_dY.value)
            onZpYChanged: zeroPosition_In = Qt.point(Job_Offset_dX.value, Job_Offset_dY.value)

            showMachine: Machine_bShowMachineSize.value
            isRotationUsed:  bUseRotation.value
            machineSize: Qt.size(macW, macH)
            zeroPosition_In: Qt.point(Job_Offset_dX.value, Job_Offset_dY.value)

            machinePosition:Qt.point(Axis_X_dPosition.value, Axis_Y_dPosition.value)

            fileErrorText: db.dbTr("ERROR\nFILE NOT FOUND\nOR NOT SUPPORTED FORMAT") + db.tr
            actualElem: text_field_line_in_job.__textIO.value

            //needs diecadviewer 2.0.5.0
            //movesDimension: strUnitM.value
            //movesFactor: dMMToM.value
            movesDimension: strUnitMM.value
            movesFactor: dAccUnit.value
            isFileCheckedRegularly: false

            onFileLoaded:
            {
                //zeroPosition_In = Qt.point(Job_Offset_dX.value, Job_Offset_dY.value)
                zeroPosition_In = Qt.point(0, 0)
                Job_Offset_dX.value = 0
                Job_Offset_dY.value = 0

//                Job_dMaxX.value = maxX
//                Job_dMaxY.value = maxY
//                Job_dMinX.value = minX
//                Job_dMinY.value = minY

                engravingTool = engravingToolNumber()
                zoomAll()
                if( diecadviewer.isEngraving && Machine_Configuration_bAutoEngravingOptimization.value)
                {
                    Service_Convert_bStart.value = true
                }
                //console.log("macW", macW , "macH", macH)
            }

        }


        states: [
            State {
                name: "FULL"
            },
            State {
                name: "REDUCED"
            }
        ]
        transitions: [
            Transition {
                from: "FULL"
                to: "REDUCED"
                NumberAnimation {target:toolbarJob; properties: "y"; from: 770; to: 570 ; easing.type: Easing.InCirc }
                NumberAnimation {target:diecadviewerContainer; properties: "height"; from: 620; to: 420 ; easing.type: Easing.InCirc }
            },
            Transition {
                from: "REDUCED"
                to: "FULL"
                NumberAnimation {target:diecadviewerContainer; properties: "height"; from: 420; to: 620 ; easing.type: Easing.InCirc}
                NumberAnimation {target:toolbarJob; properties: "y"; from: 570; to: 770 ; easing.type: Easing.InCirc }
            }
        ]
    }



    ToggleButton {
        id: button_showTool0
        x: 20
        y: 178
        width: 32
        height: 32
        foregroundImageNormal: imagePath + "Show_Emptymoves_32.png"
        foregroundImagePressed: imagePath + "Show_Emptymoves_32.png"

        __lightImageOff: ""
        __lightImageOn: ""

        onClicked:{
            diecadviewer.toolIndex = 0
            diecadviewer.toolShow = __pressed
            //diecadviewer.filename = "files:Z:/opt/v8/application/machine/simulator.acc"
        }
    }


    ToggleButton {
        id: button_arrowsShow
        x: 20
        y: 216
        width: 32
        height: 32
        foregroundImageNormal: imagePath + "Show_Directions_32.png"
        foregroundImagePressed: imagePath + "Show_Directions_32.png"

        __lightImageOff: ""
        __lightImageOn: ""

        onClicked:{
            //directionShow = __pressed
            diecadviewer.directionShow = __pressed
        }
    }

    ToggleButton {
        id: button_machineShow
        x: 20
        y: 252
        width: 32
        height: 32
        foregroundImageNormal: imagePath + "machine.png"
        foregroundImagePressed: imagePath + "machine.png"
        stateIO: "Machine_bShowMachineSize"
        __lightImageOff: ""
        __lightImageOn: ""

        onClicked:{
            diecadviewer.showMachine = __pressed
        }
    }


    ToolbarJob{
        id: toolbarJob
        width:parent.width - 340 //position_component.width
    }

}

