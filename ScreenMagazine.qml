//buld date: 2018-10-25
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    id:screenMagazine
    width: 1216
    height: 800

    VisualItemModel{
        id: magazinepages
        ScreenMagazine1{}
        ScreenMagazine2{}
    }

    VisualItemModel{
        id: emptypages
    }

    ListView {
        id: magazineflickableLV
        anchors.fill: parent
        focus: true
        clip: true
        interactive: false
        highlightRangeMode: ListView.StrictlyEnforceRange
        orientation: ListView.Vertical
        snapMode: ListView.SnapOneItem
        cacheBuffer: 2000
        model: bEnableMilling.value === true ? magazinepages : emptypages
        flickDeceleration: 200
        highlightMoveDuration:200
        onCurrentItemChanged: { parent.state = currentIndex }
        ListViewScroller{
            owner:magazineflickableLV
            orientation: magazineflickableLV.orientation
            currentIndex: magazineflickableLV.currentIndex
            count: magazineflickableLV.count
        }
     }


    states: [
        State {
            name: "0"
            PropertyChanges {
                target: toolchange_widget
                visible: true
            }
        },
        State {
            name: "1"
            PropertyChanges {
                target: toolchange_widget
                visible: true
            }
        }
    ]

}

