//Build date: 2019-03-13
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    id:screenMagazine1
    width: 1216
    height: 1000


    Item{
        id:magazineGrid
        x:20
        y:136

        state: toolchange_widget.state === "AUTOTOOLCHANGE" ? "AUTOTOOLCHANGE" : "MANUALTOOLCHANGE"

        Connections{
            target:mainrect
            onToolChangeWidgetStateChanged:{
                state = pState
            }
        }


        states:[
            State{
                name:"AUTOTOOLCHANGE"
            },
            State{
                name:"MANUALTOOLCHANGE"
            }
        ]

        transitions: [
            Transition {
                from: "AUTOTOOLCHANGE"
                to: "MANUALTOOLCHANGE"
                NumberAnimation {target:magazineGrid; properties: "y"; from: 136; to: 236 ; easing.type: Easing.InCirc }
            },
            Transition {
                from: "MANUALTOOLCHANGE"
                to: "AUTOTOOLCHANGE"
                NumberAnimation {target:magazineGrid; properties: "y"; from: 236; to: 136 ; easing.type: Easing.OutCirc}
            }
        ]

        Label{
            x: 0
            text: db.dbTr("Position") + db.tr
            colorChoice: 1
        }
        Label{
            x: 90
            text: db.dbTr("Tool") + db.tr
            colorChoice: 1
        }
        Label{
            x: 180
            text: db.dbTr("Name") + db.tr
            colorChoice: 1
        }
        Label{
            x: 560//380
            text: db.dbTr("Lifetime") + db.tr
            colorChoice: 1
        }

/*
        Label{
            x: 620
            text: db.dbTr("Position") + db.tr
            colorChoice: 1
        }
        Label{
            x: 620+80
            text: db.dbTr("Tool") + db.tr
            colorChoice: 1
        }
        Label{
            x: 620+180
            text: db.dbTr("Name") + db.tr
            colorChoice: 1
        }
        Label{
            x: 620+380
            text: db.dbTr("Lifetime") + db.tr
            colorChoice: 1
        }
*/


        VisualItemModel {
            id: magazineItems

            MagazineItem { hopper_num: "1" }
            MagazineItem { hopper_num: "2" }
            MagazineItem { hopper_num: "3" }
            MagazineItem { hopper_num: "4" }
            MagazineItem { hopper_num: "5" }
            MagazineItem { hopper_num: "6" }
            /*
            MagazineItem { hopper_num: "7" }
            MagazineItem { hopper_num: "8" }
            MagazineItem { hopper_num: "9" }
            MagazineItem { hopper_num: "10" }
            MagazineItem { hopper_num: "11" }
            MagazineItem { hopper_num: "12" }
            MagazineItem { hopper_num: "13" }
            MagazineItem { hopper_num: "14" }
            MagazineItem { hopper_num: "15" }
            MagazineItem { hopper_num: "16" }
            MagazineItem { hopper_num: "17" }
            MagazineItem { hopper_num: "18" }
            MagazineItem { hopper_num: "19" }
            MagazineItem { hopper_num: "20" }
            */
        }


        GridView {
            id: list_view1
            y: 30
            width: 1240
            height: 600
            cellWidth: 800 //620
            cellHeight: 60
            flow: GridView.TopToBottom

            snapMode: ListView.SnapOneItem
            model: magazineItems
            interactive: false
            delegate: Item {}
        }
    }
}

