import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    width: 1216
    height: 1000

    GroupBox {
        id: group_magazine
        x: 38
        y: 8
        width: 895
        height: 620
        colorChoice: 0
        headerWidth: 160
        headerText: db.dbTr("magazin position") + db.tr
        Item{
            x:0
            y:54

            width: 895
            height: 511
            clip: true

            VisualItemModel {
                id: magazineItems

                MagazineItem { hopper_num: "1" }
                MagazineItem { hopper_num: "2" }
                MagazineItem { hopper_num: "3" }
                MagazineItem { hopper_num: "4" }
                MagazineItem { hopper_num: "5" }
                MagazineItem { hopper_num: "6" }

            }


            GridView {
                id: list_view1
                x: 0
                y: 0
                width: 351
                height: 500
                cellWidth: 400
                cellHeight: 50
                flow: GridView.TopToBottom

                snapMode: ListView.SnapOneItem
                model: magazineItems
                interactive: false
                delegate: Item {}

            }
        }
    }

    MultilineTextField {
        id: label_requested_tool
        x: 38
        y: 598
        width: 160
        height: 43
        text: db.dbTr("requested tool") + db.tr
        readOnly: true
        isEnabled: false
        fontSize: 10
    }

    TextField {
        id: text_field_requested_tool
        x: 203
        y: 598
        width: 111
        height: 43
        text: "0"
        readOnly: false
        isEnabled: true
        fontSize: 14
        digitsAfterComma: 0
        horizontalTextAlignement: 2
        unitEnable: false
        //textIO: "ToolChange_Milling_New_iTool"
        validator: IntValidator {}
    }

}

