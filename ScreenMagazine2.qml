//build date:2019-05-24
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0


Item {
    width: 1215
    height: 1000


    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    Item{
        y: 200
        ComponentPositionMagazine {
            id: componentpositionsetup1
            x: 20
            y: 0
        }

        Override{
            x: 660
            y: 180
            labelWidth: 128
        }

        GroupBox {
            id: teach_position
            x: 660
            y: 273
            width: 306
            height: 272
            headerText: db.dbTr("teach position") + db.tr
            headerWidth: 348
            headerVisible: true
            colorChoice: 3
            fontSize: 14

            TextField {
                id: label_x
                x: 0
                y: 28
                width: 43
                height: 43
                text: bUseRotation ? "A" : "X"
                horizontalTextAlignement: 4
                fontSize: 16
                unit: ""
                textIO: ""
                readOnly: true


            }

            ELNumericInput {
                id: numeric_input_Hopper_Position_dTeachX
                x: 48
                y: 28
                width: 258
                height: 43
                initialValue: 1
                maxValue: 2.2
                rangeVisible: false
                selectByMouse: true
                unitEnable: true
                valueIO: "Hopper_Position_dTeachX"
                ioFactor: dMToMM.value
                unit: strUnitMM.value
                textFieldEnabled: false
                //numValidator: DoubleValidator{top:2.2}

            }

            TextField {
                id: label_Y
                x: 0
                y: 79
                width: 43
                height: 43
                text: "Y"
                horizontalTextAlignement: 4
                fontSize: 16
                readOnly: true

            }

            ELNumericInput {
                id: numeric_input_Hopper_Position_dTeachY
                x: 48
                y: 79
                width: 258
                height: 43
                rangeVisible: false
                valueIO: "Hopper_Position_dTeachY"
                initialValue: 1
                unitEnable: true
                ioFactor: dMToMM.value
                unit: strUnitMM.value
                //maxValue: 1.3
                selectByMouse: true
                textFieldEnabled: false
                //numValidator: DoubleValidator{top:1.3}

            }

            TextField {
                id: label_Z
                x: 0
                y: 130
                width: 43
                height: 43
                text: "Z"
                horizontalTextAlignement: 4
                fontSize: 16
                readOnly: true

            }

            ELNumericInput {
                id: numeric_input_Hopper_Position_dTeachZ
                x: 48
                y: 130
                width: 258
                height: 43
                rangeVisible: false
                valueIO: "Hopper_Position_dTeachZ"
                ioFactor: dMToMM.value
                unitEnable: true
                initialValue: 1
                unit: strUnitMM.value
                selectByMouse: true
                maxValue: 0.2
                textFieldEnabled: false
                //numValidator: DoubleValidator{top:0.2}
            }

            TextField {
                id: label_Place
                x: 0
                y: 181
                width: 86
                height: 43
                text: db.dbTr("Place:") + db.tr
                readOnly: true

            }

            ELNumericInput {
                id: numeric_input_Hopper_Position_iTeachPlace
                x: 92
                y: 181
                width: 214
                height: 43
                digitsAfterComma: 0
                minValue: 0
                rangeVisible: false
                valueIO: "Hopper_Position_iTeachPlace"
                initialValue: 0
                unitEnable: false
                ioFactor: 1
                unit: ""
                maxValue: 6
                selectByMouse: true
                numValidator: IntValidator{top:6; bottom: 1}
            }

            property int hopperPosition: Hopper_Position_iTeachPlace.value

            onHopperPositionChanged: {
                switch(Hopper_Position_iTeachPlace.value){
                case 0:{
                    Hopper_Position_dTeachX.value = Hopper_Place0_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place0_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place0_Position_dZ.value
                    break
                }
                case 1:{
                    Hopper_Position_dTeachX.value = Hopper_Place1_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place1_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place1_Position_dZ.value
                    break
                }
                case 2:{
                    Hopper_Position_dTeachX.value = Hopper_Place2_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place2_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place2_Position_dZ.value
                    break
                }
                case 3:{
                    Hopper_Position_dTeachX.value = Hopper_Place3_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place3_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place3_Position_dZ.value
                    break
                }
                case 4:{
                    Hopper_Position_dTeachX.value = Hopper_Place4_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place4_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place4_Position_dZ.value
                    break
                }
                case 5:{
                    Hopper_Position_dTeachX.value = Hopper_Place5_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place5_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place5_Position_dZ.value
                    break
                }
                case 6:{
                    Hopper_Position_dTeachX.value = Hopper_Place6_Position_dX.value
                    Hopper_Position_dTeachY.value = Hopper_Place6_Position_dY.value
                    Hopper_Position_dTeachZ.value = Hopper_Place6_Position_dZ.value
                    break
                }

                }
            }




            Button {
                id: button_style_Teach
                x: 0
                y: 232
                width: 306
                height: 43
                fontSize: 10
                text: db.dbTr("Teach") + db.tr
                onClicked: {
                    Hopper_Position_dTeachX.value = Axis_X_dPosition.value;
                    Hopper_Position_dTeachY.value = Axis_Y_dPosition.value;
                    Hopper_Position_dTeachZ.value = Axis_Z_dPosition.value;
                    Hopper_Position_bTeach.value = true;
                }
            }
        }

        GroupBox{
            id: groupbox_moveposition
            x:660
            y:34

            ComponenMovePosition{
                id:move_X
                x:0
                y:0
                width: 306
                targetIO: bUseRotation.value ? "Axis_A_dDistanceNominal" : "Axis_X_dDistanceNominal"
            }

            ComponenMovePosition{
                id:move_Y
                x:0
                y:48
                width: 306
                targetIO: "Axis_Y_dDistanceNominal"
            }

            ComponenMovePosition{
                id:move_Z
                x:0
                y:96
                width: 306
                targetIO: "Axis_Z_dDistanceNominal"
            }
        }

        ToggleButton {
            id: toggle_button_Spindle_ClampingSleeve_Out_bOpenClamp
            x: 30
            y: 487
            height:43
            text: db.dbTr("Clamp On/Off") + db.tr
            stateIO: bEnableMilling.value ? "Spindle_ClampingSleeve_Out_bOpenClamp" : ""
            enableIO: ""
            opacity: 1

            Indicator {
                id: indicator_Spindle_ClampingSleeve_bSensor1
                x: 0
                y: -34
                valueIO: "Spindle_ClampingSleeve_bSensor1"
                opacity: 1
            }
            Indicator {
                id: indicator_Spindle_ClampingSleeve_bSensor2
                x: 53
                y: -34
                clip: true
                valueIO: "Spindle_ClampingSleeve_bSensor2"
                opacity: 1
            }
            Indicator {
                id: indicator_Spindle_ClampingSleeve_bSensor3
                x: 106
                y: -34
                valueIO: "Spindle_ClampingSleeve_bSensor3"
                opacity: 1
            }
        }

        Column{
            id: switches_column
            x: 337
            y: 276
            spacing:24
            width: 300

            ToggleButtonFlat{
                width: parent.width
                height: 74
                text: db.dbTr("Reference start") + db.tr
                stateIO: "Axis_A_Rotation_Reference_In_bStart"
            }
            ToggleButtonFlat{
                width: parent.width
                height: 74
                text: db.dbTr("Magazine cover open") + db.tr
                stateIO: "Toolchanger_InOut_Magazine_Cover_bOpen"
            }
            ToggleButtonFlat{
                width: parent.width
                height: 74
                text: db.dbTr("Magazine Up") + db.tr
                stateIO: "Toolchange_Milling_PneumaticCylinder_Out_bUp"
            }
        }
 /*
        FlipFlopButton {
            id: flipflopbutton_AxisARotationReferenceInbStart
            x: 337
            y: 402
            width: 298
            height: 43
            right_text: db.dbTr("Reference \nstop") + db.tr
            left_text: db.dbTr("Reference \nstart") + db.tr
            stateIO: "Axis_A_Rotation_Reference_In_bStart"
        }


        FlipFlopButton {
            id: flipflopbutton_magazineCoverOpenClose
            x: 337
            y: 451
            width: 298
            height: 43
            right_text: db.dbTr("magazine cover \nclose") + db.tr
            left_text: db.dbTr("magazine cover \nopen") + db.tr
            stateIO: "Toolchanger_InOut_Magazine_Cover_bOpen"
        }

        FlipFlopButton {
            id: flipflopbutton_magazineUpDown
            x: 337
            y: 500
            width: 298
            height: 43
            right_text: db.dbTr("Magazine Down") + db.tr
            left_text: db.dbTr("Magazine Up") + db.tr
            stateIO: "Toolchange_Milling_PneumaticCylinder_Out_bUp"
        }
*/
    }


}

