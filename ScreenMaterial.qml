//buld date: 2018-10-30
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    id:screenMaterial
    width: 1276
    height: 870

    property alias viewScroller: listViewScroller
    property alias serviceflickableLV: serviceflickableLV

    VisualItemModel{
        id: materialpages
        ScreenMaterial1{}
        ScreenMaterialImport{}
    }

    ListView {
        id: serviceflickableLV
        anchors.fill: parent
        focus: true
        clip: true
        highlightRangeMode: ListView.StrictlyEnforceRange
        orientation: ListView.Vertical
        snapMode: ListView.SnapOneItem
        highlightMoveDuration:500
        flickDeceleration: 200
        cacheBuffer: 1953
        interactive: false
        model: materialpages
        onCurrentItemChanged: parent.state = currentIndex


        ListViewScroller{
            id: listViewScroller
            owner:serviceflickableLV
            orientation: serviceflickableLV.orientation
            currentIndex: serviceflickableLV.currentIndex
            count: serviceflickableLV.count
            visible: true
        }
    }




    ToolbarMaterial{
        id: toolbarMaterial
        //width:parent.width - 340 //position_component.width
    }

    states: [
        State {
            name: "0"
            PropertyChanges {
                 target: toolbarMaterial
                 visible: true
            }
            PropertyChanges {
                target: viewScroller
                visible: false
            }
        },
        State {
            name: "1"
/*            PropertyChanges {
                target: toolBarLoader
                source: "ToolbarService2.qml"
            }*/

            PropertyChanges {
                 target: toolbarMaterial
                 visible: false
            }
            PropertyChanges {
                target: viewScroller
                visible: true
            }
            StateChangeScript{
                script:serviceflickableLV.currentItem.initializeDBViewer()
            }
        }
    ]
}

