//build date:2019-03-15, modified SB 21.05.19
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "DBFunctions.js" as DBF

Item {
    width: 1216
    height: 776
    //width: parent.width
    //height: parent.height
    property int rowHeight: 43
    property int columnWidth: 162
    property int columnSpacing: 40
    property int labelHeight: 28

    property int materialiMaterial: Material_iMaterial.value
    property int materialiType:     Material_iType.value
    property double materialdRotaryDiameter: Material_dRotaryDiameter.value


    onMaterialdRotaryDiameterChanged: {
        //Job_Range_dMaxX.value = (Math.PI * Material_dRotaryDiameter.value) * 185/360
		Machine_Configuration_Job_Range_Rotation_dMaxX.value = (Math.PI * Material_dRotaryDiameter.value) * 185/360
        //Axis_X_Rotation_Master_NominalPositionSupervision_In_dMax.value = (Math.PI * Material_dRotaryDiameter.value) * 190/360
    }

    onMaterialiMaterialChanged: dropdown_material.valueItem = materialiMaterial

    onMaterialiTypeChanged: dropdown_type.valueItem = materialiType







    Row{
        id: row1
        x: 20
        y: 80
        width: parent.width
        height: 340
        spacing: columnSpacing
        Item{
            id: col1
            width: columnWidth
            height: parent.height
            //color:"transparent"
            //spacing: 1
            Label{
                height: labelHeight
                text: db.dbTr("Material") + db.tr
                width:parent.width
                colorChoice: 1
            }
            DropDown{
                id: dropdown_material
                y: labelHeight
                width: parent.width
                height: 43
                //MH on rotamate we have only wood
                //TODO: make it only wood if it is rotamate
                textitems: [db.dbTr("las_Wood") + db.tr, db.dbTr("Steel") + db.tr]
                valuitems: [1, 2]
                //SB: Material_iMaterial 2 should set material_bSteel to true
				textIO: "Material_iMaterial"
                onIsDirtyChanged: if(isDirty) isMaterialDirty = true
            }
            ToggleButtonFlatReverse{
                id: auto_z_adjust
                y: dropdown_material.y + dropdown_material.height + columnSpacing
                height: 74
                width: parent.width
                text: db.dbTr("Auto Z adjust") + db.tr
                stateIO:"Material_bStripperboard"
                onClicked: isMaterialDirty = true
            }
			
            ///// engraver ini file
            Row{
                id: row2
                x: 0
                y: auto_z_adjust.y + auto_z_adjust.height + columnSpacing
                width: parent.width
                height: 43
                spacing: columnSpacing
                visible: bEnableEngraving.value
                Button{
                    id: selectIni
                    height: parent.height
                    width: row1.width / 6 - 40
                    text: db.dbTr("Engraver ini file") + db.tr

                    onClicked: {
                        var path = "z:\\opt\\v8\\application\\EngraveOptimizer\\"
                        //path = path.replace(/\//g, "\\")
                        var dlg = dialogFileChooser

                        dlg.titleText =         db.dbTr("Select Engraver ini file") + db.tr
                        dlg.fileChooserText =   db.dbTr("Select File") + db.tr
                        dlg.buttonOkText =      db.dbTr("Select") + db.tr
                        dlg.localFileSystem =   true
                        dlg.ok.connect(selectIni.selectIni)
                        dlg.no.connect(function(){selectIni.__isPressed = false})
                        dlg.fileExtension = "ini"
                        dlg.currentPath = path
                        var fullName = Material_strEngraverIniFile.value
                        //fullName = fullName.replace(/\//g, "\\")
                        //console.log("Select Job: ", "path: ", path, "fullName: ", fullName )

                        dlg.currentFileName = fullName.substring(fullName.lastIndexOf('\\') + 1)
                        dlg.initializeDir()

                        dlg.open()

                    }

                    function selectIni(){
                        //console.log("---------- selectJob ---------------")
                        selectIni.__isPressed = false;
                        var path = dialogFileChooser.currentPath
                        path = path.replace(/\//g, "\\");
                        //path = "Z:" + path;
                        Material_strEngraverIniFile.value = path + dialogFileChooser.currentFileName
                        //console.log("---------- selectJob ---------------", path + dialogFileChooser.currentFileName)
                    }
                }

                TextField{
                    id: text_field_engraver_ini_filename
                    textIO: "Material_strEngraverIniFile"
                    height: parent.height
                    width: (row1.width / 6)*5 - 40
                    isEnabled: false
                }
            }
            ///// engraver ini file
            ///// tool sequence
            Row{
                id: row3
                x: 0
                y: row2.y + row3.height + columnSpacing
                width: parent.width
                height: 43
                spacing: columnSpacing
                Button{
                    id: toolSequences
                    height: parent.height
                    width: row1.width / 6 - 40
                    fontSize: 12
                    text: db.dbTr("Sequence of \ntools") + db.tr

                    onClicked: {
                        toolSequenceDialog.toolSequenceLoaderr.source = "ScreenToolSequence.qml"
                        toolSequenceDialog.open()
                    }
                }

                TextField{
                    id: text_field_strToolSequence
                    textIO: "Material_OptimizeEmptyMoves_strToolSequence"
                    height: parent.height
                    width: (row1.width / 6)*5 - 40
                    isEnabled: false
                }
            }
            ModalDlg{
                id: toolSequenceDialog
                property alias toolSequenceLoaderr: toolSequenceLoader

                margin:20
                dialogboxheight: 580
                dialogboxwidth: 960
                title: db.dbTr("Sequence of tools ") + db.tr
                Loader{
                    id: toolSequenceLoader
                    parent:toolSequenceDialog.container
                    anchors.fill: parent
                }

                onOK:{
                    Material_OptimizeEmptyMoves_strToolSequence.value = toolSequenceLoader.item.strToolSequence
                }
            }

            /////  tool sequence


        }
        Item{
            id: col2
            width: columnWidth
            height: parent.height
            Label{
                height: labelHeight
                text: db.dbTr("Type") + db.tr
                width:parent.width
                colorChoice: 1
            }
            DropDown{
                id: dropdown_type
                y: labelHeight
                width: parent.width
                height: 43
                //MH on rotamate we have only Shell
                //TODO: make it shell if it is rotamate
                textitems: [db.dbTr("Sheet") + db.tr, db.dbTr("Shell") + db.tr]
                valuitems: [1, 2]
                textIO: "Material_iType"
            }
			
            ToggleButtonFlat{
                id: reorder_tools
                y: dropdown_type.y + dropdown_type.height + columnSpacing
                height: 74
                width: parent.width
                text: db.dbTr("Reorder tools") + db.tr
                stateIO:"Material_OptimizeEmptyMoves_bActive"
                onClicked: isMaterialDirty = true
            }
        }
        Item{
            id: col3
            width: columnWidth
            height: parent.height
            Label{
                height: labelHeight
                text: db.dbTr("Thickness") + db.tr
                width:parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_thickness
                y: labelHeight
                height:43
                width: parent.width
                unitEnable: true
                ioFactor: dMToMM.value
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                textIO: "Material_dThickness"
                validator: DoubleValidator{}
            }
			//SB: Don't show option for contour look-ahead, should be always on
			/*
			ToggleButtonFlat{
                id: button_lookahead
                y: textfield_thickness.y + textfield_thickness.height + columnSpacing
                height: 74
                width: parent.width
                text: db.dbTr("Look ahead") + db.tr
                stateIO:"Material_bContourLookAhead"
                onClicked: isMaterialDirty = true
				visible: false
            }
			*/
        }
        /*
        Item{
            id: col4
			//SB: Don't show slope time, should not be changed by the user
            width: columnWidth
            height: parent.height
            Label{
                height: labelHeight
                text: db.dbTr("Slope time") + db.tr
                width:parent.width
                colorChoice: 1
				visible: false
            }
            TextField{
                id: textfield_slopetime
                y: labelHeight
                width: parent.width
                height:43
                textIO: "Material_dSlopeTime"
                ioFactor: 1000
                unitEnable: true
                unit: "ms"
                validator: DoubleValidator{}
				visible: false
            }
			//SB: Don't show option for dynamic pulse, should be always set to true
            ToggleButtonFlat{
                id: togglebutton_lookahead
                y: textfield_slopetime.y + textfield_slopetime.height + columnSpacing
                width: parent.width
                height: 74
                text: db.dbTr("Dynamic Pulse") + db.tr
                stateIO:"Laser_bDynamicFrequency"
                onClicked: isMaterialDirty = true
            }
        }
         */
        Item{
            id: col5
            width: columnWidth
            height: parent.height
            Label{
                height: labelHeight
                text: db.dbTr("Half shell Ø") + db.tr
                width:parent.width
                colorChoice: 1
				visible: bUseRotation.value
            }
            TextField{
                id: textfield_shell_diameter
                y: labelHeight
                width: parent.width
                height:43
                textIO: "Material_dRotaryDiameter"
                unitEnable: true
                ioFactor: dMToMM.value
                unit: strUnitMM.value
				//display 4 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 4 : 2
                validator: DoubleValidator{}
				visible: bUseRotation.value
            }
            ToggleButtonFlat{
                id: togglebutton_bsteel
                y: dropdown_material.y + dropdown_material.height + columnSpacing
                //y: textfield_slopetime.y + textfield_slopetime.height + columnSpacing
                width: parent.width
                height: 74
                text: db.dbTr("High pressure") + db.tr
                stateIO:"Material_bSteel"
                onClicked: isMaterialDirty = true
                //visible: Material_bSteel.value
                visible: Material_iMaterial.value === 2
            }
        }
        Item{
            id: col6
            width: columnWidth
            height: parent.height
            Label{
                height: labelHeight
                text: db.dbTr("Shaft height") + db.tr
                width:parent.width
                colorChoice: 1
				visible: bUseRotation.value

            }
            TextField{
                id: textfield_shaft_height
                y: labelHeight
                width: parent.width
                height:43
                textIO: "Material_dZ1Height"
                unitEnable: true
                ioFactor: dMToMM.value
                unit: strUnitMM.value
				//display 4 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 4 : 2
                validator: DoubleValidator{}
				visible: bUseRotation.value
				}
            }
        }

    ToggleButtonFlat{
        id: four_direction_material
        x: 20
        y: 620
        height: 74
        width: columnWidth
        text: db.dbTr("4 axis") + db.tr
        stateIO:"bFour_Axis_Material"

    }

/*
    Button{
        id: importMaterialScreen
        x: 20
        y: 720
        width: 160
        height: 43
		fontSize: 12
        text: db.dbTr("Import material") + db.tr
        onClicked: {
            screenMaterial.serviceflickableLV.currentIndex = 1
        }
    }
*/
}

