//Build date: 2018-10-30
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
import "DBFunctions.js" as DBF


Item {
    id: root
    width: 1276
    //width: parent.width
    height: 770
    //anchors.fill:parent

    property bool bDoImport: false
    property alias dbViewer: dbViewer

    function initializeDBViewer() {
        dbViewer.elements = db.materials
    }

    WorkerScript{
        id: importer
        source: "ImportMaterialWorker.js"
        property bool bEndImport: false
        onMessage: {
            //console.log("-- WorkerScript onMessage -- ", messageObject.indexVal, messageObject.fileName, messageObject.endImport)

            if( !bDoImport ) return
            bDoImport = !messageObject.endImport
            if( !bDoImport ) return
            if( bEndImport ) return

            fileChooser.updateView( messageObject.indexVal )
            Material_strFile.value = fileChooser.currentPath + "/" + messageObject.fileName;
            Material_bLoad.value = true;
            while( Material_bLoad.value){

            }
            var fileName = messageObject.fileName.substring(0, messageObject.fileName.length - 9 )
            Material_MaterialFile.value =  fileName
            DBF.saveMat()

            dbViewer.elements = db.materials
            dbViewer.updateView(fileName)

        }

    }

    Row{
        x: 20
        y: 100
        spacing: 10
        GroupBox{
            id: maindialogBox
            colorChoice:1
            height: 720
            width: 610
            headerText: db.dbTr("Import material files into DB") + db.tr
            headerWidth: width
            fontSize: 16

            Rectangle{
                anchors.fill: parent
                border.color: "darkgrey"
                border.width: 4
                color: "transparent"
            }

            ImportFileChooser{
                id: fileChooser
                width: parent.width
                height: parent.height - 110

                anchors{
                    top: parent.top
                    topMargin: maindialogBox.__headerHeight * 2
                }

                title: db.dbTr("Select material folder to import file from:") + db.tr
                currentFile: ""
                currentPath: "Z:/opt/v8/application/machine/Materials"
                localFileSystem: true
                fileExtension: "material"

                function updateView( indexVal ){
                    fileChooseListView.currentIndex = indexVal;
                    fileChooseListView.forceActiveFocus();
                    userInputField = "Importing file: " + fileNameList[indexVal];
                }

                function importFiles(){
                    importer.sendMessage({fileNameList: fileNameList, fileInfoList: fileInfoList, currentPath: currentPath})

                }
            }

            Row{
                id: buttonRow
                height: 45
                anchors{
                    bottom: parent.bottom
                    bottomMargin: 5
                    horizontalCenter: maindialogBox.horizontalCenter
                }
                spacing: S.Style.dialogButtonHeightInitial / 2

                Button{
                    id: acceptButton
                    width: 280
                    height: 43
                    text: db.dbTr("Start import files to DB") + db.tr
                    enabled: !bDoImport
                    onClicked: {
                        bDoImport = true
                        importer.bEndImport = false
                        fileChooser.importFiles()
                    }
                    __isPressed: bDoImport
                }

                Button{
                    id: cancelButton
                    width: 280
                    height: 43
                    text:db.dbTr("Stop") + db.tr
                    enabled: bDoImport
                    onClicked: {
                        bDoImport = false
                        importer.bEndImport = true
                    }
                }
            }

        }

        GroupBox{
            id: maindialogBox2
            colorChoice:1
            height: 720
            width: 610
            headerText: db.dbTr("Files in DB") + db.tr
            headerWidth: width
            fontSize: 16

            Rectangle{
                anchors.fill: parent
                border.color: "darkgrey"
                border.width: 4
                color: "transparent"
            }

            DBChooser{
                id: dbViewer
                y: 45
                width: parent.width
                height: parent.height
                interactive: true
                title: ""
                currentFile: ""
                inputFieldVisible: false
                selectable: false

                function updateView( criteria) {
                    for(var i = 0; i < dbViewer.elements.length; ++i){
                        if (criteria === dbViewer.elements[i] )  {
                            dbViewer.__currentIndex = i
                            dbViewer.forceActiveFocus()
                            return
                        }
                    }
                }
            }
        }
    }
}



