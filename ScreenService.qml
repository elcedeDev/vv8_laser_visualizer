//buld date: 2018-10-25
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    id:screenService
    width: 1276
    height: 770

    property alias viewScroller: listViewScroller
    property alias serviceflickableLV: serviceflickableLV

    VisualItemModel{
        id: servicepages
        ScreenService0{}
        ScreenService1{}
        ScreenService2{}
        ScreenService3{}
        ScreenService4{}
        //ScreenService5{}
    }

    ListView {
        id: serviceflickableLV
        anchors.fill: parent
        focus: true
        clip: true
        highlightRangeMode: ListView.StrictlyEnforceRange
        orientation: ListView.Vertical
        snapMode: ListView.SnapOneItem
        highlightMoveDuration:500
        flickDeceleration: 200
        cacheBuffer: 1953
        interactive: false
        model: servicepages
        onCurrentItemChanged: parent.state = currentIndex


        ListViewScroller{
            id: listViewScroller
            owner:serviceflickableLV
            orientation: serviceflickableLV.orientation
            currentIndex: serviceflickableLV.currentIndex
            count: serviceflickableLV.count
            visible: true
        }
    }


    Connections{
        target:mainrect
        onPositionControlStateChanged:{
            //console.log("onPositionControlStateChanged ---- ", pState)
            toolbarService.state = pState === "SHOWED" ? "POS_CONTROL_SHOWED" : "POS_CONTROL_HIDDEN"
            height = pState === "SHOWED" ? 570 : 770
        }
    }


    ToolbarService{
        id: toolbarService
        width:parent.width - 340 //position_component.width
    }

    states: [
        State {
            name: "0"
            PropertyChanges {
                target: viewScroller
                visible: true
            }
        },
        State {
            name: "1"
            PropertyChanges {
                target: viewScroller
                visible: true
            }

        },
        State {
            name: "2"

            PropertyChanges {
                target: viewScroller
                visible: true
            }
        },
        State {
            name: "3"
            PropertyChanges {
                target: viewScroller
                visible: true
            }
            PropertyChanges {
                target: positionSetup
                visible: true
            }
            PropertyChanges {
                target: toolbarService.button_positionwidgetshow
                enabled: true
            }

        },


        State {
            name: "4"
/*            PropertyChanges {
                target: positionControl
                state: "HIDDEN"
            }
*/
            PropertyChanges {
                target: viewScroller
                visible: true
            }
            PropertyChanges {
                target: positionSetup
                visible: false
            }
            PropertyChanges {
                target: toolbarService.button_positionwidgetshow
                enabled: true
            }

        }
    ]
}

