import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

FocusScope {
    id: screenService1
    width: 1216
    height: 770


    SelectLanguage{
        id:selectLanguage
        x: 30
        y: 30
        width: 277
        height: 43
    }


    ToggleButtonFlat{
        id: flipflop_bUseInches_on_off
        x: 30
        y: 81
        height: 74
        text: db.dbTr("Use inches") + db.tr
        stateIO: "bUseInches"
    }


    ToggleButtonFlat{
        id: flipflop_bEnableMilling_on_off
        x: 30
        y: 81 + 85
        height: 74
        text: db.dbTr("Enable milling") + db.tr
        stateIO: "bEnableMilling"
    }

    Label{
        id: importMaterialScreen_message
        x: 30
        y: 81 + 2 * 85
        height: 74
        width: parent.width
        colorChoice: 1
        text: "The import material can be done on Material screen after pressing the button below"
    }

    Button{
        id: importMaterialScreen
        x: 30
        y: 81 + 3*85
        width: 160
        height: 43
        fontSize: 12
        text: db.dbTr("Import material") + db.tr
        onClicked: {
            screenMaterial.serviceflickableLV.currentIndex = 1
        }
    }
}



