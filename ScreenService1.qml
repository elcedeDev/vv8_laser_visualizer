import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0
import elcede.chartcontrol 1.0

FocusScope {
    id: screenService1
    width: 1216
    height: 770

    property int rowHeight: 48
    property int rowSpacing: 25
    property int colWitdh: 160

    property int xPercent: xPercentCalc()
    property int yPercent: yPercentCalc()
    property int zPercent: zPercentCalc()

    function xPercentCalc(){
        var percent
        if(bUseRotation.value) {
            //percent = ((Lubrication_Rotation_Distance_dIntervalX.value - Lubrication_Rotation_Distance_dElapsedX.value)*dMToM.value).toFixed(2)
            percent = 100 - Lubrication_Rotation_Distance_dElapsedX.value/Lubrication_Rotation_Distance_dIntervalX.value*100
            return percent
        }else{
            //percent = ((Lubrication_Flatbed_Distance_dIntervalX.value - Lubrication_Flatbed_Distance_dElapsedX.value)*dMToM.value).toFixed(2)
            percent = 100 - Lubrication_Flatbed_Distance_dElapsedX.value/Lubrication_Flatbed_Distance_dIntervalX.value*100
            return percent
        }
    }

    function yPercentCalc(){
        var percent
        if(bUseRotation.value) {
            percent = 100 -Lubrication_Rotation_Distance_dElapsedY.value/Lubrication_Rotation_Distance_dIntervalY.value*100
            return percent
        }else{
            percent = 100 - Lubrication_Flatbed_Distance_dElapsedY.value/Lubrication_Flatbed_Distance_dIntervalY.value*100
            return percent
        }
    }

    function zPercentCalc(){
        var percent
        if(bUseRotation.value) {
            percent = 100 - Lubrication_Rotation_Distance_dElapsedZ.value/Lubrication_Rotation_Distance_dIntervalZ.value*100
            return percent
        }else{
            percent = 100 - Lubrication_Flatbed_Distance_dElapsedZ.value/Lubrication_Flatbed_Distance_dIntervalZ.value*100
            return percent
        }
    }


    //text: db.dbTr("") + db.tr
    Column{
        x:30
        y:30
        width:parent.width
        spacing: 20
        Row{
            id:row_0
            height: 20
            spacing: rowSpacing

            Label{
                text: db.dbTr("Axis") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("total distance") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("last maintenance") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("next maintenance") + db.tr
                width: colWitdh
                colorChoice: 1
            }

        }
        Row{
            id:row_1
            height: rowHeight
            spacing: rowSpacing
            TextField{
                width: colWitdh
                height: rowHeight
                text: "X"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value ? "Statistics_Rotation_Distance_dCompleteX" : "Statistics_Flatbed_Distance_dCompleteX"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value ? "Lubrication_Rotation_Distance_dElapsedX" : "Lubrication_Flatbed_Distance_dElapsedX"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: bUseRotation.value ? ((Lubrication_Rotation_Distance_dIntervalX.value - Lubrication_Rotation_Distance_dElapsedX.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalX.value - Lubrication_Flatbed_Distance_dElapsedX.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: xPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11
                onClicked: {
                    if(bUseRotation.value)
                       Lubrication_Rotation_Distance_dElapsedX.value = 0;
                    else
                       Lubrication_Flatbed_Distance_dElapsedX.value = 0;
                }
                onReleased: {
                    __isPressed = false
                }
            }
        }
        Row{
            id:row_2
            height: rowHeight
            spacing: rowSpacing
            TextField{
                width: colWitdh
                height: rowHeight
                text: "Y"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value ? "Statistics_Rotation_Distance_dCompleteY" : "Statistics_Flatbed_Distance_dCompleteY"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value ? "Lubrication_Rotation_Distance_dElapsedY" : "Lubrication_Flatbed_Distance_dElapsedY"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: bUseRotation.value ? ((Lubrication_Rotation_Distance_dIntervalY.value - Lubrication_Rotation_Distance_dElapsedY.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalY.value - Lubrication_Flatbed_Distance_dElapsedY.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: yPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11
                onClicked: {
                    if(bUseRotation.value)
                        Lubrication_Rotation_Distance_dElapsedY.value = 0;
                    else
                        Lubrication_Flatbed_Distance_dElapsedY.value =0;
                }
                onReleased: {
                    __isPressed = false
                }
            }
        }
        Row{
            id:row_3
            height: rowHeight
            spacing: rowSpacing
            TextField{
                width: colWitdh
                height: rowHeight
                text: "Z"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value ? "Statistics_Rotation_Distance_dCompleteZ" : "Statistics_Flatbed_Distance_dCompleteZ"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value ? "Lubrication_Rotation_Distance_dElapsedZ" : "Lubrication_Flatbed_Distance_dElapsedZ"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: bUseRotation.value ? ((Lubrication_Rotation_Distance_dIntervalZ.value - Lubrication_Rotation_Distance_dElapsedZ.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalZ.value - Lubrication_Flatbed_Distance_dElapsedZ.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: zPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11
                onClicked: {
                    if(bUseRotation.value)
                        Lubrication_Rotation_Distance_dElapsedZ.value = 0;
                    else
                        Lubrication_Flatbed_Distance_dElapsedZ.value = 0;
                }
                onReleased: {
                    __isPressed = false
                }
            }
        }
    }
}

