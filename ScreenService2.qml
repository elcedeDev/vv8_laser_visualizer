import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    width: 1216
    height: 770
    property string styleName: S.styleName



    GroupBox {
        id: groupbox_testlaser
        x: 30
        y: 30
        width: 276
        height: 213
        headerWidth: 276
        fontSize: 12
        headerText: db.dbTr("Test Laser") + db.tr
        colorChoice: 1

        Column{
            y: 29
            width: 276
            height: 213
            spacing: 20

            DropDown{
                id:comboBox
                width: parent.width
                height:43
                textitems: [db.dbTr("paper") + db.tr, db.dbTr("thermo paper") + db.tr, db.dbTr("wood") + db.tr, db.dbTr("tape") + db.tr]
                valuitems: [1, 2, 3, 4]
                displaytext: "drop down"

                onValueItemChanged:{
                    switch(valueItem){
                    case 1:{
                        LaserTest_dAnalogPower.value =  1.0
                        LaserTest_dPulseWidth.value =   1.0
                        LaserTest_dFrequency.value =    1000.0
                        LaserTest_dTime.value =         0.032
                        break;
                        }
                    case 2:{
                        LaserTest_dAnalogPower.value =  1;
                        LaserTest_dPulseWidth.value =   1;
                        LaserTest_dFrequency.value =    1000;
                        LaserTest_dTime.value =         0.01;
                        break;
                        }
                    case 3:{
                        LaserTest_dAnalogPower.value =  1;
                        LaserTest_dPulseWidth.value =   1;
                        LaserTest_dFrequency.value =    1000;
                        LaserTest_dTime.value =         0.07;
                        break;
                        }
                    case 4:{
                        LaserTest_dAnalogPower.value =  0.3;
                        LaserTest_dPulseWidth.value =   0.05;
                        LaserTest_dFrequency.value =    1000;
                        LaserTest_dTime.value =         0.01;

                        break;
                        }
                    }
                }
            }


            Row{
                height: 43
                spacing: 5


                TextField {
                    id: text_field_lablel_AP
                    width: 154
                    height: 43
                    text: db.dbTr("Power") + db.tr
                    fontColorChoice: -2
                    readOnly: true
                    fontSize: 12
                }


                TextField {
                    id: text_field_AP
                    height: 43
                    textIO: "LaserTest_dAnalogPower"
                    unitEnable: true
                    unit: "%"
                    ioFactor: 100
                    horizontalTextAlignement: 2
                    fontSize: 12
                    digitsAfterComma: 0
                }
            }


            Row{
                height: 43
                spacing: 5
                TextField {
                    id: text_field_lablel_P
                    width: 154
                    height: 43
                    text: db.dbTr("Pulse width") + db.tr
                    fontSize: 12
                    readOnly: true
                }

                TextField {
                    id: text_field_P
                    height: 43
                    textIO: "LaserTest_dPulseWidth"
                    ioFactor: 100
                    horizontalTextAlignement: 2
                    fontSize: 12
                    unitEnable: true
                    unit: "%"

                }
            }
            Row{
                height: 43
                spacing: 5
                TextField {
                    id: text_field_lablel_f

                    width: 154
                    height: 43
                    text: db.dbTr("Frequency") + db.tr
                    readOnly: true
                    fontSize: 12
                }

                TextField {
                    id: text_field_f

                    height: 43
                    textIO: "LaserTest_dFrequency"
                    digitsAfterComma: 0
                    horizontalTextAlignement: 2
                    fontSize: 12
                    unitEnable: true
                    unit: "Hz"

                }
            }
            Row{
                height: 43
                spacing: 5


                TextField {
                    id: text_field_lablel_t
                    width: 154
                    height: 43
                    text: db.dbTr("Time") + db.tr
                    readOnly: true
                    fontSize: 12
                }


                TextField {
                    id: text_field_t
                    height: 43
                    textIO: "LaserTest_dTime"
                    horizontalTextAlignement: 2
                    fontSize: 12
                    unitEnable: true
                    unit: "ms"
                    ioFactor: 1000
                    digitsAfterComma: 0
                }
            }


            FlipFlopButton{
                id:flipflop_engraving_laser
                height: 43
                width: parent.width
                iconImage: "11EngraveLaser.png"
                left_text: db.dbTr("engraving") + db.tr
                right_text: db.dbTr("laser") + db.tr
                stateIO: "Engraving_InOut_Relais_bActivate"
                onRightClicked: {
                    Engraving_InOut_Laser_bOn.value = false
                }
            }
            FlipFlopButton{
                id:flipflop_laser_on_off
                height: 43
                width: parent.width
                iconImage: "11EngraveLaser.png"
                left_text: db.dbTr("Laser on") + db.tr
                right_text: db.dbTr("Laser off") + db.tr
                visible: Engraving_InOut_Relais_bActivate.value
                stateIO: "Engraving_InOut_Laser_bOn"
            }

            Button{
                id: button_testshoot
                width: parent.width
                height: 43
                text: db.dbTr("Shoot") + db.tr
                onClicked: { LaserTest_bStart.value = true }
            }
        }
    }
}

