import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "MainFunctions.js" as MF
//Spindle pitch compensation"
Item {
    id: screenService3
    width: 1216
    height: 770

    property string styleName: S.styleName
    property string axis: "X"
    property bool bDoSpindlePitch: false



    function moveTo(dMoveTo ){
        console.log("screenService3.axis: ", screenService3.axis)
        MF.pause(Compensation_SpindleGradient_Flatbed_dDelay.value*1000)
        if( screenService3.axis === "X")
        {
            Axis_X_dDistanceNominal.value = dMoveTo;
        }
        else
        {
            Axis_Y_dDistanceNominal.value = dMoveTo;
        }
        MoveMachine_bMove.value = true;
        dSpindlePitchMachinePosition += dMoveTo;
        v8Msg.postMessage(v8Msg.ms_INFO, "axis moveto: " + screenService3.axis + " " + dMoveTo);
    }

    property int iSpindlePitchCycle: -1
    property int iSpindlePitchNumberOfStops
    property int iSpindlePitchEndPoint
    property double dSpindlePitchGrid
    property double dSpindlePitchMachinePosition

    property bool bMoveMachine: bMachineIsMoving.value

    onBMoveMachineChanged: {
        if(bDoSpindlePitch && !bMoveMachine)
        {
            iSpindlePitchCycle++
        }
    }

    onISpindlePitchCycleChanged: {

        if( !bDoSpindlePitch ) return;
        if( iSpindlePitchCycle >= iSpindlePitchNumberOfStops ){
            bDoSpindlePitch = false;
            return;
        }
        if( iSpindlePitchCycle === 0)
            moveTo( - 0.003 )
        else if( iSpindlePitchCycle === 1)
            moveTo( 0.003 )
        else if( iSpindlePitchCycle === iSpindlePitchEndPoint )
            moveTo( 0.003 )
        else if( iSpindlePitchCycle === iSpindlePitchEndPoint + 1)
            moveTo(  - 0.003 )
        else if( iSpindlePitchCycle < iSpindlePitchEndPoint && iSpindlePitchCycle > 1)
            moveTo( dSpindlePitchGrid  )
        else if( iSpindlePitchCycle > iSpindlePitchEndPoint + 1 )
            moveTo( - dSpindlePitchGrid )

        v8Msg.postMessage(v8Msg.ms_INFO, "iSpindlePitchCycle: " + iSpindlePitchCycle + " iSpindlePitchEndPoint: " + iSpindlePitchEndPoint + " dSpindlePitchMachinePosition: " + dSpindlePitchMachinePosition + " dSpindlePitchGrid " + dSpindlePitchGrid);
    }


    GroupBox {
        id: groupbox_pitch
        x: 30
        y: 30
        width: 276
        height: 213
        headerWidth: 276
        fontSize: 12
        headerText: db.dbTr("Spindle pitch compensation") + db.tr
        colorChoice: 1


        Column{
            y: 29
            width: 276
            height: 213
            spacing: 20

            DropDown{
                id:comboBox
                width: parent.width
                height:43
                textitems: [db.dbTr("X Axis") + db.tr, db.dbTr("Xs Axis") + db.tr, db.dbTr("Y Axis") + db.tr, db.dbTr("Ys Axis") + db.tr]
                valuitems: [1, 2, 3, 4]
                displaytext: "drop down"

                onValueItemChanged:{
                    switch(valueItem){
                    case 1:{
                        Compensation_SpindleGradient_Flatbed_dLength.value  = 3.000;
                        Compensation_SpindleGradient_Flatbed_dGrid.value    = 0.050;
                        Compensation_SpindleGradient_Flatbed_dDelay.value   = 5.0;
                        Compensation_SpindleGradient_Flatbed_iCycles.value  = 2;
                        screenService3.axis = "X";
                        break;
                    }
                    case 2:{
                        Compensation_SpindleGradient_Flatbed_dLength.value  = 3.000;
                        Compensation_SpindleGradient_Flatbed_dGrid.value    = 0.050;
                        Compensation_SpindleGradient_Flatbed_dDelay.value   = 5.0;
                        Compensation_SpindleGradient_Flatbed_iCycles.value  = 2;
                        screenService3.axis = "X";
                        break;
                    }
                    case 3:{
                        Compensation_SpindleGradient_Flatbed_dLength.value  = 1.000;
                        Compensation_SpindleGradient_Flatbed_dGrid.value    = 0.050;
                        Compensation_SpindleGradient_Flatbed_dDelay.value   = 5.0;
                        Compensation_SpindleGradient_Flatbed_iCycles.value  = 2;
                        screenService3.axis = "Y";
                        break;
                    }

                    case 4:{
                        Compensation_SpindleGradient_Flatbed_dLength.value  = 1.000;
                        Compensation_SpindleGradient_Flatbed_dGrid.value    = 0.050;
                        Compensation_SpindleGradient_Flatbed_dDelay.value   = 5.0;
                        Compensation_SpindleGradient_Flatbed_iCycles.value  = 2;
                        screenService3.axis = "Y";
                        break;
                    }
                    }
                    console.log("screenService3.axis: ", screenService3.axis)
                }
            }


            Row{
                height: 43
                spacing: 5


                TextField {
                    id: text_field_lablel_length
                    width: 154
                    height: 43
                    text: db.dbTr("Length") + db.tr
                    fontColorChoice: -2
                    readOnly: true
                    fontSize: 12
                }


                TextField {
                    id: text_field_length
                    height: 43
                    textIO: bUseRotation.value ? "Compensation_SpindleGradient_Rotation_dLength" : "Compensation_SpindleGradient_Flatbed_dLength"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    horizontalTextAlignement: 2
                    fontSize: 12
                    digitsAfterComma: 0
                }
            }


            Row{
                height: 43
                spacing: 5
                TextField {
                    id: text_field_lablel_distance
                    width: 154
                    height: 43
                    text: db.dbTr("Distance") + db.tr
                    fontSize: 12
                    readOnly: true
                }

                TextField {
                    id: text_field_distance
                    height: 43
                    textIO: bUseRotation.value ? "Compensation_SpindleGradient_Rotation_dGrid" : "Compensation_SpindleGradient_Flatbed_dGrid"
                    unitEnable: true
                    unit: strUnitMM.value
                    ioFactor: dMToMM.value
                    horizontalTextAlignement: 2
                    fontSize: 12
                }
            }
            Row{
                height: 43
                spacing: 5
                TextField {
                    id: text_field_lablel_time

                    width: 154
                    height: 43
                    text: db.dbTr("Time") + db.tr
                    readOnly: true
                    fontSize: 12
                }

                TextField {
                    id: text_field_time

                    height: 43
                    textIO: bUseRotation.value ? "Compensation_SpindleGradient_Rotation_dDelay" : "Compensation_SpindleGradient_Flatbed_dDelay"
                    digitsAfterComma: 0
                    horizontalTextAlignement: 2
                    fontSize: 12
                    unitEnable: true
                    unit: "s"
                    ioFactor: 1

                }
            }
            Row{
                height: 43
                spacing: 5


                TextField {
                    id: text_field_lablel_cycles
                    width: 154
                    height: 43
                    text: db.dbTr("Cycles") + db.tr
                    readOnly: true
                    fontSize: 12
                }


                TextField {
                    id: text_field_cycles
                    height: 43
                    textIO: bUseRotation.value ? "Compensation_SpindleGradient_Rotation_iCycles" : "Compensation_SpindleGradient_Flatbed_iCycles"
                    horizontalTextAlignement: 2
                    fontSize: 12
                    unitEnable: false
                    digitsAfterComma: 0
                }
            }






            Button{
                id: button_spindlepitch_start
                width: parent.width
                height: 43
                text: db.dbTr("Start") + db.tr
                enabled: !bDoSpindlePitch


                //Compensation is turned on for the movement
				onClicked: {
                    if(bDoSpindlePitch) return
                    Compensation_SpindleGradient_Axis_X_Flatbed_Master_bOn.value = true
                    Compensation_SpindleGradient_Axis_X_Flatbed_Slave_bOn.value = true
                    Compensation_SpindleGradient_Axis_Y_Flatbed_Master_bOn.value = true
                    Compensation_SpindleGradient_Axis_Y_Flatbed_Slave_bOn.value = true
					if(bUseRotation.value){
						Compensation_SpindleGradient_Axis_Y_Rotation_Master_bOn.value = true
						Compensation_SpindleGradient_Axis_Y_Rotation_Slave_bIsOn.value = true
					}

                    iSpindlePitchNumberOfStops = parseInt(Compensation_SpindleGradient_Flatbed_dLength.value / Compensation_SpindleGradient_Flatbed_dGrid.value)
                    iSpindlePitchNumberOfStops = (iSpindlePitchNumberOfStops + 1)*Compensation_SpindleGradient_Flatbed_iCycles.value + 2
                    iSpindlePitchEndPoint = iSpindlePitchNumberOfStops / Compensation_SpindleGradient_Flatbed_iCycles.value
                    dSpindlePitchGrid = Compensation_SpindleGradient_Flatbed_dGrid.value
                    v8Msg.postMessage(v8Msg.ms_INFO, "START --- spindle pitch procedure number stops: " + iSpindlePitchNumberOfStops + " iSpindlePitchEndPoint: " + iSpindlePitchEndPoint + " dSpindlePitchMachinePosition: " + dSpindlePitchMachinePosition + " dSpindlePitchGrid " + dSpindlePitchGrid);
                    bDoSpindlePitch = true
                    iSpindlePitchCycle = 0
                }
            }

            Button{
                id: button_spindlepitch_stop
                width: parent.width
                height: 43
                text: db.dbTr("Stop") + db.tr
                enabled: bDoSpindlePitch
                onClicked: {
                    bDoSpindlePitch = false
                    v8Msg.postMessage(v8Msg.ms_INFO, "STOP --- spindle pitch procedure stopped at: " + iSpindlePitchCycle + " stop");
                }
            }




/*
            WorkerScript{
                id: spindlePitchWorker
                source: "SpindlePitchWorker.js"
                property bool bEndSpindlePitch: false
                //property bool bMoveMachine: MoveMachine_bMove.value



                onMessage:{
                    v8Msg.postMessage(v8Msg.ms_INFO, "-- WorkerScript spindlePitchWorker - messageObject.moveTo:" + messageObject.moveTo + "endSpindlePitch:" + messageObject.endSpindlePitch);
                    console.log(" ------- WorkerScript spindlePitchWorker onMessage --------------- messageObject.moveTo:", messageObject.moveTo, "endSpindlePitch", messageObject.endSpindlePitch)
                    if( !bDoSpindlePitch ) return
                    bDoSpindlePitch = !messageObject.endSpindlePitch
                    if( !bDoSpindlePitch ) return
                    if( bEndSpindlePitch ) return

                    if( axis === "X")
                    {
                        Axis_X_dDistanceNominal.value = messageObject.moveTo;
                    }
                    else
                    {
                        Axis_Y_dDistanceNominal.value = messageObject.moveTo;
                    }
                    console.log("WorkerScript ------------ MoveMachine_bMove start" )
                    MoveMachine_bMove.value = true;

                }
            }
*/
        }
    }
}

