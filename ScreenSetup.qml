//buld date: 2019-03-04
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "MainFunctions.js" as MF


Item {
    width: 1215
    height: 651
    y: 100
    id:screenSetup
    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath
    
	//function to disable button when machine is not on clear table position
    function woodLifterEnabled(){
        if( (Math.abs(Math.abs(Axis_X_dPosition.value) - Math.abs(ClearTable_Position_dX.value)) < 0.003) &&
                (Math.abs(Math.abs(Axis_Y_dPosition.value) - Math.abs(ClearTable_Position_dY.value)) < 0.003) &&
                !MoveMachine_bMove.value){
            return true
        }else
            return false
    }


    Button{
        id:button_back
        x: 910
        y: 568
        width: 30
        height:positionControl.height
        //height:198
        foregroundImageNormal:  imagePath + "arrow-04-w.png"
        foregroundImagePressed: imagePath + "arrow-04.png"
        __imageNormal: imagePath +  "EmptyDown.png"
        visible: positionControl.state === "HIDDEN"
        onClicked: {
            positionControl.state       = "SHOWED"
            screenJob.diecadviewerContainer.state = "REDUCED"
        }
    }

    Column{
        id: col_swithes
        x: 20
        y: 20
        width: parent.width
        height: 800
        spacing: 30

        move: Transition {
              NumberAnimation { properties: "y"; duration: 500; easing.type: Easing.OutBounce }
        }

        Row{
            id: row_swithes0
            width: parent.width
            height: 55
            spacing: 30

            RadioButton{
                id: button_toRotation
                width: 320
                height:parent.height
                text: db.dbTr("Rotary mode") + db.tr
                onClicked: MF.toRotation()
                __pressed: bUseRotation.value
                enabled: !Machine_bBusy.value
				//Only display for combo machine
                visible: Machine_Configuration_Machine_Type.value === "LCS_Combo" //LaserMate ?? LaserMate_old
            }
            RadioButton{
                id: button_toFlatbed
                width: 320
                height:parent.height
                text: db.dbTr("Flatbed mode") + db.tr
                onClicked: MF.toFlatbed()
                __pressed: bUseFlatbed.value
                enabled: !Machine_bBusy.value
				//Only display for combo machine
                visible: Machine_Configuration_Machine_Type.value === "LCS_Combo"
            }
        }

		property bool bStartChangeInShell: ChangeShell_In_bStartChangeInShell.value
		
		onBStartChangeInShellChanged:{
			if(ChangeShell_In_bStartChangeInShell.value === false)
				Axis_X_Rotation_Master_NominalPositionSupervision_In_dMax.value = (Math.PI * Material_dRotaryDiameter.value) * 190/360
		}
		
        Row{
            id: row_swithes1
            width: parent.width
            height: 55
            spacing: 30
            visible: bUseRotation.value
            Button{
                id: button_load_shell
                width: 320
                height:parent.height
                text: db.dbTr("Load shell") + db.tr
                onClicked: ChangeShell_In_bStartChangeInShell.value = true
                enabled: !Machine_bBusy.value
            }

            Button{
                id: button_unload_shell
                width: 320
                height:parent.height
                text: db.dbTr("Unload shell") + db.tr
                onClicked: ChangeShell_In_bStartChangeOutShell.value = true
                enabled: !Machine_bBusy.value
            }
        }

        Row{
            id: row_swithes2
            width: parent.width
            height: 55
            spacing: 30
            visible: bUseRotation.value

            Button{
                id:button_clamping_open
                width: 320
                height: parent.height
                text: db.dbTr("Clamping open") + db.tr
                onClicked: ChangeShell_Clamping_Out_bOpen.value = true
                enabled: !Machine_bBusy.value
            }
            Button{
                id:button_clamping_close
                width: 320
                height: parent.height
                text: db.dbTr("Clamping close") + db.tr
                onClicked: ChangeShell_Clamping_Out_bClose.value = true
                enabled: !Machine_bBusy.value
                //visible: bUseRotation.value
            }
        }

        Row{
            width: parent.width
            height: 74
            spacing: 30
			//Clear table and set actual position as zeroposition are shown in the position widget
			/*
            ToggleButtonFlat{
                id: toggle_cleartable_on_off
                width: 160
                height: 74
                text: db.dbTr("Clear on job end") + db.tr
                stateIO: "ClearTable_bOn"
            }
            ToggleButtonFlat{
                id: toggle_bSetActualPositionAsZeroPosition
                width: 160
                height: 74
                text: db.dbTr("Set as Zero Position") + db.tr
                stateIO: "bSetActualPositionAsZeroPosition"
            }
			*/
            ToggleButtonFlatReverse{
                id: toggle_detect_ignore_strike_fault
                width: 160
                height: 74
                text: db.dbTr("Detect strike fault") + db.tr
                stateIO: "LaserUnit_In_bIgnoreHV"
            }
        }
        Row{
            width: parent.width
            height: 74
            spacing: 30
            ToggleButtonFlat{
                id: toggle_woodlifter_on_off
                width: 160
                height: 74
                text: db.dbTr("Wood lifter UP") + db.tr
                stateIO: Machine_Configuration_UseWoodLifter.value ? "WoodLifter_bUp" : ""
                enabled: woodLifterEnabled()
                visible: Machine_Configuration_UseWoodLifter.value
            }
            ToggleButtonFlat{
                id: toggle_suction_on_off
                width: 160
                height: 74
                text: db.dbTr("Suction") + db.tr
                stateIO: "VacuumCleanerNozzle_bOn"
            }
			//This is an Rotary option
            ToggleButtonFlat{
                id: toggle_cover_on_off
                width: 160
                height: 74
                text: db.dbTr("Cover close") + db.tr
                stateIO: "Rotation_PneumaticCylinder_Cover_Out_bClose"
				//only display if bUseRotation is true
				visible: bUseRotation.value
            }
			
        }
    }
}


