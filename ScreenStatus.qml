import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "localVariables.js" as LV

Item {
    width: 1260
    height: 851
    id:screenStatus

     property string imagePath: S.Style[S.styleName].imagePath

    RadioGroup{
        id:statusMenu
        stateTarget: "screenStatus"
        initialState: "DRIVES"
        width: 1000
        height: 50

        Rectangle{
            id: menuLine
            x:20
            y: parent.height + 20
            width:parent.width - 100
            height:2
            color:"black"
        }
/*
//warning icon on Spidle 2nd menu item
        Image{
            x: 150+40+2+40+150+20
            y:20
            id: warningSpindle
            source: imagePath + "warning-5-32.png"
            width: 32
            height:32
            visible: !Spindle_WarmUp_bOk.value
        }
*/
        Row{
            id: statusMenuRow
            x: 20
            y: 20
            width: parent.width - 20
            height: parent.height
            spacing: 40

            TabButton{
                width:150
                height: parent.height
                __text: db.dbTr("Drives") + db.tr
                imageNormal: __imagePath + "Empty.png"
                imagePressed: __imagePath + "Empty.png"
                selectionLine: true
                stateTarget: statusMenu.stateTarget
                onClicked_changeStateTo: "DRIVES"
                __pressed: screenStatus.state == "DRIVES"
                fontSize: 20
                fontColorNormal: "black"
            }

            Rectangle{
                y: 15
                height: parent.height - 20
                width:2
                color:"black"
            }

            TabButton{
                width:150
                height: 50
                __text: db.dbTr("Laser") + db.tr
                imageNormal: __imagePath + "Empty.png"
                imagePressed: __imagePath + "Empty.png"
                selectionLine: true
                stateTarget: statusMenu.stateTarget
                onClicked_changeStateTo: "LASER"
                __pressed: screenStatus.state == "LASER"
                fontSize: 20
                fontColorNormal: "black"
            }

            Rectangle{
                y: 15
                height: parent.height - 20
                width:2
                color:"black"
            }

            TabButton{
                width:150
                height: 50
                __text: db.dbTr("Error-Log") + db.tr
                imageNormal: __imagePath + "Empty.png"
                imagePressed: __imagePath + "Empty.png"
                selectionLine: true
                stateTarget: statusMenu.stateTarget
                onClicked_changeStateTo: "ERROR_LOG"
                __pressed: screenStatus.state == "ERROR_LOG"
                fontSize: 20
                fontColorNormal: "black"
            }

            Rectangle{
                y: 15
                height: parent.height - 20
                width:2
                color:"black"
            }

            TabButton{
                width:150
                height: 50
                __text: db.dbTr("Language") + db.tr
                imageNormal: __imagePath + "Empty.png"
                imagePressed: __imagePath + "Empty.png"
                selectionLine: true
                stateTarget: statusMenu.stateTarget
                onClicked_changeStateTo: "LANGUAGE"
                __pressed: screenStatus.state == "LANGUAGE"
                fontSize: 20
                fontColorNormal: "black"
            }
        }
    }

    Loader{
        id:statusScreenLoader
        x: 20
        y: 60
        source: "ScreenStatus1.qml"

    }


    ScreenStatus3{
        id:v8Kernellog
        x: 10
        y: 60
        width: parent.width
        height: parent.height
        visible: false
    }

    states: [
        State {
            name: "DRIVES"
            PropertyChanges {
                target: statusScreenLoader
                source: "ScreenStatus1.qml"
            }
        },
        State {
            name: "LASER"
            PropertyChanges {
                target: statusScreenLoader
                source: "ScreenStatus2.qml"
            }
        },
        State {
            name: "SPINDLE"
            PropertyChanges {
                target: statusScreenLoader
                source: "ScreenStatus2.qml"
            }
        },
        State {
            name: "ERROR_LOG"
            PropertyChanges {
                target: statusScreenLoader
                source: ""
            }
            PropertyChanges {
                target: v8Kernellog
                visible: true
            }
        },
        State {
            name: "LANGUAGE"
            PropertyChanges {
                target: statusScreenLoader
                source: "ScreenStatus4.qml"
            }
        }
     ]
}

