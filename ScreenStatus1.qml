//build date:2019-03-13
import QtQuick 2.0
import "style/Style.js" as S
//import tools 2.0
//import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0
import elcede.chartcontrol 1.0
import "MainFunctions.js" as MF

FocusScope {
    id: screenStatus1
    width: 1216
    height: 770

    property int rowHeight: 48
    property int rowSpacing: 25
    property int colWitdh: 170

    property int xPercent: xPercentCalc()
    property int yPercent: yPercentCalc()
    property int zPercent: zPercentCalc()

    function xPercentCalc(){
        var percent

        if(bUseRotation.value)
            percent = 100 - Lubrication_Rotation_Distance_dElapsedX.value/Lubrication_Rotation_Distance_dIntervalX.value*100
        else
            percent = 100 - Lubrication_Flatbed_Distance_dElapsedX.value/Lubrication_Flatbed_Distance_dIntervalX.value*100

        return percent
    }

    function yPercentCalc(){
        var percent

        if(bUseRotation.value)
            percent = 100 - Lubrication_Rotation_Distance_dElapsedY.value/Lubrication_Rotation_Distance_dIntervalY.value*100
        else
            percent = 100 - Lubrication_Flatbed_Distance_dElapsedY.value/Lubrication_Flatbed_Distance_dIntervalY.value*100

        return percent
    }

    function zPercentCalc(){
        var percent

        if(bUseRotation.value)
            percent = 100 - Lubrication_Rotation_Distance_dElapsedZ.value/Lubrication_Rotation_Distance_dIntervalZ.value*100
        else
            percent = 100 - Lubrication_Flatbed_Distance_dElapsedZ.value/Lubrication_Flatbed_Distance_dIntervalZ.value*100

        return percent
    }


    //text: db.dbTr("") + db.tr
    Column{
        x:30
        y:30
        width:parent.width
        spacing: 20
        Row{
            id:row_0
            height: 20
            spacing: rowSpacing

            Label{
                text: db.dbTr("Axis") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("Amplifier") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("total distance") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("last maintenance") + db.tr
                width: colWitdh
                colorChoice: 1
            }
            Label{
                text: db.dbTr("next maintenance") + db.tr
                width: colWitdh
                colorChoice: 1
            }

        }
        Row{
            id:row_1
            height: rowHeight
            spacing: rowSpacing
            TextField{
                width: colWitdh
                height: rowHeight
                text: "X"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            Row{
                height: rowHeight
                width: colWitdh
                spacing: 10
                StatusButton{
                    y: 8
                    width: colWitdh / 2 - 10
                    height: 30
                    indicator_text: "X"
                    indicator_IO: bUseRotation.value === true ? "VD50_Axis_X_Rotation_Master_In_bEnable" : "VD50_Axis_X_Flatbed_Master_In_bEnable"
                }
                StatusButton{
                    y: 8
                    width: colWitdh / 2 - 10
                    height: 30
                    indicator_text: "XS"
                    indicator_IO: bUseRotation.value === true ? "VD50_Axis_X_Rotation_Slave_In_bEnable" : "VD50_Axis_X_Flatbed_Slave_In_bEnable"
                }
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value === true ? "Statistics_Rotation_Distance_dCompleteX" : "Statistics_Flatbed_Distance_dCompleteX"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value === true ? "Lubrication_Rotation_Distance_dElapsedX" : "Lubrication_Flatbed_Distance_dElapsedX"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: bUseRotation.value === true ? ((Lubrication_Rotation_Distance_dIntervalX.value - Lubrication_Rotation_Distance_dElapsedX.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalX.value - Lubrication_Flatbed_Distance_dElapsedX.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: xPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11
                onClicked: {
                       if(bUseRotation.value === true)
                           Lubrication_Rotation_Distance_dElapsedX.value = 0;
                       else
                           Lubrication_Flatbed_Distance_dElapsedX.value = 0;
                }
                onReleased: {
                    __isPressed = false
                }
            }
        }
        Row{
            id:row_2
            height: rowHeight
            spacing: rowSpacing
            TextField{
                width: colWitdh
                height: rowHeight
                text: "Y"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            Row{
                height: rowHeight
                width: colWitdh
                spacing: 10
                StatusButton{
                    y: 8
                    width: colWitdh / 2 - 10
                    height: 30
                    indicator_text: "Y"
                    indicator_IO: bUseRotation.value === true ? "VD50_Axis_Y_Rotation_Master_In_bEnable" : "VD50_Axis_Y_Flatbed_Master_In_bEnable"
                }
                /*
                // MH 20190523: error in vectovision log ReferenceError: Can't find variable: VD50_Axis_Y_Rotation_Slave_In_bEnable
                // this should be fixed on the next LCS or LCS_Combo
                StatusButton{
                    y: 8
                    width: colWitdh / 2 - 10
                    height: 30
                    indicator_text: "YS"
                    indicator_IO: bUseRotation.value === true ? "VD50_Axis_Y_Rotation_Slave_In_bEnable" : "VD50_Axis_Y_Flatbed_Slave_In_bEnable"
					visible: Machine_Configuration_Machine_Type.value === "LCS" || Machine_Configuration_Machine_Type.value === "LCS_Combo"
                }
                */
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value === true ? "Statistics_Rotation_Distance_dCompleteY" : "Statistics_Flatbed_Distance_dCompleteY"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO: bUseRotation.value === true ? "Lubrication_Rotation_Distance_dElapsedY" : "Lubrication_Flatbed_Distance_dElapsedY"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: bUseRotation.value === true ? ((Lubrication_Rotation_Distance_dIntervalY.value - Lubrication_Rotation_Distance_dElapsedY.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalY.value - Lubrication_Flatbed_Distance_dElapsedY.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: yPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11
                onClicked: {
                    if(bUseRotation.value === true )
                        Lubrication_Rotation_Distance_dElapsedY.value =0;
                    else
                        Lubrication_Flatbed_Distance_dElapsedY.value =0;
                }
                onReleased: {
                    __isPressed = false
                }
            }
        }
        Row{
            id:row_3
            height: rowHeight
            spacing: rowSpacing
            TextField{
                width: colWitdh
                height: rowHeight
                text: "Z"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            StatusButton{
                y: 8
                width: colWitdh
                height: 30
                indicator_text: "Z"
                indicator_IO: bUseRotation.value === true ? "VD50_Axis_Z_Rotation_In_bEnable" : "VD50_Axis_Z_Flatbed_In_bEnable"
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO:  bUseRotation.value === true ? "Statistics_Rotation_Distance_dCompleteZ" : "Statistics_Flatbed_Distance_dCompleteZ"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                textIO:  bUseRotation.value === true ? "Lubrication_Rotation_Distance_dElapsedZ" : "Lubrication_Flatbed_Distance_dElapsedZ"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: bUseRotation.value === true ? ((Lubrication_Rotation_Distance_dIntervalZ.value - Lubrication_Rotation_Distance_dElapsedZ.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalZ.value - Lubrication_Flatbed_Distance_dElapsedZ.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: zPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11
                onClicked: {
                    if( bUseRotation.value === true )
                        Lubrication_Rotation_Distance_dElapsedZ.value = 0;
                    else
                        Lubrication_Flatbed_Distance_dElapsedZ.value = 0;
                }
                onReleased: {
                    __isPressed = false
                }
            }
        }
/*
//TODO: should be decided if we need it or not
        Row{
            id:row_4
            height: rowHeight
            spacing: rowSpacing
            visible: bUseRotation.value
            TextField{
                width: colWitdh
                height: rowHeight
                text: "Z1"
                readOnly: true
                horizontalTextAlignement: TextInput.AlignHCenter
            }
            StatusButton{
                y: 8
                width: colWitdh
                height: 30
                indicator_text: "Z1"
                indicator_IO: "VD50_Axis_Rotation_Z1_In_bEnable"
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                //textIO:  bUseRotation.value === true ? "Statistics_Rotation_Distance_dCompleteZ" : "Statistics_Flatbed_Distance_dCompleteZ"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                text: "0.00"
                //textIO:  bUseRotation.value === true ? "Lubrication_Rotation_Distance_dElapsedZ" : "Lubrication_Flatbed_Distance_dElapsedZ"
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            TextField{
                width: colWitdh
                height: rowHeight
                //text: bUseRotation.value === true ? ((Lubrication_Rotation_Distance_dIntervalZ.value - Lubrication_Rotation_Distance_dElapsedZ.value)*dMToM.value).toFixed(2) : ((Lubrication_Flatbed_Distance_dIntervalZ.value - Lubrication_Flatbed_Distance_dElapsedZ.value)*dMToM.value).toFixed(2)
                unitEnable: true
                unit: strUnitM.value
                horizontalTextAlignement: 2
                __verticalTextAlignement: 2
                ioFactor: dMToM.value
                readOnly: true
            }
            Rectangle{
                height: rowHeight
                width: rowHeight
                color: "transparent"
                ChartControl{
                    anchors.fill: parent
                    percent: zPercent
                }
            }
            Button{
                height: rowHeight
                width: 60
                text: db.dbTr("reset") + db.tr
                fontSize: 11

                onClicked: {
                    if( bUseRotation.value === true )
                    //    Lubrication_Rotation_Distance_dElapsedZ.value = 0;
                    else
                    //    Lubrication_Flatbed_Distance_dElapsedZ.value = 0;
                }

                onReleased: {
                    __isPressed = false
                }
            }
        }
*/
    }

    Button{
        x: 30
        y: parent.height - height
        height: 60
        width: colWitdh
        text: db.dbTr("Go to\nservice position") + db.tr
        onClicked: MF.gotoServicePosition()
    }

}

