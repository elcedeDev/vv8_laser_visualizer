import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    id:screenStatus1
    width: 1221
    height: 651
    property string styleName: S.styleName
    property string __imagePath: S.Style[styleName].imagePath

    property bool isFlatBed: bUseFlatbed.value
    property bool isMillingEnabled: bEnableMilling.value
    property bool isMachineUseJobSplitting: Machine_bUseJobSplitting.value
    property bool isRotationUsed: bUseRotation.value
    property bool isWoodLifterOption: WoodLifter_bOption.value


    Column {
        id: statusindicators
        x:10
        y:21
        spacing: 20

        move: Transition {
              NumberAnimation { properties: "y"; duration: 500; easing.type: Easing.OutBounce }
        }


        StatusButton {
            id: indicator_Laser_Ready
            indicator_IO: "LaserUnit_In_bHighVoltageOn"
            indicator_text: db.dbTr("Laser Ready") + db.tr
        }

        StatusButton {
            id: indicator_spindle_warmup
            indicator_IO: "Spindle_WarmUp_bOk"
            indicator_text: db.dbTr("spindle - warmup") + db.tr
            visible: isMachineUseJobSplitting
        }

        StatusButton {
            id: indicator_reference
            indicator_IO: "Axis_A_Rotation_Reference_Out_bOk"
            indicator_text: db.dbTr("reference") + db.tr
            visible: !isFlatBed
        }

        StatusButton {
            id: indicator_Laser_gas
            indicator_IO: "LaserUnit_In_bGasChangeRequired"
            indicator_text: db.dbTr("Laser gas") + db.tr
            inverseState: true
        }

        StatusButton {
            id: indicator_Shell_height_sensor
            indicator_IO: "Axis_Z1_Rotation_In_bHeightSensor"
            indicator_text: db.dbTr("Shell height sensor") + db.tr
            visible: !isFlatBed
        }

        StatusButton {
            id: indicator_Wood_lifter_down
            indicator_IO: (!bUseRotation.value && WoodLifter_bOption.value) ? "WoodLifter_bIsDown" : ""
            indicator_text: db.dbTr("Wood-lifter down") + db.tr
            visible:isWoodLifterOption
        }
    }


    Override{
        x: 633
        y: 585
        labelWidth: 156
    }

    GroupBox{
        visible: isMillingEnabled
        MultilineTextField {
            id: label_requested_tool
            x: 10
            y: 585
            width: 149
            height: 43
            text: db.dbTr("requested tool:") + db.tr
            readOnly: false
            fontSize: 10
            horizontalTextAlignement: 0
            isEnabled: false
        }

        TextField {
            id: text_field_requested_tool
            x: label_requested_tool.x + label_requested_tool.width +8
            y: label_requested_tool.y
            width: 150
            height: 43
            text: "0"
            unitEnable: false
            ioFactor: 1
            readOnly: false
            //textIO: "ToolChange_Milling_New_iTool"
            fontSize: 14
            horizontalTextAlignement: 2
            validator: IntValidator {
                top: 40
                bottom: 0
            }
            digitsAfterComma: 0
            __verticalTextAlignement: 2
            isEnabled: true
        }
    }

}

