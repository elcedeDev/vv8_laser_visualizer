import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0

FocusScope {
    id: screenStatus2_Laser
    width: 1216
    height: 770


    Column {
        id: laserstatusindicators
        x:30
        y:50
        width: 350
        spacing: 30

        move: Transition {
              NumberAnimation { properties: "y"; duration: 500; easing.type: Easing.OutBounce }
        }

        TextField{
            id: laser_label
            width: 200
            height: 50
            text:"Laser"
            readOnly: true
            horizontalTextAlignement: Qt.AlignHCenter
        }
/*
        StatusButton {
            id: indicator_Laser_Mains
            //-- indicator IO??
            indicator_IO: "LaserUnit_InOut_bMainsOn"
            indicator_text: db.dbTr("Mains") + db.tr
        }
*/
/*
        StatusButton {
            id: indicator_Laser_High_Voltage
            indicator_IO: "LaserUnit_In_bHighVoltageOn"
            indicator_text: db.dbTr("High voltage") + db.tr
        }
		*/

        StatusButton {
            id: indicator_Laser_Standby_Ready
            indicator_IO: "LaserUnit_In_bStandbyReady"
            indicator_text: db.dbTr("Standby") + db.tr
            //inverseState: true
        }

                StatusButton {
            id: indicator_Laser_Ready
            indicator_IO: "LaserUnit_In_bHighVoltageOn"
            indicator_text: db.dbTr("Laser ready") + db.tr
            //inverseState: true
        }
		
        StatusButton {
            id: indicator_Laser_gas
            indicator_IO: "LaserUnit_In_bGasChangeRequired"
            indicator_text: db.dbTr("Laser gas") + db.tr
            inverseState: true
        }

        ToggleButtonFlat{
            id: toggle_start_warmup
            height: 74
            text: db.dbTr("Change gas") + db.tr
            stateIO: "LaserUnit_InOut_bChangeGas"
        }
/*
        StatusButton {
            id: indicator_Laser_Change_gas
            //-- indicator IO??
            indicator_IO: "LaserUnit_Out_bGasExchange"
            indicator_text: db.dbTr("Change gas") + db.tr
            inverseState: true
        }
		*/
    }

/*
    Column {
        id: quickmarkstatusindicators
        x:380
        y:50
        spacing: 30

        move: Transition {
              NumberAnimation { properties: "y"; duration: 500; easing.type: Easing.OutBounce }
        }

        TextField{
            id: quickmark_label
            width: 200
            height: 50
            text:"QuickSCAN"
            readOnly: true
            horizontalTextAlignement: Qt.AlignHCenter
        }

        StatusButton {
            id: indicator_quickmark_SupplyVoltage
            //-- indicator IO??
            indicator_IO: "Engraving_InOut_bSetSupplyVoltage"
            indicator_text: db.dbTr("Supply voltage") + db.tr
        }

        StatusButton {
            id: indicator_quickmark_High_Voltage
            //-- indicator IO??
            indicator_IO: "LaserUnit_InOut_bHighVoltageOn"
            indicator_text: db.dbTr("High voltage") + db.tr
        }

        StatusButton {
            id: indicator_quickmark_Ready
            //-- indicator IO??
            indicator_IO: "LaserUnit_Out_bReady"
            indicator_text: db.dbTr("Laser ready") + db.tr
            //inverseState: true
        }

        StatusButton {
            id: indicator_quickmark_gas
            //-- indicator IO??
            indicator_IO: "LaserUnit_In_bGasChangeRequired"
            indicator_text: db.dbTr("Laser gas") + db.tr
            inverseState: true
        }

        StatusButton {
            id: indicator_quickmark_airpressure
            //-- indicator IO??
            indicator_IO: "Air_in_bOk"
            indicator_text: db.dbTr("Air pressure") + db.tr
            inverseState: true
        }
    }
*/
}

