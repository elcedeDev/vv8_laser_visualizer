import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0

FocusScope {
    id: screenStatus3
    width: 1216
    height: 770

    V8WarningLog{
        id: v8Warninglog
        __styleName:  "ELCEDE_FLAT"
        anchors{
          rightMargin: 20
          bottomMargin: 50
          leftMargin: 10
          topMargin: 30
          fill: parent
        }
        showInfos: false
        showWarnings: false
        maxMessages: 200
    }
}

