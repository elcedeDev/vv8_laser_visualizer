import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0

FocusScope {
    id: screenStatus1
    width: 1216
    height: 770
    SelectLanguage{
        id:selectLanguage
        x: 30
        y: 30
        width: 277
        height: 43
    }


    ToggleButtonFlat{
        id: flipflop_bUseInches_on_off
        x: 30
        y: 81
        height: 74
        text: db.dbTr("Use inches") + db.tr
        stateIO: "bUseInches"
    }

}

