//Build date: 2019-03-21, modified SB 21.05.19
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import elcede.database 1.0
import "customerSettings.js" as CS
import "MainFunctions.js" as MF

Item {
    id: screenTool
    width: 1216
    height: 1000
    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    property int columnWidth: 162 //width / 6 - 40
    property int currentTool: 1
    property int columnSpacing: 35
    property int labelHeight: 28
    property int rowSpacing: 40

    property bool bFourAxisMaterial: bFour_Axis_Material.value


    property alias  dSpeedAdjust: textfield_modify_speed.text
    property alias  dHeightAdjust: textfield_modify_height.text

    property int tool_number: 1

    property string strNote_io:                     "Material_Tool_" + tool_number + "_strNote"
    property string iControlType_io:                "Material_Tool_" + tool_number + "_iControlType"
    property string iPocketTool_io:                 "Material_Tool_" + tool_number + "_iPocketTool"
    property string iPocketType_io:                 "Material_Tool_" + tool_number + "_iPocketType"
    property string iType_io:                       "Material_Tool_" + tool_number + "_iType"
    property string dWorkHeight_io:                 "Material_Tool_" + tool_number + "_dWorkHeight"
    property string dHeightXp_io:                   "Material_Tool_" + tool_number + "_dHeightXp"
    property string dHeightXm_io:                   "Material_Tool_" + tool_number + "_dHeightXm"
    property string dHeightYp_io:                   "Material_Tool_" + tool_number + "_dHeightYp"
    property string dHeightYm_io:                   "Material_Tool_" + tool_number + "_dHeightYm"
    property string dHeightXpYp_io:                 "Material_Tool_" + tool_number + "_dHeightXpYp"
    property string dHeightXpYm_io:                 "Material_Tool_" + tool_number + "_dHeightXpYm"
    property string dHeightXmYp_io:                 "Material_Tool_" + tool_number + "_dHeightXmYp"
    property string dHeightXmYm_io:                 "Material_Tool_" + tool_number + "_dHeightXmYm"
    property string dWorkspeedXp_io:                "Material_Tool_" + tool_number + "_dWorkSpeedXp"
    property string dWorkspeedXm_io:                "Material_Tool_" + tool_number + "_dWorkSpeedXm"
    property string dWorkspeedYp_io:                "Material_Tool_" + tool_number + "_dWorkSpeedYp"
    property string dWorkspeedYm_io:                "Material_Tool_" + tool_number + "_dWorkSpeedYm"
    property string dWorkspeedXpYp_io:              "Material_Tool_" + tool_number + "_dWorkSpeedXpYp"
    property string dWorkspeedXpYm_io:              "Material_Tool_" + tool_number + "_dWorkSpeedXpYm"
    property string dWorkspeedXmYp_io:              "Material_Tool_" + tool_number + "_dWorkSpeedXmYp"
    property string dWorkspeedXmYm_io:              "Material_Tool_" + tool_number + "_dWorkSpeedXmYm"
    property string dLsrAnalogPower_io:             "Material_Tool_" + tool_number + "_dLsrAnalogPower"
    property string dLsrPulseWidthOffset_io:        "Material_Tool_" + tool_number + "_dLsrPulseWidthOffset"
    property string dStartMacroTime_io:             "Material_Tool_" + tool_number + "_dStartMacroTime"
    property string dStartMacroLsrFrequency_io:     "Material_Tool_" + tool_number + "_dStartMacroLsrFrequency"
    property string dStartMacroLsrPulseWidth_io:    "Material_Tool_" + tool_number + "_dStartMacroLsrPulseWidth"
    property string dStartMacroAnalogPower_io:      "Material_Tool_" + tool_number + "_dStartMacroAnalogPower"
    property string dStopMacroTime_io:              "Material_Tool_" + tool_number + "_dStopMacroTime"
    property string dStopMacroLsrFrequency_io:      "Material_Tool_" + tool_number + "_dStopMacroLsrFrequency"
    property string dStopMacroLsrPulseWidth_io:     "Material_Tool_" + tool_number + "_dStopMacroLsrPulseWidth"
    property string dStopMacroAnalogPower_io:       "Material_Tool_" + tool_number + "_dStopMacroAnalogPower"
    property string dAccLsrFrequency_io:            "Material_Tool_" + tool_number + "_dAccLsrFrequency"
    property string dAccLsrPulseWidth_io:           "Material_Tool_" + tool_number + "_dAccLsrPulseWidth"
    property string dConstLsrFrequency_io:          "Material_Tool_" + tool_number + "_dConstLsrFrequency"
    property string dConstLsrPulseWidth_io:         "Material_Tool_" + tool_number + "_dConstLsrPulseWidth"
    property string dDeccLsrFrequency_io:           "Material_Tool_" + tool_number + "_dDeccLsrFrequency"
    property string dDeccLsrPulseWidth_io:          "Material_Tool_" + tool_number + "_dDeccLsrPulseWidth"
    property string dBeamRotation_io:               "Material_Tool_" + tool_number + "_dBeamRotation"
    property string iDigitalOutputNr_io:            "Material_Tool_" + tool_number + "_iDigitalOutputNr"
    property string dAnalogOutput_io:               "Material_Tool_" + tool_number + "_dAnalogOutput"
    property string dMaxDeviation_io:               "Material_Tool_" + tool_number + "_dMaxDeviation"
    property string dEnvelopeCut_io:                "Material_Tool_" + tool_number + "_dEnvelopeCut"
    property string dSpeedMax_io:                   "Material_Tool_" + tool_number + "_dSpeedMax"
    property string dMaxPulseWidthSpeed_io:         "Material_Tool_" + tool_number + "_dMaxPulseWidthSpeed"
    property string dAccelerationMax_io:            "Material_Tool_" + tool_number + "_dAccelerationMax"
    property string bBoarderOnly_io:                "Material_Tool_" + tool_number + "_bBoarderOnly"
    property string bDelete_io:                     "Material_Tool_" + tool_number + "_bDelete"
    property string bEngravingTool_io:              "Material_Tool_" + tool_number + "_bEngravingTool"

    property string dLsrFactorAnalogPower_io:       "Material_Tool_" + tool_number + "_dLsrFactorAnalogPower"
    property string dLsrExponentAnalogPower_io:     "Material_Tool_" + tool_number + "_dLsrExponentAnalogPower"
    property string dLsrFactorFrequency_io:         "Material_Tool_" + tool_number + "_dLsrFactorFrequency"
    //milling tool parameters
    property string dSpindleSpeed_io:               "Material_Tool_" + tool_number + "_dSpindleSpeed"
    property string dDistanceDepth_io:              "Material_Tool_" + tool_number + "_dDistanceDepth"
    property string dPiercingSpeed_io:              "Material_Tool_" + tool_number + "_dPiercingSpeed"

    property string dDiameter_io:                   "Material_Tool_" + tool_number + "_dDiameter"
    property string dChannelWidth_io:               "Material_Tool_" + tool_number + "_dChannelWidth"
    property string dAngle_io:                      "Material_Tool_" + tool_number + "_dAngle"
    property string iToolType_io:                   "Material_Tool_" + tool_number + "_iToolType"
    property string bPulseOn_io:                    "Material_Tool_" + tool_number + "_bPulseOn"
    property string dLsrSpotDistance_io:            "Material_Tool_" + tool_number + "_dLsrSpotDistance"

    property int piToolType: piToolTypeValue()
    property int piControlType: piControlTypeValue()

    onPiToolTypeChanged: piToolTypeCanged()

    onPiControlTypeChanged: piControlTypeChanged()
    // 3 digist before and after decimal only pozitiv value
    RegExpValidator { id:heightValidator; regExp: /^(\d{1,3})([\.|,]\d{1,3})?$/ }
    // 2 digist before and after decimal only pozitiv value
    RegExpValidator { id:speedValidator; regExp: /^(\d{1,2})([\.|,]\d{1,2})?$/ }
    // 5 digist before and after decimal only pozitiv value
    RegExpValidator { id: regValidator5; regExp: /^(\d{1,5})([\.|,]\d{1,5})?$/ }
    //galvo power validator
    IntValidator{id: galvoPowerValidator; top:Machine_Configuration_LaserPowerControl_dMaxEngravingPower.value;bottom:0}
    IntValidator{id: percentValidator; top:100;bottom:0}
    IntValidator{ id: intValidator}

    property bool bMaterialLoaded:db.bDBMaterialLoaded

    onBMaterialLoadedChanged: if(bMaterialLoaded) tool_number = 1


    property double tool0SpeedMax: Material_Tool_0_dSpeedMax.value

    onTool0SpeedMaxChanged: {

        if(state === "LAS_GALVO")
        {
            var materialItem1 = screenTool
            var dWorkspeedXp    = eval(materialItem1.dWorkspeedXp_io)
            var dWorkspeedXm    = eval(materialItem1.dWorkspeedXm_io)
            var dWorkspeedYp    = eval(materialItem1.dWorkspeedYp_io)
            var dWorkspeedYm    = eval(materialItem1.dWorkspeedYm_io)
            var dWorkspeedXpYp  = eval(materialItem1.dWorkspeedXpYp_io)
            var dWorkspeedXpYm  = eval(materialItem1.dWorkspeedXpYm_io)
            var dWorkspeedXmYp  = eval(materialItem1.dWorkspeedXmYp_io)
            var dWorkspeedXmYm  = eval(materialItem1.dWorkspeedXmYm_io)

            dWorkspeedXp.value =    Material_Tool_0_dSpeedMax.value
            dWorkspeedXm.value =    Material_Tool_0_dSpeedMax.value
            dWorkspeedYp.value =    Material_Tool_0_dSpeedMax.value
            dWorkspeedYm.value =    Material_Tool_0_dSpeedMax.value
            dWorkspeedXpYp.value =  Material_Tool_0_dSpeedMax.value
            dWorkspeedXpYm.value =  Material_Tool_0_dSpeedMax.value
            dWorkspeedXmYp.value =  Material_Tool_0_dSpeedMax.value
            dWorkspeedXmYm.value =  Material_Tool_0_dSpeedMax.value
        }
    }


    function changeLaserPowerInputField(bGalvo){
        if(bGalvo) {
            textfield_laser_power.unit = "W"
            textfield_laser_power.ioFactor = LaserPowerControl_dLaserUnitPower.value
            textfield_laser_power.digitsAfterComma = 0
            textfield_laser_power.validator = galvoPowerValidator
        }
        else
        {
            textfield_laser_power.unit = "%"
            textfield_laser_power.ioFactor =  100
            textfield_laser_power.digitsAfterComma = 0
            textfield_laser_power.validator = percentValidator
        }
    }

    function adjustSpeed(bIncrease){
        var materialItem1 = screenTool

        var dWorkspeedXp    = eval(materialItem1.dWorkspeedXp_io)
        var dWorkspeedXm    = eval(materialItem1.dWorkspeedXm_io)
        var dWorkspeedYp    = eval(materialItem1.dWorkspeedYp_io)
        var dWorkspeedYm    = eval(materialItem1.dWorkspeedYm_io)
        var dWorkspeedXpYp  = eval(materialItem1.dWorkspeedXpYp_io)
        var dWorkspeedXpYm  = eval(materialItem1.dWorkspeedXpYm_io)
        var dWorkspeedXmYp  = eval(materialItem1.dWorkspeedXmYp_io)
        var dWorkspeedXmYm  = eval(materialItem1.dWorkspeedXmYm_io)

        var num = new Number(materialItem1.dSpeedAdjust)/dMPSToMPM.value

        if(bIncrease){
            if(toggle_Xp.__pressed)   dWorkspeedXp.value += num
            if(toggle_Xm.__pressed)   dWorkspeedXm.value += num
            if(toggle_Yp.__pressed)   dWorkspeedYp.value += num
            if(toggle_Ym.__pressed)   dWorkspeedYm.value += num
            if(!bFourAxisMaterial){
                if(toggle_XpYp.__pressed) dWorkspeedXpYp.value += num
                if(toggle_XpYm.__pressed) dWorkspeedXpYm.value += num
                if(toggle_XmYp.__pressed) dWorkspeedXmYp.value += num
                if(toggle_XmYm.__pressed) dWorkspeedXmYm.value += num
            }
            else
            {
                adjust4Speed()
            }
        }
        else
        {
            if(toggle_Xp.__pressed)  { if( num < dWorkspeedXp.value  ){ dWorkspeedXp.value   -= num} else dWorkspeedXp.value   = 0}
            if(toggle_Xm.__pressed)  { if( num < dWorkspeedXm.value  ){ dWorkspeedXm.value   -= num} else dWorkspeedXm.value   = 0}
            if(toggle_Yp.__pressed)  { if( num < dWorkspeedYp.value  ){ dWorkspeedYp.value   -= num} else dWorkspeedYp.value   = 0}
            if(toggle_Ym.__pressed)  { if( num < dWorkspeedYm.value  ){ dWorkspeedYm.value   -= num} else dWorkspeedYm.value   = 0}
            if(!bFourAxisMaterial){
                if(toggle_XpYp.__pressed){ if( num < dWorkspeedXpYp.value){ dWorkspeedXpYp.value -= num} else dWorkspeedXpYp.value = 0}
                if(toggle_XpYm.__pressed){ if( num < dWorkspeedXpYm.value){ dWorkspeedXpYm.value -= num} else dWorkspeedXpYm.value = 0}
                if(toggle_XmYp.__pressed){ if( num < dWorkspeedXmYp.value){ dWorkspeedXmYp.value -= num} else dWorkspeedXmYp.value = 0}
                if(toggle_XmYm.__pressed){ if( num < dWorkspeedXmYm.value){ dWorkspeedXmYm.value -= num} else dWorkspeedXmYm.value = 0}
            }
            else
            {
                adjust4Speed()
            }

        }
        isMaterialDirty = true

    }

    function adjust4Speed()
    {
        if(bFourAxisMaterial)
        {
            MF.pause(100)
            var materialItem1 = screenTool

            var dWorkspeedXp    = eval(materialItem1.dWorkspeedXp_io)
            var dWorkspeedXm    = eval(materialItem1.dWorkspeedXm_io)
            var dWorkspeedYp    = eval(materialItem1.dWorkspeedYp_io)
            var dWorkspeedYm    = eval(materialItem1.dWorkspeedYm_io)
            var dWorkspeedXpYp  = eval(materialItem1.dWorkspeedXpYp_io)
            var dWorkspeedXpYm  = eval(materialItem1.dWorkspeedXpYm_io)
            var dWorkspeedXmYp  = eval(materialItem1.dWorkspeedXmYp_io)
            var dWorkspeedXmYm  = eval(materialItem1.dWorkspeedXmYm_io)

            dWorkspeedXpYp.value = (dWorkspeedXp.value + dWorkspeedYp.value) / 2.0
            dWorkspeedXpYm.value = (dWorkspeedXp.value + dWorkspeedYm.value) / 2.0
            dWorkspeedXmYp.value = (dWorkspeedXm.value + dWorkspeedYp.value) / 2.0
            dWorkspeedXmYm.value = (dWorkspeedXm.value + dWorkspeedYm.value) / 2.0
        }
    }

    function adjustHeight(bIncrease){
        var materialItem1 = screenTool

        var dHeightXp    = eval(materialItem1.dHeightXp_io)
        var dHeightXm    = eval(materialItem1.dHeightXm_io)
        var dHeightYp    = eval(materialItem1.dHeightYp_io)
        var dHeightYm    = eval(materialItem1.dHeightYm_io)
        var dHeightXpYp  = eval(materialItem1.dHeightXpYp_io)
        var dHeightXpYm  = eval(materialItem1.dHeightXpYm_io)
        var dHeightXmYp  = eval(materialItem1.dHeightXmYp_io)
        var dHeightXmYm  = eval(materialItem1.dHeightXmYm_io)

        var num = new Number(materialItem1.dHeightAdjust)/dMToMM.value

        if(bIncrease){
            if(toggle_Xp.__pressed)   dHeightXp.value += num
            if(toggle_Xm.__pressed)   dHeightXm.value += num
            if(toggle_Yp.__pressed)   dHeightYp.value += num
            if(toggle_Ym.__pressed)   dHeightYm.value += num

            if(!bFourAxisMaterial){
                if(toggle_XpYp.__pressed) dHeightXpYp.value += num
                if(toggle_XpYm.__pressed) dHeightXpYm.value += num
                if(toggle_XmYp.__pressed) dHeightXmYp.value += num
                if(toggle_XmYm.__pressed) dHeightXmYm.value += num
            }
            else
            {
                adjust4Height()
            }
        }
        else
        {
            if(toggle_Xp.__pressed)  { if( num < dHeightXp.value  ){ dHeightXp.value   -= num} else dHeightXp.value   = 0}
            if(toggle_Xm.__pressed)  { if( num < dHeightXm.value  ){ dHeightXm.value   -= num} else dHeightXm.value   = 0}
            if(toggle_Yp.__pressed)  { if( num < dHeightYp.value  ){ dHeightYp.value   -= num} else dHeightYp.value   = 0}
            if(toggle_Ym.__pressed)  { if( num < dHeightYm.value  ){ dHeightYm.value   -= num} else dHeightYm.value   = 0}
            if(!bFourAxisMaterial){
                if(toggle_XpYp.__pressed){ if( num < dHeightXpYp.value){ dHeightXpYp.value -= num} else dHeightXpYp.value = 0}
                if(toggle_XpYm.__pressed){ if( num < dHeightXpYm.value){ dHeightXpYm.value -= num} else dHeightXpYm.value = 0}
                if(toggle_XmYp.__pressed){ if( num < dHeightXmYp.value){ dHeightXmYp.value -= num} else dHeightXmYp.value = 0}
                if(toggle_XmYm.__pressed){ if( num < dHeightXmYm.value){ dHeightXmYm.value -= num} else dHeightXmYm.value = 0}
            }
            else
            {
                adjust4Height()
            }
        }
        isMaterialDirty = true
    }

    function adjust4Height()
    {
        if(bFourAxisMaterial)
        {
            MF.pause(100)
            var materialItem1 = screenTool

            var dHeightXp    = eval(materialItem1.dHeightXp_io)
            var dHeightXm    = eval(materialItem1.dHeightXm_io)
            var dHeightYp    = eval(materialItem1.dHeightYp_io)
            var dHeightYm    = eval(materialItem1.dHeightYm_io)
            var dHeightXpYp  = eval(materialItem1.dHeightXpYp_io)
            var dHeightXpYm  = eval(materialItem1.dHeightXpYm_io)
            var dHeightXmYp  = eval(materialItem1.dHeightXmYp_io)
            var dHeightXmYm  = eval(materialItem1.dHeightXmYm_io)

            dHeightXpYp.value = (dHeightXp.value + dHeightYp.value) / 2.0
            dHeightXpYm.value = (dHeightXp.value + dHeightYm.value) / 2.0
            dHeightXmYp.value = (dHeightXm.value + dHeightYp.value) / 2.0
            dHeightXmYm.value = (dHeightXm.value + dHeightYm.value) / 2.0

            //console.log("adjust4Height:", "dHeightXp:", dHeightXp.value, "dHeightXm:", dHeightXm.value, "dHeightYp:", dHeightYp.value, "dHeightYm:", dHeightYm.value )
        }
    }

    function selectAll(bSelect){
        toggle_Xp.__pressed = bSelect
        toggle_Xm.__pressed = bSelect
        toggle_Yp.__pressed = bSelect
        toggle_Ym.__pressed = bSelect
        if(!bFourAxisMaterial){
            toggle_XpYp.__pressed = bSelect
            toggle_XpYm.__pressed = bSelect
            toggle_XmYp.__pressed = bSelect
            toggle_XmYm.__pressed = bSelect
        }
    }

    function setStartMacroFrequency(strDebug){
        //bug: this calulation value is not always applied although the parameters are good
        if( eval(dStartMacroTime_io) !== undefined){
            var dVal = eval(dStartMacroTime_io).value
            if(dVal !== 0.0){
                var dStartMacroLsrFrequency =  eval(dStartMacroLsrFrequency_io)
                dStartMacroLsrFrequency.value = Material_iStartMacroLsrImpulses.value / dVal
                //console.log( "setStartMacroFrequency()", "strDebug", strDebug, screenTool.iToolType_io, "dStartMacroLsrFrequency.value:", dStartMacroLsrFrequency.value, "dVal:", dVal )
            }
        } else
            console.log( "setStartMacroFrequency() else", "strDebug", strDebug, screenTool.iToolType_io, "eval(dStartMacroLsrFrequency_io).value:", eval(dStartMacroLsrFrequency_io).value, "dVal:", dVal )
        isMaterialDirty = true
    }

    function setiStartMacroLsrImpulses(){
        if( eval(dStartMacroTime_io) !== undefined){
            Material_iStartMacroLsrImpulses.value = eval(dStartMacroLsrFrequency_io).value * eval(dStartMacroTime_io).value
        }
        else
        {
            Material_iStartMacroLsrImpulses.value = 0
        }
        //console.log( "setiStartMacroLsrImpulses()", screenTool.iToolType_io, "Material_iStartMacroLsrImpulses:", Material_iStartMacroLsrImpulses.value,  "eval(dStartMacroLsrFrequency_io).value:", eval(dStartMacroLsrFrequency_io).value, "eval(dStartMacroTime_io).value:",  eval(dStartMacroTime_io).value)
        isMaterialDirty = true
    }

    function setStopMacroFrequency(){
        if( eval(dStopMacroTime_io) !== undefined){
            var dVal = eval(dStopMacroTime_io).value
            if(dVal !== 0.0){
                var dStopMacroTime = eval(dStopMacroLsrFrequency_io)
                dStopMacroTime.value = Material_iStopMacroLsrImpulses.value / dVal
            }
        }
        //console.log( "setStopMacroFrequency()", screenTool.iToolType_io)
        isMaterialDirty = true
    }

    function setiStopMacroLsrImpulses(){
        if( eval(dStopMacroTime_io) !== undefined){
            Material_iStopMacroLsrImpulses.value = eval(dStopMacroLsrFrequency_io).value * eval(dStopMacroTime_io).value
        }
        else
        {
            Material_iStopMacroLsrImpulses.value = 0
        }

        //console.log( "setiStopMacroLsrImpulses()", screenTool.iToolType_io,  " Material_iStopMacroLsrImpulses:", Material_iStopMacroLsrImpulses.value, "dStopMacroLsrFrequency_io", eval(dStopMacroLsrFrequency_io).value, "dStopMacroTime_io", eval(dStopMacroTime_io).value)
        isMaterialDirty = true
    }


    Component.onCompleted: {
        piToolTypeValue()
        piControlTypeValue()
    }

    signal toolNumberChanged()

    onTool_numberChanged: {
        //console.log("1 ------------------ onTool_numberChanged: -------------------------------- tool_number:", tool_number, "screenTool.iToolType_io", "Material_iStartMacroLsrImpulses.value", screenTool.iToolType_io.value )
        //set the leaving tool frequency here because of the setStopMacroFrequency() not always works
        setiStartMacroLsrImpulses()
        setiStopMacroLsrImpulses()
        screenTool.bEngravingTool_io            = "Material_Tool_" + tool_number + "_bEngravingTool"
        screenTool.iToolType_io                 = "Material_Tool_" + tool_number + "_iToolType"
        screenTool.iControlType_io              = "Material_Tool_" + tool_number + "_iControlType"
        screenTool.iPocketType_io               = "Material_Tool_" + tool_number + "_iPocketType"
        screenTool.dAccLsrPulseWidth_io         = "Material_Tool_" + tool_number + "_dAccLsrPulseWidth"
        screenTool.dConstLsrPulseWidth_io       = "Material_Tool_" + tool_number + "_dConstLsrPulseWidth"
        screenTool.bPulseOn_io                  = "Material_Tool_" + tool_number + "_bPulseOn"
        screenTool.dStartMacroTime_io           = "Material_Tool_" + tool_number + "_dStartMacroTime"
        screenTool.dStartMacroLsrFrequency_io   = "Material_Tool_" + tool_number + "_dStartMacroLsrFrequency"
        screenTool.dStopMacroTime_io            = "Material_Tool_" + tool_number + "_dStopMacroTime"
        screenTool.dStopMacroLsrFrequency_io    = "Material_Tool_" + tool_number + "_dStopMacroLsrFrequency"
        screenTool.dConstLsrFrequency_io        = "Material_Tool_" + tool_number + "_dConstLsrFrequency"
        screenTool.dLsrSpotDistance_io          = "Material_Tool_" + tool_number + "_dLsrSpotDistance"
        screenTool.dLsrAnalogPower_io           = "Material_Tool_" + tool_number + "_dLsrAnalogPower"

        screenTool.dAnalogOutput_io             = "Material_Tool_" + tool_number + "_dAnalogOutput"

        screenTool.constPulseWidth      = eval(screenTool.dConstLsrPulseWidth_io).value

        if(constPulseWidth < 0.99){
            //togglebutton_pulse.state = "pressed"
            eval(screenTool.bPulseOn_io).value = true
        }else{
            //togglebutton_pulse.state = "notpressed"
            eval(screenTool.bPulseOn_io).value = false
        }
        //console.log("------------------ onTool_numberChanged: -------------------------------- tool_number:", tool_number, screenTool.iControlType_io, "screenTool.constPulseWidth", screenTool.constPulseWidth )

        if(tool_number <= 20){
            //dropdown_type.textitems = ["Kerf", "Etch", "Galvo", "Envelope"]
			dropdown_type.textitems = [db.dbTr("Kerf") + db.tr, db.dbTr("Etch") + db.tr, db.dbTr("Galvo") + db.tr, db.dbTr("Envelope") + db.tr]
            dropdown_type.valuitems = [0, 1, 2, 3]
        }
        else
        {
            dropdown_type.textitems = ["Channel", "Contour in", "Contour out", "Pocket"]
            dropdown_type.valuitems = [4, 5, 6, 7]
        }

        piToolType = piToolTypeValue()
        piToolTypeCanged()
        dropdown_type.displaytext = dropdown_type.displayText()

        piControlType = piControlTypeValue()
        piControlTypeChanged()
        //set the current tool
        setiStartMacroLsrImpulses()
        setiStopMacroLsrImpulses()
        //console.log("2 ------------------ onTool_numberChanged: -------------------------------- tool_number:", tool_number, "screenTool.iToolType_io", "Material_iStartMacroLsrImpulses.value", screenTool.iToolType_io.value )
        toolNumberChanged()
    }


    function piToolTypeCanged(){
        //console.log("1 ------------------ onpiToolTypeChanged(): -------------------------------- tool_number:", tool_number, screenTool.state)
        switch(piToolType){
        case 0:
            if(tool_number <= 20)
                screenTool.state = "LASERTOOL"
            else
                screenTool.state = "MILLINGTOOL"
            break;
        case 1:
            screenTool.state = "LASERTOOL"
            break;
        case 2:
            screenTool.state = "LAS_GALVO"
            break;
        case 3:
            screenTool.state = "LAS_ENVELOPE"
            break;
        case 4:
            screenTool.state = "MILLINGTOOL"
            break;
        case 5:
            screenTool.state = "MILLINGTOOL"
            break;
        case 6:
            screenTool.state = "MILLINGTOOL"
            break;
        case 7:
            screenTool.state = "MIL_POCKET"
            break;
        }
        //console.log("2 ------------------ onpiToolTypeChanged(): -------------------------------- tool_number:", tool_number, screenTool.state)
    }


    function iTypeValue(){
        var iType = eval(screenTool.iType_io)
        return iType.value
    }

    function piControlTypeValue(){
        var iControlType = eval(screenTool.iControlType_io)
        return iControlType.value
    }
/*
    function piControlTypeChanged(){

    }
*/
    function piToolTypeValue(){
        var iToolType = eval(screenTool.iToolType_io)
        //console.log("--------------- function piToolTypeValue() -----------------: ", iToolType.value, screenTool.iToolType_io)
        return iToolType.value
    }

    property real constPulseWidth: eval(dConstLsrPulseWidth_io).value
    /*
    onConstPulseWidthChanged: {
        console.log("--------------- onConstPulseWidthChanged -----------------: ", constPulseWidth)
        if(constPulseWidth < 0.99)
            togglebutton_pulse.state = "pressed"
        else
            togglebutton_pulse.state = "notpressed"
    }
*/


    Row{
        id: row1
        x: 20
        y: 83
        height: parent.height
        width: parent.width
        spacing: rowSpacing

        Item{
            id: col1
            height: parent.height
            width: columnWidth

            Label{
                id:label_tool_number
                height: labelHeight
                text: db.dbTr("Tool number") + db.tr
                width:parent.width
                colorChoice: 1
            }

            Button{
                id: button_tool_number
                y: labelHeight
                width: parent.width
                height: 43
                text: tool_number
                property bool bClicked
                onClicked:{
                    bClicked = true
                    dlgToolChooser.open()
                }
                /*
                //TODO: main.dlgToolChooser
                Connections {
                    target: dlgToolChooser
                    onToolChosen:{
                        if(button_tool_number.bClicked){
                            button_tool_number.bClicked = false
                            screenTool.tool_number = dlgToolChooser.dlg_tool_number
                        }
                    }
                }
                */
            }

        }

        Item{
            id: col2
            height: parent.height
            width: columnWidth

            Label{
                id: label_type
                height: labelHeight
                text: db.dbTr("Type") + db.tr
                width:parent.width
                colorChoice: 1
            }

            DropDown{
                id: dropdown_type
                y: labelHeight
                width: parent.width
                height: 43
                //textitems: ["Kerf", "Etch", "Galvo", "Envelope"]
				textitems: [db.dbTr("Kerf") + db.tr, db.dbTr("Etch") + db.tr, db.dbTr("Galvo") + db.tr, db.dbTr("Envelope") + db.tr]
                valuitems: [0, 1, 2, 3]
                textIO: iToolType_io
                onValueItemChanged: {
                    screenTool.piToolType = valueItem
                    var bEngraving = eval(bEngravingTool_io)
                    if(bEngraving !== undefined){
                        if(valueItem === 2){
                            bEngraving.value = true
                            Laser_bDynamicFrequency.value = true
                        }else
                            bEngraving.value = false
                    }
                }
                onIsDirtyChanged: if(isDirty)isMaterialDirty = true
                valueItem: piToolTypeValue()
                displaytext: displayText() // setTextItem()//textitems[piToolType]

                function displayText(){
                    if((tool_number > 20) && (valueItem < 4)){
                        return valueItem.toString()
                    }
                    if((tool_number <= 20) && (valueItem > 3)){
                        return valueItem.toString()
                    }
                    return setTextItem()
                }
            }
        }

        Item{
            id: col3
            height: parent.height
            width: columnWidth
            Label{
                height: labelHeight
                text: db.dbTr("Name") + db.tr
                width:parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_name
                y: labelHeight
                height:43
                //width: columnWidth*2 + columnSpacing
                width: columnWidth
                textIO: strNote_io
                //fontColorChoice: 1
                onAccepted: isMaterialDirty = true
            }

        }
        //laser tool parameters
        Item{
            id: las_row1_col4
            height: parent.height
            width: columnWidth

            Label{
                id: label_acceleration
                height: labelHeight
                text: db.dbTr("Acceleration") + db.tr
                width: parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_acceleration
                y: labelHeight
                height:43
                width: parent.width
                textIO: dAccelerationMax_io
				unitEnable: true
				unit: strUnitAcc.value
                //ioFactor: dMToMM.value
                //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: speedValidator
                onAccepted: isMaterialDirty = true
            }
        }
        //milling tool parameters
        Item{
            id: mil_row1_col4
            height: parent.height
            width: columnWidth
            visible: false

            Label{
                id: label_depth
                height: labelHeight
                text: db.dbTr("Depth") + db.tr
                width: parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_depth
                y: labelHeight
                height:43
                width: parent.width
                //textIO: dDistanceDepth_io
                textIO: dWorkHeight_io
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                ioFactor: dMToMM.value
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: speedValidator
                onAccepted:  isMaterialDirty = true
            }
        }

        //milling tool parameters
        Item{
            id: mil_row1_col5
            height: parent.height
            width: columnWidth
            visible: false

            Label{
                id: label_width
                height: labelHeight
                text: db.dbTr("Width") + db.tr
                width: parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_width
                y: labelHeight
                height:43
                width: parent.width
                textIO: dChannelWidth_io
                //Added Unit, SB
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                ioFactor: dMToMM.value
                substituteCommaWithPoint: true
                horizontalTextAlignement: 2
                validator: speedValidator
                onAccepted: isMaterialDirty = true
            }
        }

        Item{
            id: mil_row1_col6
            width: columnWidth
            height: parent.height
            visible: false

            ToggleButtonFlat{
                id: togglebutton_lubricaton
                width: parent.width
                height: 70
                text: db.dbTr("Lubrication") + db.tr
                //TODO: IO ??
                onClicked: isMaterialDirty = true
            }
        }

        Item{
            id: las_row1_col5
            height: parent.height
            width: columnWidth

            Label{
                height: labelHeight
                text: db.dbTr("Max. deviation") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_max_deviation
                y: labelHeight
                height:43
                width: parent.width
                textIO: dMaxDeviation_io
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                ioFactor: dMToMM.value
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: heightValidator
                onAccepted: isMaterialDirty = true
            }

        }
        Item{
            id: las_row1_col6
            height: parent.height
            width: columnWidth
            visible: false

            Label{
                id: label_offset
                height: labelHeight
                text: db.dbTr("Offset") + db.tr
                width:parent.width
                colorChoice: 1
            }
            //TODO: offset IO???
            TextField{
                id: textfield_offset
                y: label_offset.y + label_offset.height
                height:43
                width: parent.width
                textIO: dEnvelopeCut_io
                ioFactor: dMToMM.value
                horizontalTextAlignement: 2
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                substituteCommaWithPoint: true
                validator: heightValidator
                onAccepted: isMaterialDirty = true
            }

        }
    }



    Row{
        id: las_row2
        width: 1216
        height: 83
        spacing: rowSpacing
        x: 20
        y: 180

        Item{
            id: row2_col1
            width: columnWidth
            height: parent.height

            Label{
                id:label_power
                height: labelHeight
                text: db.dbTr("Analog Power") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_laser_power
                y: label_power.y + label_power.height
                height:43
                width: parent.width
                textIO: dLsrAnalogPower_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: percentValidator
                onAccepted: isMaterialDirty = true
            }

        }
        Item{
            id: row2_col2
            width: columnWidth
            height: parent.height

            Label{
                id:label_power_factor
                height: labelHeight
                text: db.dbTr("Power Factor") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_power_factor
                y: label_power_factor.y + label_power_factor.height
                height:43
                width: parent.width
				//changed calculation to display minimum power
                textIO: dLsrFactorAnalogPower_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top: 100; bottom: 0}
                onAccepted: isMaterialDirty = true
            }
        }

        Item{
            id: row2_col3
            width: columnWidth
            height: parent.height
            Label{
                id: label_power_exponent
                height: labelHeight
                text: db.dbTr("Power Exponent") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_power_exponent
                y: label_power_exponent.y  + label_power_exponent.height
                height:43
                width: parent.width
                textIO: dLsrExponentAnalogPower_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top: 150; bottom: 0}
                onAccepted: isMaterialDirty = true
            }
        }
        Item{
            id: row2_col4
            width: columnWidth
            height: parent.height
            ToggleButtonFlat{
                id: togglebutton_pulse
                width: parent.width
                height: 70
                text: db.dbTr("Pulse") + db.tr
                stateIO: bPulseOn_io

                property bool bClicked: false

                Connections{
                    target: screenTool
                    onToolNumberChanged:{
                        togglebutton_pulse.bClicked = false
                    }
                }

                onClicked: {
                   bClicked = true
                   isMaterialDirty = true
                }

                onStateChanged: {
                    if(bClicked){
                        if(state === "pressed"){
                            eval(dConstLsrPulseWidth_io).value = 0.85
                        }else{
                            eval(dConstLsrPulseWidth_io).value = 1
                        }
                    }
                }
            }
        }
        Item{
            id: row2_col5_pulse_length
            width: columnWidth
            height: parent.height
            visible: eval(bPulseOn_io).value && (state !== "LAS_GALVO")
            Label{
                id: label_pulse_length
                height: labelHeight
                text: db.dbTr("Pulse length") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_pulse_length
                y: label_pulse_length.y + label_pulse_length.height
                height:43
                width: parent.width
                textIO: dLsrSpotDistance_io
                ioFactor: dMToMM.value
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                substituteCommaWithPoint: true
                horizontalTextAlignement: 2
                validator: regValidator5
                onAccepted: isMaterialDirty = true

            }
        }
        Item{
            id: row2_col5_Frequency
            width: columnWidth
            height: parent.height
            visible: (state === "LAS_GALVO")
            Label{
                id: label_Frequency
                height: labelHeight
                text: db.dbTr("Frequency") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_Frequency
                y: label_pulse_length.y + label_pulse_length.height
                height:43
                width: parent.width
                textIO: dConstLsrFrequency_io
                ioFactor: 1
                unitEnable: true
                unit: "Hz"
                substituteCommaWithPoint: true
                horizontalTextAlignement: 2
                digitsAfterComma: 0
                validator: intValidator
                onAccepted: isMaterialDirty = true

            }
        }

        Item{
            id: row2_col6
            width: columnWidth
            height: parent.height
            visible: eval(bPulseOn_io).value
            Label{
                id: label_pulse_width
                height: labelHeight
                text: db.dbTr("Pulse width") + db.tr
                width:parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_pulse_width
                y: label_pulse_width.y + label_pulse_width.height
                width:parent.width
                height: 43
                textIO:dConstLsrPulseWidth_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top:100;bottom:0}
                onAccepted: isMaterialDirty = true
            }
        }
    }

    Row{
        id: mil_row2
        width: 1216
        height: 83
        spacing: rowSpacing
        x: 20
        y: 180
        visible: false
        Item{
            id: mil_row2_col1
            width: columnWidth
            height: parent.height

            Label{
                id:label_geometry
                height: labelHeight
                text: db.dbTr("Geometry") + db.tr
                width:parent.width
                colorChoice: 1
            }

            DropDown2{
                id: dropdown_geometry
                y: labelHeight
                width: parent.width
                height: 43
                imageitems: [imagePath + "ToolButtonADown.png",
                    imagePath + "ToolButtonBDown.png",
                    imagePath + "ToolButtonCDown.png",
                    imagePath + "ToolButtonDDown.png",
                    imagePath + "ToolButtonEDown.png"
                ]
                textitems: ["A", "B", "C", "D", "E"]
                //textitems: [" ", " ", " ", " ", " "]
                valuitems: [0, 1, 2, 3, 4]
                textIO: iType_io
                valueItem: iTypeValue()
                onIsDirtyChanged: if(isDirty) isMaterialDirty = true
            }
        }

        Item{
            id: mil_row2_col2
            width: columnWidth
            height: parent.height

            Label{
                id:label_diameter
                height: labelHeight
                text: db.dbTr("Diameter") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_diameter
                width:parent.width
                y: label_marking_speed.y + label_marking_speed.height
                height: 43
                textIO: dDiameter_io
                //Changed units, SB
                //ioFactor: dMMToM.value
                //unitEnable: true
                //unit: strUnitMM.value
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                ioFactor: dMToMM.value
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: speedValidator
                onAccepted: isMaterialDirty = true
            }
        }


        Item{
            id: mil_row2_col3
            width: columnWidth
            height: parent.height

            Label{
                id:label_edges
                height: labelHeight
                text: db.dbTr("Edges") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_edges
                width:parent.width
                y: label_marking_speed.y + label_marking_speed.height
                height: 43
                //iEdges is missing in this version
                //textIO: "dDiameter_io"
                //ioFactor: dMToMM.value
                //unitEnable: true
                //unit: strUnitMM.value
                horizontalTextAlignement: 2
                onAccepted: isMaterialDirty = true
            }
        }

        Item{
            id: mil_row2_col4
            width: columnWidth
            height: parent.height

            Label{
                id:label_angle
                height: labelHeight
                text: db.dbTr("Angle") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_angle
                width:parent.width
                y: label_marking_speed.y + label_marking_speed.height
                height: 43
                textIO: dAngle_io
                unitEnable: true
                unit: "°"
                horizontalTextAlignement: 2
                onAccepted: isMaterialDirty = true
            }
        }

    }

    Column{
        id: mil_colum3
        width: 1216
        height: 383
        spacing: columnSpacing
        x:20
        y: 280
        visible: false

        Row{
            id: mil_row3_col1
            width: columnWidth
            height: 43
            spacing: rowSpacing


            TextField{
                id:label_spindle_speed
                height: parent.height
                text: db.dbTr("Spindle speed") + db.tr
                width:parent.width
                readOnly: true
            }

            TextField{
                id: textfield_spindle_speed
                width:parent.width
                height: parent.height
                textIO: dSpindleSpeed_io
                unitEnable: true
                unit: "rpm"
                ioFactor: 60
                digitsAfterComma: 0
                horizontalTextAlignement: 2
                validator: IntValidator{}
                onAccepted: isMaterialDirty = true
            }
        }

        Row{
            id: mil_row3_col2
            width: columnWidth
            height: 43
            spacing: rowSpacing

            TextField{
                id:label_mil_acceleration
                height: parent.height
                text: db.dbTr("Acceleration") + db.tr
                width:parent.width
                readOnly: true
            }

            TextField{
                id: textfield_mil_acceleration
                width:parent.width
                height: parent.height
                textIO: dAccelerationMax_io
                unitEnable: true
                unit: strUnitAcc.value
                //ioFactor: dMToMM.value
                //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: speedValidator
                onAccepted: isMaterialDirty = true
            }
        }

        Row{
            id: mil_row3_col3
            width: columnWidth
            height: 43
            spacing: rowSpacing

            TextField{
                id:label_mil_feedrate
                height: parent.height
                text: db.dbTr("Feedrate") + db.tr
                width:parent.width
                readOnly: true
            }

            TextField{
                id: textfield_mil_feedrate
                width:parent.width
                height: parent.height
                //No textIO
                textIO:dSpeedMax_io
                unitEnable: true
                unit: strUnitSpeed.value
                ioFactor:  dMPSToMPM.value
                //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: IntValidator{}
                onAccepted: isMaterialDirty = true
            }
        }

        Row{
            id: mil_row3_col4
            width: columnWidth
            height: 43
            spacing: rowSpacing
            TextField{
                id:label_mil_piercingspeed
                height: parent.height
                text: db.dbTr("Piercing speed") + db.tr
                width:parent.width
                readOnly: true
            }

            TextField{
                id: textfield_mil_piercingspeed
                width:parent.width
                height: parent.height
                textIO: dPiercingSpeed_io
                unitEnable: true
                unit: strUnitSpeed.value
                ioFactor:  dMPSToMPM.value
                digitsAfterComma: 3
                horizontalTextAlignement: 2
                onAccepted: isMaterialDirty = true
                validator: heightValidator
            }
        }

        Row{
            id: mil_row3_col5
            width: columnWidth
            height: 43
            spacing: rowSpacing

            TextField{
                id:label_mil_infeed
                height: parent.height
                text: db.dbTr("Infeed") + db.tr
                width:parent.width
                readOnly: true
            }

            TextField{
                id: textfield_mil_infeed
                width:parent.width
                height: parent.height
                textIO: dDistanceDepth_io
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                ioFactor:  dMToMM.value
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: heightValidator
                onAccepted: isMaterialDirty = true
            }
        }

        Row{
            id: mil_row3_col6
            width: columnWidth
            height: 43
            spacing: rowSpacing

            TextField{
                id:label_mil_stepover
                height: parent.height
                text: db.dbTr("Stepover") + db.tr
                width:parent.width
                readOnly: true
            }

            TextField{
                id: textfield_mil_stepover
                width:parent.width
                height: parent.height
                //dStepover is missing in this version
                //textIO:
                unitEnable: true
                unit: strUnitMM.value
				//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
				digitsAfterComma: bUseInches.value ? 3 : 2
                ioFactor:  dMToMM.value
                horizontalTextAlignement: 2
                onAccepted: isMaterialDirty = true
            }
        }
    }

    Row{
        id: las_galvo_row3
        width: 1216
        height: 383
        spacing: rowSpacing
        x:20
        y: 280
        visible: false

        Item{
            id: las_galvo_col1
            width: columnWidth
            height: parent.height

            Label{
                id:label_marking_speed
                height: labelHeight
                text: db.dbTr("Marking speed") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_marking_speed
                width:parent.width
                y: label_marking_speed.y + label_marking_speed.height
                height: 43
                //textIO: "Material_dMarkSpeed"
                textIO: dAnalogOutput_io
                ioFactor: dMPSToMPM.value
                unit: strUnitSpeed.value
                unitEnable: true
                digitsAfterComma: 2
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: speedValidator
                onAccepted: isMaterialDirty = true

            }

            Label{
                id:label_working_height
                height: labelHeight
                y: textfield_marking_speed.y + textfield_marking_speed.height + columnSpacing
                text: db.dbTr("Work height") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_working_height
                width:parent.width
                height: 43
                y: label_working_height.y + label_working_height.height
                textIO:dHeightXp_io
                ioFactor: dMToMM.value
                digitsAfterComma: 3
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                validator: heightValidator
                onAccepted: {
                    console.log("------------------------- onAccepted dHeightXp_io")
                    adjust4Height()
                    isMaterialDirty = true
                }
            }

        }

        Item {
            id: las_galvo_col2
            width: columnWidth
            height: parent.height


            Label{
                id:label_jump_speed
                height: labelHeight
                text: db.dbTr("Jump speed") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_jump_speed
                width:parent.width
                height: 43
                y: label_jump_speed.y + label_jump_speed.height
                textIO: "ScanLab_In_dJumpSpeed"
                ioFactor: dMPSToMPM.value
                unit: strUnitSpeed.value
                unitEnable: true
                digitsAfterComma: 2
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                onAccepted: isMaterialDirty = true
            }

        }

        Item {
            id: las_galvo_col3
            width: columnWidth
            height: parent.height

            Label{
                id:label_displacement
                height: labelHeight
                text: db.dbTr("Displacement") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_displacement
                width:parent.width
                height: 43
                y: label_displacement.y + label_displacement.height
                textIO: "Material_Tool_0_dSpeedMax"
                ioFactor: dMPSToMPM.value
                unit: strUnitSpeed.value
                unitEnable: true
                digitsAfterComma: 2
                horizontalTextAlignement: 2
                substituteCommaWithPoint: true
                onAccepted: isMaterialDirty = true
            }

        }

    }


    Row{
        id: laser_tools_row
        width: 1216
        height: 380
        spacing: rowSpacing
        x: 20
        y: 280

        Item{
            id: lasertool_col1
            width: columnWidth
            height: parent.height

            Label{
                id:label_piercing
                height: labelHeight
                text: db.dbTr("Piercing") + db.tr
                width:parent.width
                colorChoice: 1
            }
            TextField{
                id: label_start
                y: label_piercing.y + label_piercing.height
                height:43
                width: parent.width
                readOnly: true
                text: db.dbTr("Start") + db.tr
            }
            TextField{
                id: label_end
                y: label_start.y + label_start.height + columnSpacing
                height:43
                width: parent.width
                readOnly: true
                text: db.dbTr("End") + db.tr
            }
            TextField{
                id: label_axis
                y: label_end.y + label_end.height + columnSpacing
                height:43
                width: parent.width
                readOnly: true
                text: db.dbTr("Axis") + db.tr
            }
            TextField{
                id: label_height
                y: label_axis.y + label_axis.height + columnSpacing
                height:43
                width: parent.width
                readOnly: true
                text: db.dbTr("Work height") + db.tr
            }
            TextField{
                id: label_speed
                y: label_height.y + label_height.height + columnSpacing
                height:43
                width: parent.width
                readOnly: true
                text: db.dbTr("Speed") + db.tr
            }

        }
        Item{
            id: lasertool_col2
            width: columnWidth
            height: parent.height

            Label{
                id:label_time
                height: labelHeight
                text: db.dbTr("Time") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_time_start
                y: label_time.y + label_time.height
                height:43
                width: parent.width
                textIO: dStartMacroTime_io
                unitEnable: true
                unit: "ms"
                ioFactor: 1000
                digitsAfterComma: 0
                validator: IntValidator{}
                horizontalTextAlignement: 2
                onFocusChanged: {
                    if(!focus) setStartMacroFrequency("dStartMacroTime_io")
                }
                onAccepted: isMaterialDirty = true

            }

            TextField{
                id: textfield_time_end
                y: textfield_time_start.y + textfield_time_start.height + columnSpacing
                height:43
                width: parent.width
                textIO: dStopMacroTime_io
                unitEnable: true
                unit: "ms"
                ioFactor: 1000
                digitsAfterComma: 0
                validator: IntValidator{}
                horizontalTextAlignement: 2
                onFocusChanged: {
                    if(!focus) setStopMacroFrequency()
                }
                onAccepted: isMaterialDirty = true
            }

            Row{
                width: parent.width
                spacing: 20
                anchors.top: textfield_time_end.bottom
                anchors.margins: columnSpacing
                Column{
                    id:col2_1
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_Xp
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"X+"
                    }

                    TextField{
                        id: textfield_Xp_height
                        width:parent.width
                        height: 43
                        textIO:dHeightXp_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: {
                            adjust4Height()
                            isMaterialDirty = true
                        }
                    }

                    TextField{
                        id: textfield_Xp_speed
                        width:parent.width
                        height: 43
                        textIO:dWorkspeedXp_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: {
                            adjust4Speed()
                            isMaterialDirty = true
                        }
                    }
                }

                Column{
                    id:col2_2
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_Yp
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"Y+"
                    }

                    TextField{
                        id: textfield_Yp_height
                        width:parent.width
                        height: 43
                        textIO:dHeightYp_io
                        ioFactor: dMToMM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: {
                            console.log("------ onAccepted: dHeightYp_io")
                            adjust4Height()
                            isMaterialDirty = true
                        }
                    }

                    TextField{
                        id: textfield_Yp_speed
                        width:parent.width
                        height: 43
                        textIO:dWorkspeedYp_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: {
                            adjust4Speed()
                            isMaterialDirty = true
                        }
                    }
                }
            }

        }
        Item{
            id: lasertool_col3
            width: columnWidth
            height: parent.height

            Label{
                id: label_impulses
                //y: textfield_power_exponent.y  + textfield_power_exponent.height + columnSpacing
                height: labelHeight
                text: db.dbTr("Impulses") + db.tr
                width:parent.width
                colorChoice: 1
            }
            //TODO: what is impulses???
            TextField{
                id: textfield_impulses_start
                y: label_impulses.y + label_impulses.height
                height:43
                width: parent.width
                //textIO: dStartMacroLsrFrequency_io
                textIO: "Material_iStartMacroLsrImpulses"

                digitsAfterComma: 0
                horizontalTextAlignement: 2
                validator: IntValidator{}

                onFocusChanged: {
                    if(!focus) setStartMacroFrequency("Material_iStartMacroLsrImpulses")
                }
                onAccepted: isMaterialDirty = true

            }
            TextField{
                id: textfield_impulses_end
                y: textfield_impulses_start.y + textfield_impulses_start.height + columnSpacing
                height:43
                width: parent.width
                //textIO: dStopMacroLsrFrequency_io
                textIO: "Material_iStopMacroLsrImpulses"
                unitEnable: true
                //unit: "Hz"
                digitsAfterComma: 0
                horizontalTextAlignement: 2
                validator: IntValidator{}

                onFocusChanged: {
                    if(!focus) setStopMacroFrequency()
                }
                onAccepted: isMaterialDirty = true

            }
            Row{
                width: parent.width
                spacing: 20
                anchors.top: textfield_impulses_end.bottom
                anchors.margins: columnSpacing
                Column{
                    id:col3_1
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_Xm
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"X-"
                    }

                    TextField{
                        id: textfield_Xm_height
                        width:parent.width
                        height: 43
                        textIO:dHeightXm_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: {
                            console.log("dHeightXm_io")
                            adjust4Height()
                            isMaterialDirty = true
                        }
                    }

                    TextField{
                        id: textfield_Xm_speed
                        width:parent.width
                        height: 43
                        textIO: dWorkspeedXm_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: {
                            adjust4Speed()
                            isMaterialDirty = true
                        }
                    }
                }

                Column{
                    id:col3_2
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_Ym
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"Y-"
                    }


                    TextField{
                        id: textfield_Ym_height
                        width:parent.width
                        height: 43
                        textIO: dHeightYm_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: {
                            console.log("dHeightYm_io")
                            adjust4Height()
                            isMaterialDirty = true
                        }
                    }

                    TextField{
                        id: textfield_Ym_speed
                        width:parent.width
                        height: 43
                        textIO:dWorkspeedYm_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: {
                            adjust4Speed()
                            isMaterialDirty = true
                        }
                    }
                }
            }

        }
        Item{
            id: lasertool_col4
            width: columnWidth
            height: parent.height

            Label{
                id: label_pulse_width_piercing
                //y: togglebutton_pulse.y + togglebutton_pulse.height + columnSpacing
                height: labelHeight
                text: db.dbTr("Pulse width") + db.tr
                width:parent.width
                colorChoice: 1
            }
            TextField{
                id: textfield_pulse_width_start
                y: label_pulse_width_piercing.y + label_pulse_width_piercing.height
                height:43
                width: parent.width
                textIO: dStartMacroLsrPulseWidth_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top:100;bottom:0}
                onAccepted: isMaterialDirty = true
            }

            TextField{
                id: textfield_pulse_width_end
                y: textfield_pulse_width_start.y + textfield_pulse_width_start.height + columnSpacing
                height:43
                width: parent.width
                textIO: dStopMacroLsrPulseWidth_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top:100;bottom:0}
                onAccepted: isMaterialDirty = true
            }

            Row{
                width: parent.width
                spacing: 20
                anchors.top: textfield_pulse_width_end.bottom
                anchors.margins: columnSpacing
                Column{
                    id:col4_1
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_XpYp
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"X+/Y+"
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XpYp_height
                        width:parent.width
                        height: 43
                        textIO:dHeightXpYp_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XpYp_speed
                        width:parent.width
                        height: 43
                        textIO:dWorkspeedXpYp_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }
                }

                Column{
                    id:col4_2
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_XmYm
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"X-/Y-"
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XmYm_height
                        width:parent.width
                        height: 43
                        textIO:dHeightXmYm_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial

                    }

                    TextField{
                        id: textfield_XmYm_speed
                        width:parent.width
                        height: 43
                        textIO:dWorkspeedXmYm_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }
                }
            }

        }

        Item{
            id: lasertool_col5
            width: columnWidth
            height: parent.height

            Label{
                id: label_analog_power
                height: labelHeight
                text: db.dbTr("Analog Power") + db.tr
                width:parent.width
                colorChoice: 1
            }

            TextField{
                id: textfield_analog_power_start
                y: label_analog_power.y + label_analog_power.height
                height:43
                width: parent.width
                textIO: dStartMacroAnalogPower_io
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top:100;bottom:0}
                onAccepted: isMaterialDirty = true
            }


            TextField{
                id: textfield_analog_power_end
                y: textfield_analog_power_start.y + textfield_analog_power_start.height + columnSpacing
                height:43
                width: parent.width
                textIO: dStopMacroAnalogPower_io
                anchors.top: textfield_analog_power_start.bottom
                anchors.margins: columnSpacing
                unitEnable: true
                unit: "%"
                digitsAfterComma: 0
                ioFactor: 100
                horizontalTextAlignement: 2
                validator: IntValidator{top:100;bottom:0}
                onAccepted: isMaterialDirty = true
            }


            Row{
                width: parent.width
                spacing: 20
                anchors.top: textfield_analog_power_end.bottom
                anchors.margins: columnSpacing
                Column{
                    id:col5_1
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_XmYp
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"X-/Y+"
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XmYp_height
                        width:parent.width
                        height: 43
                        textIO: dHeightXmYp_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XmYp_speed
                        width:parent.width
                        height: 43
                        textIO: dWorkspeedXmYp_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }
                }

                Column{
                    id:col5_2
                    width: parent.width / 2 - 10
                    spacing: columnSpacing
                    ToggleButton{
                        id: toggle_XpYm
                        width:parent.width
                        height: 43
                        __imageNormal: imagePath + S.Style[styleName].buttonImageNormal
                        __imagePressed: imagePath + S.Style[styleName].buttonImagePressed
                        __lightImageOn: imagePath + "check-mark-16.png"
                        __lightImageHeight: 16
                        __lightImageWidth: 16
                        __lightImageOff: ""
                        __verticalTextAlignement: Qt.AlignCenter
                        __horizontalTextAlignement: Qt.AlignHCenter
                        text:"X+/Y-"
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XpYm_height
                        width:parent.width
                        height: 43
                        textIO: dHeightXpYm_io
                        ioFactor: dMToMM.value
                        //display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: heightValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }

                    TextField{
                        id: textfield_XpYm_speed
                        width:parent.width
                        height: 43
                        textIO: dWorkspeedXpYm_io
                        ioFactor: dMPSToMPM.value
						//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
						digitsAfterComma: bUseInches.value ? 3 : 2
                        horizontalTextAlignement: 2
                        substituteCommaWithPoint: true
                        validator: speedValidator
                        onAccepted: isMaterialDirty = true
                        isEnabled: !bFourAxisMaterial
                    }
                }
            }

        }
        Item{
            id: lasertool_col6
            width: columnWidth
            height: parent.height

            Row{
                id: selection_buttons_row
                y: 184
                width: parent.width
                height: 43
                spacing: 5

                Button{
                    height: 43
                    width: parent.width / 2 - parent.spacing/2
                    text:db.dbTr("select all") + db.tr
                    fontSize: 9
                    __verticalTextAlignement: Text.AlignVCenter
                    __horizontalTextAlignement: Text.AlignHCenter
                    onPressed: selectAll(true)
                }
                Button{
                    height: 43
                    width: parent.width / 2 - parent.spacing/2
                    text:db.dbTr("deselect all") + db.tr
                    fontSize: 9
                    __verticalTextAlignement: Text.AlignVCenter
                    __horizontalTextAlignement: Text.AlignHCenter
                    onPressed: selectAll(false)
                }
            }

            Row{
                id: row_modify_height
                width: parent.width
                height: 43
                spacing: 5
                y: 264
                //anchors.top: textfield_pulse_width.bottom
                //anchors.margins: 316
                Button{
                    height: 43
                    width: 43
                    text:"-"
                    fontSize: 22
                    __verticalTextAlignement: Text.AlignVCenter
                    __horizontalTextAlignement: Text.AlignHCenter
                    onPressed: adjustHeight(false)
                }
                TextField{
                    id: textfield_modify_height
                    width:parent.width - 86 - parent.spacing*2
                    height: 43
                    text: dHeightAdjust
                    p_onlyNumbers: true
					//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
					digitsAfterComma: bUseInches.value ? 3 : 2
                    validator: heightValidator
                    __verticalTextAlignement: Text.AlignVCenter
                    horizontalTextAlignement: Text.AlignHCenter
                    substituteCommaWithPoint: true
                    fontSize: 12
                }
                Button{
                    height: 43
                    width: 43
                    text:"+"
                    fontSize: 22
                    __verticalTextAlignement: Text.AlignVCenter
                    __horizontalTextAlignement: Text.AlignHCenter
                    onPressed: adjustHeight(true)
                }
            }

            Row{
                width: parent.width
                height: 43
                spacing: 5
                anchors.top: row_modify_height.bottom
                //anchors.margins: 400
                anchors.margins: columnSpacing
                Button{
                    height: parent.height
                    width: height
                    text:"-"
                    fontSize: 22
                    __verticalTextAlignement: Text.AlignVCenter
                    __horizontalTextAlignement: Text.AlignHCenter
                    onPressed: adjustSpeed(false)
                }
                TextField{
                    id: textfield_modify_speed
                    width:parent.width - 86 - parent.spacing*2
                    height: parent.height
                    text: dSpeedAdjust
                    p_onlyNumbers: true
					//display 3 digits when bUseInches is true, 2 digits when bUseInches is false
					digitsAfterComma: bUseInches.value ? 3 : 2
                    validator:  speedValidator
                    __verticalTextAlignement: Text.AlignVCenter
                    horizontalTextAlignement: Text.AlignHCenter
                    substituteCommaWithPoint: true
                    fontSize: 12

                }
                Button{
                    height: parent.height
                    width: height
                    text:"+"
                    fontSize: 22
                    __verticalTextAlignement: Text.AlignVCenter
                    __horizontalTextAlignement: Text.AlignHCenter
                    onPressed: adjustSpeed(true)
                }
            }

        }

    }

    Row{
        id: control_type_row
        width: 1216
        height: 83
        spacing: rowSpacing
        x: 20
        y: laser_tools_row.y + laser_tools_row.height + columnSpacing

        Item{
            id: control_type_row_col1
            height: parent.height
            width: columnWidth

            visible: tool_number < 21

            Label{
                id: label_icontroltype
                height: labelHeight
                text: db.dbTr("Control type") + db.tr
                width:parent.width
                colorChoice: 1
            }

            DropDown{
                id: dropdown_controltype
                y: labelHeight
                width: parent.width
                height: 43
                textitems: [db.dbTr("Z Adjust") + db.tr,
                    db.dbTr("Fixed Z") + db.tr]
                valuitems: [1, 2]
                textIO: iControlType_io
                displaytext: setTextItem()
                onIsDirtyChanged: if(isDirty) isMaterialDirty = true
            }
        }
    }


    ToolbarMaterialTool{
        id: toolbarMaterialTool
    }

    Dialog{
        id:dlgToolChooser
        width: 980
        height: bEnableMilling.value ? 610 : 330
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#444444"
            opacity: 0.95
            radius: 8
        }
        Grid {
            x: 30; y: 30
            rows: bEnableMilling.value ? 8 : 4;
            columns:  5
            spacing: 30

            Repeater {
                model: bEnableMilling.value ? 40 : 20
                Button {
                    width: 160; height: 43
                    text: "[" + (index + 1) + "] " + eval("Material_Tool_" + (index + 1) + "_strNote").value
                    onClicked: {
                        screenTool.tool_number = index + 1
                        dlgToolChooser.close()
                    }
                }
            }
        }
    }



    states: [
        State {
            name: "LASERTOOL"
            PropertyChanges {
                target: las_row2
                visible: true
            }
            StateChangeScript {
                script:{
                    changeLaserPowerInputField(false)
                }
            }
            PropertyChanges {
                target: laser_tools_row
                visible: true
            }
        },
        State {
            name: "LAS_ENVELOPE"
            PropertyChanges {
                target: las_row1_col6
                visible: true
            }
            PropertyChanges {
                target: las_row2
                visible: true
            }
            StateChangeScript {
                script:{
                    changeLaserPowerInputField(false)
                }
            }
            PropertyChanges {
                target: laser_tools_row
                visible: true
            }

        },
        State {
            name: "LAS_GALVO"
            //--hide
            PropertyChanges {
                target: laser_tools_row
                visible: false
            }

            //--show
            PropertyChanges {
                target: las_row2
                visible: true
            }

            PropertyChanges {
                target: row2_col5_pulse_length
                visible: false
            }

            PropertyChanges {
                target: row2_col5_Frequency
                visible: true
            }

            PropertyChanges {
                target: las_galvo_row3
                visible: true
            }

            StateChangeScript {
                script:{
                    changeLaserPowerInputField(true)
                }
            }

        },
        State {
            name: "MILLINGTOOL"
            //hide
            PropertyChanges {
                target: laser_tools_row
                visible: false
            }
            PropertyChanges {
                target: las_row2
                visible: false
            }
            PropertyChanges {
                target: las_row1_col5
                visible: false
            }
            PropertyChanges {
                target: las_row1_col4
                visible: false
            }
            //show
            PropertyChanges {
                target: mil_row1_col4
                visible: true
            }
            PropertyChanges {
                target: mil_row1_col5
                visible: true
            }
            PropertyChanges {
                target: mil_row1_col6
                visible: true
            }
            PropertyChanges {
                target: mil_row2
                visible: true
            }
            PropertyChanges {
                target: mil_colum3
                visible: true
            }

        },
        State {
            name: "MIL_POCKET"
            //hide
            PropertyChanges {
                target: laser_tools_row
                visible: false
            }
            PropertyChanges {
                target: las_row2
                visible: false
            }
            PropertyChanges {
                target: las_row1_col5
                visible: false
            }
            PropertyChanges {
                target: las_row1_col4
                visible: false
            }
            //show
            PropertyChanges {
                target: mil_row1_col4
                visible: true
            }
            /*
            PropertyChanges {
                target: mil_row1_col5
                visible: true
            }*/
            PropertyChanges {
                target: mil_row1_col6
                visible: true
            }
            PropertyChanges {
                target: mil_row2
                visible: true
            }
            PropertyChanges {
                target: mil_column3
                visible: true
            }
        }
    ]

}

