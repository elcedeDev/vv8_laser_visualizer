//build date:2018-10-24
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0

Item {
    id: screenToolSequence
    anchors.fill: parent
    property string strToolSequence: Material_OptimizeEmptyMoves_strToolSequence.value

    Component.onCompleted: {
        inToolSequence()
    }

    onStrToolSequenceChanged: {
        inToolSequence()
    }

    ModalDlg{
        id: toolNumberDialog
        margin:20
        dialogboxheight: 800
        dialogboxwidth: 400
        title: db.dbTr("Select tool for sequence number: ") + toolIndex + db.tr

        property alias toolNumber: tool_list.currentIndex
        property int   toolIndex

        onOK: {
            var i = toolIndex -1
            tool_sequence.children[i].tool_number = toolNumber
            outToolSequence()

        }

        ListView {
            parent: toolNumberDialog.container
            id: tool_list
            spacing: 5
            anchors.fill: parent
            //highlightRangeMode: ListView.ApplyRange
            orientation: ListView.Vertical
            snapMode: ListView.SnapOneItem
            flickDeceleration: 1000
            highlightMoveDuration:1000
            smooth: true
            clip: true

            model: 21

            delegate: tool_buttons_model
        }


        Component {
            id: tool_buttons_model

            Row{
                spacing: 10
                height:40
                RadioButton {
                    width: 100
                    height: parent.height
                    text: "tool " + index
                    fontSize: 18
                    onClicked_changeStateTo: index
                    onClicked:  tool_list.currentIndex = index
                    __pressed: tool_list.currentIndex === index
                    __styleName: S.styleName
                }


                TextField {
                    id: tool_buttons_toollabel
                    width: 250
                    height: parent.height
                    readOnly: true
                    enabled: false
                    fontSize:10
                    textIO: "Material_Tool_" + index + "_strNote"
                }
            }
        }

  }


    function inToolSequence(){
        var str = strToolSequence
        str = str.replace(/\(|\)/g,"");

        var numbers = str.split(",");

        for(var i = 0; i < 20; ++i ){
            if( numbers.length - 1 > i ){
               // tool_sequence.children[i].tool_number = numbers[i]
                 if(tool_sequence.children[i].tool_number !== undefined)
                    tool_sequence.children[i].tool_number = numbers[i]
            }else{
                tool_sequence.children[i].tool_number = 0
            }
        }
    }

    function outToolSequence(){
        var str = "";
        for(var i=0; i < 20; ++i )
        {
            if(tool_sequence.children[i].tool_number !== 0)
                str += "(" + tool_sequence.children[i].tool_number + "),"

        }
         //Material_OptimizeEmptyMoves_strToolSequence.value = str;
        strToolSequence = str;
    }

    Flow{
        id: tool_sequence
        width: 600
        anchors.fill: parent
        anchors.margins: 4
        spacing: 6
        flow:Flow.TopToBottom
        clip: true
        Repeater {
            model: 20
            SequenceToolItem{sequence_mumber: index + 1; tool_number: 0}

        }
    }
}

