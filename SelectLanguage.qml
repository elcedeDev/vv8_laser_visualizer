import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "LanguageFunctions.js" as LF


Item {
    id:selectLanguageItem
    property string styleName: S.styleName

    function selectlanguage(button){
        dlgLanguage.close()
        showLangButton.text = button.text
        showLangButton.foregroundImageNormal =  S.Style[styleName].imageFolder + S.Style[styleName].languageButtonImage[LF.selectedLangShort(button.languageString)]
        showLangButton.foregroundImagePressed =  S.Style[styleName].imageFolder + S.Style[styleName].languageButtonImage[LF.selectedLangShort(button.languageString)]
        strVVSwitchLanguage.value = button.languageString
    }


    Component.onCompleted: {
        showLangButton.text = LF.selectedLangLong(strVVSwitchLanguage.value)
        showLangButton.languageString = strVVSwitchLanguage.value
        showLangButton.foregroundImageNormal =  S.Style[styleName].imageFolder + S.Style[styleName].languageButtonImage[LF.selectedLangShort(strVVSwitchLanguage.value)]
        showLangButton.foregroundImagePressed =  S.Style[styleName].imageFolder + S.Style[styleName].languageButtonImage[LF.selectedLangShort(strVVSwitchLanguage.value)]
    }

    Dialog{
        id: dlgLanguage
        x: 10
        y: 10
        width: 360
        height: 7*65+60
        opacity: 1
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#444444"
            opacity: 0.95
            radius: 8
        }


        VisualItemModel{
            id:languageModel
            LanguageButton{id:langEN; width: 300; text:"English";  languageString:"English"; __languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langEN)
            }
            LanguageButton{id:langHU; width: 300; text:"Magyar";   languageString:"Magyar";  __languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langHU)
            }
            LanguageButton{id:langDE; width: 300; text:"Deutsch";  languageString:"Deutsch"; __languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langDE)
            }
            LanguageButton{id:langES; width: 300; text:"Espanol";  languageString:"Espanol"; __languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langES)
            }
            LanguageButton{id:langJP; width: 300; text:"日本語";    languageString:"Japanese";__languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langJP)
            }
            LanguageButton{id:langFR; width: 300; text:"Francais"; languageString:"Francais";__languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langFR)
            }
            LanguageButton{id:langDK; width: 300; text:"Danish";   languageString:"Danish";  __languageString_short: LF.selectedLangShort(languageString); __pressed:  strVVSwitchLanguage.value === languageString;
                onClicked:selectlanguage(langDK)
            }

        }

        ListView {
            parent: dlgLanguage
            id: languageLV
            spacing: 10
            anchors.fill: dlgLanguage
            anchors.margins: 30
            highlightRangeMode: ListView.ApplyRange
            orientation: ListView.Vertical
            snapMode: ListView.SnapOneItem
            flickDeceleration: 1000
            highlightMoveDuration:1000
            smooth: true
            clip: true
            model: languageModel
        }
    }

    Button{
        id:showLangButton
        width: selectLanguageItem.width
        height:selectLanguageItem.height
        text: strVVSwitchLanguage.value
        property string languageString: "English"
        foregroundImageNormal:  S.Style[styleName].imageFolder + S.Style[styleName].languageButtonImage["en"]
        foregroundImagePressed:  S.Style[styleName].imageFolder + S.Style[styleName].languageButtonImage["en"]
        __foregroundHorizontalAlignement : S.Style[styleName].languagebuttonImageHorizontalAlignement
        __foregroundVerticalAlignement : S.Style[styleName].languagebuttonImageVerticalAlignement
        __foregroundXShift: S.Style[styleName].languagebuttonImageXShift
        __foregroundYShift: S.Style[styleName].languagebuttonImageYShift

        onClicked: {
            for(var i=0; i < languageLV.count; ++i){
                languageLV.contentItem.children[i].__pressed = (strVVSwitchLanguage.value === languageLV.contentItem.children[i].languageString )
            }

            dlgLanguage.open()
        }
    }
}




