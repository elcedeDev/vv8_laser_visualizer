import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Row {
    id: oneSequenceToolItem

    property string styleName: S.styleName
    property string imagePath: S.Style[styleName].imagePath

    spacing: 10
    property int        sequence_mumber
    property int        tool_number

    property string     strNote_io:         "Material_Tool_" + tool_number + "_strNote"
    property string     strToolLabel:       "Tool " + tool_number


    width: 460
    height: 43

    TextField {
        id: sequencetoolitem_sequencenumber
        width: 43
        height: parent.height
        __verticalTextAlignement: Text.AlignVCenter
        text: sequence_mumber
        readOnly: true
        enabled:false
        fontSize:14
    }

    Button{
        id: button_tool_number
        height: parent.height
        width: 100
        text: strToolLabel
        fontSize:14
        __verticalTextAlignement: Text.AlignVCenter
        foregroundImageNormal: imagePath + S.Style[styleName].buttonEmpty
        foregroundImagePressed: imagePath + S.Style[styleName].buttonEmpty
        onClicked: {
            toolNumberDialog.open()
            toolNumberDialog.toolNumber = tool_number
            toolNumberDialog.toolIndex = sequence_mumber
        }

    }

    TextField {
        id: sequencetoolitem_toollabel
        width: 280
        height: parent.height
        text: strToolLabel
        readOnly: true
        enabled: false
        fontSize:10
        textIO: strNote_io
    }
}
