//Build date: 2018-10-30

function pause(millis)
{
    var date = new Date();
    var curDate = null;
    do {
        curDate = new Date();
    } while((curDate - date) < millis);
}



WorkerScript.onMessage = function(message) {
    //console.log("WorkerScript.onMessage = function(message) --- start")
    //var endPoint = message.numberOfStop / message.numberOfCycles

    for(var i = 0; i < message.numberOfStops; ++i)
    {
        //console.log("WorkerScript.onMessage = function(message) --- start", i)
        var strIO = "'" + message.machineMove + "'"

        console.log("WorkerScript.bMoveMachine:",  eval(message.machineMove).value )
        while(WorkerScript.bMoveMachine){
            pause(50)
        }

        if( WorkerScript.bEndSpindlePitch ) break;
        if( i === 0)
            WorkerScript.sendMessage({moveTo: - 0.003, endSpindlePitch: false })
        if( i === 1)
            WorkerScript.sendMessage({moveTo: 0.003, endSpindlePitch: false })
        if( i === message.endPoint -1)
            WorkerScript.sendMessage({moveTo: 0.003, endSpindlePitch: false })
        if( i === message.endPoint )
            WorkerScript.sendMessage({moveTo: - 0.003, endSpindlePitch: false })
        if( i < message.endPoint && i > 1)
            WorkerScript.sendMessage({moveTo: message.grid , endSpindlePitch: false })
        if( i > message.endPoint )
            WorkerScript.sendMessage({moveTo: - message.grid, endSpindlePitch: false })

        pause(message.delay)
    }

    WorkerScript.sendMessage({moveTo: 0, endSpindlePitch: true })
}
