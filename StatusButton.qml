import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Item {
    id: status_button

    property string indicator_text: "indicator"
    property string indicator_IO
    property alias  labelWidth: indicator_label.width
    property int    space: 4
    property int    colorChoice:1
    property bool inverseState: false

    height: 32
    width: 300

    Indicator {
        id: ligth
        width: parent.height
        height: parent.height
        doubleIndicator: false
        valueIO: indicator_IO
        inverseState: parent.inverseState
    }


    Label {
        id: indicator_label
        x: parent.height + space
        width: parent.width - ligth.width - space
        height: parent.height
        text: indicator_text
        //horizontalTextAlignement: 1
        fontSize: 12
        colorChoice: status_button.colorChoice
    }

}
