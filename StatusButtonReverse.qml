import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Item {
    id: status_button_reverse

    property string indicator_text
    property string indicator_IO
    property alias  labelWidth: indicator_label.width
    property int    space: 4

    height: 43
    width: 300

    TextField {
        id: ligth_label
        width: 43
        height: parent.height
        unitEnable: false
        readOnly: true
        selectByMouse: false
        Indicator {
            id: ligth
            width: parent.width - 2
            height: parent.height - 2
            doubleIndicator: false
            __singleColors: [S.Style[__styleName].indicatorColor1,
                    S.Style[__styleName].indicatorColor0,
                    S.Style[__styleName].indicatorColor2,
                    S.Style[__styleName].indicatorColor3]
            valueIO: indicator_IO
        }

    }

    TextField {
        id: indicator_label
        x: 47
        width: parent.width - ligth_label.width - space
        height: parent.height
        text: indicator_text
        ioFactor: 1
        readOnly: true
        horizontalTextAlignement: 1
        fontColorChoice: 1
        fontPlaceholderColorChoice: 0
        fontSize: 11
        ioOffset: 2
        transformOrigin: Item.Center
        unitEnable: false
        selectByMouse: false
    }

}
