import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Item {
    id: tabButton
    objectName: "ELCEDEtabButton"

    property string stateTarget: ""//parent.stateTarget
    property string __text: ""
    property string __normalIcon: ""
    property string __pressedIcon: ""
    property string __style: S.styleName
    property string __imagePath: S.Style[__style].imagePath
    property string onClicked_changeStateTo: ""
    property bool   __pressed: false
    property bool   enabled: true
    property alias foregroundImageNormal:textMenu.foregroundImageNormal
    property alias foregroundImagePressed: textMenu.foregroundImagePressed
    property alias imageNormal: textMenu.__imageNormal
    property alias imagePressed: textMenu.__imagePressed
    property alias verticalTextAlignement: textMenu.__verticalTextAlignement
    property alias horizontalTextAlignement: textMenu.__horizontalTextAlignement
    property alias fontColorNormal: textMenu.__fontColorNormal
    property alias fontSize: textMenu.fontSize

    property variant __target: eval(stateTarget)

    property bool selectionLineUnder: true
    property alias selectionLineColor: selectionLineRect.color
    property bool selectionLine:  false

    width: 232
    height: 43

    signal clicked()
    signal pressed()

    Keys.onPressed: {
            if (event.key === Qt.Key_Escape) {
                //console.log("menuButton Key_Escape");
                event.accepted = true;
             }
        }


    function changeState(){
        tabButton.clicked()
        if( __target === undefined )
        {
            __target = parent
        }

        if (onClicked_changeStateTo != ""){
            __target.state = onClicked_changeStateTo
        }

    }

    RadioButton {
        id: textMenu
        text: parent.__text
        height: parent.height
        width: parent.width
        anchors.left: parent.left
        anchors.leftMargin: 5
        onClicked: parent.onClicked
        __pressed: parent.__pressed
        foregroundImageNormal: __imagePath + S.Style[__style].buttonEmpty
        foregroundImagePressed: __imagePath + S.Style[__style].buttonEmpty
        enabled: parent.enabled
        __verticalTextAlignement: Qt.AlignVCenter
        __horizontalTextAlignement: Qt.AlignHCenter
    }

    Rectangle{
        id:selectionLineRect
        y: selectionLineUnder ? textMenu.height - 2 : - 2
        anchors.left: parent.left
        anchors.leftMargin: 5
        width:parent.width
        height: 4
        color:"cyan"
        visible: selectionLine && __pressed
    }


    MouseArea {
        id: behavior
        anchors.fill: parent
        hoverEnabled: true

        onClicked: {
            if(parent.enabled){
                changeState()
                //console.log("Menubuton enabled")
            }
        }
     }

}

