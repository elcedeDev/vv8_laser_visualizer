// $Revision: 17742 $
import QtQuick 2.0
import "style/Style.js"  as S
import tools.shortcut 2.0
import tools 2.0



/*!
    \qmltype Button
    \inqmlmodule ??? NotClearYet
    \since ??? NotClearYet
    \ingroup ??? NotClearYet
    \brief A push button with a text label.

 */
FocusScope {
    id: button
    objectName: "ToggleButtonFlat"

    property int __textHeight: 25

    /*!
        \qmlProperty bool ToggleButton::activeFocusOnPress

        Specifies whether the button should
        gain active focus when pressed.

        The default value is \c true. */
    property bool __activeFocusOnPress: false


//    property int testEnum: IoType.IOT_BOOLEAN
//    property variant textenmu: testEnum
    /*!
        \qmlProperty string ToggleButton::changeTextTo
        The text to be displayed by target after button push.
        if this has a  value requires:
        \list
        \li target needs to be set
        \li target needs to have a text property
      */
    property string __changeTextTo

    /*!
        \qmlProperty string ToggleButton::changeStateTo
        The state a target is changet to after a button push.
       if this has a  value requires:
        \list
        \li target needs to be set
        \li target needs to have a state property
      */
    property string __changeStateTo


    /*!
        \qmlProperty string ToggleButton::fontColorNormal
        Font color of displayed Text, when button is active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorNormal: S.Style[__styleName].togglebuttonFontPressedColor //togglebuttonFontColor
    /*!
        \qmlProperty string ToggleButton::fontColorDisabled
        Font color of displayed Text, when button is disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorDisabled: S.Style[__styleName].togglebuttonFontDisabledColor
    /*!
        \qmlProperty string ToggleButton::fontColorPressed
        Font color of displayed Text, while button is pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorPressed: S.Style[__styleName].togglebuttonFontPressedColor
    /*!
        \qmlProperty string ToggleButton::fontColorPressedAndDisabled
        Font color of displayed Text, while button is pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __fontColorPressedAndDisabled: S.Style[__styleName].togglebuttonFontPressedAndDisabledColor

    /*!
        \qmlProperty string ToggleButton::__foregroundImageNormal
        String that holds the URL of the foreground image.
        The default value is \c "".
      */

    /*!
        \qmlProperty string ToggleButton::__foregroundImageNormal
        String that holds the URL of the foreground image.
        The default value is \c "".
      */

    property alias foregroundImageNormal: buttonForegroundImage.source
    /*!
        \qmlProperty string ToggleButton::__foregroundImagePressed
        String that holds the URL of the foreground image when pressed.
        The default value is \c "".
      */
    property alias foregroundImagePressed: buttonForegroundImagePressed.source

    /*!
        \qmlProperty int ToggleButton::__foregroundImageHeight
        Height of an additional Image for the Button.
        The default value is \c 0.
      */

    property int __foregroundImageHeight: S.Style[__styleName].togglebuttonForegroundImageHeight
    /*!
        \qmlProperty int ToggleButton::__foregroundImageWidth
        Width of an additional Image for the Button.
        The default value is \c 0.
      */
    property int __foregroundImageWidth: S.Style[__styleName].togglebuttonForegroundImageHeight

    /*!
        \qmlProperty bool ToggleButton::__foregroundAnchorRight
        When \c true Additional Image is anchored right, otherwise
        it is anchored left.
        The default value is \c false.
      */
    property int __foregroundHorizontalAlignement: Qt.AlignLeft
    /*!
        \qmlProperty bool ToggleButton::__foregroundAnchorBottom
        When \c true Additional Image is anchored at the top, otherwise
        it is anchored on the bottom.
        The default value is \c false.
      */
    property int __foregroundVerticalAlignement: Qt.AlignVCenter
    /*!
        \qmlProperty int ToggleButton::__foregroundXShift
        \brief x offset
        Offset of the additional Imagen in x - direction
        The default value is \c 0.
      */
    property int __foregroundXShift: 0
    /*!
        \qmlProperty int ToggleButton::__foregroundXShift
        \brief y offset
        Offset of the additional Imagen in y - direction
        The default value is \c 0.
      */
    property int __foregroundYShift: 0

    /*! \internal
        \qmlmethod ToggleButton::__pressedLeftShift
        Change of BackgroundImage Size on pressed in x direction
    */
    property int __pressedLeftShift: S.Style[__styleName].togglebuttonPressedLeftShift

    /*! \internal
        \qmlmethod ToggleButton::__pressedRightShift
        Change of BackgroundImage Size on pressed in Pixels in negative x- direction
    */
    property int __pressedRightShift: S.Style[__styleName].togglebuttonPressedRightShift
    /*! \internal
        \qmlmethod ToggleButton::__pressedTopShift
        Change of BackgroundImage Size on pressed in Pixels in  y- direction
    */
    property int __pressedTopShift: S.Style[__styleName].togglebuttonPressedTopShift
    /*! \internal
        \qmlmethod ToggleButton::__pressedBottomShift
        Change of BackgroundImage Size on pressed in Pixels in negative y- direction
    */
    property int __pressedBottomShift: S.Style[__styleName].togglebuttonPressedBottomShift

    /*!
        \qmlProperty string ToggleButton::imageNormal
        Background image URL of button, when  active. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageNormal: S.Style[__styleName].imageFolder + "ToggleNormalOn.png"

    /*! \qmlproperty bool TextField::__hovered

        This property holds whether the control is being hovered.
    */
    property alias __hovered: behavior.containsMouse

    /*!
        \qmlProperty string ToggleButton::imageDisabled
        Background image URL of button, when  disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imageDisabled: S.Style[__styleName].imageFolder + "ToggleNormalOn.png"
    /*!
        \qmlProperty string ToggleButton::imagePressed
        Background image URL of button, while pressed. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imagePressed: S.Style[__styleName].imageFolder + "ToggleNormalOff.png"
    /*!
        \qmlProperty string ToggleButton::imagePressedAndDisabled
        Background image URL of button, while pressed and disabled. Is per default
        set to global Style Settings. If edited manually in editor it
        will lose automatic style changing capability.
      */
    property string __imagePressedAndDisabled: S.Style[__styleName].imageFolder + "ToggleNormalOff.png"


    property string __focusImage: (S.Style[__styleName].togglebuttonImageFocus !== "") ?
                                    S.Style[__styleName].imageFolder + S.Style[__styleName].togglebuttonImageFocus : ""



    /*!
        \qmlProperty bool ToggleButton::isEnabled
        Enables the button. Is used for intitial settings
        The default value is \c true.
      */
    property bool isEnabled:true
    onIsEnabledChanged: {
        enabled = isEnabled
    }
    enabled: (__enableIO === undefined) ? isEnabled: __enableIO.value


    /*!
        \qmlProperty bool ToggleButton::isPressed
        Presses the button. Is used for intitial settings
        The default value is \c true.
      */
    property bool __isPressedInitially: false

    /*!
        \qmlProperty bool ToggleButton::keyBinding
        Button can get a hotkey like F1 - F12 for different key values
        use \l Qt::KeyHotLinks and insert values without "Qt::Key_SysReq"
        For example:
        If you want to use page down as a hotkey, the list shows
        "Qt::Key_PageDown" and you need to insert "PageDown"
      */
    property string keyBinding

    /*!
        \qmlProperty string ToggleButton::__leftTextMargin
        \brief Left margin of text on top of image margin:
        Additional left Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.leftTextMargin.
      */
    property int __leftTextMargin: S.Style[__styleName].togglebuttonTextLeftMargin
    /*!
        \qmlProperty string ToggleButton::__rightTextMargin
        \brief Right margin of text on top of image margin:
        Additional rightMargin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.rightTextMargin.
      */
    property int __rightTextMargin: S.Style[__styleName].togglebuttonTextRightMargin
    /*!
        \qmlProperty string ToggleButton::__topTextMargin
        \brief Top margin of text on top of image margin:
        Additional top Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.topTextMargin.
      */
    property int __topTextMargin: S.Style[__styleName].togglebuttonTextTopMargin
    /*!
        \qmlProperty string ToggleButton::__bottomTextMargin
        \brief Bottom margin of text on top of image margin:
        Additional bottom Margin of the text that is added to the already existing margin of
        the background image.
        The default value is \c <Stylefile>.bottomTextMargin.
      */
    property int __bottomTextMargin: S.Style[__styleName].togglebuttonTextBottomMargin


    /*!
        \qmlProperty bool ToggleButton::sendToComponentOutOfScope
        If you want to send an Object to a Component out of Scope,
        you have to check this flag and give your target an objName
        The default value is \c false.*/
    property bool __sendToComponentOutOfScope: false


    /*!
        \qmlProperty string ToggleButton::target
        QML target Name for changeTextTo/changeStateTo
        needs to be a string with the id name of the target.
      */
    property string __buttonTarget




    /*!
        \qmlProperty string ToggleButton::text
        This property holds the text shown on the button. If the button has no
        text, the \l text property will be an empty string.

        The default value is the empty string.
    */
    property string text
    /*!
        \qmlProperty string ToggleButton::buttonTextIO
        Connects the text shown on the button to an IO value.

        The default value is the empty string.
    */
    property variant __textIO: eval(textIO)
    property string textIO
    onTextIOChanged: {
        if (textIO !== ""){
            __textIO = eval(textIO)
        }
    }
    property variant textIOValue: (__textIO !== undefined) ? __textIO.value : ""

    /*!
        \qmlProperty string ToggleButton::buttonClickIO
        A string that gives the name of an IO. The IO must be a bool.
        The state of the Toggle Button will pressed(true)/not pressed(false)
        will always be synchronized with the io.
    */
    property variant __stateIO: eval(stateIO)
    property string stateIO
    onStateIOChanged: {
        if (stateIO !== ""){
            __stateIO = eval(stateIO)
        }
    }
    /*!
        \qmlProperty string ToggleButton::buttonStateIO
        A string that gives the name of an IO. The IO must be a bool.
        The enabled status of the Toggle Button will enabled(true)/not disabled(false)
        will always be synchronized with the io.
    */
    property variant __enableIO: eval(enableIO)
    property string enableIO
    // for Intialisation
    onEnableIOChanged: {
        if (enableIO !== ""){
            __enableIO = eval(enableIO)
        }
    }




    property int fontSize: S.Style[__styleName].togglebuttonFontSize

    /*!
        \qmlproperty enumeration TextField::verticalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignTop
        \li Text.AlignBottom
        \li Text.AlignVCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __verticalTextAlignement: S.Style[__styleName].togglebuttonVerticalAlignement

    /*!
        \qmlproperty enumeration TextField::horizontalAlignment

        Sets the alignment of the text within the TextField item's width.

        By default, the horizontal text alignment follows the natural alignment
        of the text, for example text that is read from left to right will be
        aligned to the left.

        The possible alignment values are:
        \list
        \li Text.AlignLeft
        \li Text.AlignRight
        \li Text.AlignHCenter
        \endlist

        When using the attached property, LayoutMirroring::enabled, to mirror
        application layouts, the horizontal alignment of text will also be
        mirrored. However, the property \c horizontalAlignment will remain
        unchanged. To query the effective horizontal alignment of TextField, use
        the read-only property \c effectiveHorizontalAlignment.
    */
    property int __horizontalTextAlignement: S.Style[__styleName].togglebuttonHorizontalAlignement

    property bool __smooth: S.isFast
    /*!
     \qmlsignal ToggleButton::clicked()
     This signal is emitted when the user clicks the button. A click is defined
     as a press followed by a release. The corresponding handler is
     \c onClicked.
     */
    signal clicked()
    /*!
        \qmlsignal ToggleButton::styleChanged(string strStyle)

        When an online style change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal styleChanged(string strStyle)
    /*!
        \qmlsignal ToggleButton::languageChanged(string strStyle)

        When an online language change is done, this signal is emitted to
        the c++ main class and from there emitted to all AradexComponents
    */
    signal languageChanged()

    /*
        \qmlsignal TextField::outOfScopeStateChange(string objName, string stateName)

        If a state change is directed at a component out of change a signal
        containing \a objName and \a stateName has to be send to c++ main.
      */
    signal outOfScopeStateChange(string objName, string stateName)
    /*
        \qmlsignal TextField::outOfScopeTextChange(string objName, string stateName)

        If a text change is directed at a component out of change a signal
        containing \a objName and \a newText has to be send to c++ main.
      */
    signal outOfScopeTextChange(string objName, string newText)

    signal scaledSizeChanged(real scaleFactor)

    /*! \internal
        \qmlmethod ToggleButton::getText()
        Takes the value of the IO connected to the text on the button and formats it
        appropriately, by reducig digits and adding a unit.

    */
    function getText(){
        if (__textIO !==  undefined){
             return String(__textIO.value)
        }else{
            return String(button.text)
        }

    }

    /*!
        \qmlmethod ToggleButton::changeState()
        Behaviour when Button is Clicked including change of appearance
        and changeTextTo/changeStateTo Actions.

    */
    function changeState(){
        button.clicked()
    }
    /*!
        \qmlmethod ToggleButton::pressedAction()
        Behaviour when Button is pressed including change of appearance.


    */

    function pressedAction(){
        if(__stateIO !== undefined && __stateIO.type === IoType.IOT_BOOLEAN){
            __stateIO.value = !__pressed
        }else{
            __pressed = !__pressed
        }

        if (enabled) changeState()


    }



    /*! \internal
        The corresponding Qt.Key to the Keey binding input.
    */
    property variant shortKey: eval("Qt.Key_" + keyBinding)

    /*! \internal
        The style from Style.js needs to be redirected
        to a property in order to trigger events on change.
    */
    property string __styleName: S.styleName

    property bool __pressed

    /*!
        \internal
        \qmlProperty string ToggleButton::__target
        QML target Object for changeTextTo/changeStateTo.
        Is initialized with target
      */
    property variant __target: eval(__buttonTarget)


    implicitWidth:  S.Style.togglebuttonWidthInitial
    implicitHeight:  S.Style.togglebuttonHeightInitial
    clip: false

    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }



//    onIsPressedInitiallyChanged: __pressed = __isPressedInitially
    //property bool buttonClickIOValue: (buttonClickIO !== "") ?  eval(buttonClickIO + ".value") : false //__pressed
    /*!
        \internal
        \qmlProperty string ToggleButton::buttonClickIOValue
        Only exists to receive change event if clickIO value changes on PLC.
        Has wrong type(variant) on purpose to disable Editor visibility.
      */
    property variant buttonClickIOValue:  (__stateIO !== undefined) ? __stateIO.value : ""



    onButtonClickIOValueChanged: {
        if(__stateIO !== ""){
            if(buttonClickIOValue === true){
                __pressed = true
            }else{
                __pressed = false
            }
        }

    }


    Shortcut {
        key: shortKey
        onActivated: {
            if(enabled){
                pressedAction()
            }
        }
    }



    MouseArea {
        id: behavior
        anchors.fill: parent
        hoverEnabled: true
        onPressed: pressedAction()
        onClicked: button.focus = true
    }


    BorderImage {
        id: buttonImage
        anchors.fill: button
        anchors.topMargin: __textHeight
        border{
            left: S.Style[__styleName].togglebuttonBorderLeftSize
            top: S.Style[__styleName].togglebuttonBorderTopSize
            right: S.Style[__styleName].togglebuttonBorderRightSize
            bottom: S.Style[__styleName].togglebuttonBorderBottomSize
        }
        source: __imageNormal
        visible: (enabled)
        smooth: __smooth
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: buttonImage
                source : __imagePressed

                anchors.leftMargin: __pressedLeftShift
                anchors.rightMargin: __pressedRightShift
                //anchors.topMargin: __pressedTopShift
                anchors.topMargin: __textHeight
                anchors.bottomMargin: __pressedBottomShift

            }

        }
        transitions: Transition {
            NumberAnimation {
                properties: "anchors.leftMargin, anchors.rightMargin, anchors.topMargin,anchors.bottomMargin"
                easing.type: Easing.Linear
                duration: S.isFast ? S.Style[__styleName].togglebuttonSizeAnimationTime : 0

            }

        }
        BorderImage {
            id: focus
            opacity:  (button.focus && S.focusOn) ? S.Style[__styleName].focusOpacity : 0
            anchors{
                fill: parent
                leftMargin: S.Style[__styleName].togglebuttonFocusShiftLeft
                topMargin:  S.Style[__styleName].togglebuttonFocusShiftRight
                rightMargin: S.Style[__styleName].togglebuttonFocusShiftTop
                bottomMargin: S.Style[__styleName].togglebuttonFocusShiftBottom
            }
            border{
                left: S.Style[__styleName].togglebuttonFocusBorderLeft
                right: S.Style[__styleName].togglebuttonFocusBorderRight
                top: S.Style[__styleName].togglebuttonFocusBorderTop
                bottom: S.Style[__styleName].togglebuttonFocusBorderBottom
            }
            source: __focusImage
            smooth: __smooth
        }
    }

    BorderImage {
        id: buttonImageDisabled
        anchors.fill: button
        anchors.topMargin: __textHeight
        border{
            left: S.Style[__styleName].togglebuttonBorderLeftSize
            top: S.Style[__styleName].togglebuttonBorderTopSize
            right: S.Style[__styleName].togglebuttonBorderRightSize
            bottom: S.Style[__styleName].togglebuttonBorderBottomSize
        }
        source: __imageDisabled
        visible: (!enabled)
        smooth: __smooth
        states: State {
            name: "pressed"; when: __pressed

            PropertyChanges {
                target: buttonImageDisabled
                source : __imagePressedAndDisabled

                anchors.leftMargin: __pressedLeftShift
                anchors.rightMargin: __pressedRightShift
                //anchors.topMargin: __pressedTopShift
                anchors.topMargin: __textHeight
                anchors.bottomMargin: __pressedBottomShift

            }

        }
        transitions: Transition {
            NumberAnimation {
                properties: "anchors.leftMargin, anchors.rightMargin, anchors.topMargin,anchors.bottomMargin"
                easing.type: Easing.Linear
                duration: S.isFast ? S.Style[__styleName].togglebuttonSizeAnimationTime : 0

            }

        }
    }



    Image {
        id: buttonForegroundImage
        height: button.__foregroundImageHeight
        width :button.__foregroundImageWidth
        // calculate Image Position for different Alignement settings
        x: ((__foregroundHorizontalAlignement === Qt.AlignLeft) ? 0 :
                (__foregroundHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width) / 2 :
                (parent.width - width)
            ) + __foregroundXShift

        y: ((__foregroundVerticalAlignement === Qt.AlignTop) ? 0 :
                (__foregroundVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height) / 2 :
                (parent.height - height)
            )+ __foregroundYShift

        source: ""
        smooth: __smooth
        opacity: __pressed ? 0 : 1

    }
    Image {
        id: buttonForegroundImagePressed
        height: button.__foregroundImageHeight
        width :button.__foregroundImageWidth
        // calculate Image Position for different Alignement settings
        x: ((__foregroundHorizontalAlignement === Qt.AlignLeft) ? (0 + __pressedLeftShift) :
                (__foregroundHorizontalAlignement === Qt.AlignHCenter) ? (parent.width - width - __pressedRightShift) / 2 :
                (parent.width - width)
           ) + __foregroundXShift
        y: ((__foregroundVerticalAlignement === Qt.AlignTop) ? 0 + __pressedTopShift :
                (__foregroundVerticalAlignement === Qt.AlignVCenter) ? (parent.height - height - __pressedBottomShift) / 2 :
                (parent.height - height)
           ) + __foregroundYShift


        source: ""
        smooth: __smooth
        opacity: __pressed ? 1 : 0

    }


    Text {
        id: label
        objectName: "buttonLabel"

        signal languageChanged()
        font.pointSize: {fontSize > 0 ? fontSize : 1}
        font.family: S.Style[__styleName].togglebuttonFontFamily;
        anchors{
            fill: parent
        }


        horizontalAlignment: Text.AlignLeft // button.__horizontalTextAlignement
        verticalAlignment: Text.AlignTop // button.__verticalTextAlignement

        smooth: __smooth
        text: (__textIO !== undefined) ? getText() : qsTr(button.text)
        onLanguageChanged: {
            text = (__textIO !== undefined) ? getText() : qsTr(button.text)
        }
        color: {
            if(!enabled){
                __fontColorDisabled
            }else{
                __fontColorNormal
            }
        }
        states: State {
            name: "pressed"; when: __pressed
            PropertyChanges {
                target: label
                color: {
                    if(!enabled){
                        __fontColorPressedAndDisabled
                    }else{
                        __fontColorPressed

                    }
                }
            }
        }
        transitions: Transition {
                ColorAnimation {
                target: label
                property: "color"

                duration: S.Style[__styleName].togglebuttonTextAnimationTime
            }

        }
        clip: true
        elide: Text.ElideRight
    }

    Text {
        id: label_on
        objectName: "buttonLabelon"

        signal languageChanged()
        font.pointSize: {fontSize > 0 ? fontSize : 1}
        font.family: S.Style[__styleName].togglebuttonFontFamily;
        anchors{
            fill: buttonImage //.parent
            leftMargin: button.width / 2 + __leftTextMargin
            rightMargin: S.Style[__styleName].togglebuttonTextBorderRightSize + __rightTextMargin
        }


        //horizontalAlignment: button.__horizontalTextAlignement
        //verticalAlignment: Text.AlignVCenter //  button.__verticalTextAlignement

        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment:  Qt.AlignVCenter

        smooth: __smooth
        text: db.dbTr("ON") + db.tr

        color: {
            if(!enabled){
                __fontColorDisabled
            }else{
                S.Style[__styleName].lightFontColor
            }
        }

        clip: true
        elide: Text.ElideRight
    }

    Text {
        id: label_off
        objectName: "buttonLabeloff"

        signal languageChanged()
        font.pointSize: {fontSize > 0 ? fontSize : 1}
        font.family: S.Style[__styleName].togglebuttonFontFamily;
        anchors{
            fill: buttonImage
            leftMargin: S.Style[__styleName].togglebuttonTextBorderLeftSize + __leftTextMargin
            rightMargin: button.width / 2 + __rightTextMargin
        }

        visible:false
        //horizontalAlignment: button.__horizontalTextAlignement
        //verticalAlignment:  Text.AlignVCenter

        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment:  Qt.AlignVCenter

        smooth: __smooth
        text: db.dbTr("OFF") + db.tr

        color: {
            if(!enabled){
                __fontColorDisabled
            }else{
                S.Style[__styleName].lightFontColor
            }
        }

        clip: true
        elide: Text.ElideRight
    }




    states: [
        State {
            name: "pressed"; when: __pressed
            StateChangeScript {
                  script: __pressed = true
            }
            PropertyChanges {
                target: label_off
                visible: true
            }
            PropertyChanges {
                target: label_on
                visible: false
            }
            /*
            PropertyChanges {
                target: buttonForegroundImage
                opacity: 1
            }
            PropertyChanges {
                target: buttonForegroundImagePressed
                opacity: 0
            }
            */
        },
        State {
            name: "notpressed"; when: !__pressed
            StateChangeScript {
                script: __pressed = false
            }
            PropertyChanges {
                target: label_on
                visible: true
            }
            PropertyChanges {
                target: label_off
                visible: false
            }
            /*
            PropertyChanges {
                target: buttonForegroundImage
                opacity: 0
            }
            PropertyChanges {
                target: buttonForegroundImagePressed
                opacity: 1
            }
            */


        }
    ]

}
