import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
import "MainFunctions.js" as MF

Item {
    id: toolbarJob
    objectName: "ELCEDEtoolbar"

    property string __style: S.styleName //mainrect.rootStyle
    property string __imagePath: S.Style[__style].imagePath

    x: 0
    y: 770
    width: 600
    height: 100

    property int buttonWidth: 160

    property bool isJobStart: Job_bStart.value
    property bool isFullScreen: screenJob.state === "1"
    property bool isJob_bFinished: Job_bFinished.value


    onIsJob_bFinishedChanged: {
        if( (Job_bFinished.value === true )&& (ClearTable_bOn.value === true)){
            //ClearTable_bJobStarted.value = true
            MF.clearTable()
            Job_bFinished.value === false
        }
    }


    function debugEngravingTool(strMsg){
        var i = 1
        for(i = 1; i <= 40; ++i){
            var variToolType = eval("Material_Tool_" + i + "_iToolType")
            if(variToolType.value === 2){
                var varbEngravingTool = eval("Material_Tool_" + i  + "_bEngravingTool")
                console.log(strMsg, "Material_Tool_" + i + "_iToolType:", variToolType.value,  "Material_Tool_" + i  + "_bEngravingTool:", varbEngravingTool.value)
            }
        }
    }

    //TODO: engrave optimize button
    Row{
        x: 20
        y: 20
        spacing: 20
        width: parent.width
        height: 60
        Button{
            id:button_jobstart
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "play-w.png"
            foregroundImagePressed: __imagePath + "play.png"
            //text: db.dbTr("Start") + db.tr
            enabled: MF.isEnabled()

            onClicked: {
                if(positionControl.state       === "SHOWED"){
                    positionControl.state       =  "HIDDEN"
                    diecadviewerContainer.state = "FULL"
                }
                //debugEngravingTool("---- before job start:  ")
				Job_bFinished.value = false
                Job_bStart.value = true
                //debugEngravingTool("---- after job start:  ")
             }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_jobpause
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "pause-w.png"
            foregroundImagePressed: __imagePath + "pause.png"

            //text: db.dbTr("Pause") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                //JobStartRequested.value = false --> ilyen változó nincs
                Job_bCancelAtTool0.value = true
            }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_jobclear
            width: buttonWidth
            height: parent.height
            text: db.dbTr("Clear") + db.tr
            enabled: MF.isEnabled()

            onClicked: MF.clearTable()
            onReleased: {
                __isPressed = false
            }
        }
        Button{
            id:button_jobshow
            width: buttonWidth
            height: parent.height
            text: db.dbTr("Show") + db.tr
            enabled: MF.isEnabled()

            onClicked: {
                MoveMachine_bMove.value = false
                //visShowJobSize.value = true
                Job_InOut_bShowJobSize.value = true
            }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_positionwidgetshow
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "screenshot-w.png"
            foregroundImagePressed: __imagePath + "screenshot.png"

            onClicked: {
                if(positionControl.state       === "SHOWED"){
                    positionControl.state       =  "HIDDEN"
                    diecadviewerContainer.state = "FULL"
                }
                else
                {
                    positionControl.state       = "SHOWED"
                    //not always responding the signal mainrect.positionControlStateChanged
                    diecadviewerContainer.state = "REDUCED"
                }
            }
            onReleased: { __isPressed = false }
        }
    }
}

