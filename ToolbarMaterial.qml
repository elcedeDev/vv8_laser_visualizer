import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
import "DBFunctions.js" as DBF
import "MainFunctions.js" as MF

Item {
    id: toolbarMaterial
    objectName: "ELCEDEtoolbar"

    property string __style: S.styleName //mainrect.rootStyle
    property string __imagePath: S.Style[__style].imagePath

    x:0
    y: 775 //parent.height - height
    //width:parent.width - 290 //position_component.width
    width: 600
    height: 120

    property int buttonWidth: 160

    Row{
        x: 20
        y: 20
        spacing: 20
        width: parent.width
        height: 60
        Button{
            id:button_savematerial
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "save-w.png"
            foregroundImagePressed: __imagePath + "save.png"
            //text: db.dbTr("Save") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                DBF.saveMat()
            }
        }

        Button{
            id:button_deletematerial
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "bin-w.png"
            foregroundImagePressed: __imagePath + "bin.png"
            //text: db.dbTr("Delete") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                dialogDeleteMaterial.initializeDir()
                dialogDeleteMaterial.open()
                __isPressed = true
            }

            //onReleased: { __isPressed = false }
        }
        Button{
            id:button_newmaterial
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "document-w.png"
            foregroundImagePressed: __imagePath + "document.png"
            //text: db.dbTr("New") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                var dlg = getstring_dlg

                dlg.titleText =                 db.dbTr("Enter name for the new Material in DB") + db.tr
                dlg.buttonOk.text =             db.dbTr("OK") + db.tr
                dlg.buttonCancel.text =         db.dbTr("Cancel")  + db.tr
                dlg.width =                     700
                dlg.inputField.passwordInput =  false
                dlg.inputField.text =           db.dbTr("new material") + db.tr
                dlg.buttonOk.isEnabled =        db.material.isValidName(getstring_dlg.inputField.text)
                dlg.ok.connect(addNewMaterial)
                dlg.textInputChanged.connect(setOkEnabled)
                dlg.open()

            }
            function addNewMaterial(){
                //console.log("------ function addNewMaterial(){-----")
                var id = db.material.newMat(getstring_dlg.inputField.text)
                if(id > 0)
                {
                    Material_MaterialFile.value = getstring_dlg.inputField.text
                    db.material.name = Material_MaterialFile.value
                    getstring_dlg.ok.disconnect(addNewMaterial)
                    getstring_dlg.textInputChanged.disconnect(setOkEnabled)
                    DBF.loadMat()
                    getstring_dlg.close()
                }
                else
                {
                    getstring_dlg.ok.disconnect(addNewMaterial)
                    getstring_dlg.textInputChanged.disconnect(setOkEnabled)
                    getstring_dlg.close()
                }
            }

            function setOkEnabled(){
                getstring_dlg.buttonOk.isEnabled = db.material.isValidName(getstring_dlg.inputField.text)
            }
            onReleased: {
                __isPressed = false
            }
        }
        Button{
            id:button_copymaterial
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "copy-w.png"
            foregroundImagePressed: __imagePath + "copy.png"
            //text: db.dbTr("Copy") + db.tr
            //enabled: MF.isEnabled()
            property bool bResult: false
            onBResultChanged: {
                __isPressed = !bResult
            }

            onClicked: {
                bResult = true
                __isPressed = true
                bResult = DBF.copy()
            }
        }

        RegExpValidator {
            id: text_validator
            regExp: /[a-zA-Z0-9_()/-:.,;]+/
        }

        Button{
            id:button_renammaterial
            width: buttonWidth
            height: parent.height
            text: db.dbTr("Rename") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                var dlg = getstring_dlg

                dlg.titleText =                 db.dbTr("Enter new name for Material in DB") + db.tr
                dlg.buttonOk.text =             db.dbTr("Rename") + db.tr
                dlg.buttonCancel.text =         db.dbTr("Cancel")  + db.tr
                dlg.width =                     700
                dlg.inputField.passwordInput =  false
                dlg.inputField.validator =      text_validator
                dlg.inputField.text =           Material_MaterialFile.value
                dlg.buttonOk.isEnabled =        false
                dlg.ok.connect(materialRename)
                dlg.textInputChanged.connect(setOkEnabled)
                dlg.open()

            }

            function materialRename(){
                //console.log("------function materialRename(){-----")
                if(db.material.rename(Material_MaterialFile.value, getstring_dlg.inputField.text))
                {
                    Material_MaterialFile.value = getstring_dlg.inputField.text
                    db.material.name = Material_MaterialFile.value
                    getstring_dlg.ok.disconnect(materialRename)
                    getstring_dlg.textInputChanged.disconnect(setOkEnabled)
                    DBF.loadMat()
                    getstring_dlg.close()
                }
            }

            function setOkEnabled(){
                getstring_dlg.buttonOk.isEnabled = db.material.isValidName(getstring_dlg.inputField.text)
            }
        }
    }
}
