import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
import "DBFunctions.js" as DBF
import "MainFunctions.js" as MF

Item {
    id: toolbarMaterial
    objectName: "ELCEDEtoolbar"

    property string __style: S.styleName //mainrect.rootStyle
    property string __imagePath: S.Style[__style].imagePath

    x:0
    y: 775 //parent.height - height
    //width:parent.width - 290 //position_component.width
    width: 600
    height: 120

    property int buttonWidth: 160

    IntValidator{
        id: tool_number_validator
        bottom: 1
        top: 21
    }

    Row{
        x: 20
        y: 20
        spacing: 20
        width: parent.width
        height: 60
        Button{
            id:button_savematerial
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "save-w.png"
            foregroundImagePressed: __imagePath + "save.png"
            //text: db.dbTr("Save") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                DBF.saveTool(screenTool.tool_number)
            }
        }

        Button{
            id:button_cancelchanges
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "iconmonstr-x-mark-red-240.png"
            foregroundImagePressed: __imagePath + "iconmonstr-x-mark-black-240.png"
            //text: db.dbTr("Delete") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                DBF.cancelToolChanges(screenTool.tool_number)
            }

            //onReleased: { __isPressed = false }
        }

        Button{
            id:button_copymaterialtool
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "copy-w.png"
            foregroundImagePressed: __imagePath + "copy.png"
            property bool bResult: false
            onBResultChanged: {
                __isPressed = !bResult
            }

            onClicked: {

                var dlg = getstring_dlg

                dlg.titleText =                 db.dbTr("Enter tool number to copy this tool to") + db.tr
                dlg.buttonOk.text =             db.dbTr("OK") + db.tr
                dlg.buttonCancel.text =         db.dbTr("Cancel")  + db.tr
                dlg.width =                     300
                dlg.inputField.passwordInput =  false
                dlg.inputField.validator =      tool_number_validator
                dlg.inputField.text =           ""

                //dlg.inputField.text =           db.dbTr("new material") + db.tr
                //dlg.buttonOk.isEnabled =        screenTool.tool_number != dlg.inputField.value
                dlg.ok.connect(copyCurrentToolf)
                dlg.textInputChanged.connect(textInputChangedf)
                dlg.open()
            }
            function copyCurrentToolf(){
                DBF.copyTool(screenTool.tool_number, parseInt(getstring_dlg.inputField.text))
            }
            function textInputChangedf()
            {
                getstring_dlg.buttonOk.isEnabled = (parseInt(getstring_dlg.inputField.text) !== screenTool.tool_number )
            }
        }
    }
}
