//buld date: 2018-10-30
import QtQuick 2.0
import "style/Style.js"  as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1

Item {
    id: toolbarService
    objectName: "ELCEDEtoolbar"

    property string __style: S.styleName //mainrect.rootStyle
    property string __imagePath: S.Style[__style].imagePath
    property alias button_positionwidgetshow: button_positionwidgetshow

    x: 0
    y: 770
    width: 600
    height: 110

    property int buttonWidth: 160

    state: "POS_CONTROL_HIDDEN"

    states:[
        State{
            name: "POS_CONTROL_SHOWED"
        },
        State{
            name: "POS_CONTROL_HIDDEN"
        }
    ]

    transitions: [
        Transition {
            from: "POS_CONTROL_SHOWED"
            to: "POS_CONTROL_HIDDEN"
            NumberAnimation {target:toolbarService; properties: "y"; from: 570; to: 770 ; easing.type: Easing.InCirc }
        },
        Transition {
            from: "POS_CONTROL_HIDDEN"
            to: "POS_CONTROL_SHOWED"
            NumberAnimation {target:toolbarService; properties: "y"; from: 770; to: 570 ; easing.type: Easing.OutCirc}
        }
    ]



    Row{
        x: 20
        y: 20
        spacing: 20
        width: parent.width
        height: 60
        Button{
            id:button_drives
            width: buttonWidth
            height: parent.height
            text: db.dbTr("Drives") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                screenService.serviceflickableLV.currentIndex = 1
             }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_laser
            width: buttonWidth
            height: parent.height
            text: db.dbTr("Laser") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                screenService.serviceflickableLV.currentIndex = 2
            }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_pitch
            width: buttonWidth
            height: parent.height
            text: db.dbTr("Pitch") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                screenService.serviceflickableLV.currentIndex = 3
            }
            onReleased: {
                __isPressed = false
            }
        }
        Button{
            id:button_jobshow
            width: buttonWidth
            height: parent.height
            text: db.dbTr("V8 log") + db.tr
            //enabled: MF.isEnabled()

            onClicked: {
                screenService.serviceflickableLV.currentIndex = 4
            }
            onReleased: { __isPressed = false }
        }
        Button{
            id:button_positionwidgetshow
            width: buttonWidth
            height: parent.height
            foregroundImageNormal:  __imagePath + "screenshot-w.png"
            foregroundImagePressed: __imagePath + "screenshot.png"

            onClicked: {
                if(positionControl.state       === "SHOWED"){
                    positionControl.state       =  "HIDDEN"
                }
                else
                {
                    positionControl.state       = "SHOWED"
                }
            }
            onReleased: { __isPressed = false }
        }
    }
}

