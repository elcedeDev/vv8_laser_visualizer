import QtQuick 2.0
import tools 2.0
import vectovision 1.1
import "./style/Style.js"  as S


Rectangle{
    id: idV8KernelLog
    objectName: "AradexV8KernelLog"
    
    x: 0
    y: 0
    width: 450
    height: 250
    color: '#00000000' // 'transparent'
    clip : true

    property bool showErrors: true
    property bool showWarnings: true
    property bool showInfos: true

    property int maxMessages: 2500
    property int __messagesShown : listModelLogView.count
    property int __messagesLost : 0

    property string __textColorNormal: ""
    property int colorChoice: 1

    property int fontSize: S.Style[__styleName].logFontSize
    property string __fontFamily: S.Style[__styleName].logFontFamily

    property string __styleName: S.styleName

    property string __errorImage:   (S.Style[__styleName].errorImage !== "") ?
                                     S.Style[__styleName].imageFolder +
                                     S.Style[__styleName].errorImage : ""
    property string __warningImage: (S.Style[__styleName].warningImage !== "") ?
                                     S.Style[__styleName].imageFolder +
                                     S.Style[__styleName].warningImage : ""
    property string __messageImage: (S.Style[__styleName].messageImage !== "") ?
                                     S.Style[__styleName].imageFolder +
                                     S.Style[__styleName].messageImage : ""
    property int __logEntryHeight: 28
    property int __logEntryWidth: 50
    property int __logEntryDateTimeWidth : 20

    function refresh() {

        listModelLogView.clear()
        for(var i = 0; i < listModelLogData.count;i++)
        {
            if((showErrors && (eSeverity === 0 || eSeverity === 1)) || (showWarnings && (eSeverity === 2)) || (showInfos && (eSeverity === 3)) ){
                if (idFilter.text === "" || listModelLogData.get(i).Message.toLowerCase().indexOf(idFilter.text.toLowerCase()) !== -1)
                {
                    listModelLogView.append({"Severity": listModelLogData.get(i).Severity, "DateTime": " " + listModelLogData.get(i).DateTime, "Message": " -- " + listModelLogData.get(i).Message})
                    console.log("idV8KernelLog function refresh()  " + i)
                }
            }
        }
        listViewErrorLog.contentY = listViewErrorLog.contentHeight-listViewErrorLog.height // scroll to the end
        listViewErrorLog.returnToBounds()
        console.log("idV8KernelLog function refresh() ")
    }


    signal styleChanged(string strStyle)
    onStyleChanged:{
        __styleName = strStyle
    }

    signal newStyleAdded(string strNewStyleName, variant newStyleObject)
    onNewStyleAdded: {
        S.Style[strNewStyleName] = newStyleObject.Style[strNewStyleName]
    }


    ListModel {
        id: listModelLogData
    }

    Text{
        id: labelTestSize
        height:50
        width:5000
        font.pointSize  : {idV8KernelLog.fontSize > 0 ? idV8KernelLog.fontSize : 1}
        font.family     : idV8KernelLog.__fontFamily
        visible : false
        property bool first: true
    }

    TextField{
        id: idFilter
        width:  parent.width - idFilterClearBtn.width - idFilterClearBtn.spacing
        height: __textBorderBottomSize + __textBorderTopSize + __textHeight
        placeholderText: db.dbTr("Filter text") + db.tr
        fontSize : {idV8KernelLog.fontSize > 0 ? idV8KernelLog.fontSize : 1}

        onTextChanged: {
            listModelLogView.clear()
            for(var i = 0; i < listModelLogData.count;i++)
            {
                if (idFilter.text === "" || listModelLogData.get(i).Message.toLowerCase().indexOf(idFilter.text.toLowerCase()) !== -1)
                {
                    listModelLogView.append({"Severity": listModelLogData.get(i).Severity, "DateTime": " " + listModelLogData.get(i).DateTime, "Message": " -- " + listModelLogData.get(i).Message})
                }
            }
            listViewErrorLog.contentY = listViewErrorLog.contentHeight-listViewErrorLog.height // scroll to the end
            listViewErrorLog.returnToBounds()
        }
    }
    Button{
        id: idFilterClearBtn
        x : idFilter.width + spacing
        width:  30
        height: idFilter.height
        fontSize : {idV8KernelLog.fontSize > 0 ? idV8KernelLog.fontSize : 1}
        text: "x"
        //foregroundImageNormal:S.Style[__styleName].imageFolder + S.Style[__styleName].errorImage
        //foregroundImagePressed: foregroundImageNormal
        property int spacing : 5
        onClicked:{ idFilter.text = "" }
    }

    signal  newLogEntryReceived(string gstrMessage, string gstrDateTime, int eSeverity, int eAppInfo, int iLostMessages)
    onNewLogEntryReceived: {        
        //console.log(gstrDateTime + ": " + gstrMessage)
        //console.log(gstrDateTime + ": " + gstrMessage + " " +  eSeverity)

        if((showErrors && (eSeverity === 0 || eSeverity === 1)) || (showWarnings && (eSeverity === 2)) || (showInfos && (eSeverity === 3)) ){
            idV8KernelLog.__messagesLost = iLostMessages

            //labelDebug.text = "contentY" + listViewErrorLog.contentY + " contentHeight:" + listViewErrorLog.contentHeight + " height:" + listViewErrorLog.height
            var autoscroll = false
            if (listViewErrorLog.contentY == listViewErrorLog.contentHeight-listViewErrorLog.height || listViewErrorLog.contentHeight < listViewErrorLog.height)
            {
                autoscroll = true;
            }

            if (listModelLogData.count >= idV8KernelLog.maxMessages)
            {
                listModelLogData.remove(0);
            }
            listModelLogData.append({"Severity": eSeverity, "DateTime": gstrDateTime, "Message": gstrMessage})


            if (idFilter.text === "" || gstrMessage.toLowerCase().indexOf(idFilter.text) != -1)
            {
                if (listModelLogView.count >= idV8KernelLog.maxMessages)
                {
                    listModelLogView.remove(0);
                    if (!autoscroll)
                    {
                        listViewErrorLog.contentY = listViewErrorLog.contentY - idV8KernelLog.__logEntryHeight
                    }
                }
                listModelLogView.append({"Severity": eSeverity, "DateTime": " " + gstrDateTime, "Message": " -- " + gstrMessage})

                if (autoscroll)
                {
                    listViewErrorLog.contentY = listViewErrorLog.contentHeight-listViewErrorLog.height
                    listViewErrorLog.returnToBounds()
                }

                // check text size
                // idV8KernelLog.__logEntryHeight is also the width of the message icon
                if (labelTestSize.first === true)
                {
                    labelTestSize.text = " " + gstrDateTime
                    idV8KernelLog.__logEntryDateTimeWidth = labelTestSize.paintedWidth
                    labelTestSize.first = false
                }


                labelTestSize.text = " -- " + gstrMessage
                idV8KernelLog.__logEntryHeight = labelTestSize.paintedHeight
                if (labelTestSize.paintedWidth > idV8KernelLog.__logEntryWidth - idV8KernelLog.__logEntryHeight - idV8KernelLog.__logEntryDateTimeWidth)
                {
                    idV8KernelLog.__logEntryWidth = labelTestSize.paintedWidth + idV8KernelLog.__logEntryHeight + idV8KernelLog.__logEntryDateTimeWidth
                }
            }
        }
    }
    Item{

        x: 0
        y: idFilter.height + 5
        width: parent.width
        height: parent.height - y

        Flickable {
            id: listViewErrorLog
            x: 0
            y: 0
            width: parent.width - idScrollBarVert.width
            height: parent.height - idScrollBarHorz.height

            clip: true
            contentHeight: listModelLogView.count * idV8KernelLog.__logEntryHeight
            contentWidth: idV8KernelLog.__logEntryWidth

            boundsBehavior: Flickable.StopAtBounds
            flickableDirection: Flickable.HorizontalAndVerticalFlick

            Column {
                id: idListViewRowLine
                anchors.fill: parent.anchors.fill
                width: parent.width
                height: parent.height
                spacing: 0
                Repeater
                {
                    model : ListModel {
                        id: listModelLogView
                    }

                    delegate: listViewErrorLogDelegate
                }
            }

            Component {
                id: listViewErrorLogDelegate

                Item{
                    width: listViewErrorLog.width
                    height: __logEntryHeight

                    property variant logColors: [S.Style[__styleName].logFontColor0,
                                                 S.Style[__styleName].logFontColor1,
                                                 S.Style[__styleName].logFontColor2,
                                                 S.Style[__styleName].logFontColor3]

                    function getColor() {
                        return (__textColorNormal == "") ? logColors[idV8KernelLog.colorChoice] : idV8KernelLog.__textColorNormal;
                    }


                    Rectangle{
                        anchors.fill: parent.anchors.fill
                        width: listViewErrorLog.contentWidth
                        height: parent.height
                        color: "red"
                        visible: Severity === 0 ? true : false
                    }

                    Row {
                        id: idListViewRow
                        anchors.fill: parent.anchors.fill
                        width: listViewErrorLog.width
                        height: parent.height
                        spacing: 0
                        // MS_CRITICAL_ERROR   = 0,
                        // MS_ERROR            = 1,
                        // MS_WARNING          = 2,
                        // MS_INFO             = 3
                        //Label{text: Severity; opacity: 1; height:parent.height; width: 50}
                        Image{
                            id: messageImage
                            source: (Severity === 0) ? __errorImage :
                                    (Severity === 1) ? __errorImage :
                                    (Severity === 2) ? __warningImage :
                                    (Severity === 3) ? __messageImage : ""
                            smooth: S.isFast
                            height: parent.height
                            width: height

                        }
                        Text{
                            text            : DateTime
                            color           : getColor()
                            font.pointSize  : {idV8KernelLog.fontSize > 0 ? idV8KernelLog.fontSize : 1}
                            font.family     : idV8KernelLog.__fontFamily
                            height          : parent.height
                            width           : idV8KernelLog.__logEntryDateTimeWidth
                        }
                        Text{
                            text            : Message
                            clip            : true
                            color           : getColor()
                            font.pointSize  : {idV8KernelLog.fontSize > 0 ? idV8KernelLog.fontSize : 1}
                            font.family     : idV8KernelLog.__fontFamily
                            height          : parent.height
                            width           : 3000
                        }
                    }
                }
            }
        }
        
		ScrollBar {
                id: idScrollBarVert
                scrollArea: listViewErrorLog
                width: 10
                anchors {
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                }
            }

            ScrollBar {
                id: idScrollBarHorz
                scrollArea: listViewErrorLog
                height: 10
                orientation: Qt.Horizontal
                anchors {
                    right: parent.right
                    rightMargin: 8
                    left: parent.left
                    bottom: parent.bottom
                }
            }

    }
    Label{
        id: labelDebug
		fontSize: 10
		width: 400
        visible: false
    }
}
