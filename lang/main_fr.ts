<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr" sourcelanguage="en">
<context>
    <name>ComponentPositionSetup</name>
    <message>
        <location filename="../ComponentPositionSetup.qml" line="52"/>
        <source>current position</source>
        <translation type="unfinished">current position</translation>
    </message>
    <message>
        <location filename="../ComponentPositionSetup.qml" line="341"/>
        <source>target position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ComponentPositionSetup.qml" line="455"/>
        <source>absolute
 position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ComponentPositionSetup.qml" line="465"/>
        <source>relative
 position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComponentPositionSetup_org</name>
    <message>
        <location filename="../ComponentPositionSetup_org.qml" line="54"/>
        <source>current position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ComponentPositionSetup_org.qml" line="297"/>
        <source>target position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ComponentPositionSetup_org.qml" line="377"/>
        <source>absolute
 position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ComponentPositionSetup_org.qml" line="389"/>
        <source>relative
 position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorListModel</name>
    <message>
        <location filename="../ErrorListModel.qml" line="10"/>
        <source>IO is not described in ErrorListModel. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ErrorListModel.qml" line="15"/>
        <source>No Errors present </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MagazineItem</name>
    <message>
        <location filename="../MagazineItem.qml" line="63"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaterialItem</name>
    <message>
        <location filename="../MaterialItem.qml" line="53"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaterialItem1</name>
    <message>
        <location filename="../MaterialItem1.qml" line="88"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="119"/>
        <location filename="../MaterialItem1.qml" line="351"/>
        <source>X+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="144"/>
        <location filename="../MaterialItem1.qml" line="377"/>
        <source>Y+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="168"/>
        <location filename="../MaterialItem1.qml" line="402"/>
        <source>X-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="192"/>
        <location filename="../MaterialItem1.qml" line="427"/>
        <source>Y-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="222"/>
        <location filename="../MaterialItem1.qml" line="458"/>
        <source>X+/Y+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="247"/>
        <source>X-/Y+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="271"/>
        <location filename="../MaterialItem1.qml" line="509"/>
        <source>X-/Y-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="295"/>
        <location filename="../MaterialItem1.qml" line="484"/>
        <location filename="../MaterialItem1.qml" line="534"/>
        <source>X+/Y-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="321"/>
        <source>Work height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="565"/>
        <source>Control type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="589"/>
        <source>envelope cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="614"/>
        <source>max. deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem1.qml" line="639"/>
        <source>acceleration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaterialItem2</name>
    <message>
        <location filename="../MaterialItem2.qml" line="57"/>
        <source>recalculate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="64"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="86"/>
        <source>Macro start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="99"/>
        <location filename="../MaterialItem2.qml" line="449"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="126"/>
        <location filename="../MaterialItem2.qml" line="220"/>
        <location filename="../MaterialItem2.qml" line="291"/>
        <location filename="../MaterialItem2.qml" line="385"/>
        <location filename="../MaterialItem2.qml" line="477"/>
        <source>f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="150"/>
        <location filename="../MaterialItem2.qml" line="244"/>
        <location filename="../MaterialItem2.qml" line="315"/>
        <location filename="../MaterialItem2.qml" line="409"/>
        <location filename="../MaterialItem2.qml" line="501"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="175"/>
        <location filename="../MaterialItem2.qml" line="340"/>
        <location filename="../MaterialItem2.qml" line="526"/>
        <source>AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="201"/>
        <source>Acceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="272"/>
        <source>Constant drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="366"/>
        <source>Deceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialItem2.qml" line="436"/>
        <source>Macro end</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaterialMillingTool</name>
    <message>
        <location filename="../MaterialMillingTool.qml" line="68"/>
        <source>comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialMillingTool.qml" line="474"/>
        <source>Compensation on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MaterialMillingTool.qml" line="475"/>
        <source>Compensation off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalDlg</name>
    <message>
        <location filename="../ModalDlg.qml" line="120"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ModalDlg.qml" line="130"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Override</name>
    <message>
        <location filename="../Override.qml" line="16"/>
        <source>override</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenCombo</name>
    <message>
        <location filename="../ScreenCombo.qml" line="23"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenCombo.qml" line="24"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenJob1</name>
    <message>
        <location filename="../ScreenJob1.qml" line="35"/>
        <source>current position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="135"/>
        <source>zero position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="195"/>
        <source>Wood Lifter 
Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="196"/>
        <source>Wood Lifter 
Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="217"/>
        <source>job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="240"/>
        <source>material thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="268"/>
        <source>material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="289"/>
        <source>tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="314"/>
        <source>current height above mat.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="349"/>
        <source>line in job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="380"/>
        <source>Laser Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="388"/>
        <source>Suction up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="389"/>
        <source>Suction down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="480"/>
        <source>Z override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="537"/>
        <source>ERROR
FILE NOT FOUND
OR NOT SUPPORTED FORMAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="684"/>
        <source>rotated 90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="686"/>
        <source>as in design</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenJob1.qml" line="695"/>
        <source>requested tool</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenJob2</name>
    <message>
        <location filename="../ScreenJob2.qml" line="66"/>
        <source>ERROR
FILE NOT FOUND
OR NOT SUPPORTED FORMAT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenMagazine1</name>
    <message>
        <location filename="../ScreenMagazine1.qml" line="20"/>
        <source>magazin position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenMagazine2</name>
    <message>
        <location filename="../ScreenMagazine2.qml" line="33"/>
        <source>teach position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMagazine2.qml" line="143"/>
        <source>Place:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMagazine2.qml" line="292"/>
        <source>Teach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMagazine2.qml" line="337"/>
        <source>Clamp On/Off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenMaterial</name>
    <message>
        <location filename="../ScreenMaterial.qml" line="32"/>
        <source>Material File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenMaterial1</name>
    <message>
        <location filename="../ScreenMaterial1.qml" line="32"/>
        <source>Slope time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="59"/>
        <source>Thickness:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="85"/>
        <source>Background power:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="113"/>
        <source>Shaft height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="139"/>
        <source>Halfshell diamater:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="166"/>
        <source>execution optimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="167"/>
        <source>execution standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="173"/>
        <source>auto Z adjust off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="174"/>
        <source>auto Z adjust on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="180"/>
        <source>dynamic control on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="181"/>
        <source>dynamic control off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="188"/>
        <source>Steel cutting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="189"/>
        <source>Wood cutting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="197"/>
        <source>contour lookahead on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenMaterial1.qml" line="198"/>
        <source>contour lookahead off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenMaterial4</name>
    <message>
        <location filename="../ScreenMaterial4.qml" line="28"/>
        <source>Select tool for sequence number: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenService1</name>
    <message>
        <location filename="../ScreenService1.qml" line="21"/>
        <source>total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenService1.qml" line="126"/>
        <source>lubrication interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenService1.qml" line="322"/>
        <source>distance until next lubrication</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenService2</name>
    <message>
        <location filename="../ScreenService2.qml" line="144"/>
        <source>engraving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenService2.qml" line="145"/>
        <source>laser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenService2.qml" line="155"/>
        <source>suction on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenService2.qml" line="156"/>
        <source>suction off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenService3</name>
    <message>
        <location filename="../ScreenService3.qml" line="22"/>
        <source>current position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenService3.qml" line="127"/>
        <source>measure table</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenSetup</name>
    <message>
        <location filename="../ScreenSetup.qml" line="34"/>
        <source>zero position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="106"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="111"/>
        <source>Select Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="112"/>
        <source>Load File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="113"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="149"/>
        <source>Shell Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="150"/>
        <source>Shell In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="162"/>
        <source>Cover Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="163"/>
        <source>Cover Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="171"/>
        <source>Z-Break off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="172"/>
        <source>Z-Break on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="180"/>
        <source>clear table
after job on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="181"/>
        <source>clear table
after job off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="188"/>
        <source>ignore strike fault</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="189"/>
        <source>detect strike fault</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="203"/>
        <source>Wood Lifter Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="204"/>
        <source>Wood Lifter Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="212"/>
        <source>Hoover on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="213"/>
        <source>Hoover off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="221"/>
        <source>Suction up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="222"/>
        <source>Suction down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="230"/>
        <source>Magazine up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="231"/>
        <source>Magazine down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="246"/>
        <source>Clamp Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="247"/>
        <source>Unclamp Shell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="255"/>
        <source>Compensation on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenSetup.qml" line="256"/>
        <source>Compensation off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenStatistics</name>
    <message>
        <location filename="../ScreenStatistics.qml" line="19"/>
        <source>total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatistics.qml" line="124"/>
        <source>amplifier active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatistics.qml" line="153"/>
        <source>working time 
machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatistics.qml" line="182"/>
        <source>working time 
spindle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenStatus1</name>
    <message>
        <location filename="../ScreenStatus1.qml" line="34"/>
        <source>Laser Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1.qml" line="40"/>
        <source>spindle - warmup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1.qml" line="47"/>
        <source>reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1.qml" line="54"/>
        <source>Laser gas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1.qml" line="60"/>
        <source>Shell height sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1.qml" line="67"/>
        <source>Wood-lifter down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1.qml" line="128"/>
        <source>requested tool:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenStatus1_org</name>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="28"/>
        <source>Laser Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="36"/>
        <source>spindle - warmup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="45"/>
        <source>reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="54"/>
        <source>Laser gas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="62"/>
        <source>Shell height sensor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="71"/>
        <source>Wood-lifter down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="101"/>
        <source>override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="120"/>
        <source>spindle warm up time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ScreenStatus1_org.qml" line="155"/>
        <source>requested tool:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarJob</name>
    <message>
        <location filename="../ToolbarJob.qml" line="45"/>
        <source>simulator 
full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="69"/>
        <source>change 
tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="97"/>
        <source>optimize 
engraving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="138"/>
        <source>select job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="150"/>
        <location filename="../ToolbarJob.qml" line="413"/>
        <location filename="../ToolbarJob.qml" line="464"/>
        <source>Select job file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="151"/>
        <location filename="../ToolbarJob.qml" line="414"/>
        <location filename="../ToolbarJob.qml" line="465"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="180"/>
        <source>process 
job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="205"/>
        <source>interrupt 
job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="232"/>
        <source>show job 
size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="258"/>
        <source>clear 
table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="362"/>
        <source>set zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="493"/>
        <source>go to zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarJob.qml" line="520"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarMagazine</name>
    <message>
        <location filename="../ToolbarMagazine.qml" line="32"/>
        <source>tool 
change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine.qml" line="55"/>
        <source>save 
magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine.qml" line="77"/>
        <source>go to 
position</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarMagazine1</name>
    <message>
        <location filename="../ToolbarMagazine1.qml" line="57"/>
        <source>save 
magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine1.qml" line="81"/>
        <source>reset 
magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine1.qml" line="141"/>
        <source>magazine</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarMagazine2</name>
    <message>
        <location filename="../ToolbarMagazine2.qml" line="34"/>
        <source>go to 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine2.qml" line="71"/>
        <source>magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine2.qml" line="97"/>
        <source>save teached 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMagazine2.qml" line="129"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarMaterial</name>
    <message>
        <location filename="../ToolbarMaterial.qml" line="37"/>
        <source>load 
material 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="47"/>
        <source>Select Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="48"/>
        <source>Load File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="49"/>
        <location filename="../ToolbarMaterial.qml" line="113"/>
        <location filename="../ToolbarMaterial.qml" line="195"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="74"/>
        <source>save 
material 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="102"/>
        <source>Load Laser 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="112"/>
        <source>Select laser paramater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="144"/>
        <source>Save Laser 
parametars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="154"/>
        <location filename="../ToolbarMaterial.qml" line="236"/>
        <source>Save milling parameters as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="155"/>
        <location filename="../ToolbarMaterial.qml" line="237"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="184"/>
        <source>Load Milling 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="194"/>
        <source>Select milling parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarMaterial.qml" line="226"/>
        <source>Save Milling 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarService</name>
    <message>
        <location filename="../ToolbarService.qml" line="45"/>
        <source>machine 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="68"/>
        <source>measure 
tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="91"/>
        <source>go to 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="128"/>
        <source>clear 
table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="156"/>
        <source>save zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="178"/>
        <source>lubrication
time reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="206"/>
        <source>analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService.qml" line="228"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarService1</name>
    <message>
        <location filename="../ToolbarService1.qml" line="34"/>
        <source>lubrication X 
reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="62"/>
        <source>lubrication Y 
reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="89"/>
        <source>lubrication Z
reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="120"/>
        <source>lubrication 
time reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="177"/>
        <source>Laser test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="199"/>
        <source>service 
ELCEDE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="225"/>
        <source>create 
service 
report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="247"/>
        <source>create 
backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService1.qml" line="269"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarService2</name>
    <message>
        <location filename="../ToolbarService2.qml" line="34"/>
        <source>machine 
parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="56"/>
        <source>log 
files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="79"/>
        <source>V8 Super</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="101"/>
        <source>go to 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="137"/>
        <source>Laser test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="160"/>
        <source>exit service 
ELCEDE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="186"/>
        <source>spindle pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="210"/>
        <source>analyzer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService2.qml" line="232"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarService3</name>
    <message>
        <location filename="../ToolbarService3.qml" line="34"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService3.qml" line="61"/>
        <source>measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService3.qml" line="89"/>
        <source>spindle pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarService3.qml" line="114"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarSetup</name>
    <message>
        <location filename="../ToolbarSetup.qml" line="45"/>
        <source>Service 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="74"/>
        <source>go to 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="115"/>
        <source>reset 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="141"/>
        <source>clear 
table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="171"/>
        <source>set zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="205"/>
        <source>save zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="215"/>
        <source>Save zero position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="216"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="253"/>
        <source>load zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="264"/>
        <source>Select zero position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="265"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="292"/>
        <source>go to zero 
position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarSetup.qml" line="319"/>
        <source>save 
analyzer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarStatus</name>
    <message>
        <location filename="../ToolbarStatus.qml" line="34"/>
        <source>Change gas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarStatus.qml" line="57"/>
        <source>change tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarStatus.qml" line="84"/>
        <source>Spindle
 warmup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarStatus.qml" line="108"/>
        <source>stop spindle
 warmup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolbarTools1</name>
    <message>
        <location filename="../ToolbarTools1.qml" line="43"/>
        <source>Adjust speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ToolbarTools1.qml" line="70"/>
        <source>Adjust height</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="221"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="438"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="451"/>
        <source>Setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="465"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="481"/>
        <source>Magazine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="499"/>
        <source>Job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="515"/>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="531"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.qml" line="548"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
