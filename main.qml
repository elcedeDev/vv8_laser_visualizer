//build date:2019-06-27
import QtQuick 2.0
import "style/Style.js" as S
import tools 2.0
import tools.shortcut 2.0
import vectovision 1.1
//import Analyser 1.0
import "localVariables.js" as LV
import "MainFunctions.js" as MF
import "DBFunctions.js" as DBF
import "LanguageFunctions.js" as LF
import elcede.database 1.0


FocusScope {
    id: mainrect
    objectName: "aradexRoot"
    smooth: S.isFast
    width: 1280
    height: 1024
    property string appName: "ELCEDE LaserMate Visualizer"
    property string appVersion: "1.0.0"
    property string appBuildDate: "2019-05-14"
    property string appRevision: "8"


    property real   originalWidth
    property real   originalHeight
    property real   thisScale

    property string rootStyle: S.styleName
    property string imagePath: S.Style[S.styleName].imagePath

    property bool   isRotationUsed: bUseRotation.value //bMachineIsRotation.value //
    property bool   isInchesUsed: bUseInches.value

    signal positionControlStateChanged(string pState)

    property string strdbresult

    property bool   isEnabledMilling: bEnableMilling.value
    property int tool_number: Toolchange_Milling_iTool.value
    signal toolChangeWidgetStateChanged(string pState)
    property string toolName_io:        "Material_Tool_" + tool_number + "_strNote"
    property string toolLength_io:      "Toolmeasurement_Milling_dLength"

    onTool_numberChanged: {
        toolName_io =       "Material_Tool_" + tool_number + "_strNote"
        toolLength_io =     "Toolmeasurement_Milling_dLength"
    }

    property bool isMaterialDirty: false

    property int fieldWidth: 160
    property int fieldHeight: 55
    property int rowSpacing: 40
    property int toggle_button_height: 79


    //-------------------------- emergency key ------------------------
    property bool isEmergencyChainKeyTurned: EmergencyChain_bKey.value


    onIsEmergencyChainKeyTurnedChanged: {
        emergencyChainKeyTurned()
    }

    property int runState: _ApplicationRunState_iKernel.value
    onRunStateChanged: {
        if(_ApplicationRunState_iKernel.value === 5)
            dlgStartUp.close()
    }

    Dialog{
        id: dlgStartUp
        anchors.fill: parent
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#EEEEEE"
            opacity: 0.45
        }
        Label{
            id:label_tool_number
            anchors.fill: parent
            //height: parent.height
            text: db.dbTr("Starting machine up. Please wait...") + db.tr
            //width:parent.width
            colorChoice: 1
            __verticalTextAlignement: Text.AlignVCenter
            horizontalTextAlignement: Text.AlignHCenter
            fontSize: 24
        }
    }

    //TODO: use this on ScreenTool
    Dialog{
        id:dlgToolChooser
        width: 980
        height:  4*(43+30)+30
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#444444"
            opacity: 0.95
            radius: 8
        }

        property int dlg_tool_number

        signal toolChosen()

        Grid {
            x: 30; y: 30
            rows:  4
            columns:  5
            spacing: 30

            Repeater {
                model:  20
                Button {
                    width: 160; height: 43
                    text: "[" + (index + (bUseRotation.value ? 21 : 1)) + "] " + eval("Material_Tool_" + (index + 1) + "_strNote").value
                    onClicked: {
                        dlgToolChooser.dlg_tool_number = index + (bUseRotation.value ? 21 : 1)
                        dlgToolChooser.toolChosen()
                        dlgToolChooser.close()
                    }
                }
            }
        }
    }
    DialogYesNo{
        id:dlgEmergencyKey
        width: 900
        height: 300
        dialogWidth: width - 10
        dialogHeight: height
        buttonNoVisible: false
        buttonCancelVisible: false
        opacity: 1
        colorChoice: 3
        titleText   : db.dbTr("Emergency chain key is turned") + db.tr
        messageText : db.dbTr("By pressing the OK button you confirm that you have turned the emergency chain key") + db.tr
        imageChoice : 1
        onOk: {
            v8Msg.postMessage(v8Msg.ms_INFO, "The user confirmed the emergency chain key is turned" );
            EmergencyChain_bConfirmedKey.value = true
        }
        Rectangle{
            id: dlgEmergencyKeyPanel
            anchors.fill: parent
            z:-1
            color: "#FF5555"
            opacity: 0.95
            radius: 8
        }

    }

    function emergencyChainKeyTurned(){
        var dlg = dlgEmergencyKey
        if(EmergencyChain_bKey.value === true){
            v8Msg.postMessage(v8Msg.ms_INFO, "The emergency chain key dialog box is displayed" );

            dlg.open()
        }
        else
        {
            if (dlg.visible)
                dlg.close()
        }
    }

    //-----------------------------------------------------------------
    //---------------------------UPS-----------------------------------
    property bool isUSVWaitingToShutDown: Machine_Configuration_UseUSV.value === true ? USV_Out_bWaitingToShutDown.value : false
    property bool isUSVCollectiveFault:   Machine_Configuration_UseUSV.value === true ? USV_In_bCollectiveFault.value : false

    onIsUSVWaitingToShutDownChanged: fUSVWaitingToShutDown()
    onIsUSVCollectiveFaultChanged: fUSVCollectiveFault()

    DialogYesNo{
        id:dlgUSVWaitingToShutDown
        width: 900
        height: 300
        dialogWidth: width - 10
        dialogHeight: height
        buttonNoVisible: false
        buttonCancelVisible: false
        opacity: 1
        colorChoice: 3
        titleText   : db.dbTr("UPS waiting to shut down...") + db.tr
        messageText : db.dbTr("There is a problem with the power supply. You have limited time to save your files.") + db.tr
        imageChoice : 2
        buttonOkText: "Close"
        onOk: {
            v8Msg.postMessage(v8Msg.ms_INFO, "The user pressed the close button" );
            Qt.quit()
        }
        Rectangle{
            id: dlgUSVWaitingToShutDownPanel
            anchors.fill: parent
            z:-1
            color: "#5555FF"
            opacity: 0.95
            radius: 8
            Label {
                id: name
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                horizontalTextAlignement: Text.AlignHCenter
                width: 500
                height: 50
                fontSize: 18
                __textColorNormal: "white"
                text: Machine_Configuration_UseUSV.value === true ? db.dbTr("Time left: ") + db.tr + parseInt(USV_Out_dSwitchOffTime.value) + " s": ""
            }
        }

    }

    function fUSVWaitingToShutDown(){
        var dlg = dlgUSVWaitingToShutDown

        if(USV_Out_bWaitingToShutDown.value === true){

            v8Msg.postMessage(v8Msg.ms_INFO, "UPS Waiting To Shut Down box is displayed" );
            dlg.open()
        }
        else
        {
            if (dlg.visible)
                dlg.close()
        }

    }

    function fUSVCollectiveFault(){

        if( USV_In_bCollectiveFault.value ){
            var dlg = dialogMsgBox
            if(dlg.visible) {
                USV_In_bCollectiveFault.value = false
                return
            }
            dlg.titleText   = db.dbTr("UPS General error") + db.tr
            dlg.messageText = db.dbTr("The UPS generated an error. \nPlease check the error #number on the UPS display.") + db.tr
            dlg.width = 700
            dlg.imageChoice = 1
            dlg.open()
            USV_In_bCollectiveFault.value = false
        }

    }



    //-----------------------------------------------------------------


    onIsEnabledMillingChanged: {
        radio_Magazine_Menu.enabled = bEnableMilling.value
        //if( bEnableMilling.value )
        //    screenMagazine.source = "ScreenMagazine.qml"
        //else
        //    screenMagazine.source = "ScreenEmpty.qml"
    }

    //-- props used in job screen
    property bool   bshowdirection: false
    property bool   bshowtool0: false


    onIsRotationUsedChanged: {
        console.log("----- onIsRotationUsedChanged -----", "bUseRotation.value:", bUseRotation.value )
        MF.toggleCombo()
    }

    onIsInchesUsedChanged: MF.convertUnits()

    //	signal setResizeMode
    //  iMode=0: SizeRootObjectToView (standard behave, frame windows does not change size)
    //  iMode=1: SizeViewToRootObject ( framewindow changes size according to the size of the root object (mainrect)
    signal setResizeMode(int iMode)

    // move the main window
    signal move(int x, int y)

    // signal that is received from the viewer on moving for the virtual keyboard
    signal movedForKeyboard(int x, int y)

    // signal that is received in c++ code
    signal styleBackgroundColor(color colBackground)

    // signal that is received in c++ code on style change
    signal styleChanged(string newStyle)
    onStyleChanged: {
        rootStyle = newStyle
        styleBackgroundColor(S.Style[mainrect.rootStyle].backgroundColor)
    }

    // signal that is received in c++ code
    signal scaledSizeChanged(real minScale)
    onScaledSizeChanged: thisScale = minScale

    signal languageChanged()
    signal timerActivated()
    // this signal needs to be emitted if the style is to be changed from root object
    signal changeRootStyle(string newStyle)

    DB{ id:db }


    onRootStyleChanged:{
        styleChanged(rootStyle)
        var stateTemp = mainrect.state
        mainrect.state = ""
        mainrect.state = stateTemp
    }

    function clickRoot() {
        swipearea.forceActiveFocus()
    }

    function saveMaterial(){
        if(isMaterialDirty){
            DBF.saveMat()
            isMaterialDirty = false
            console.log("automatic saveMaterial")
        }
    }



    function   jsInitMainVariable(){


        Clamp_OnOff.value =  true;
        ClearTable_bOn.value = true;

        // Change this to false if it is a Flatbed Controller
        //            SetIoValue("isRotation", true );

        if( bUseRotation.value ) {
            MoveMachine_Rotation_In_bActive.value =  true;
            MoveMachine_Flatbed_In_bActive.value =  false;
            //bEnableMilling.value =  true;
            //Machine_bUseJobSplitting.value =  true;
            //Toolchange_Milling_bActive.value =  true;
            //Toolchange_Milling_Debug_bNoReference.value =  false;

        } else {
            MoveMachine_Rotation_In_bActive.value =  false;
            MoveMachine_Flatbed_In_bActive.value =  true;
            //bEnableMilling.value =  false;
            //Machine_bUseJobSplitting.value =  false;
            //TODO: this is not found !!!! -- ToolChange -> Toolchange
            //Toolchange_Milling_bActive.value =  false;
            //Toolchange_Milling_Debug_bNoReference.value =  true;

        }
        Compensation_SpindleGradient_bIsOn.value =  true;

        //Set default Z height
        //--Axis_Z_dSetPoint.value =  0.240;

        bAbsoluteRelative.value = true;
        //--ClearTable_bJobStarted.value = false;

        MoveMachine_bUseSetupRange.value =  false;
        MoveMachine_bUseServiceRange.value =  false;

    }


    V8PostMessage {
        id: v8Msg
    }


    // signal that is emitted by the viewer app when the application is ready to run
    signal applicationStart()



    onApplicationStart: {

        try{
            if (_dCycleTime !== "undefined") {
                console.log("-------------------------- V8 connection --------------------------")
                //TODO: Combo rota flatbed
                if(Machine_Configuration_Machine_Type.value === "LCS")
                    machinelogo.source = "images/LCSevo.svg"
                else if(Machine_Configuration_Machine_Type.value === "Flatbed")
                    machinelogo.source = "images/LASERMATEevo.svg"
                else if(Machine_Configuration_Machine_Type.value === "RotaMate")
                    machinelogo.source = "images/ROTAMATEevo.svg"

                console.log("Machine_Configuration_Machine_Type.value:", Machine_Configuration_Machine_Type.value )
                if(_ApplicationRunState_iKernel.value !== 5)
                    dlgStartUp.open()
            }
        } catch( err) {
            console.log("-------------------------- Demo mode ------------------------------")
            //IO.createIOs()
        }

        //console.log("ToolChange_Milling_New_iTool: ", ToolChange_Milling_New_iTool.value )

        //LV.setIsRotation()
        originalWidth = width
        originalHeight = height
        //isRotationUsed = LV.isRotation

        console.log("------------------------main.onApplicationStart: application started-------------------------")
        if( db.connect() ){
            console.log("------------------------main.onApplicationStart: database connected -------------------------")
            mainrect.strdbresult = db.result;
            db.language = LF.selectedLangShort(strVVSwitchLanguage.value)
        }else
            console.log("------------------------main.onApplicationStart: database NOOOOT connected -------------------------")


        jsInitMainVariable()

        if(isRotationUsed && ((iEngravingOption.value === 2) || (iEngravingOption.value === 4)) ){
            bEnableEngraving.value = true
        }else if( !isRotationUsed && ((iEngravingOption.value === 1) || (iEngravingOption.value === 4)) ){
            bEnableEngraving.value = true
        }else{
            bEnableEngraving.value = false
        }

        console.log("isRotationUsed:", isRotationUsed, "iEngravingOption.value", iEngravingOption.value)

        MF.toggleCombo()
        ltTranslate.selectLanguage(strVVSwitchLanguage.value, false)
        radio_Magazine_Menu.enabled = bEnableMilling.value
        //convertUnits()
        //try to load material from database
        db.material.name = Material_MaterialFile.value
        if(DBF.loadMat())
            v8Msg.postMessage(v8Msg.ms_INFO, "Material loaded from DB: " +  Material_MaterialFile.value);
        else
            v8Msg.postMessage(v8Msg.ms_ERROR, "Material NOT loaded from DB: " +  Material_MaterialFile.value);


        if(EmergencyChain_bKey.value) emergencyChainKeyTurned()
        if(Machine_Configuration_UseUSV.value){
            if(USV_Out_bWaitingToShutDown.value) fUSVWaitingToShutDown()
        }

    }

    DialogFileChooser {
        id: dialogFileChooser
        width: 800
        height: 690
        dialogWidth: width - 30
        dialogHeight: height
        opacity: 1
        colorChoice: 2
        localFileSystem: true
        buttonCancelVisible: false
        __buttonNoText:db.dbTr("Cancel") + db.tr
        __buttonNoVisible: true
        property string type
        onOk:{
            console.log(" ----------------- dialogFileChooser.onOk ----------------- state:", state, "type:", type)
            switch(type){
            case "JOB":
                MF.selectJob()
                break
            case "SAVEZEROPOSITION":
                MF.saveZeroPosition()
                break
            case "LOADZEROPOSITION":
                MF.loadZeroPosition()
                break
            }
        }
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#DDDDDD"
            opacity: 0.95
            radius: 8
        }
    }

    DialogDBChooser {
        id: dialogLoadMaterial
        width: 800
        height: 690
        dialogWidth: width - 30
        dialogHeight: height
        opacity: 1
        colorChoice: 2
        buttonCancelVisible: false
        __buttonNoText:db.dbTr("Cancel") + db.tr
        __buttonNoVisible: true
        titleText:         db.dbTr("Select Material") + db.tr
        fileChooserText:   db.dbTr("Load Material from DB") + db.tr
        buttonOkText:      db.dbTr("Load") + db.tr
        onNo:{ close()}
        onOk: {
            db.material.name = currentFileName
            if(DBF.loadMat()){
                Material_MaterialFile.value = currentFileName
                v8Msg.postMessage(v8Msg.ms_INFO, "Material loaded from DB: " +  Material_MaterialFile.value);
            }else{
                v8Msg.postMessage(v8Msg.ms_ERROR, "Material NOT loaded from DB: " +  Material_MaterialFile.value);
            }
            close()
        }

        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#C8F0C8"
            opacity: 0.95
            radius: 8
        }
    }

    DialogDBChooser {
        id: dialogDeleteMaterial
        width: 800
        height: 690
        dialogWidth: width - 30
        dialogHeight: height
        opacity: 1
        colorChoice: 2
        buttonCancelVisible: false
        __buttonNoText:db.dbTr("Cancel") + db.tr
        __buttonNoVisible: true
        titleText:         db.dbTr("Select Material to delete") + db.tr
        fileChooserText:   db.dbTr("Delete Material from DB") + db.tr
        buttonOkText:      db.dbTr("Delete") + db.tr
        onNo:{ close()}
        onOk:{
            if(Material_MaterialFile.value !== currentFileName )
            {
                db.material.deleteMat(currentFileName)
                close()
            }
            else
            {
                console.log("Material_MaterialFile.value: ", Material_MaterialFile.value, "db.material.name: ", db.material.name, "currentFileName: ", currentFileName)
                var dlg = dialogMsgBox

                dlg.titleText   = db.dbTr("Error deleting material") + db.tr
                dlg.messageText = db.dbTr("Current material cannot be deleted!") + db.tr
                dlg.imageChoice = 1
                dlg.open()

                close()
            }
        }

        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#F0C8C8"
            opacity: 0.95
            radius: 8
        }
    }

    DialogMsgBox {
        id: dialogMsgBox
        width: 600
        height: 290
        dialogWidth: width - 30
        dialogHeight: height
        opacity: 1
        colorChoice: 1
        onOk: {EmergencyChain_bConfirmedKey.value = true
            v8Msg.postMessage(v8Msg.ms_INFO, "onIsEmergencyChainKeyTurnedChanged: messageBox is closed " );}
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#DDDDDD"
            opacity: 0.95
            radius: 8
        }
    }

    Dialog{
        id: getstring_dlg
        width: 220
        height: 140
        property alias titleText: title.text
        property alias inputField: inputField
        property alias buttonOk: getstring_dlg_ok
        property alias buttonCancel: getstring_dlg_cancel

        signal textInputChanged()
        //onOk:{}

        Rectangle{
            anchors.fill: parent
            color: "#DDDDDD"
            radius: 8
            Label{
                id:title
                x: 8
                height: 25
                width: parent.width - 16
                text: db.dbTr("Enter password") + db.tr
                fontSize: 10
                colorChoice: 1
            }

            TextField{
                id: inputField
                x: 8
                y: 25
                width: parent.width - 16
                height: 43
                passwordInput: true
                onTextChanged: getstring_dlg.textInputChanged()
            }

            Row{
                y: 85
                spacing: 20
                height: 43
                anchors.horizontalCenter: parent.horizontalCenter

                Button{
                    id: getstring_dlg_ok
                    text: db.dbTr("OK") + db.tr
                    width: 90
                    height: parent.height
                    onClicked: {
                        getstring_dlg.setOk()
                        //   getstring_dlg.close()
                    }
                }
                Button{
                    id: getstring_dlg_cancel
                    text: db.dbTr("cancel") + db.tr
                    width: 90
                    height: parent.height
                    onClicked: {
                        inputField.text = ""
                        getstring_dlg.setNo()
                        getstring_dlg.close()
                    }
                }
            }
        }
    }

    DialogYesNo {
        id: dialogEnterToolNumber
        width: 600
        height: 290
        dialogWidth: width - 30
        dialogHeight: height
        opacity: 1
        colorChoice: 3
        titleText: db.dbTr("Check tool number") + db.tr
        messageText: db.dbTr("Please check the new tool number! Is it:") + db.tr + Toolchange_Milling_iTool.value + "?"
        buttonCancelVisible: false
        buttonNoText: db.dbTr("No") + db.tr
        buttonOkText: db.dbTr("Yes") + db.tr
        onOk: {
            //close_cone.visible = false
            toolchange_widget.state = "AUTOTOOLCHANGE"
            Toolchange_Milling_iTool.value = Toolchange_Milling_New_iTool.value
            Toolmeasurement_Milling_bMeasure.value = true
        }
        onNo: {
            manual_toolchange.__pressed = true
        }
        Rectangle{
            anchors.fill: parent
            z:-1
            color: "#888888"
            opacity: 0.95
            radius: 8
        }
    }

    Rectangle{
        id: backgroundRectangle
        anchors.fill: parent
        color: S.Style[mainrect.rootStyle].backgroundColor
        x: -1

        /*

        Timer {
                id: timer
            }

        function delay(delayTime, cb) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.start();
        }
*/

        //main central rectangle
        Rectangle{
            id: mainUserWindow
            x: 5
            y: 90
            width: parent.width - 10
            height: parent.height - 145
            color: S.Style[mainrect.rootStyle].backgroundColor
            border.color: "black"
        }

        //--ELCEDE screen header{
        Image{
            id: elcedelogo
            width: 221
            height: 56
            sourceSize.height: height
            sourceSize.width: width
            anchors.left: parent.left
            anchors.leftMargin: 30
            anchors.top: parent.top
            anchors.topMargin: 30
            source: imagePath + "ELCEDELogo.svg"
        }
        Rectangle{
            id: elcedeLine
            anchors.top: parent.top
            anchors.topMargin: 76
            anchors.left: elcedelogo.right
            anchors.leftMargin: 10
            height: 10
            anchors.right: parent.right
            anchors.rightMargin: 30
            color: "#991b1e"
        }
        Image{
            id: machinelogo
            width: 231
            height: 23
            sourceSize.height: height
            sourceSize.width: width
            anchors.right: parent.right
            anchors.rightMargin: 38
            anchors.top: parent.top
            anchors.topMargin: 45
            //source: isRotationUsed ? "images/ROTAMATEevo.svg" : "images/LASERMATEevo.svg"
        }
        // --ELCEDE screen header}
        //GroupBox{
        Rectangle{
            id: label_current_date_time
            x: 1030
            y: 4
            width: 265
            height: 30
            color: "transparent"
            Image{
                source: imagePath + "Date.png"
                width: 26
                height: 30
                Label{
                    height: 36
                    anchors.left: parent.right
                    anchors.top: parent.top
                    anchors.topMargin: -5
                    fontSize:10
                    textIO:"LocalTime_Out_strDate"
                    //text:"2016-08-02"
                }
            }

            Image{
                source: imagePath + "Time.png"
                width: 33
                height: 30
                anchors.left: parent.left
                anchors.leftMargin: 120

                Label{
                    anchors.left:parent.right
                    anchors.top: parent.top
                    anchors.topMargin: -5
                    fontSize:10
                    textIO:"LocalTime_Out_strTime"
                    //text:"11:20:01"
                }
            }
        }


        //--ELCEDE screen header}

        // GroupBox {
        FocusScope{
            id: mainContainer
            x: mainUserWindow.x + 2
            y: mainUserWindow.y + 2
            width: mainUserWindow.width - 4
            height: mainUserWindow.height -4

            //colorChoice: 1
            //headerVisible: false
            /*
            Rectangle{
                width: parent.width
                height: parent.height
                color:"green"
            }
*/

            ComponentPositionSetup{
                id:positionSetup
                x:925
                y:0
                width: 315
                height: 200
                visible: false
            }

            Rectangle{
                id: positionControlContainer
                x: 0
                y: 670
                //width: 926
                width: 900
                height: 200
                clip: true
                color:"transparent"
                z:99
                visible: false
                //color:"red"

                PositionControl{
                    id:positionControl
                    x: parent.width
                    y: 0
                    width: parent.width
                    z: 1
                    visible:true

                    state: "HIDDEN"

                    states: [
                        State {
                            name: "SHOWED"
                            StateChangeScript {
                                script:{
                                    mainrect.positionControlStateChanged("SHOWED");
                                }
                            }
                        },
                        State {
                            name: "HIDDEN"
                            StateChangeScript {
                                script:{
                                    mainrect.positionControlStateChanged("HIDDEN");
                                }
                            }
                        }
                    ]
                    transitions: [
                        Transition {
                            from: "SHOWED"
                            to: "HIDDEN"
                            NumberAnimation {target:positionControl; properties: "x"; from: 0; to: 926 ; easing.type: Easing.InCirc }
                        },
                        Transition {
                            from: "HIDDEN"
                            to: "SHOWED"
                            NumberAnimation {target:positionControl; properties: "x"; from: 926; to: 0 ; easing.type: Easing.OutCirc}
                        }
                    ]
                }

            }


            Row{
                id: materialFileName
                x: 20
                y: 20
                height: 43
                spacing: rowSpacing
                width: 890
                visible: false
                Button{
                    id: selectMaterial
                    height: parent.height
                    width: fieldWidth
                    text: db.dbTr("Material") + db.tr
                    onClicked: {
                        dialogLoadMaterial.open()
                        dialogLoadMaterial.initializeDir()
                    }
                }
                TextField{
                    id: text_field_material
                    textIO: "Material_MaterialFile"
                    height: parent.height
                    width: 730
                    isEnabled: false
                }
            }

            Row{
                id: jobFileNameGroup
                x: 20
                y: 20 + 43 + 6
                width: 890
                height: 43
                spacing: rowSpacing
                visible: false
                Button{
                    id: selectJob
                    height: parent.height
                    width: fieldWidth
                    text: db.dbTr("Job") + db.tr

                    onClicked: {
                        //console.log(" ----------------- selectJob pressed ----------------- ")
                        var path = Job_strLoadPath.value
                        //path = path.replace(/\//g, "\\")
                        path = path.replace(/\\/g, "/");
                        var dlg = dialogFileChooser

                        dlg.titleText =         db.dbTr("Select job file") + db.tr
                        dlg.fileChooserText =   db.dbTr("Load File") + db.tr
                        dlg.buttonOkText =      db.dbTr("Load") + db.tr
                        dlg.localFileSystem =   true
                        dlg.fileExtension = "nc|acc|cf2|dxf|plt|hpgl|hgl|rcc|rcf|cnc|N"
                        dlg.currentPath = path
                        var fullName = Job_strFile.value
                        //fullName = fullName.replace(/\//g, "\\")
                        //console.log("Select Job: ", "path: ", path, "fullName: ", fullName )
                        dlg.type ="JOB"
                        dlg.currentFileName = fullName.substring(fullName.lastIndexOf('\\') + 1)
                        //dlg.currentFileName = fullName.substring(fullName.lastIndexOf('/') + 1)
                        dlg.initializeDir()
                        //console.log("--- currentFileName: ", dlg.currentFileName, "currentPath:" , dlg.currentPath)
                        dlg.open()

                    }

                }
                TextField{
                    id: text_field_job
                    textIO: "Job_strFile"
                    height: parent.height
                    width: 730
                    isEnabled: false
                    Rectangle{
                        id: jobProgressBar
                        height: parent.height
                        width: parent.width * (bUseRotation.value === true ? Interpolator_Rotation_Out_dCurrentContourPercent.value : Interpolator_Flatbed_Out_dCurrentContourPercent.value )
                        color: "green"
                        opacity: 0.5
                    }
                }
            }
            Column{
                x: 20
                y: 20
                id: toolchange_widget
                visible: false
                width: parent.width
                spacing: 25
                height: 130 + toggle_button_height
                state:"AUTOTOOLCHANGE"
                Row{
                    spacing: 30
                    Column{
                        width: fieldWidth
                        Label{
                            width: parent.width
                            text: db.dbTr("Current tool") + db.tr
                            colorChoice: 1
                        }
                        TextField{
                            width: parent.width
                            height: fieldHeight
                            textIO: "Toolchange_Milling_iTool"
                            readOnly: true
                            digitsAfterComma: 0
                            horizontalTextAlignement: Qt.AlignHCenter
                        }
                    }
                    Column{
                        width: fieldWidth*2
                        Label{
                            width: parent.width
                            text: db.dbTr("Name") + db.tr
                            colorChoice: 1
                        }
                        TextField{
                            width: parent.width
                            height: fieldHeight
                            textIO: toolName_io
                            readOnly: true
                            digitsAfterComma: 0
                        }
                    }
                    Column{
                        width: fieldWidth - 20
                        Label{
                            width: parent.width
                            text: db.dbTr("Length") + db.tr
                            colorChoice: 1
                        }
                        TextField{
                            width: parent.width
                            height: fieldHeight
                            textIO: toolLength_io
                            readOnly: true
                            unitEnable: true
                            unit: strUnitMM.value
                            ioFactor: dMToMM.value
                            horizontalTextAlignement: Qt.AlignRight
                        }
                    }

                    Column{
                        width: fieldWidth - 15
                        Label{
                            width: parent.width
                            text: db.dbTr("Requested tool") + db.tr
                            colorChoice: 1
                        }
                        TextField{
                            width: parent.width
                            height: fieldHeight
                            textIO: "Toolchange_Milling_New_iTool"
                            unitEnable: false
                            digitsAfterComma: 0
                            horizontalTextAlignement: Qt.AlignHCenter
                        }
                    }

                    Button{
                        anchors.top: parent.top
                        anchors.topMargin: 36
                        width: fieldWidth - 15
                        height: fieldHeight
                        text: "Toolchange"
                        __horizontalTextAlignement: Qt.AlignHCenter
                        onClicked: {
                            Toolchange_Milling_bForceChange.value = true;
                            Toolchange_Milling_bChange.value = true
                            //isToolChangeDone = true
                        }
                    }

                    RadioButton{
                        id:manual_toolchange
                        anchors.top: parent.top
                        anchors.topMargin: 36
                        width: fieldWidth - 15
                        height: fieldHeight
                        text: "Manual\n Toolchange"
                        __horizontalTextAlignement: Qt.AlignHCenter

                        property bool bMachineMoves: MoveMachine_bMove.value

                        onBMachineMovesChanged: {
                            if( !MoveMachine_bMove.value && __pressed)
                                Suction_Out_bSetUp.value = true
                        }

                        onClicked: {
                            __pressed = !__pressed

                            if(__pressed){
                                //close_cone.visible = true
                                toolchange_widget.state = "MANUALTOOLCHANGE"
                                if(bUseRotation.value){
                                    Axis_X_Rotation_Master_dMoveDistanceNominal.value = Axis_X_dPosition.value * -1;
                                    Axis_Y_Rotation_Master_dMoveDistanceNominal.value = Axis_Y_dPosition.value * -1;
                                    Axis_Z_dDistanceNominal.value = ClearTable_Position_dZ.value - Axis_Z_dPosition.value;
                                    MoveMachine_Rotation_In_bStart.value = true;
                                }
                                else
                                {
                                    Axis_X_Flatbed_Master_dMoveDistanceNominal.value = Axis_X_dPosition.value * -1;
                                    Axis_Y_Flatbed_Master_dMoveDistanceNominal.value = Axis_Y_dPosition.value * -1;
                                    Axis_Z_dMoveDistanceNominal.value = ClearTable_Position_dZ.value - Axis_Z_dPosition.value;
                                    MoveMachine_Flatbed_In_bStart.value = true;
                                }
                                while(MoveMachine_bMove.value){
                                    MF.pause(100)
                                }

                                if(!MoveMachine_bMove.value){
                                    Suction_Out_bSetUp.value = true
                                }
                            }else if(!__pressed){
                                if( Toolchange_Milling_iTool.value === Toolchange_Milling_New_iTool.value
                                        ||  Toolchange_Milling_New_iTool.value === 0)
                                {
                                    dialogEnterToolNumber.open()
                                }else{
                                    //close_cone.visible = false
                                    toolchange_widget.state = "AUTOTOOLCHANGE"
                                    Toolchange_Milling_iTool.value = Toolchange_Milling_New_iTool.value
                                    isManualToolMeasurement = true
                                    //Toolmeasurement_Milling_bMeasure.value = true
                                }
                            }
                        }
                    }
                }


                Row{
                    id: close_cone
                    //visible: false
                    visible: toolchange_widget.state == "MANUALTOOLCHANGE"
                    spacing: 30
                    x: 885
                    ToggleButtonFlatReverse{
                        id: toggle_close_cone
                        width: fieldWidth - 15
                        height: toggle_button_height
                        text: db.dbTr("Close cone") + db.tr
                        stateIO: bEnableMilling.value ?  "Spindle_ClampingSleeve_Out_bOpen" : ""
                    }
                    Column{
                        anchors.top: parent.top
                        anchors.topMargin: 25
                        StatusButton{
                            height: 27
                            indicator_text: db.dbTr("Cone closed") + db.tr
                            inverseState: true
                            indicator_IO: bEnableMilling.value ?  "Spindle_ClampingSleeve_Out_bOpenClamp" : ""
                        }
                        StatusButton{
                            height: 27
                            indicator_text: db.dbTr("Tool inserted") + db.tr
                            indicator_IO: bEnableMilling.value ?  "Spindle_ClampingSleeve_In_bIsClosedTool" : ""
                        }
                    }
                }
                states:[
                    State{
                        name:"AUTOTOOLCHANGE"
                        StateChangeScript {
                            script:{
                                mainrect.toolChangeWidgetStateChanged("AUTOTOOLCHANGE");
                            }
                        }
                    },
                    State{
                        name:"MANUALTOOLCHANGE"
                        StateChangeScript {
                            script:{
                                mainrect.toolChangeWidgetStateChanged("MANUALTOOLCHANGE");
                            }
                        }
                    }
                ]
            }


            ScreenStatus{
                id:screenStatus
                visible: mainrect.state == "STATUS"
            }
            ScreenJob1{
                id:screenJob
                visible: mainrect.state == "JOB"
                anchors.fill: parent
            }

            ScreenSetup{
                id:screenSetup
                visible: mainrect.state == "SETUP"
            }

            Loader{id:screenMagazine
                source: isEnabledMilling ? "ScreenMagazine.qml" : "ScreenEmpty.qml"
                visible: mainrect.state == "MAGAZINE"
            }

            ScreenMaterial{
                id:screenMaterial
                visible: mainrect.state == "MATERIAL"
            }

            ScreenTool{
                id:screenTool
                visible: mainrect.state == "TOOL"
            }

            ScreenService{
                id:screenService
                visible: mainrect.state == "SERVICE"
            }

        }

        RadioGroup {
            id: chooseScreensGroup
            x: mainUserWindow.x
            y: mainUserWindow.y + mainUserWindow.height + 1
            width: mainUserWindow.width - 4
            height: 50
            initialState: "STATUS"
            stateTarget: "mainrect"

            Keys.onPressed: {
                if (event.key === Qt.Key_Escape) {
                    //console.log("RadioGroup Key_Escape");
                    event.accepted = true;
                }
            }

            Row{
                id: row1
                width: parent.width
                height: 49
                spacing: 5
                TabButton{
                    id:radio_Status_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Status") + db.tr
                    __imagePath: imagePath
                    onClicked_changeStateTo: "STATUS"
                    __pressed: mainrect.state == "STATUS"
                    stateTarget:  chooseScreensGroup.stateTarget
                }

                TabButton{
                    id:radio_Job_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Job") + db.tr
                    __imagePath: imagePath
                    onClicked_changeStateTo: "JOB"
                    __pressed: mainrect.state == "JOB"
                    stateTarget:  chooseScreensGroup.stateTarget
                }

                TabButton{
                    id:radio_Setup_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Setup") + db.tr
                    __imagePath: imagePath
                    onClicked_changeStateTo: "SETUP"
                    __pressed: mainrect.state == "SETUP"
                    stateTarget:  chooseScreensGroup.stateTarget
                }

                TabButton{
                    id:radio_Material_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Material") + db.tr
                    __imagePath: imagePath
                    onClicked_changeStateTo: "MATERIAL"
                    __pressed: mainrect.state == "MATERIAL"
                    stateTarget:  chooseScreensGroup.stateTarget
                }

                TabButton{
                    id:radio_Tools_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Tools") + db.tr
                    __imagePath: imagePath
                    onClicked_changeStateTo: "TOOL"
                    __pressed: mainrect.state == "TOOL"
                    stateTarget:  chooseScreensGroup.stateTarget
                }

                TabButton{
                    id:radio_Magazine_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Magazine") + db.tr
                    __imagePath: imagePath
                    onClicked_changeStateTo: "MAGAZINE"
                    __pressed: mainrect.state == "MAGAZINE"
                    enabled: isEnabledMilling //&& !(LV.isRotation !== bUseRotation.value)
                    visible: isEnabledMilling //&& !(LV.isRotation !== bUseRotation.value)
                    stateTarget:  chooseScreensGroup.stateTarget
                }

                TabButton{
                    id:radio_Service_Menu
                    width: 135
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Service")+db.tr
                    __imagePath: imagePath
                    //onClicked_changeStateTo: "SERVICE"
                    __pressed: mainrect.state == "SERVICE"
                    //stateTarget:  chooseScreensGroup.stateTarget
                    onClicked: {
                        var dlg = getstring_dlg
                        dlg.titleText =                 db.dbTr("Enter password") + db.tr
                        dlg.buttonOk.text =             db.dbTr("Ok") + db.tr
                        dlg.buttonCancel.text =         db.dbTr("Cancel")  + db.tr
                        dlg.width =                     220
                        dlg.inputField.passwordInput =  true
                        dlg.inputField.text =           "" //TODO: tmp store entered pwd
                        //dlg.buttonOk.isEnabled =        false
                        dlg.buttonOk.isEnabled =        true
                        dlg.ok.connect(validatePwd)
                        //dlg.no.connect(clearPwd)
                        //dlg.textInputChanged.connect(setOkEnabled)
                        dlg.open()
                    }

                    function validatePwd()
                    {
                        if (getstring_dlg.inputField.text === ServiceElcede_strPassword.value )
                        {
                            mainrect.state = "SERVICE"
                        }
                        getstring_dlg.close()
                    }

                }

                TabButton{
                    id:radio_Exit_Menu
                    width: 135
                    x: parent.width - width - 5
                    __style: mainrect.rootStyle
                    __text: db.dbTr("Exit") + db.tr
                    __imagePath: imagePath
                    stateTarget: chooseScreensGroup.stateTarget
                    onClicked: {
                        if(typeof _EmergencyChain_InOut_bActive === 'undefined' || _EmergencyChain_InOut_bActive === null){
                            console.log("-------------------------- quit Demo mode --------------------------------")
                            Qt.quit();
                        }else{
                            _EmergencyChain_InOut_bActive.value = false;
                            console.log("-------------------------- quit v8 connection --------------------------------")
                            Qt.quit();
                        }
                    }
                }
            }
        }
    }

    states: [
        State {
            name: "STATUS"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = false
                    materialFileName.visible = false
                    jobFileNameGroup.visible = false
                    positionControlContainer.visible = false
                    toolchange_widget.visible = false
                }
            }
        },
        State {
            name: "JOB"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = true
                    materialFileName.visible = true
                    jobFileNameGroup.visible = true
                    positionControlContainer.visible = true
                    toolchange_widget.visible = false
                }
            }
        },
        State {
            name: "SETUP"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = true
                    materialFileName.visible = true
                    jobFileNameGroup.visible = false
                    positionControlContainer.visible = true
                    toolchange_widget.visible = false
                    //toolchange_widget.y = 20
                    //toolbarMaterial.visible = false
                }
            }
        },
        State {
            name: "MATERIAL"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = false
                    materialFileName.visible = true
                    jobFileNameGroup.visible = false
                    positionControlContainer.visible = false
                    toolchange_widget.visible = false
                }
            }
        },
        State {
            name: "TOOL"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = false
                    materialFileName.visible = true
                    jobFileNameGroup.visible = false
                    positionControlContainer.visible = false
                    toolchange_widget.visible = false
                }
            }
        },
        State {
            name: "MAGAZINE"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = false
                    materialFileName.visible = false
                    jobFileNameGroup.visible = false
                    positionControlContainer.visible = false
                    toolchange_widget.visible = true
                    toolchange_widget.y = 20
                    //toolbarMaterial.visible = false
                }
            }
        },

        State {
            name: "SERVICE"
            StateChangeScript {
                script:{
                    saveMaterial()
                    positionSetup.visible = true
                    materialFileName.visible = false //true
                    jobFileNameGroup.visible = false //true
                    positionControlContainer.visible = true
                    toolchange_widget.visible = false//true
                    //toolchange_widget.y = 120
                }
            }
        }
    ]

    DBErrorSpinner {
        id: error_widget1
        x: 260
        y: 4
        width: 660
        height: 76
        //opacity: (Amplifier_rWaitToStartTime.value > 0) && _EmergencyChain_Out_bIsClosed.value ? 1 : 0
        //opacity:  !Machine_bEnable.value || Error_NonCritical_Out_bActive.value || Error_Critical_Out_bActive.value || Warning_Out_bActive.value ? 1 : 0
        showMessages: true
        showErrors: true
        showWarnings: true
        errorCounterWidth: 0.12
        errorNumberWidth: 0.23
        errorTextWidth: 0.65
        //errorModel: db.errorList
        Connections{
            target: db
            onConnected:{
                console.log("----- error_widget1 ------------");
                //error_widget1.errorModel =  db.errorList;
            }
            onDbLanguageChanged: error_widget1.errorModel =  db.errorList;
        }
    }

    Button {
        id: error_reset
        x: error_widget1.x + error_widget1.width + 4
        y: 4
        width: 76
        height: 76
        text: db.dbTr("RESET \nAlt+F1") + db.tr
        keyBinding: "Alt+F1"
        //display only when _ApplicationRunState_iKernel >= 5
        opacity: (!Machine_bEnable.value || Error_NonCritical_Out_bActive.value || Error_Critical_Out_bActive.value) && (_ApplicationRunState_iKernel.value >= 5) ? 1 : 0
        onClicked: {
            Error_bReset.value = true
            Machine_bEnable.value = true

            _EmergencyChain_InOut_bActive.value = true

        }
        __imageNormal: imagePath + "ErrorResetEmpty.png"
        __imagePressed: imagePath + "ErrorResetDown.png"
    }


}
